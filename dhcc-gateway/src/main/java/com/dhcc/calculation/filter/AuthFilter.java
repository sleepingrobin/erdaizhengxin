package com.dhcc.calculation.filter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;

/**
 * @author haokai
 * @create 2019/2/27
 */
//@Configuration
public class AuthFilter implements GlobalFilter, Ordered {

    @Value("${sso.user-check}")
    private String userCheck;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        String token = exchange.getRequest().getHeaders().getFirst("accessToken");
//        if (token == null || token.isEmpty()) {
//            exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);
//            byte[] bytes = "{\"code\":\"ERROR\",\"msg\":\"Token不能为空\"}".getBytes(StandardCharsets.UTF_8);
//            DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
//            return exchange.getResponse().writeWith(Flux.just(buffer));
//        } else {
//            //TODO://此处调PAAS账户中心验证token是否有效,此处模拟无效token
//            if (true) {
//                exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
//                byte[] bytes = "{\"code\":\"ERROR\",\"msg\":\"Token无效\"}".getBytes(StandardCharsets.UTF_8);
//                DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
//                return exchange.getResponse().writeWith(Flux.just(buffer));
//            }
//        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }
}

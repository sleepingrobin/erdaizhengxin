package com.dhcc.calculation.fallback;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author haokai
 * @create 2019/2/27
 */
@RestController
public class ApiSourceFallbackController {

    @GetMapping("dhcccipsfallback")
    public String fallback() {
        return "dhcc-cips fallback !";
    }
}

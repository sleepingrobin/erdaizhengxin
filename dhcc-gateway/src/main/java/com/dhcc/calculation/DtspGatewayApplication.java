package com.dhcc.calculation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DtspGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(DtspGatewayApplication.class, args);
    }
}


package cn.com.dhcc.query.creditquerycommon.util;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

@Slf4j
public class KeyProperties {
    private static final String PROPERTIES_FILENAME = "config.properties";
    private static final String CONFIG_PATH_KEY = "config.path";
    private static final String FILE_ENCODEING = "UTF-8";

    private static Map<String, Properties> propertiesMap = Maps.newConcurrentMap();
    private static Object lock = new Object();

    private static Properties getProperties(String propertiesFilename) {
        if (StringUtils.isBlank(propertiesFilename)) {
            propertiesFilename = "config.properties";
        }
        Properties properties = (Properties) propertiesMap.get(propertiesFilename);
        if (null == properties) {
            synchronized (lock) {
                properties = loadProperties(propertiesFilename);
                propertiesMap.put(propertiesFilename, properties);
            }
        }
        return properties;
    }

    public static String getProperty(String key) {
        Properties properties = getProperties("");
        return (String) properties.get(key);
    }

    public static String getProperty(String key, String propertiesFilename) {
        Properties properties = getProperties(propertiesFilename);
        return (String) properties.get(key);
    }


    private static Properties loadProperties(String propertiesFilename) {
        FileInputStream in = null;
        try {
            Properties prop = new Properties();
            String path = System.getProperty("config.path");
            if (null == path || (path != null && path.trim().length() == 0)) {
                PropertiesLoaderUtils.fillProperties(prop, new EncodedResource(new ClassPathResource(propertiesFilename), "UTF-8"));
            } else {

                path = path + File.separator;
                String filePath = path + propertiesFilename;
                log.debug("getProperties properties file path={}",filePath);
                in = new FileInputStream(filePath);
                prop.load(in);
                in.close();
            }
            return prop;
        } catch (Exception e) {
            log.error("read Properties file error!!!! exception=", e);
            return null;
        } finally {
            if (null != in)
                try {
                    in.close();
                } catch (IOException e) {
                    log.error("read Properties file error!!!! exception=", e);
                }
        }
    }
}

//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.ratinform;

import cn.com.dhcc.creditquery.ent.query.bo.BaseBo;
import cn.com.dhcc.query.creditquerycommon.xstream.annotation.XStreamIsNotNull;
import com.alibaba.fastjson.annotation.JSONField;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.Date;
import java.util.List;

public class ErrrRatingInfo extends BaseBo {
    @JSONField(
            name = "EH010I01"
    )
    @XStreamIsNotNull
    @XStreamAlias("EH010I01")
    private String ratingId;
    @JSONField(
            name = "EH010Q01"
    )
    @XStreamIsNotNull
    @XStreamAlias("EH010Q01")
    private String ratingOrgname;
    @JSONField(
            name = "EH010D01"
    )
    @XStreamIsNotNull
    @XStreamAlias("EH010D01")
    private String ratingResult;
    @JSONField(
            name = "EH010R01",
            format = "yyyy-MM-dd"
    )
    @XStreamIsNotNull
    @XStreamAlias("EH010R01")
    @XStreamImplicit(
            itemFieldName = "EH010R01"
    )
    private Date ratingTime;

    private List<String> ratingTimeList;

    public ErrrRatingInfo() {
    }

    public String getRatingId() {
        return this.ratingId;
    }

    public void setRatingId(String ratingId) {
        this.ratingId = ratingId;
    }

    public String getRatingOrgname() {
        return this.ratingOrgname;
    }

    public void setRatingOrgname(String ratingOrgname) {
        this.ratingOrgname = ratingOrgname;
    }

    public String getRatingResult() {
        return this.ratingResult;
    }

    public void setRatingResult(String ratingResult) {
        this.ratingResult = ratingResult;
    }

    public Date getRatingTime() {
        return this.ratingTime;
    }

    public void setRatingTime(Date ratingTime) {
        this.ratingTime = ratingTime;
    }

    public List<String> getRatingTimeList() {
        return this.ratingTimeList;
    }

    public void setRatingTimeList(List<String> ratingTimeList) {
        this.ratingTimeList = ratingTimeList;
    }
}

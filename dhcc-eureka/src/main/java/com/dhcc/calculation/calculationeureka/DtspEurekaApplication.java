package com.dhcc.calculation.calculationeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class DtspEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(DtspEurekaApplication.class, args);
    }

}


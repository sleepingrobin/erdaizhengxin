package com.dhcc.calculation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class DtspConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(DtspConfigApplication.class, args);
    }

}


package com.dhcc.cips.config;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:    mybatis-plus 分页插件
 * @Author:         shenmiao
 * @CreateDate:     2018/12/11 14:29
 * @Version:        1.0
 */
@Configuration
@MapperScan(basePackages = {"com.dhcc.calculation.mapper"})
public class MybatisPlusConfig {

    /**
     * mybatis-plus分页插件
     * @author shenmiao
     * @param
     * @return
     * @exception
     * @date 2018/12/11 14:30
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * 乐观锁插件
     * @author shenmiao
     * @param
     * @return
     * @exception
     * @date 2018/12/11 14:30
     */
    @Bean
    public OptimisticLockerInterceptor optimisticLockerInterceptor(){
        return new OptimisticLockerInterceptor();
    }


}

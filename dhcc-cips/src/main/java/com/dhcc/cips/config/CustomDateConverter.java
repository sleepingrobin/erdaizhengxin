package com.dhcc.cips.config;

import com.thoughtworks.xstream.converters.basic.DateConverter;
import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class CustomDateConverter extends DateConverter {

    /**
     * 可格式化 的日期 字串
     */
    private static final List<String> formarts = new ArrayList();

    static {
        formarts.add("yyyy");
        formarts.add("yyyy-MM");
        formarts.add("yyyy-MM-dd");
        formarts.add("yyyy-MM-dd'T'HH:mm:ss");
    }

    public CustomDateConverter() {};

    /**
     * 构造方法
     *
     * @param string
     * @param strings
     */
    public CustomDateConverter(String string, String[] strings, TimeZone timeZone) {
        super(string, strings, timeZone);
    }

    /**
     * 时间日期转换
     */
    @Override
    public Object fromString(String source) {
        if (StringUtils.isBlank(source)) {
            return null;
        }else if (source.matches("^\\d{4}$")) {
            return parseDate(source, formarts.get(0));
        } else if (source.matches("^\\d{4}-\\d{1,2}$")) {
            return parseDate(source, formarts.get(1));
        } else if (source.matches("^\\d{4}-\\d{1,2}-\\d{1,2}$")) {
            return parseDate(source, formarts.get(2));
        }else if (source.matches("^^\\d{4}-\\d{1,2}-\\d{1,2}T{1}\\d{1,2}:\\d{1,2}:\\d{1,2}$")) {
            return parseDate(source, formarts.get(3));
        }else {
            return super.fromString(source);
        }
    }

    /**
     * 功能描述：格式化日期
     *
     * @param dateStr String 字符型日期
     * @param format  String 格式
     * @return Date 日期
     */
    public Date parseDate(String dateStr, String format) {
        Date date = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat(format);
            date = (Date) dateFormat.parse(dateStr);
        } catch (Exception e) {
        }
        return date;
    }
}

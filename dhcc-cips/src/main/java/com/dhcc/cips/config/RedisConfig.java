package com.dhcc.cips.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.io.Serializable;

/**
 * lettuce版redis客户端配置
 * @author YangJiaheng
 * @date 2019-5-5 18:23:42
 */
@Configuration
public class RedisConfig {
    @Bean
    public RedisTemplate<String, Serializable> redisTemplate(LettuceConnectionFactory connectionFactory){
        // 定义redis模板，定义了一些redis的操作
        RedisTemplate<String, Serializable> redisTemplate = new RedisTemplate<>();
        // 设置键序列化器，序列化对象json
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        // 设置值序列化器，可直接序列化对象为json
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        // 设置hash序列化器
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new StringRedisSerializer());
        // 设置连接工厂
        redisTemplate.setConnectionFactory(connectionFactory);
        return redisTemplate;
    }
}

package com.dhcc.cips.config;

import com.dhcc.cips.service.impl.other.EsbWebServiceImpl;
import com.dhcc.cips.service.impl.other.OrgsManagerServiceImpl;
import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.xml.ws.Endpoint;


@Configuration
public class CxfConfig implements WebMvcConfigurer {

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public Endpoint orgEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), new OrgsManagerServiceImpl());
        endpoint.publish("/orgservice");
        return endpoint;
    }

    @Bean
    public Endpoint esbEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), new EsbWebServiceImpl());
        endpoint.publish("/esbservice");
        return endpoint;
    }
}

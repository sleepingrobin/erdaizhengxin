package com.dhcc.cips.response;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:    restful 接口返回List的实体类
 * @Author:         shenmiao
 * @CreateDate:     2018/12/24 13:21
 * @Version:        1.0
 */
@ApiModel(description = "列表类结果集")
public class ListResult<T> {

    @ApiModelProperty(value = "数据集",name = "data",example = "{}")
    private List<T> data;
    @ApiModelProperty(value = "总条数",name = "totalCounts",example = "100")
    private long totalCounts;

    public ListResult(Object o){
        if (null != o){
            if (o instanceof Page){
                Page page = (Page)o;
                data = page.getRecords();
                totalCounts = page.getTotal();
            }else {
                data = new ArrayList<T>();
                totalCounts = 0;
            }
        }else {
            data = new ArrayList<T>();
            totalCounts = 0;
        }
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public long getTotalCounts() {
        return totalCounts;
    }

    public void setTotalCounts(int totalCounts) {
        this.totalCounts = totalCounts;
    }
}

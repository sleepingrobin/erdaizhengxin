package com.dhcc.cips.response;


import com.dhcc.cips.constants.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description:    接口返回信息的实体类
 * @Author:         shenmiao
 * @CreateDate:     2018/12/13 13:54
 * @Version:        1.0
 */
@ApiModel(description = "服务器端返回结果")
public class EntityResponse<T> {
    /**
     * 请求接口正常返回
     */
    public static final String CODE_OK = Constants.SUCCESS_CODE;

    @ApiModelProperty(value = "结果集",name = "result",example = "字符串或json数据")
    private T result;
    @ApiModelProperty(value = "代码",name = "code",example = "DR0013")
    private String code;

    @ApiModelProperty(value = "描述",name = "msg",example = "成功")
    private String msg;

    private EntityResponse(ResponseBuilder<T> builder) {
        this.result = builder.result;
        this.code = builder.code;
        this.msg = builder.msg;
    }

    public T getResult() {
        if(result == null){
            return (T) "";
        }
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    /**
     *
     * @param result
     * @param <T>
     * @return
     */
    public static <T> EntityResponse<T> ok(T result){
        return new ResponseBuilder(result)
                .code(CODE_OK)
                .build();
    }

    public static EntityResponse error(String code, String message) {
        return new ResponseBuilder("")
                .code(code)
                .msg(message)
                .build();
    }

    public static class ResponseBuilder<T> {
        private T result;
        private String  code;
        private String msg;

        public ResponseBuilder(T result) {
            this.result = result;
        }

        public ResponseBuilder code(String code) {
            this.code = code;
            if(CODE_OK.equalsIgnoreCase(code)){
                this.msg = "操作成功";
            }else{
                this.msg = "操作失败";
            }
            return this;
        }

        public ResponseBuilder result(T result) {
            this.result = result;
            return this;
        }

        public ResponseBuilder msg(String msg) {
            this.msg = msg;
            return this;
        }

        public EntityResponse<T> build() {
            return new EntityResponse(this);
        }
    }
}

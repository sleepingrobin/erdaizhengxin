package com.dhcc.cips.service.person;


import com.dhcc.cips.bo.base.Request;

public interface PersonReportFileService {

    String download(Request request);

}

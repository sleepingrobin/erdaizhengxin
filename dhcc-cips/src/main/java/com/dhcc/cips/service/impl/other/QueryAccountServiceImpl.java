package com.dhcc.cips.service.impl.other;

import com.dhcc.cips.mapper.company.CeqConfigMapper;
import com.dhcc.cips.mapper.person.CpqConfigMapper;
import com.dhcc.cips.mapper.other.CqCreditUserMapper;
import com.dhcc.cips.service.other.QueryAccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class QueryAccountServiceImpl implements QueryAccountService {

    @Resource
    private CpqConfigMapper cpqConfigMapper;
    @Resource
    private CeqConfigMapper ceqConfigMapper;
    @Resource
    private CqCreditUserMapper cqCreditUserMapper;

    @Override
    public String getQueryUser(String branchId, String type) throws Exception {

        try {
            String orgId = null;
            String queryMode = null;
            //根据用户id查询用户所在机构号，通过机构号获取法人机构号，映射法人机构号对应的查询方式
            Map<String, String> queryModeMap = new HashMap<>();
            if ("P".equals(type)) {//个人
                queryModeMap = cpqConfigMapper.selectQueryType(branchId);
            } else if ("C".equals(type)) {//企业
                queryModeMap = ceqConfigMapper.selectQueryType(branchId);
            }
            //信用报告单笔查询方式（1：接口；2：页面）
            if (queryModeMap != null && queryModeMap.size() > 0) {
                queryMode = queryModeMap.get("CONFIG_VALUE");
                orgId = queryModeMap.get("ORG_ID");
            }

            List<String> accountList;
            //匹配查询账号
            if (StringUtils.isNotBlank(queryMode)) {
                //查询账号（接口查询）
                if ("1".equals(queryMode) && StringUtils.isNotBlank(orgId)) {
                    accountList = cqCreditUserMapper.selectCreditUser(orgId, queryMode);
                    if (null != accountList && accountList.size() > 0) {
                        return getQueryUser(accountList, type);
                    }
                }
                //查询账号列表（页面查询）
                if ("2".equals(queryMode)) {
                    //查询机构号有效性判断
                    if (branchId.length() <= 7) {
                        throw new Exception("机构号：" + branchId + "未匹配到征信用户");
                    }
                    //先匹配前七位（支行）
                    accountList = cqCreditUserMapper.selectCreditUser(branchId.substring(0, 7), queryMode);
                    if (null != accountList && accountList.size() > 0) {
                        return getQueryUser(accountList, type);
                    } else {
                        //再匹配前4位（分行）
                        accountList = cqCreditUserMapper.selectCreditUser(branchId.substring(0, 4), queryMode);
                        if (null != accountList && accountList.size() > 0) {
                            return getQueryUser(accountList, type);
                        } else {
                            throw new Exception("机构号：" + branchId + "未匹配到征信用户");
                        }
                    }
                }
            }
            throw new Exception("法人业务参数查询方式：" + queryMode + "为空，未匹配到征信用户");
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    @Override
    public String getReportQueryType(String branchId, String type) {
        if ("P".equals(type)) {//个人
            return cpqConfigMapper.selectReportQueryType(branchId);
        }
        if ("C".equals(type)) {//企业
            return ceqConfigMapper.selectReportQueryType(branchId);
        }
        return "";
    }

    private String getQueryUser(List<String> accountList, String type) {
        String creditUser = accountList.get((int) Math.random() * accountList.size());
        if (StringUtils.isNotBlank(creditUser)) {
            if ("P".equals(type)) {
                return cqCreditUserMapper.selectPersonQueryUser(creditUser);
            }
            if ("C".equals(type)) {
                return cqCreditUserMapper.selectCompanyQueryUser(creditUser);
            }
        }
        return "";
    }
}
package com.dhcc.cips.service.person;

import com.dhcc.cips.bo.other.CooperationReqBo;

/**
 * 对外合作个人征信报告解析服务类
 */
public interface CooperationPersonParseService {
    /**
     *  解析xml版征信报告
     * @param cooperationReqBo
     * @return
     * @throws Exception
     */
    void parseWithXml(CooperationReqBo cooperationReqBo) throws Exception;

    /**
     * 解析html版征信报告
     * @param cooperationReqBo
     * @throws Exception
     */
    void parseWithHtml(CooperationReqBo cooperationReqBo) throws Exception;
}

package com.dhcc.cips.service.impl.company;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dhcc.cips.bo.base.Request;
import com.dhcc.cips.bo.base.ResponseHead;
import com.dhcc.cips.bo.base.ResponseSysHead;
import com.dhcc.cips.bo.base.ResponseSysHeadRet;
import com.dhcc.cips.bo.company.CompanyExtGuarantyInfoBo;
import com.dhcc.cips.bo.company.CompanyLoanInfoBo;
import com.dhcc.cips.bo.company.CompanyZBInfoResBo;
import com.dhcc.cips.bo.company.CompanyZBInfoResBodyBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.mapper.company.CeqResultInfoMapper;
import com.dhcc.cips.mapper.company.CompanyZBInfoMapper;
import com.dhcc.cips.po.company.CeqResultInfoPo;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.dhcc.cips.service.company.CompanyZBInfoService;
import com.dhcc.cips.util.XmlUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @author Yang Jiaheng
 * @date 2019年3月28日
 * @description 企业征信指标接口实现类
 */
@Slf4j
@Service
public class CompanyZBInfoServiceImpl implements CompanyZBInfoService {
    @Resource
    private CompanyZBInfoMapper companyZBInfoMapper;
    @Resource
    private CeqResultInfoMapper ceqResultInfoMapper;

    @Override
    public String exec(Request request) {
        //返回报文
        String retXml;
        //返回报文对象
        CompanyZBInfoResBo companyZBInfoResBo = new CompanyZBInfoResBo();
        CompanyZBInfoResBodyBo companyZBInfoResBodyBo = new CompanyZBInfoResBodyBo();

        //返回交易信息
        String retCode;//交易返回码
        String retMesaage;//交易返回信息
        String retStatus;//交易返回状态
        try {

            //获取请求参数
            String globalType = request.getRequestBody().getGlobalType();//证件类型
            String globalId = request.getRequestBody().getGlobalId();//证件号码（企业统一传贷款卡号）

            //请求参数校验
            /*if (StringUtils.isEmpty(globalType)) {
                new Exception(Constants.GLOBAL_TYPE_NULL_ANO);//证件类型不能为空
            }*/
            if (StringUtils.isEmpty(globalType)) {
                throw new Exception(Constants.LOANCARD_NO_NULL_ANO);//贷款卡号不能为空
            }

            //根据证件号码和证件类型查询报告编号
            HashMap<Object, Object> map = Maps.newHashMap();//查询条件
            map.put("loancardNo", globalId);//贷款卡号
            CeqResultInfoPo ceqResultInfoPo = ceqResultInfoMapper.selectByLoancardNo(map);
            if (ceqResultInfoPo == null) {
                throw new Exception(Constants.ZB_NULL);
            }
            String reportNo = ceqResultInfoPo.getReportId();//报告编号
            if (StringUtils.isEmpty(reportNo)) {
                throw new Exception(Constants.ZB_NULL);
            }

            //根据报告编号查询指标
            map.clear();
            map.put("reportNo", reportNo);
            CompanyZBInfoPo companyZBInfoPo = companyZBInfoMapper.selectCompanyZBInfoByReportNo(map);
            if (companyZBInfoPo == null) {
                throw new Exception(Constants.ZB_NULL);
            }

            //填充返回体
            companyZBInfoResBodyBo.setCompanyZBInfoPo(companyZBInfoPo);

            String comLoanInfo = companyZBInfoPo.getComLoanInfo();
            if (StringUtils.isNotBlank(comLoanInfo)) {
                JSONObject comLoanInfoObject = JSONObject.parseObject(comLoanInfo);
                //贷款信息数组
                String loan = String.valueOf(comLoanInfoObject.get("companyLoanInfoBoList"));
                if (StringUtils.isNotBlank(loan)) {
                    List<CompanyLoanInfoBo> companyLoanInfoBoList = JSONArray.parseArray(loan, CompanyLoanInfoBo.class);
                    companyZBInfoResBodyBo.setCompanyLoanInfoBoList(companyLoanInfoBoList);
                }

                //对外担保信息数组
                String ext = String.valueOf(comLoanInfoObject.get("companyExtGuarantyInfoBoList"));
                if (StringUtils.isNotBlank(ext)) {
                    List<CompanyExtGuarantyInfoBo> companyExtGuarantyInfoBoList = JSONArray.parseArray(ext, CompanyExtGuarantyInfoBo.class);
                    companyZBInfoResBodyBo.setCompanyExtGuarantyInfoBoList(companyExtGuarantyInfoBoList);
                }
            }
            //交易返回码
            retCode = Constants.SUCCESS_CODE;
            //交易返回信息
            retMesaage = "交易成功";
            //交易返回状态
            retStatus = Constants.SUCCESS_S;
        } catch (Exception e) {
            log.error("", e);
            //交易返回码
            retCode = Constants.FAIL_CODE;
            //交易返回信息
            retMesaage = "交易失败，" + e.getMessage();
            //交易返回状态
            retStatus = Constants.FAIL_F;
        }
        //设置返回对象参数
        ResponseHead responseHead = new ResponseHead();
        ResponseSysHead responseSysHead = new ResponseSysHead();
        BeanUtils.copyProperties(request.getRequestSysHead(), responseSysHead);
        //返回状态码
        responseSysHead.setRetStatus(retStatus);
        responseHead.setResponseSysHead(responseSysHead);
        ResponseSysHeadRet responseSysHeadRet = new ResponseSysHeadRet();
        //交易返回码
        responseSysHeadRet.setRetCode(retCode);
        //交易返信息
        responseSysHeadRet.setRetMsg(retMesaage);
        responseHead.getResponseSysHead().setResponseSysHeadRet(responseSysHeadRet);

        //填充返回体
        companyZBInfoResBo.setCompanyZBInfoResBodyBo(companyZBInfoResBodyBo);

        //转xml
        retXml = XmlUtil.converJavaBean2Xml(companyZBInfoResBo, companyZBInfoResBo.getClass());

        return retXml;
    }
}
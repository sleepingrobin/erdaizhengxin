package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.*;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author StivenYang
 * @description 三（信贷交易信息明细）-二、三、四 相关指标计算
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateCreditInfoDkServiceImpl implements PersonZBCalculteThreadService {

    /**
     * @param cpqReportBo 信用报告bo
     * @return 指标对象
     */
    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();

        try {
            /*******************************指标声明start************************/
            BigDecimal accountQuota = new BigDecimal("0.00");
            //107. 最近1个月贷款笔数
            int lateMonLoanNum = 0;
            //108. 最近1个月贷款授信总额
            BigDecimal lateMonCredit = new BigDecimal(0);
            //110. 最近3个月贷款笔数
            int lateSeaLoanNum = 0;
            //111. 最近3个月内借款金额总和
            BigDecimal lateSeaCredit = new BigDecimal(0);
            //113. 最近6个月内的账户数
            int lateHlyLoanNum = 0;
            //114. 最近6个月内借款金额总和
            BigDecimal lateHlyCredit = new BigDecimal(0);
            //116. 过去2年内贷款最高逾期期数
            int l2YLdMaxNum = 0;
            //117. 个人住房贷款余额
            BigDecimal housingLoanAmt = new BigDecimal(0);
            //118. 个人汽车贷款余额
            BigDecimal carLoanAmt = new BigDecimal(0);
            //119. 个人经营性贷款余额
            BigDecimal perJyxLoanBalan = new BigDecimal(0);
            //124. 最近五年内经营性贷款逾期机构数
            int late5yJyxLoanOdNum = 0;
            //128. 剔除贷款单月最高逾期金额
            BigDecimal remLoanCatPerMonOdAmt = new BigDecimal(0);
            //131. 剔除贷款类别银行服务家数
            int remLoanCatBankNum = 0;
            //149. 近两年经营性贷款逾期次数
            int late2yJyxLoanOdNum = 0;
            //150. 近2年消费性贷款逾期次数
            int late2yXfxLoanOdNum = 0;
            //152. 近2年经营性贷款最长逾期月数
            int late2yJyxLoanMaxOdNum = 0;
            //153. 消费性贷款最长逾期月数
            int late2yXfxLoanMaxOdNum = 0;
            //155. 贷款五级分类为非正常的余额
            BigDecimal unNormalLoanBalan = new BigDecimal(0);
            //164. 最近24个月贷款最长逾期数
            int late2yLoanMaxOdNum = 0;
            //172. 未结清经营性贷款逾期金额
            BigDecimal settJyxLoanOdAmt = new BigDecimal(0);
            //173. 贷款五级分类为非正常的笔数
            int unNormalLoanNum = 0;
            //175. 最近60个月经营性贷款授信机构数
            int late60mJyxLoanOrgnum = 0;
            //179. 个人经营性贷款余额
            BigDecimal currJyxLoanBalan = new BigDecimal(0);
            //186. 他行经营性贷款授信机构数
            int othrBnkMngmtLoanOrgNum = 0;
            //187. 最近5年内经营性贷款逾期银行家数
            int late5yJyxLoanOdBkNum = 0;
            HashSet<Object> late5yJyxLoanOdBkNumSet = Sets.newHashSet();
            //188. 最近2年内经营性贷款单月最高逾期金额
            BigDecimal l2yJyxLoanPmonMaxOdAmt = new BigDecimal(0);
            //190. 他行贷款授信机构数
            int othBankLoanOrgNum = 0;
            HashSet<Object> othBankLoanOrgNumSet = Sets.newHashSet();
            //191. 他行贷款授信余额
            BigDecimal othBankLoanCreditBal = new BigDecimal(0);
            //192. 他行经营性贷款授信余额
            BigDecimal othBankMngmtLoanBal = new BigDecimal(0);
            //193. 他行消费性贷款授信机构数
            int othBankXfxLoanOrgNum = 0;
            //194. 他行消费性贷款授信余额
            BigDecimal othBankXfxLoanBal = new BigDecimal(0);
            //201. 近3个月到期的贷款笔数
            int lateSeaEndLoanNum = 0;
            //205. 最近2个月贷款到期笔数
            int late2MLoanNum = 0;
            //206. 近两年办理过住房贷款的笔数
            int late2YHousingLoanNum = 0;
            //208. 当前贷款逾期期数
            int currUnsettLoanOdNum = 0;
            //210. 当前贷款最高逾期金额
            BigDecimal currUnsettLoanMaxOdAmt = new BigDecimal(0);
            //212. 特殊交易（“提前结清”的除外）的贷款笔数
            int specWjqLoanNum = 0;
            //213. 特殊交易类型为以资抵债总次数
            int specYzdzLoanNum = 0;
            //217. 近2个月消费金融公司等非银行金融机构发放贷款的笔数
            int late2mLoanFyhNum = 0;
            //218. 近1年消费金融公司等非银行金融机构发放贷款的笔数
            int late1yLoanFyhNum = 0;
            //29. 将来2个月未结清经营性贷款到期笔数
            int f2MUnSettJyxLoanNum = 0;
            //33. 他行经营性贷款最高额度
            BigDecimal oBankJyxLoanMaxAmt = new BigDecimal(0);
            //34. 他行未结清非抵质押贷款笔数
            int oBUnSettLoanFdzyNum = 0;
            //35. 他行未结清非抵质押贷款余额
            BigDecimal oBUnSettLoanFdzyAmt = new BigDecimal(0);
            //40. 最近1年贷款最大合同金额
            BigDecimal l1YMaxLoanAmt = new BigDecimal(0);
            //45. 过去两年内经营性贷款逾期两期以上（包括两期）贷款笔数
            int l2YJyxLoanOd2TimeNum = 0;
            //97. 未来1个月内到期贷款笔数
            int fMonLoanCount = 0;
            //98. 未来1个月内到期贷款金额
            BigDecimal fMonLoanMoney = new BigDecimal(0);
            //99. 未来1个月内到期贷款余额
            BigDecimal fMonLoanBalance = new BigDecimal(0);
            //100. 未来3个月内到期贷款笔数
            int fSeaLoanCount = 0;
            //101. 未来3个月内到期贷款金额
            BigDecimal fSeaLoanMoney = new BigDecimal(0);
            //102. 未来3个月内到期贷款余额
            BigDecimal fSeaLoanBalance = new BigDecimal(0);
            //103. 未来6个月内到期贷款笔数
            int fHlyLoanCount = 0;
            //104. 未来6个月内到期贷款金额
            BigDecimal fHlyLoanMoney = new BigDecimal(0);
            //105. 未来6个月内到期贷款余额
            BigDecimal fHlyLoanBalance = new BigDecimal(0);
            //他行经营性贷款最高额度
            BigDecimal max = new BigDecimal("0.00");

            HashSet<Object> othBankXfxLoanOrgNumSet = Sets.newHashSet();
            Set<String> late5yJyxLoanOdNumSet = new TreeSet<>();
            HashSet<Object> late60mJyxLoanOrgnumSet = Sets.newHashSet();
            Set<String> set5 = new TreeSet<>();
            List<String> late2yJyxLoanOdNumList = Lists.newArrayList();
            HashSet<Object> othrBnkMngmtLoanOrgNumSet = Sets.newHashSet();
            /*******************************指标声明end************************/


            if (cpqReportBo != null
                    && cpqReportBo.getBorAccInfBo() != null
                    && cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList() != null) {
                //报告日期
                Date reportTime = cpqReportBo.getRepheadBo().getRepheadUnitBo().getPchrPchrreportinfo().getReportTime();
                //前1个月
                Date aimMonthDate1 = DateUtil.getAimMonthDate(reportTime, -1);
                //前2个月
                Date aimMonthDate2 = DateUtil.getAimMonthDate(reportTime, -2);
                //前3个月
                Date aimMonthDate3 = DateUtil.getAimMonthDate(reportTime, -3);
                //前6个月
                Date aimMonthDate6 = DateUtil.getAimMonthDate(reportTime, -6);
                //前12个月
                Date aimYearDate1 = DateUtil.getAimYearDate(reportTime, -1);
                //前2年
                Date aimYearDate2 = DateUtil.getAimYearDate(reportTime, -2);
                //前60个月
                Date aimMonthDate60 = DateUtil.getAimMonthDate(reportTime, -60);
                //未来1个月
                Date aimMonthDate1b = DateUtil.getAimMonthDate(reportTime, 1);
                //未来3个月
                Date aimMonthDate3b = DateUtil.getAimMonthDate(reportTime, 3);
                //未来6个月
                Date aimMonthDate6b = DateUtil.getAimMonthDate(reportTime, 6);

                List<BorAccInfUnitBo> borAccInfUnitBoList = cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList();
                if (null != borAccInfUnitBoList && borAccInfUnitBoList.size() > 0) {
                    for (BorAccInfUnitBo borAccInfUnitBo : borAccInfUnitBoList) {
                        /***************************账户公共变量开始**************************/
                        //基本信息段
                        PcabBasicinfoseg pcabBasicinfoseg = borAccInfUnitBo.getPcabBasicinfoseg();
                        //开立日期
                        Date issuanceDate = null;
                        //到期日期
                        Date dueDate = null;
                        //账户类型
                        String accountType = null;
                        //交易类型
                        String businessType = null;
                        //公司类型
                        String orgType = null;
                        //组织代码
                        String orgCode = null;
                        //借款金额
                        BigDecimal loanAmount = new BigDecimal(0);
                        //担保方式
                        String guaranteeMode = null;
                        if (null != pcabBasicinfoseg) {
                            issuanceDate = pcabBasicinfoseg.getIssuanceDate();
                            dueDate = pcabBasicinfoseg.getDueDate();
                            accountType = pcabBasicinfoseg.getAccountType();
                            businessType = pcabBasicinfoseg.getBusinessType();
                            orgType = pcabBasicinfoseg.getOrgType();
                            orgCode = pcabBasicinfoseg.getOrgCode();
                            guaranteeMode = pcabBasicinfoseg.getGuaranteeMode();
                            if (null != pcabBasicinfoseg.getLoanAmount()) {
                                loanAmount = new BigDecimal(pcabBasicinfoseg.getLoanAmount());
                            }
                        }
                        //最新表现字段
                        PcanFirinfseg pcanFirinfseg = borAccInfUnitBo.getPcanFirinfseg();
                        //最近两年还款状态
                        PcatTwentyfourpaymentstat pcatTwentyfourpaymentstat = borAccInfUnitBo.getPcatTwentyfourpaymentstat();
                        //最近两年支付列表
                        List<PcatTwentyfourpaymentstatsSub> twentyfourpaymentstatsSubList = null;
                        if (pcatTwentyfourpaymentstat != null) {
                            twentyfourpaymentstatsSubList = pcatTwentyfourpaymentstat.getSub();
                        }
                        //特殊交易类型列表
                        PcaiSpeinfseg pcaiSpeinfseg = borAccInfUnitBo.getPcaiSpeinfseg();
                        List<PcaiSpeinfsegSub> pcaiSpeinfsegSubList = null;
                        if (null != pcaiSpeinfseg) {
                            pcaiSpeinfsegSubList = pcaiSpeinfseg.getPcaiSpeinfsegSubList();
                        }
                        //最近五年贷款信息
                        PcapRecfivehisinfo pcapRecfivehisinfo = borAccInfUnitBo.getPcapRecfivehisinfo();
                        //最近五年贷款信息列表
                        List<PcapRecfivehisinfoSub> pcapRecfivehisinfoSubList = null;
                        if (pcapRecfivehisinfo != null) {
                            pcapRecfivehisinfoSubList = pcapRecfivehisinfo.getPcapRecfivehisinfoSubList();
                        }
                        //五级分类
                        String fiveAssort = null;
                        //账户状态
                        String accStatus = null;
                        //最新表现字段 余额
                        BigDecimal balance = new BigDecimal(0);
                        if (pcanFirinfseg != null) {
                            fiveAssort = pcanFirinfseg.getFiveAssort();
                            accStatus = pcanFirinfseg.getAccStatus();
                            if (pcanFirinfseg.getBalance() != null) {
                                balance = new BigDecimal(pcanFirinfseg.getBalance());
                            }
                        }
                        //账户为R4的授信额度
                        BigDecimal pcabAccountQuota = new BigDecimal("0.00");

                        //最近一个月的账户信息
                        PcamMonthInformation pcamMonthInformation = borAccInfUnitBo.getPcamMonthInformation();
                        //当前逾期总额
                        BigDecimal overdueAmount = new BigDecimal(0);
                        //最近一个月余额
                        BigDecimal monthBalance = new BigDecimal(0);
                        //当前逾期期数
                        String overduePeriod = null;
                        if (pcamMonthInformation != null) {
                            if (StringUtils.isNotEmpty(pcamMonthInformation.getOverdueAmount())) {
                                overdueAmount = new BigDecimal(pcamMonthInformation.getOverdueAmount());
                            }
                            if (StringUtils.isNotEmpty(pcamMonthInformation.getOverduePeriod())) {
                                overduePeriod = pcamMonthInformation.getOverduePeriod();
                            }
                            if (StringUtils.isNotBlank(pcamMonthInformation.getBalance())) {
                                monthBalance = new BigDecimal(pcamMonthInformation.getBalance());
                            }

                        }
                        /***************************账户公共变量结束**************************/

                        //107. 最近1个月贷款笔数
                        //108. 最近1个月贷款授信总额
                        //110. 最近3个月贷款笔数
                        //111. 最近三个月内借款金额总和
                        //113. 最近六个月内的账户数
                        //114. 最近六个月内借款金额总和
                        if (Constants.ACC_TYPE_R1.equals(accountType)
                                || Constants.ACC_TYPE_R4.equals(accountType)
                                || Constants.ACC_TYPE_D1.equals(accountType)) {
                            if (issuanceDate != null) {
                                //借款金额
                                if (aimMonthDate1.before(issuanceDate)) {
                                    //最近一个月贷款笔数
                                    lateMonLoanNum = lateMonLoanNum + 1;
                                    //最近一个月授信总额
                                    lateMonCredit = lateMonCredit.add(loanAmount);
                                }
                                if (aimMonthDate3.before(issuanceDate)) {
                                    //最近三个月贷款笔数
                                    lateSeaLoanNum = lateSeaLoanNum + 1;

                                    if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                        if (pcabBasicinfoseg.getAccountQuota() != null) {
                                            accountQuota = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                        }
                                    }
                                    //最近三个月授信总额
                                    lateSeaCredit = lateSeaCredit.add(loanAmount).add(accountQuota);
                                }
                                if (aimMonthDate6.before(issuanceDate)) {
                                    //最近六个月贷款笔数
                                    lateHlyLoanNum = lateHlyLoanNum + 1;
                                    //最近六个月授信总额
                                    lateHlyCredit = lateHlyCredit.add(loanAmount).add(accountQuota);
                                }
                            }


                            //116. 过去2年内贷款最高逾期期数
                            if (twentyfourpaymentstatsSubList != null) {
                                for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                    String paymentStats = subList.getPaymentStats();
                                    if (StringUtils.isNotEmpty(paymentStats)
                                            && Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                        if (l2YLdMaxNum < Integer.parseInt(paymentStats)) {
                                            l2YLdMaxNum = Integer.parseInt(paymentStats);
                                        }
                                    }
                                }
                            }

                            //117. 个人住房贷款余额
                            if (Constants.BUS_TYPE_11.equals(businessType) ||
                                    Constants.BUS_TYPE_12.equals(businessType) ||
                                    Constants.BUS_TYPE_13.equals(businessType)) {
                                if (!Constants.ACC_STAT_4.equals(accStatus)) {
                                    //个人住房贷款余额
                                    if (StringUtils.isNotBlank(pcanFirinfseg.getBalance())) {
                                        housingLoanAmt = housingLoanAmt.add(balance);
                                    } else {
                                        housingLoanAmt = housingLoanAmt.add(monthBalance);
                                    }
                                }
                            }

                            if (Constants.BUS_TYPE_21.equals(businessType)) {
                                //118. 个人汽车贷款余额
                                if (!Constants.ACC_STAT_3.equals(accStatus)) {
                                    if (StringUtils.isNotBlank(pcanFirinfseg.getBalance())) {
                                        carLoanAmt = carLoanAmt.add(balance);
                                    } else {
                                        carLoanAmt = carLoanAmt.add(monthBalance);
                                    }
                                }
                            }

                            if (Constants.BUS_TYPE_41.equals(businessType) ||
                                    Constants.BUS_TYPE_51.equals(businessType) ||
                                    Constants.BUS_TYPE_52.equals(businessType)) {
                                //119. 个人经营性贷款余额
                                if (!Constants.ACC_STAT_4.equals(accStatus)) {
                                    if (StringUtils.isNotBlank(pcanFirinfseg.getBalance())) {
                                        perJyxLoanBalan = perJyxLoanBalan.add(balance);
                                    } else {
                                        perJyxLoanBalan = perJyxLoanBalan.add(monthBalance);
                                    }
                                }
                            }

                            //124. 最近五年内经营性贷款逾期机构数
                            if (pcapRecfivehisinfoSubList != null) {
                                for (PcapRecfivehisinfoSub subList : pcapRecfivehisinfoSubList) {
                                    String repayStatus = subList.getRepayStatus();
                                    if (repayStatus != null && Constants.OVERDUE_STATUS.contains(repayStatus) && StringUtils.isNotBlank(orgCode)) {
                                        late5yJyxLoanOdNumSet.add(orgCode);
                                    }
                                }
                            }
                            late5yJyxLoanOdNum = late5yJyxLoanOdNumSet.size();

                            //128 剔除贷款单月最高逾期金额
                            if (StringUtils.isNotBlank(businessType)
                                    && (Constants.BUS_TYPE_41).equals(businessType)
                                    || (Constants.BUS_TYPE_51).equals(businessType)
                                    || (Constants.BUS_TYPE_52).equals(businessType)) {
                                if (StringUtils.isNotEmpty(accStatus)
                                        && Constants.ACC_STAT_4.equals(accStatus)) {
                                    remLoanCatPerMonOdAmt = remLoanCatPerMonOdAmt.add(balance);
                                } else {
                                    //最近一个月度变现信息段余额-最新表现字段余额大于等于0
                                    if (monthBalance.compareTo(balance) >= 0) {
                                        remLoanCatPerMonOdAmt = remLoanCatPerMonOdAmt.add(overdueAmount);
                                    }
                                }
                            }

                            //131 剔除贷款类别银行服务家数
                            //剔除指定贷款类型 个人经营性贷款+农户贷款+其他
                            if (StringUtils.isNotBlank(businessType)
                                    && (Constants.BUS_TYPE_41).equals(businessType)
                                    || (Constants.BUS_TYPE_51).equals(businessType)
                                    || (Constants.BUS_TYPE_52).equals(businessType)) {
                                if (!Constants.ACC_STAT_3.equals(accStatus) && StringUtils.isNotBlank(orgCode)) {
                                    set5.add(orgCode);
                                }
                            }
                            remLoanCatBankNum = set5.size();

                            //149 近两年经营性贷款逾期次数
                            if (StringUtils.isNotEmpty(businessType)
                                    && Constants.BUS_TYPE_41.equals(businessType)) {
                                if (twentyfourpaymentstatsSubList != null) {
                                    for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                        String paymentStats = subList.getPaymentStats();
                                        if (StringUtils.isNotEmpty(paymentStats)) {
                                            if (Constants.OVERDUE.contains(paymentStats)) {
                                                late2yJyxLoanOdNumList.add(paymentStats);
                                            }
                                        }
                                    }
                                }
                            }
                            late2yJyxLoanOdNum = late2yJyxLoanOdNumList.size();

                            //150 近两2消费性贷款逾期次数
                            if (StringUtils.isNotEmpty(businessType)
                                    && Constants.BUS_TYPE_11.equals(businessType)
                                    || Constants.BUS_TYPE_12.equals(businessType)
                                    || Constants.BUS_TYPE_13.equals(businessType)
                                    || Constants.BUS_TYPE_21.equals(businessType)
                                    || Constants.BUS_TYPE_31.equals(businessType)
                                    || Constants.BUS_TYPE_91.equals(businessType)) {
                                if (twentyfourpaymentstatsSubList != null) {
                                    for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                        String paymentStats = subList.getPaymentStats();
                                        if (Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                            late2yXfxLoanOdNum = late2yXfxLoanOdNum + 1;
                                        }
                                    }
                                }
                            }

                            //152 近2年经营性贷款最长逾期月数
                            if (StringUtils.isNotEmpty(businessType)
                                    && Constants.BUS_TYPE_41.equals(businessType)) {
                                if (twentyfourpaymentstatsSubList != null) {
                                    for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                        String paymentStats = subList.getPaymentStats();
                                        if (Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                            if (late2yJyxLoanMaxOdNum < Integer.parseInt(paymentStats)) {
                                                late2yJyxLoanMaxOdNum = Integer.parseInt(paymentStats);
                                            }
                                        }
                                    }
                                }
                            }

                            //153 近2年消费性贷款最长逾期月数
                            if (StringUtils.isNotEmpty(businessType)
                                    && Constants.BUS_TYPE_41.equals(businessType)
                                    || (Constants.BUS_TYPE_53).equals(businessType)
                                    || (Constants.BUS_TYPE_91).equals(businessType)) {
                                if (null != twentyfourpaymentstatsSubList) {
                                    for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                        String paymentStats = subList.getPaymentStats();
                                        if (StringUtils.isNotBlank(paymentStats)) {
                                            if (Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                                if (late2yXfxLoanMaxOdNum < Integer.parseInt(paymentStats)) {
                                                    late2yXfxLoanMaxOdNum = Integer.parseInt(paymentStats);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            //155 贷款五级分类为非正常的余额
                            if (!Constants.FIF_TYPE_1.equals(fiveAssort)) {
                                unNormalLoanBalan = unNormalLoanBalan.add(balance);
                            }

                            //164 最近24个月贷款最长逾期数
                            if (twentyfourpaymentstatsSubList != null) {
                                for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                    String paymentStats = subList.getPaymentStats();
                                    if (null != subList.getPaymentStats()) {
                                        if (Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                            if (late2yLoanMaxOdNum < Integer.parseInt(paymentStats)) {
                                                late2yLoanMaxOdNum = Integer.parseInt(paymentStats);
                                            }
                                        }
                                    }
                                }
                            }

                            //172. 未结清经营性贷款逾期金额
                            // 如果业务种类为 个人经营性贷款、农户贷款、其他贷款 且 账户状态不为呆账 的 当前逾期总额
                            if (StringUtils.isNotEmpty(businessType)
                                    && StringUtils.isNotEmpty(accStatus)
                                    && Constants.BUS_TYPE_41.equals(businessType)
                                    || Constants.BUS_TYPE_51.equals(businessType)
                                    || Constants.BUS_TYPE_52.equals(businessType)) {
                                if (!Constants.ACC_STAT_4.equals(accStatus)) {
                                    if (overdueAmount != null) {
                                        //最近一个月度变现信息段余额-最新表现字段余额大于等于0
                                        if (monthBalance.compareTo(balance) >= 0) {
                                            settJyxLoanOdAmt = settJyxLoanOdAmt.add(overdueAmount);
                                        }
                                    }
                                }
                                // 账户状态为呆账的 余额
                                if (Constants.ACC_STAT_4.equals(accStatus)) {
                                    settJyxLoanOdAmt = settJyxLoanOdAmt.add(balance);
                                }
                            }

                            //173. 贷款五级分类为非正常的笔数
                            if (StringUtils.isNotEmpty(fiveAssort)
                                    && !Constants.FIF_TYPE_1.equals(fiveAssort)) {
                                unNormalLoanNum++;
                            }

                            //175. 最近60个月经营性贷款授信机构数
                            // 业务类型为 个人经营性贷款、农户贷款、其他
                            if (StringUtils.isNotEmpty(businessType)
                                    && Constants.BUS_TYPE_41.equals(businessType)
                                    || Constants.BUS_TYPE_51.equals(businessType)
                                    || Constants.BUS_TYPE_52.equals(businessType)) {
                                // 开立日期为60个月内
                                if (issuanceDate.after(aimMonthDate60)
                                        && StringUtils.isNotEmpty(orgCode)) {
                                    late60mJyxLoanOrgnumSet.add(orgCode);
                                }
                            }
                            late60mJyxLoanOrgnum = late60mJyxLoanOrgnumSet.size();

                            //179. 个人经营性贷款余额
                            //186 他行经营性贷款授信机构数

                            //187 业务类型为“个人经营性贷款+农户贷款+其他贷款”

                            //188. 最近2年内经营性贷款单月最高逾期金额
                            int length = pcapRecfivehisinfoSubList == null ? 0 : pcapRecfivehisinfoSubList.size();
                            length = length > 24 ? 24 : length;
                            //192. 他行经营性贷款授信余额
                            if (StringUtils.isNotEmpty(businessType)
                                    && balance != null
                                    && Constants.BUS_TYPE_41.equals(businessType)
                                    || Constants.BUS_TYPE_51.equals(businessType)
                                    || Constants.BUS_TYPE_52.equals(businessType)) {
                                currJyxLoanBalan = currJyxLoanBalan.add(balance);

                                if (!Constants.ACC_STAT_3.equals(accStatus) && StringUtils.isNotBlank(orgCode)) {
                                    othrBnkMngmtLoanOrgNumSet.add(orgCode);
                                }

                                if (Constants.ACC_STAT_3.equals(accStatus)
                                        || Constants.ACC_STAT_4.equals(accStatus)) {
                                    // “管理机构”的去重总数
                                    if (StringUtils.isNotBlank(orgCode)) {
                                        late5yJyxLoanOdBkNumSet.add(orgCode);
                                    }
                                }

                                for (int i = 0; i < length; i++) {
                                    PcapRecfivehisinfoSub recfivehisinfoSub = pcapRecfivehisinfoSubList.get(i);
                                    //24个月
                                    if (recfivehisinfoSub != null && recfivehisinfoSub.getOverdueMoney() != null) {
                                        String overdueMoney = recfivehisinfoSub.getOverdueMoney();
                                        if (l2yJyxLoanPmonMaxOdAmt.doubleValue() < Double.valueOf(overdueMoney)) {
                                            l2yJyxLoanPmonMaxOdAmt = new BigDecimal(overdueMoney);
                                        }
                                    }
                                }

                                if (!Constants.ACC_STAT_3.equals(accStatus)) {
                                    if (StringUtils.isNotEmpty(orgCode)
                                            && !"浙江泰隆商业银行".equals(orgCode)
                                            && balance != null) {
                                        othBankMngmtLoanBal = othBankMngmtLoanBal.add(balance);
                                    }
                                }
                            }
                            othrBnkMngmtLoanOrgNum = othrBnkMngmtLoanOrgNumSet.size();
                            late5yJyxLoanOdBkNum = late5yJyxLoanOdBkNumSet.size();

                            //190. 他行贷款授信机构数
                            //191. 他行贷款授信余额
                            if (!Constants.ACC_STAT_3.equals(accStatus)) {
                                if (StringUtils.isNotEmpty(orgCode)
                                        && !"浙江泰隆商业银行".equals(orgCode)
                                        && balance != null) {
                                    othBankLoanOrgNumSet.add(orgCode);
                                    othBankLoanCreditBal = othBankLoanCreditBal.add(balance);
                                }
                            }
                            othBankLoanOrgNum = othBankLoanOrgNumSet.size();

                            //193. 他行消费性贷款授信机构数
                            //194. 他行消费性贷款授信余额
                            if (Constants.BUS_TYPE_11.equals(businessType)
                                    || Constants.BUS_TYPE_12.equals(businessType)
                                    || Constants.BUS_TYPE_13.equals(businessType)
                                    || Constants.BUS_TYPE_21.equals(businessType)
                                    || Constants.BUS_TYPE_31.equals(businessType)
                                    || Constants.BUS_TYPE_91.equals(businessType)) {
                                if (!Constants.ACC_STAT_3.equals(accStatus)) {
                                    if (!"浙江泰隆商业银行".equals(orgCode)) {
                                        othBankXfxLoanOrgNumSet.add(orgCode);
                                        if (StringUtils.isNotBlank(pcanFirinfseg.getBalance())) {
                                            othBankXfxLoanBal = othBankXfxLoanBal.add(balance);
                                        } else {
                                            othBankXfxLoanBal = othBankXfxLoanBal.add(monthBalance);
                                        }
                                    }
                                }
                            }
                            othBankXfxLoanOrgNum = othBankXfxLoanOrgNumSet.size();

                            //201. 近3个月到期的贷款笔数
                            //账户状态不为“结清”且到期日期早于当前3个月内的账户总数
                            if (dueDate != null
                                    && dueDate.after(aimMonthDate3)
                                    && dueDate.before(reportTime)
                                    && StringUtils.isNotEmpty(accStatus)
                                    && !Constants.ACC_STAT_3.equals(accStatus)) {
                                lateSeaEndLoanNum++;
                            }

                            //205. 最近2个月贷款到期笔数
                            //账户状态不为“结清”且到期日期早于当前2个月内的账户总数
                            if (dueDate != null
                                    && dueDate.after(aimMonthDate2)
                                    && dueDate.before(reportTime)
                                    && StringUtils.isNotEmpty(accStatus)
                                    && !Constants.ACC_STAT_3.equals(accStatus)) {
                                late2MLoanNum++;
                            }

                            //206. 近两年办理过住房贷款的笔数
                            //业务种类为“个人住房商业贷款，个人商用房（含商住两用）贷款，个人住房公积金贷款”
                            // 且“开立日期”距离当前2年内的账户总数
                            if (StringUtils.isNotEmpty(businessType)
                                    && Constants.BUS_TYPE_11.equals(businessType)
                                    || Constants.BUS_TYPE_12.equals(businessType)
                                    || Constants.BUS_TYPE_13.equals(businessType)) {
                                if (issuanceDate.after(aimYearDate2)) {
                                    late2YHousingLoanNum++;
                                }
                            }

                            //208. 当前贷款逾期期数
                            //最近一个月度变现信息段余额-最新表现字段余额大于等于0
                            if (StringUtils.isNotEmpty(overduePeriod)
                                    && balance.compareTo(monthBalance) >= 0) {
                                currUnsettLoanOdNum += Integer.parseInt(overduePeriod);
                            }

                            //210. 当前贷款最高逾期金额
                            //各账户“当前逾期总额”最大值
                            if (balance.compareTo(monthBalance) > 0
                                    && currUnsettLoanMaxOdAmt.compareTo(overdueAmount) < 0) {
                                currUnsettLoanMaxOdAmt = overdueAmount;
                            }

                            //212. 特殊交易（“提前结清”的除外）的贷款笔数
                            // 存在特殊交易且特殊交易不为“提前还款、提前结清”且账户状态不为“结清”的账户总数
                            if (StringUtils.isNotEmpty(accStatus)
                                    && !Constants.ACC_STAT_3.equals(accStatus)) {
                                int count = 0;
                                if (null != pcaiSpeinfsegSubList) {
                                    for (PcaiSpeinfsegSub pcaiSpeinfsegSub : pcaiSpeinfsegSubList) {
                                        String specialType = pcaiSpeinfsegSub.getSpecialType();
                                        if (!Constants.SPECIAL_TYPE_4.equals(specialType)
                                                && !Constants.SPECIAL_TYPE_5.equals(specialType)) {
                                            count++;
                                        }
                                    }
                                    if (pcaiSpeinfsegSubList.size() == count) {
                                        specWjqLoanNum = specWjqLoanNum + 1;
                                    }
                                }
                            }

                            //213. 特殊交易类型为以资抵债总次数
                            //特殊交易类型为“以资抵债”的总次数
                            if (Constants.ACC_TYPE_R1.equals(accountType)
                                    || Constants.ACC_TYPE_R4.equals(accountType)
                                    || Constants.ACC_TYPE_D1.equals(accountType)) {
                                if (pcaiSpeinfsegSubList != null) {
                                    for (PcaiSpeinfsegSub pcaiSpeinfsegSub : pcaiSpeinfsegSubList) {
                                        if (Constants.SPECIAL_TYPE_3.equals(pcaiSpeinfsegSub.getSpecialType())) {
                                            ++specYzdzLoanNum;
                                        }
                                    }
                                }
                            }

                            //217. 近2个月消费金融公司等非银行金融机构发放贷款的笔数
                            //开立日期为2个月内且业务管理机构类型不为“11、12、14、15”的账户总数
                            //218. 近1年消费金融公司等非银行金融机构发放贷款的笔数
                            //开立日期为1年内且业务管理机构类型不为“11、12、14、15”的账户总数
                            if (StringUtils.isNotEmpty(orgType)
                                    && !"11".equals(orgType)
                                    && !"12".equals(orgType)
                                    && !"14".equals(orgType)
                                    && !"15".equals(orgType)) {
                                if (issuanceDate.after(aimMonthDate2)) {
                                    ++late2mLoanFyhNum;
                                }
                                if (issuanceDate.after(aimYearDate1)) {
                                    ++late1yLoanFyhNum;
                                }
                            }

                            //29. 将来2个月未结清经营性贷款到期笔数
                            //各账户业务种类为“个人经营性贷款、农户贷款、其他”且账户状态为“正常”且到期日期距离当前不超过2个月的账户总数
                            //33. 他行经营性贷款最高额度
                            //各账户业务种类为“个人经营性贷款、农户贷款、其他”且账户状态为“正常”且管理机构不等于“浙江泰隆商业银行”的账户的借款金额最大值
                            if (StringUtils.isNotEmpty(businessType)
                                    && Constants.BUS_TYPE_41.equals(businessType)
                                    || Constants.BUS_TYPE_51.equals(businessType)
                                    || Constants.BUS_TYPE_52.equals(businessType)) {
                                if (StringUtils.isNotEmpty(accStatus)
                                        && Constants.ACC_STAT_1.equals(accStatus)) {
                                    if (issuanceDate.after(aimMonthDate2)) {
                                        ++f2MUnSettJyxLoanNum;
                                    }

                                    if (!"浙江泰隆商业银行".equals(orgCode)) {
                                        if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                            if (pcabBasicinfoseg.getAccountQuota() != null) {
                                                pcabAccountQuota = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                            }
                                        } else if (oBankJyxLoanMaxAmt.doubleValue() < loanAmount.doubleValue()) {
                                            oBankJyxLoanMaxAmt = loanAmount;
                                        }
                                        max = pcabAccountQuota.max(pcabAccountQuota);
                                    }

                                }
                            }

                            //34. 他行未结清非抵质押贷款笔数
                            //各账户余额不为0且担保方式不为“质押、抵押”且管理机构不等于“浙江泰隆商业银行”的账户总数
                            if (balance.intValue() != 0) {
                                if (!Constants.GUARANTY_1.equals(guaranteeMode)
                                        && !Constants.GUARANTY_2.equals(guaranteeMode)) {
                                    if (!"浙江泰隆商业银行".equals(orgCode)) {
                                        oBUnSettLoanFdzyNum = oBUnSettLoanFdzyNum + 1;
                                        //35. 他行未结清非抵质押贷款余额
                                        //各账户余额不为0且担保方式不为“质押、抵押”且管理机构不等于“浙江泰隆商业银行”的账户余额总值
                                        oBUnSettLoanFdzyAmt = oBUnSettLoanFdzyAmt.add(balance);
                                    }
                                }
                            }


                            //40. 最近1年贷款最大合同金额
                            //各账户开立日期距离当日1年内的借款金额最大值
                            if (issuanceDate != null) {
                                if (aimYearDate1.before(issuanceDate)
                                        && issuanceDate.before(reportTime)) {
                                    if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                        if (pcabBasicinfoseg.getAccountQuota() != null) {
                                            accountQuota = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                        }
                                    }
                                    if (l1YMaxLoanAmt.compareTo(balance) < 0) {
                                        l1YMaxLoanAmt = balance;
                                    }
                                    if (accountQuota != null) {
                                        if (l1YMaxLoanAmt.compareTo(accountQuota) < 0) {
                                            l1YMaxLoanAmt = accountQuota;
                                        }
                                    }
                                }
                            }

                            //45. 过去两年内经营性贷款逾期两期以上（包括两期）贷款笔数
                            //业务种类为“个人经营性贷款+农户贷款+其他”过去24个月还款记录中出现大于等于2的账户总数
                            if (Constants.BUS_TYPE_41.equals(businessType)
                                    || Constants.BUS_TYPE_51.equals(businessType)
                                    || Constants.BUS_TYPE_52.equals(businessType)) {
                                boolean judge = true;
                                if (twentyfourpaymentstatsSubList != null) {
                                    for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                        if (judge && Constants.OVERDUE1.contains(subList.getPaymentStats())) {
                                            ++l2YJyxLoanOd2TimeNum;
                                            judge = false;
                                        }
                                    }
                                }
                            }

                            //97. 未来1个月内到期贷款笔数
                            //98. 未来1个月内到期贷款金额
                            //99. 未来1个月内到期贷款余额
                            //100. 未来3个月内到期贷款笔数
                            //101. 未来3个月内到期贷款金额
                            //102. 未来3个月内到期贷款余额
                            //103. 未来6个月内到期贷款笔数
                            //104. 未来6个月内到期贷款金额
                            //105. 未来6个月内到期贷款余额
                            if (StringUtils.isNotEmpty(accStatus)
                                    && loanAmount != null
                                    && balance != null) {
                                if (dueDate != null
                                        && dueDate.before(aimMonthDate1b)) {
                                    if (Constants.ACC_STAT_1.equals(accStatus)) {
                                        //未来一个月内到期贷款笔数
                                        fMonLoanCount = fMonLoanCount + 1;
                                        //授信额度
                                        if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                            if (pcabBasicinfoseg.getAccountQuota() != null) {
                                                accountQuota = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                            }
                                        }
                                        //未来一个月内到期贷款金额
                                        fMonLoanMoney = fMonLoanMoney.add(loanAmount).add(accountQuota);
                                        //未来一个月内到期贷款余额
                                        fMonLoanBalance = fMonLoanBalance.add(balance);
                                    }
                                }
                                if (dueDate != null
                                        && dueDate.before(aimMonthDate3b)) {
                                    if (Constants.ACC_STAT_1.equals(accStatus)) {
                                        //未来三个月内到期贷款笔数
                                        fSeaLoanCount = fSeaLoanCount + 1;
                                        //未来三个月内到期贷款金额
                                        //授信额度
                                        if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                            if (pcabBasicinfoseg.getAccountQuota() != null) {
                                                accountQuota = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                            }
                                        }
                                        fSeaLoanMoney = fSeaLoanMoney.add(loanAmount).add(accountQuota);
                                        //未来三个月内到期贷款余额
                                        fSeaLoanBalance = fSeaLoanBalance.add(balance);
                                    }
                                }
                                if (dueDate != null
                                        && dueDate.before(aimMonthDate6b)) {
                                    if (Constants.ACC_STAT_1.equals(accStatus)) {
                                        //未来六个月内到期贷款笔数
                                        fHlyLoanCount = fHlyLoanCount + 1;
                                        //授信额度
                                        if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                            if (pcabBasicinfoseg.getAccountQuota() != null) {
                                                accountQuota = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                            }
                                        }
                                        //未来六个月内到期贷款金额
                                        fHlyLoanMoney = fHlyLoanMoney.add(loanAmount).add(accountQuota);
                                        //未来六个月内到期贷款余额
                                        fHlyLoanBalance = fHlyLoanBalance.add(balance);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            DecimalFormat formatTwo = new DecimalFormat("0.00");
            formatTwo.setRoundingMode(RoundingMode.UP);

            //107. 最近1个月贷款笔数
            zxPZbInfo.setLateMonLoanNum(String.valueOf(lateMonLoanNum));
            //108. 最近1个月贷款授信总额
            zxPZbInfo.setLateMonCredit(formatTwo.format(lateMonCredit));
            //110. 最近3个月贷款笔数
            zxPZbInfo.setLateSeaLoanNum(String.valueOf(lateSeaLoanNum));
            //111. 最近3个月内借款金额总和
            zxPZbInfo.setLateSeaCredit(formatTwo.format(lateSeaCredit));
            //113. 最近6个月内的账户数
            zxPZbInfo.setLateHlyLoanNum(String.valueOf(lateHlyLoanNum));
            //114. 最近6个月内借款金额总和
            zxPZbInfo.setLateHlyCredit(formatTwo.format(lateHlyCredit));
            //116. 过去2年内贷款最高逾期期数
            zxPZbInfo.setL2YLdMaxNum(String.valueOf(l2YLdMaxNum));
            //117. 个人住房贷款余额
            zxPZbInfo.setHousingLoanAmt(formatTwo.format(housingLoanAmt));
            //118. 个人汽车贷款余额
            zxPZbInfo.setCarLoanAmt(formatTwo.format(carLoanAmt));
            //119. 个人经营性贷款余额
            zxPZbInfo.setPerJyxLoanBalan(formatTwo.format(perJyxLoanBalan));
            //124. 最近五年内经营性贷款逾期机构数
            zxPZbInfo.setLate5yJyxLoanOdNum(String.valueOf(late5yJyxLoanOdNum));
            //128. 剔除贷款单月最高逾期金额
            zxPZbInfo.setRemLoanCatPerMonOdAmt(formatTwo.format(remLoanCatPerMonOdAmt));
            //131. 剔除贷款类别银行服务家数
            zxPZbInfo.setRemLoanCatBankNum(String.valueOf(remLoanCatBankNum));
            //149. 近两年经营性贷款逾期次数
            zxPZbInfo.setLate2yJyxLoanOdNum(String.valueOf(late2yJyxLoanOdNum));
            //150. 近2年消费性贷款逾期次数
            zxPZbInfo.setLate2yXfxLoanOdNum(String.valueOf(late2yXfxLoanOdNum));
            //152. 近2年经营性贷款最长逾期月数
            zxPZbInfo.setLate2yJyxLoanMaxOdNum(String.valueOf(late2yJyxLoanMaxOdNum));
            //153. 消费性贷款最长逾期月数
            zxPZbInfo.setLate2yXfxLoanMaxOdNum(String.valueOf(late2yXfxLoanMaxOdNum));
            //155. 贷款五级分类为非正常的余额
            zxPZbInfo.setUnNormalLoanBalan(formatTwo.format(unNormalLoanBalan));
            //164. 最近24个月贷款最长逾期数
            zxPZbInfo.setLate2yLoanMaxOdNum(String.valueOf(late2yLoanMaxOdNum));
            //172. 未结清经营性贷款逾期金额
            zxPZbInfo.setSettJyxLoanOdAmt(formatTwo.format(remLoanCatPerMonOdAmt));
            //173. 贷款五级分类为非正常的笔数
            zxPZbInfo.setUnNormalLoanNum(String.valueOf(unNormalLoanNum));
            //175. 最近60个月经营性贷款授信机构数
            zxPZbInfo.setLate60mJyxLoanOrgnum(String.valueOf(late60mJyxLoanOrgnum));
            //179. 个人经营性贷款余额
            zxPZbInfo.setCurrJyxLoanBalan(formatTwo.format(currJyxLoanBalan));
            //186. 他行经营性贷款授信机构数
            zxPZbInfo.setOthrBnkMngmtLoanOrgNum(String.valueOf(othrBnkMngmtLoanOrgNum));
            //187. 最近5年内经营性贷款逾期银行家数
            zxPZbInfo.setLate5yJyxLoanOdBkNum(String.valueOf(late5yJyxLoanOdBkNum));
            //188. 最近2年内经营性贷款单月最高逾期金额
            zxPZbInfo.setL2yJyxLoanPmonMaxOdAmt(formatTwo.format(l2yJyxLoanPmonMaxOdAmt));
            //190. 他行贷款授信机构数
            zxPZbInfo.setOthBankLoanOrgNum(String.valueOf(othBankLoanOrgNum));
            //191. 他行贷款授信余额
            zxPZbInfo.setOthBankLoanCreditBal(formatTwo.format(othBankLoanCreditBal));
            //192. 他行经营性贷款授信余额
            zxPZbInfo.setOthBankMngmtLoanBal(formatTwo.format(othBankMngmtLoanBal));
            //193. 他行消费性贷款授信机构数
            zxPZbInfo.setOthBankXfxLoanOrgNum(String.valueOf(othBankXfxLoanOrgNum));
            //194. 他行消费性贷款授信余额
            zxPZbInfo.setOthBankXfxLoanBal(formatTwo.format(othBankXfxLoanBal));
            //201. 近3个月到期的贷款笔数
            zxPZbInfo.setLateSeaEndLoanNum(String.valueOf(lateSeaEndLoanNum));
            //205. 最近2个月贷款到期笔数
            zxPZbInfo.setLate2MLoanNum(String.valueOf(late2MLoanNum));
            //206. 近两年办理过住房贷款的笔数
            zxPZbInfo.setLate2YHousingLoanNum(String.valueOf(late2YHousingLoanNum));
            //208. 当前贷款逾期期数
            zxPZbInfo.setCurrUnsettLoanOdNum(String.valueOf(currUnsettLoanOdNum));
            //210. 当前贷款最高逾期金额
            zxPZbInfo.setCurrUnsettLoanMaxOdAmt(formatTwo.format(currUnsettLoanMaxOdAmt));
            //212. 特殊交易（“提前结清”的除外）的贷款笔数
            zxPZbInfo.setSpecWjqLoanNum(String.valueOf(specWjqLoanNum));
            //213. 特殊交易类型为以资抵债总次数
            zxPZbInfo.setSpecYzdzLoanNum(String.valueOf(specYzdzLoanNum));
            //217. 近2个月消费金融公司等非银行金融机构发放贷款的笔数
            zxPZbInfo.setLate2mLoanFyhNum(String.valueOf(late2mLoanFyhNum));
            //218. 近1年消费金融公司等非银行金融机构发放贷款的笔数
            zxPZbInfo.setLate1yLoanFyhNum(String.valueOf(late1yLoanFyhNum));
            //29. 将来2个月未结清经营性贷款到期笔数
            zxPZbInfo.setF2MUnSettJyxLoanNum(String.valueOf(f2MUnSettJyxLoanNum));
            //33. 他行经营性贷款最高额度
            zxPZbInfo.setOBankJyxLoanMaxAmt(formatTwo.format(max));
            //34. 他行未结清非抵质押贷款笔数
            zxPZbInfo.setOBUnSettLoanFdzyNum(String.valueOf(oBUnSettLoanFdzyNum));
            //35. 他行未结清非抵质押贷款余额
            zxPZbInfo.setOBUnSettLoanFdzyAmt(formatTwo.format(oBUnSettLoanFdzyAmt));
            //40. 最近1年贷款最大合同金额
            zxPZbInfo.setL1YMaxLoanAmt(formatTwo.format(l1YMaxLoanAmt));
            //45. 过去两年内经营性贷款逾期两期以上（包括两期）贷款笔数
            zxPZbInfo.setL2YJyxLoanOd2TimeNum(String.valueOf(l2YJyxLoanOd2TimeNum));
            //97. 未来1个月内到期贷款笔数
            zxPZbInfo.setFMonLoanCount(String.valueOf(fMonLoanCount));
            //98. 未来1个月内到期贷款金额
            zxPZbInfo.setFMonLoanMoney(formatTwo.format(fMonLoanMoney));
            //99. 未来1个月内到期贷款余额
            zxPZbInfo.setFMonLoanBalance(formatTwo.format(fMonLoanBalance));
            //100. 未来3个月内到期贷款笔数
            zxPZbInfo.setFSeaLoanCount(String.valueOf(fSeaLoanCount));
            //101. 未来3个月内到期贷款金额
            zxPZbInfo.setFSeaLoanMoney(formatTwo.format(fSeaLoanMoney));
            //102. 未来3个月内到期贷款余额
            zxPZbInfo.setFSeaLoanBalance(formatTwo.format(fSeaLoanBalance));
            //103. 未来6个月内到期贷款笔数
            zxPZbInfo.setFHlyLoanCount(String.valueOf(fHlyLoanCount));
            //104. 未来6个月内到期贷款金额
            zxPZbInfo.setFHlyLoanMoney(formatTwo.format(fHlyLoanMoney));
            //105. 未来6个月内到期贷款余额
            zxPZbInfo.setFHlyLoanBalance(formatTwo.format(fHlyLoanBalance));
        } catch (Exception e) {
            throw new Exception(e);
        }

        return zxPZbInfo;
    }
}

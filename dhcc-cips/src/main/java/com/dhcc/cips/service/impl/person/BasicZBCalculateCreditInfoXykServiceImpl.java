package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcabBasicinfoseg;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcamMonthInformation;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcatTwentyfourpaymentstatsSub;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * 基础指标计算（信用卡明细信息部分）
 */
@Service
public class BasicZBCalculateCreditInfoXykServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo reportbo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();
        try {
            //PDA(借贷账户信息)
            BorAccInfBo borAccInfBo = reportbo.getBorAccInfBo();
            //PD01(借贷账户信息单元)
            List<BorAccInfUnitBo> borAccInfUnitBoList = null;
            if (borAccInfBo != null) {
                borAccInfUnitBoList = borAccInfBo.getBorAccInfUnitBoList();
            }

            //26贷记卡账户最大使用率
            BigDecimal c_djk_account_max_use_rate = new BigDecimal("0.000");
            //28当前贷记卡逾期总额求和
            BigDecimal curr_credit_card_due_sum = new BigDecimal("0.00");
            //30 贷记卡账户最高逾期期数
            int c_due_djk_max_num = 0;
            //32 贷记卡账户两年内逾期期数大于等于3的账户数
            int late_2y_djk_due_3_account_num = 0;
            //27 准贷记卡账户最大使用率
            BigDecimal c_zdjk_account_max_use_rate = new BigDecimal("0.000");
            //29 准贷记卡透支180天以上未支付余额总额
            BigDecimal un_pay_180_more_balan_sum = new BigDecimal("0.00");
            //31贷记卡账户最高逾期期数
            int c_due_zdjk_max_num = 0;
            //33准贷记卡账户两年内逾期期数大于等于3的账户数
            int late_2y_zdjk_due_3_account_num = 0;
            //个人指标21 历史信用卡最高额度使用率
            BigDecimal histCred=new BigDecimal("0.000");

            //基础指标计算（信用卡明细信息部分）
            if (borAccInfUnitBoList != null && borAccInfUnitBoList.size() > 0) {
                for (BorAccInfUnitBo list : borAccInfUnitBoList) {
                    //基本信息段(PD01A)
                    PcabBasicinfoseg pcabBasicinfoseg = list.getPcabBasicinfoseg();
                    //最近一次月度变现信息段(PD01C)
                    PcamMonthInformation pcamMonthInformation = list.getPcamMonthInformation();
                    //当前逾期总额
                    String overdueAmount=null;
                    if (pcamMonthInformation!=null&&pcamMonthInformation.getOverdueAmount()!=null){
                        overdueAmount= pcamMonthInformation.getOverdueAmount();
                    }

                    if (pcabBasicinfoseg!=null&&pcabBasicinfoseg.getAccountType()!=null) {
                        //贷记卡账户
                        if (Constants.ACC_TYPE_R2.equals(pcabBasicinfoseg.getAccountType())) {
                            BigDecimal quota = new BigDecimal("0.000");
                            BigDecimal max = new BigDecimal("0.000");
                            if (pcabBasicinfoseg.getAccountQuota()!= null) {
                                //授信额度
                                quota = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                            }
                            //最大使用额度
                            if (pcamMonthInformation != null&&pcamMonthInformation.getMaxQuota()!=null) {
                                max = new BigDecimal(pcamMonthInformation.getMaxQuota());
                            }
                            BigDecimal rate = new BigDecimal("0.000");
                            if (max != null && quota != null && quota.compareTo(BigDecimal.ZERO) == 1) {
                                rate = max.divide(quota, 3, RoundingMode.HALF_UP);
                            }
                            if (null != c_djk_account_max_use_rate && null != rate) {
                                if (c_djk_account_max_use_rate.compareTo(rate) == -1) {
                                    //贷记卡账户最大使用率
                                    c_djk_account_max_use_rate = rate;
                                }
                            }

                            if (pcamMonthInformation != null && overdueAmount != null ) {
                                //最近一个月度变现信息段逾期总额
                                BigDecimal formationBalance=new BigDecimal("0.00");
                                if (list.getPcamMonthInformation()!=null&&list.getPcamMonthInformation().getOverdueAmount()!=null){
                                    formationBalance = new BigDecimal(list.getPcamMonthInformation().getOverdueAmount());
                                }
                                //最新表现字段还款金额
                                BigDecimal firinfsegBalance=new BigDecimal("0.00");
                                if (list.getPcanFirinfseg()!=null&&list.getPcanFirinfseg().getLastRepmoney()!=null){
                                    firinfsegBalance  = new BigDecimal(list.getPcanFirinfseg().getLastRepmoney());
                                }
                                BigDecimal balance = formationBalance.subtract(firinfsegBalance) ;
                                //最近一个月度变现信息段余额-最新表现字段余额大于等于0
                                if (balance.compareTo(BigDecimal.ZERO)>0) {
                                    //当前贷记卡逾期总额求和
                                    curr_credit_card_due_sum = curr_credit_card_due_sum.add(new BigDecimal(overdueAmount));
                                }
                            }

                            if (list.getPcatTwentyfourpaymentstat() != null) {
                                List<PcatTwentyfourpaymentstatsSub> sub = list.getPcatTwentyfourpaymentstat().getSub();
                                if (sub != null) {
                                    boolean judge=true;
                                    for (PcatTwentyfourpaymentstatsSub subList : sub) {
                                        if (subList.getPaymentStats() != null) {
                                            if (Constants.OVERDUE_STATUS.contains(subList.getPaymentStats())) {
                                                int paymentStats = Integer.parseInt(subList.getPaymentStats());
                                                if (c_due_djk_max_num < paymentStats) {
                                                    //贷记卡账户最高逾期期数
                                                    c_due_djk_max_num = paymentStats;
                                                }
                                                //贷记卡账户两年内逾期期数大于等于3的账户数
                                                if (judge&&paymentStats >= 3) {
                                                    //贷记卡账户两年内逾期期数大于等于3的账户数
                                                    late_2y_djk_due_3_account_num = late_2y_djk_due_3_account_num + 1;
                                                    judge=false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //准贷记卡账户
                        if (Constants.ACC_TYPE_R3.equals(pcabBasicinfoseg.getAccountType())) {                            BigDecimal maxBalamce = new BigDecimal("0.000");
                            //授信额度
                            BigDecimal account = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                            //最大使用额度
                            if (pcamMonthInformation != null) {
                                maxBalamce = new BigDecimal(pcamMonthInformation.getMaxBalance());
                            }
                            BigDecimal rateMax = new BigDecimal("0.000");
                            if (account != null && maxBalamce != null && account.compareTo(BigDecimal.ZERO) > 0) {
                                rateMax = maxBalamce.divide(account, 3, RoundingMode.HALF_UP);
                            }
                            if (null != c_zdjk_account_max_use_rate && null != rateMax) {
                                if (c_zdjk_account_max_use_rate.compareTo(rateMax) == -1) {
                                    //准贷记卡账户最大使用率
                                    c_zdjk_account_max_use_rate = rateMax;
                                }
                            }

                            if (pcamMonthInformation != null) {
                                String outstandingBalance = pcamMonthInformation.getOutstandingBalance();
                                //准贷记卡透支180天以上未支付余额总额
                                if (outstandingBalance!=null) {
                                    un_pay_180_more_balan_sum = un_pay_180_more_balan_sum.add(new BigDecimal(outstandingBalance));
                                }
                            }

                            if (list.getPcatTwentyfourpaymentstat() != null) {
                                List<PcatTwentyfourpaymentstatsSub> sub = list.getPcatTwentyfourpaymentstat().getSub();
                                if (sub != null && sub.size() > 0) {
                                    boolean judge = true;
                                    for (PcatTwentyfourpaymentstatsSub subList : sub) {
                                        if (subList.getPaymentStats()!=null) {
                                            if (Constants.OVERDUE_STATUS.contains(subList.getPaymentStats())) {
                                                int paymentStats = Integer.parseInt(subList.getPaymentStats());
                                                if (c_due_zdjk_max_num < paymentStats) {
                                                    //贷记卡账户最高逾期期数
                                                    c_due_zdjk_max_num = paymentStats;
                                                }
                                                //准贷记卡账户两年内逾期期数大于等于3的账户数
                                                if (judge && paymentStats >= 3) {
                                                    late_2y_zdjk_due_3_account_num = late_2y_zdjk_due_3_account_num + 1;
                                                    judge = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //贷记卡账户最大使用率
            zxPZbInfo.setCDjkAccountMaxUseRate(String.valueOf(c_djk_account_max_use_rate));
            //当前贷记卡逾期总额求和
            zxPZbInfo.setCurrCreditCardDueSum(String.valueOf(curr_credit_card_due_sum));
            //贷记卡账户最高逾期期数
            zxPZbInfo.setCDueDjkMaxNum(String.valueOf(c_due_djk_max_num));
            //贷记卡账户两年内逾期期数大于等于3的账户数
            zxPZbInfo.setLate2yDjkDue3AccountNum(String.valueOf(late_2y_djk_due_3_account_num));
            //准贷记卡账户最大使用率
            zxPZbInfo.setCZdjkAccountMaxUseRate(String.valueOf(c_zdjk_account_max_use_rate));
            //准贷记卡透支180天以上未支付余额总额
            zxPZbInfo.setUnPay180MoreBalanSum(String.valueOf(un_pay_180_more_balan_sum));
            //贷记卡账户最高逾期期数
            zxPZbInfo.setCDueZdjkMaxNum(String.valueOf(c_due_zdjk_max_num));
            //准贷记卡账户两年内逾期期数大于等于3的账户数
            zxPZbInfo.setLate2yZdjkDue3AccountNum(String.valueOf(late_2y_zdjk_due_3_account_num));

            //个人指标21 历史信用卡最高额度使用率
            histCred = new BigDecimal("0.000");
            if (!(c_zdjk_account_max_use_rate.compareTo(BigDecimal.ZERO) == 0)) {
                histCred = c_djk_account_max_use_rate.max(c_zdjk_account_max_use_rate);
            }
            BigDecimal hist_cred_card_use_frequ = histCred.setScale(3, BigDecimal.ROUND_HALF_UP);
            //历史信用卡最高额度使用率
            zxPZbInfo.setHistCredCardUseFrequ(String.valueOf(hist_cred_card_use_frequ));

            //个人指标22 当前信用卡逾期总额
            BigDecimal totalBalan = curr_credit_card_due_sum.add(un_pay_180_more_balan_sum);
            BigDecimal cred_card_total_balan = totalBalan.setScale(2, BigDecimal.ROUND_HALF_UP);
            zxPZbInfo.setCOdAmt(String.valueOf(cred_card_total_balan));

            //个人指标23 过去2年内信用卡最高逾期期数
            int l_2_y_od_max_time = 0;
            if (c_due_djk_max_num > c_due_zdjk_max_num) {
                l_2_y_od_max_time = c_due_djk_max_num;
            } else {
                l_2_y_od_max_time = c_due_zdjk_max_num;
            }
            zxPZbInfo.setL2YOdMaxTime(String.valueOf(l_2_y_od_max_time));

            //个人指标25 过去两年内逾期三期以上（包括三期）信用卡张数
            int l_2_y_od_2_time_cred_num = late_2y_djk_due_3_account_num + late_2y_zdjk_due_3_account_num;
            zxPZbInfo.setL2YOd2TimeCredNum(String.valueOf(l_2_y_od_2_time_cred_num));
        } catch (Exception e) {
            throw new Exception(e);
        }
        return zxPZbInfo;
    }
}

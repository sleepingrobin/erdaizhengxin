package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcabBasicinfoseg;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcamMonthInformation;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcatTwentyfourpaymentstat;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcatTwentyfourpaymentstatsSub;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author StivenYang
 * @description 三（信贷交易信息明细）-六
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateCreditInfoZdjkServiceImpl implements PersonZBCalculteThreadService {

    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();
        try {

            /*******************************指标声明start************************/
            //163. 最近2年准贷记卡最长逾期月数
            int late2yZdjkMaxOdNum = 0;
            //66. 当前最大逾期期数
            int cds = 0;
            /*******************************指标声明end************************/

            if (cpqReportBo != null
                    && cpqReportBo.getBorAccInfBo() != null
                    && cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList() != null) {

                List<BorAccInfUnitBo> borAccInfUnitBoList = cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList();
                if (null != borAccInfUnitBoList && borAccInfUnitBoList.size() > 0) {
                    for (BorAccInfUnitBo borAccInfUnitBo : borAccInfUnitBoList) {

                        /***************************账户公共变量开始**************************/
                        //基本信息段
                        PcabBasicinfoseg pcabBasicinfoseg = borAccInfUnitBo.getPcabBasicinfoseg();
                        //账户类型
                        String accountType = null;
                        if (null != pcabBasicinfoseg) {
                            accountType = pcabBasicinfoseg.getAccountType();
                        }
                        //最近两年还款状态
                        PcatTwentyfourpaymentstat pcatTwentyfourpaymentstat = borAccInfUnitBo.getPcatTwentyfourpaymentstat();
                        //最近两年支付列表
                        List<PcatTwentyfourpaymentstatsSub> twentyfourpaymentstatsSubList = null;
                        if (pcatTwentyfourpaymentstat != null) {
                            twentyfourpaymentstatsSubList = pcatTwentyfourpaymentstat.getSub();
                        }
                        //最近一个月的账户信息
                        PcamMonthInformation pcamMonthInformation = borAccInfUnitBo.getPcamMonthInformation();
                        //当前逾期总额
                        BigDecimal overdueAmount = null;
                        //当前逾期期数
                        String overduePeriod = null;
                        if (pcamMonthInformation != null) {
                            if (pcamMonthInformation.getOverdueAmount() != null) {
                                overdueAmount = new BigDecimal(pcamMonthInformation.getOverdueAmount());
                            }
                            if (pcamMonthInformation.getOverduePeriod() != null) {
                                overduePeriod = pcamMonthInformation.getOverduePeriod();
                            }
                        }
                        /***************************账户公共变量结束**************************/

                        //163. 最近2年准贷记卡最长逾期月数
                        //所有账户24个月还款记录取最大值
                        if (StringUtils.isNotEmpty(accountType) && Constants.ACC_TYPE_R3.equals(accountType)) {
                            if (null != twentyfourpaymentstatsSubList) {
                                for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                    String paymentStats = subList.getPaymentStats();
                                    if (StringUtils.isNotEmpty(paymentStats)) {
                                        if (Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                            if (late2yZdjkMaxOdNum < Integer.parseInt(paymentStats)) {
                                                late2yZdjkMaxOdNum = Integer.parseInt(paymentStats);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //66. 当前最大逾期期数
                        //逾期金额>300的各账户“当期逾期期数”最大值
                        if (Constants.ACC_TYPE_R2.equals(accountType)
                                || Constants.ACC_TYPE_R3.equals(accountType)
                                || Constants.ACC_TYPE_D1.equals(accountType)) {
                            //最近一个月度变现信息段余额
                            int formationBalance=0;
                            if (borAccInfUnitBo.getPcamMonthInformation()!=null&&borAccInfUnitBo.getPcamMonthInformation().getBalance()!=null){
                                formationBalance = Integer.parseInt(borAccInfUnitBo.getPcamMonthInformation().getBalance());
                            }
                            //最新表现字段余额
                            int firinfsegBalance=0;
                            if (borAccInfUnitBo.getPcanFirinfseg()!=null&&borAccInfUnitBo.getPcanFirinfseg().getBalance()!=null){
                                firinfsegBalance  = Integer.parseInt(borAccInfUnitBo.getPcanFirinfseg().getBalance());
                            }
                            if (overdueAmount != null && StringUtils.isNotEmpty(overduePeriod)) {
                                if ((formationBalance-firinfsegBalance)>0&&300 < overdueAmount.doubleValue()) {
                                    if (cds < Integer.parseInt(overduePeriod)) {
                                        cds = Integer.parseInt(overduePeriod);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //163. 最近2年准贷记卡最长逾期月数
            zxPZbInfo.setLate2yZdjkMaxOdNum(String.valueOf(late2yZdjkMaxOdNum));
            //66. 当前最大逾期期数
            zxPZbInfo.setCds(String.valueOf(cds));
        } catch (Exception e) {
            throw new Exception(e);
        }

        return zxPZbInfo;
    }
}

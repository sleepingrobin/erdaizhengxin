package com.dhcc.cips.service.impl.company;

import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.CeqReportBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.debaccountinform.EdiiInterestinfo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.debaccountinform.EsalLeAccInform;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.debaccountinform.EsalLeAccRepayInformSub;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.guaaccount.EdgbGuarbasicinfoseg;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.guaaccount.EdgrGuarresponinfoseg;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.lendtranssuminf.EbboOthloantrasortsumu;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.debaccountinform.entity.LeAccInformUnitBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.guaaccount.entity.GuarAccoInfoBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.dhcc.cips.service.company.CompanyZBThreadService;
import com.dhcc.cips.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 信贷记录明细--已结清信贷
 */
@Service
@Slf4j
public class ZBCalculateUnsettledCreditInfoServiceImpl implements CompanyZBThreadService {

    @Override
    public CompanyZBInfoPo companyCalculate(CeqReportBo ceqReportBo) throws Exception {
        CompanyZBInfoPo companyZBInfoPo = new CompanyZBInfoPo();
        try {

            //报告日期
            Date createTime = ceqReportBo.getRepheadBo().getRepheadUnitBo().getEhhrReportIdentity().getReportTime();
            //借贷账户信息 EDA --借贷账户信息单元 ED01
            List<LeAccInformUnitBo> leAccInformUnitBoList = ceqReportBo.getDebitAccountInformDataBo().getLeAccInformUnitBoList();
            //信贷记录明细：未结清信贷--中长期借款+短期借款+循环透支--借款金额总合
            BigDecimal unSettLoanTotalAmt = new BigDecimal("0.00");
            //信贷记录明细：未结清信贷--中长期借款+短期借款+循环透支--余额总合
            BigDecimal oneBalanceSum = new BigDecimal("0.00");
            //未结清信贷--中长期借款+短期借款+循环透支--借款金额最大值
            BigDecimal loanMaxAmt = new BigDecimal("0.00");
            //未结清信贷--中长期借款+短期借款+循环透支--合计--账户数相加
            int oneleAccNumber = 0;
            BigDecimal credTotalAmt = new BigDecimal("0.00");
            BigDecimal credTotalBalan = new BigDecimal("0.00");
            int credBizNum = 0;//账户总数
            //未结清信贷--五级分类不为“正常和关注”的账户的借款金额、金额总和
            BigDecimal unSettBadClTotalAmt = new BigDecimal("0.00");
            BigDecimal unSettMyrzExposure = new BigDecimal("0.00");//未结清--贸易融资余额加总
            int unSettMyrzCount = 0;//未结清--贸易融资账户总数
            int credtLoanCount = 0;//五级分类为正常，关注的账户数
            int attentionCount = 0;//五级分类为关注的账户数
            BigDecimal interestBalanSum = new BigDecimal("0.00");//到期日是6个月内的余额加总
            BigDecimal unsettBao1Balan = new BigDecimal("0.00");//(13-保理融资)--余额加总
            int interestAccountSum = 0;//到期日6个月内的账户总数
            BigDecimal unsettBaohExposure = new BigDecimal("0.00");//银行保函及其他业务--（余额*（1-保证金比例））加总
            BigDecimal othBankCredAmt = new BigDecimal("0.00");//授信机构不为本机构的各帐余额总合
            Set<String> orgNumberAllSet = new HashSet<>();//未结清 账户的“授信机构”去重总数
            Set<String> unNormalLoanNum = new HashSet<>();//五级分类不为“正常”的账户的“授信机构”去重总数
            BigDecimal guanCompBalan = new BigDecimal("0.00");// 最后一次还款形式为担保代偿 余额总和
            BigDecimal unNormalLoanBalan = new BigDecimal("0.00");//五级分类不为“正常”的账户的金额总和
            int guanCompNum = 0;// 最后一次还款形式为担保代偿 账户数
            //报告日期向后推6个月
            Date futureDate = null;
            if (createTime != null) {
                futureDate = DateUtil.getAimMonthDate(createTime, 6);
            }
            // 借贷账户信息 1.未结清
            List<LeAccInformUnitBo> oneLeAccInformUnitBoList = leAccInformUnitBoList.stream().
                    filter(a -> Constants.ACTIV_STATUS_1.equals(a.getEsalLeAccInform().getAccountState())).collect(Collectors.toList());
            if (oneLeAccInformUnitBoList != null && oneLeAccInformUnitBoList.size() > 0) {
                for (LeAccInformUnitBo leAccInformUnitBo : oneLeAccInformUnitBoList) {
                    EsalLeAccInform esalLeAccInform = leAccInformUnitBo.getEsalLeAccInform();
                    if (null != esalLeAccInform) {
                        String leAccountType = esalLeAccInform.getLeAccountType(); //账户类型
                        String categoryMax = esalLeAccInform.getCategoryMax();   //借贷业务种类大类
                        String bmOrgNumber = esalLeAccInform.getBmOrgNumber();  //业务管理机构类型
                        boolean flag1 = false;
                        boolean flag2 = false;
                        boolean flag3 = false;
                        boolean flag4 = false;
                        boolean flagRepay = false;
                        List<EsalLeAccRepayInformSub> esalLeAccRepayInformSubList = leAccInformUnitBo.getEsalLeAccRepayInform().getEsalLeAccRepayInformSubList();
                        if (esalLeAccRepayInformSubList != null && esalLeAccRepayInformSubList.size() > 0) {
                            //根据日期逆序
                            List<EsalLeAccRepayInformSub> list = esalLeAccRepayInformSubList.stream().sorted(Comparator.comparing(EsalLeAccRepayInformSub::getReportInformDate).reversed()).collect(Collectors.toList());
                            EsalLeAccRepayInformSub esalLeAccRepayInformSub = list.get(0);
                            String fileClass = esalLeAccRepayInformSub.getFileClass();//五级分类
                            String lastActRepayType = esalLeAccRepayInformSub.getLastActRepayType();//最后一次还款形式
                            if (fileClass != null) {
                                //五级分类不为 1.正常 2.关注
                                flag1 = !Constants.FIF_TYPE_1.equals(fileClass) && !Constants.FIF_TYPE_2.equals(fileClass);
                                if (StringUtils.isNotBlank(leAccountType) && Constants.ACC_TYPE_D1.equals(leAccountType) || Constants.ACC_TYPE_R1.equals(leAccountType) || Constants.ACC_TYPE_R4.equals(leAccountType)) {
                                    //五级分类为 1.正常 2.关注
                                    flag2 = Constants.FIF_TYPE_1.equals(fileClass) || Constants.FIF_TYPE_2.equals(fileClass);
                                    //五级分类为  2.关注
                                    flag3 = Constants.FIF_TYPE_2.equals(fileClass);
                                    //五级分类不为 1.正常
                                    flag4 = !Constants.FIF_TYPE_1.equals(fileClass);
                                    // 最后一次还款形式为担保代偿 21-担保代偿
                                    flagRepay = Constants.LOAN_BUS_TYPE_21.equals(lastActRepayType);

                                    BigDecimal balance = esalLeAccRepayInformSub.getBalance();
                                    if (balance != null) {
                                        oneBalanceSum = oneBalanceSum.add(balance);
                                        if (flagRepay) { // 最后一次还款形式为担保代偿 余额总和
                                            if (balance != null) {
                                                guanCompBalan = guanCompBalan.add(balance);
                                            }
                                        }
                                    }
                                }
                            }
                            if (StringUtils.isNotBlank(bmOrgNumber) && !Constants.ORG_TYPE_00.equals(bmOrgNumber)) {
                                BigDecimal balance = esalLeAccRepayInformSub.getBalance();//账户余额
                                if (balance != null) {
                                    othBankCredAmt = othBankCredAmt.add(balance);
                                }
                            }
                        }
                        if (flag1) {
                            BigDecimal loadAmount = esalLeAccInform.getLoanAmount();
                            if (loadAmount != null) {
                                unSettBadClTotalAmt = unSettBadClTotalAmt.add(loadAmount);
                            }
                        }
                        if (flag2) {
                            credtLoanCount += 1;
                        }
                        if (flag3) {
                            attentionCount += 1;
                        }
                        orgNumberAllSet.add(bmOrgNumber);
                        if (flag4) {   //  五级分类不为”正常“
                            unNormalLoanNum.add(bmOrgNumber);
                            BigDecimal loanAmount = esalLeAccInform.getLoanAmount();
                            if (loanAmount != null) {
                                unNormalLoanBalan = unNormalLoanBalan.add(loanAmount);
                            }
                        }
                        if (flagRepay) {
                            guanCompNum += 1;//最后一次还款形式为担保代偿 账户数总和
                        }
//---------------------------------------------------------------------------------------------------------------------------------
                        //账户类型  D1中长期借款 + R1短期借款 + R4循环透支
                        if (leAccountType != null && (Constants.ACC_TYPE_D1.equals(leAccountType)
                                || Constants.ACC_TYPE_R1.equals(leAccountType) || Constants.ACC_TYPE_R4.equals(leAccountType))) {
                            //未结清信贷--中长期借款+短期借款+循环透支--借款金额总合
                            BigDecimal loanAmount = esalLeAccInform.getLoanAmount();
                            if (loanAmount != null) {
                                unSettLoanTotalAmt = unSettLoanTotalAmt.add(loanAmount);
                                //未结清信贷--中长期借款+短期借款+循环透支--借款金额最大值
                                loanMaxAmt = loanAmount.compareTo(loanMaxAmt) > 0 ? loanAmount : loanMaxAmt;
                            }
                            //未结清信贷--中长期借款+短期借款+循环透支--合计--账户数相加
                            oneleAccNumber = oneleAccNumber + 1;
//---------------------------------------------------------------------------------------------------------------------------------
                            //借贷业务种类大类   11贷款、12贸易融资、 13保理融资、21票据贴现
                            if (categoryMax != null && (Constants.LOAN_BUS_TYPE_11.equals(categoryMax) || categoryMax.equals(Constants.LOAN_BUS_TYPE_12)
                                    || Constants.LOAN_BUS_TYPE_13.equals(categoryMax) || categoryMax.equals(Constants.LOAN_BUS_TYPE_21))) {
                                //未结清 借款金额 ED01AJ01
                                BigDecimal loanAmounts = leAccInformUnitBo.getEsalLeAccInform().getLoanAmount();
                                if (loanAmounts != null) {
                                    credTotalAmt = credTotalAmt.add(loanAmounts);
                                }
                                //未结清 余额 ED01BJ01
                                if (esalLeAccRepayInformSubList != null && esalLeAccRepayInformSubList.size() > 0) {
                                    List<EsalLeAccRepayInformSub> sortList = esalLeAccRepayInformSubList.stream().sorted(Comparator.comparing(EsalLeAccRepayInformSub::getReportInformDate).reversed()).collect(Collectors.toList());
                                    EsalLeAccRepayInformSub esalLeAccRepayInformSub = sortList.get(0);
                                    if (esalLeAccRepayInformSub != null) {
                                        BigDecimal balance = esalLeAccRepayInformSub.getBalance();
                                        if (balance != null) {
                                            credTotalBalan = credTotalBalan.add(balance);
                                            // 12 贸易融资
                                            if (Constants.LOAN_BUS_TYPE_12.equals(categoryMax)) {
                                                unSettMyrzExposure = unSettMyrzExposure.add(balance);
                                            }
                                            // 13 保理融资
                                            if (Constants.LOAN_BUS_TYPE_13.equals(categoryMax)) {
                                                unsettBao1Balan = unsettBao1Balan.add(balance);
                                            }
                                        }
                                    }
                                }
                                credBizNum += 1;
                                // 12 贸易融资
                                if (Constants.LOAN_BUS_TYPE_12.equals(categoryMax)) {
                                    unSettMyrzCount += 1;
                                }

                            }
                        }
//--------------------------------------------------------------------------------------------------------------
                        //账户类型  D1中长期借款 + R1短期借款 + R4循环透支+ D2贴现账户
                        if (leAccountType != null && (Constants.ACC_TYPE_D1.equals(leAccountType) || Constants.ACC_TYPE_R1.equals(leAccountType)
                                || Constants.ACC_TYPE_D2.equals(leAccountType) || Constants.ACC_TYPE_R4.equals(leAccountType))) {
                            //到期日期
                            Date dueDate = esalLeAccInform.getDueDate();
                            if (dueDate != null && futureDate != null) {
                                //报告日期<到期日期  报告日期+6>到期日期
                                if (createTime.before(dueDate) && futureDate.after(dueDate)) {
                                    List<EsalLeAccRepayInformSub> esalLeAccRepayInformSubLists = leAccInformUnitBo.getEsalLeAccRepayInform().getEsalLeAccRepayInformSubList();
                                    if (esalLeAccRepayInformSubLists != null && esalLeAccRepayInformSubLists.size() > 0) {
                                        for (EsalLeAccRepayInformSub esalLeAccRepayInformSub : esalLeAccRepayInformSubLists) {
                                            BigDecimal balance = esalLeAccRepayInformSub.getBalance();//余额
                                            if (balance != null) {
                                                interestBalanSum = interestBalanSum.add(balance);
                                            }
                                        }
                                        interestAccountSum += 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // 13. 信贷记录明细-- 1 未结清信贷--中长期借款+短期借款+循环透支--借款金额最大值
            companyZBInfoPo.setLoanMaxAmt(String.valueOf(loanMaxAmt));
            // 80. 信贷记录明细-- 1 未结清信贷--中长期借款+短期借款+循环透支--借款金额总和
            companyZBInfoPo.setUnSettLoanTotalAmt(String.valueOf(unSettLoanTotalAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));

            //76.信贷记录明细--1 未结清信贷--中长期借款+短期借款+循环透支--五级分类为正常，关注的账户数
            companyZBInfoPo.setCredtLoanCount(String.valueOf(credtLoanCount));
            //77.信贷记录明细--1 未结清信贷--中长期借款+短期借款+循环透支--五级分类为关注的账户数
            companyZBInfoPo.setAttentionCount(String.valueOf(attentionCount));

            // 111. 信贷记录明细-- 1 未结清业务-- 业务种类为“保理”(13-保理融资)--余额加总
            companyZBInfoPo.setUnsettBao1Balan(String.valueOf(unsettBao1Balan.setScale(2, BigDecimal.ROUND_HALF_UP)));
//------------------------------------------------------------------------------------------------------------------
            //128 . 信贷记录明细--1未结清信贷--中长期借款+短期借款+循环透支--五级分类不为“正常”的账户的“授信机构”去重总数
            companyZBInfoPo.setUnNormalLoanNum(String.valueOf(unNormalLoanNum.size()));
            //129 . 信贷记录明细--1未结清信贷--中长期借款+短期借款+循环透支--五级分类不为“正常”的账户的金额总和
            companyZBInfoPo.setUnNormalLoanBalan(String.valueOf(unNormalLoanBalan.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //130 . 信贷记录明细--1未结清信贷--中长期借款+短期借款+循环透支--最后一次还款形式为“担保代偿”--账户总数
            companyZBInfoPo.setGuanCompNum(String.valueOf(guanCompNum));
            //131 . 信贷记录明细--1未结清信贷--中长期借款+短期借款+循环透支--最后一次还款形式为“担保代偿”--余额总合
            companyZBInfoPo.setGuanCompBalan(String.valueOf(guanCompBalan.setScale(2, BigDecimal.ROUND_HALF_UP)));
//-------------------------------------------------------------------------------------------------------------------
            BigDecimal unSettCdhpExposure = new BigDecimal("0.00");
            int unSettCdhpCount = 0;
            BigDecimal unSettXyzExposure = new BigDecimal("0.00");
            int unSettXyzCount = 0;

            //担保账户信息 EDB --担保类信息账户信息明细单元 ED04
            List<GuarAccoInfoBo> GuarAccoInfoBoList = ceqReportBo.getGuarAccoInfoDataBo().getGuarAccoInfoBoList();
            //担保账户信息  1.未结清
            List<GuarAccoInfoBo> oneGuarAccoInfoBoList = GuarAccoInfoBoList.stream().
                    filter(a -> Constants.ACTIV_STATUS_1.equals(a.getEdgrGuarresponinfoseg().getAccActivStatus())).collect(Collectors.toList());
            if (oneGuarAccoInfoBoList != null && oneGuarAccoInfoBoList.size() > 0) {
                for (GuarAccoInfoBo guarAccoInfoBo : oneGuarAccoInfoBoList) {
                    //基本信息段 ED04A
                    EdgbGuarbasicinfoseg edgbGuarbasicinfoseg = guarAccoInfoBo.getEdgbGuarbasicinfoseg();
                    if (edgbGuarbasicinfoseg != null) {
                        //担保交易业务种类细分
                        String guarTransBusiSubdi = edgbGuarbasicinfoseg.getGuarTransBusiSubdi();
                        //金额 ED04AJ01
                        BigDecimal amount = edgbGuarbasicinfoseg.getAmount();
                        if (amount != null) {
                            credTotalAmt = credTotalAmt.add(amount);
                        }
                        //余额 ED04BJ01
                        BigDecimal balance = guarAccoInfoBo.getEdgrGuarresponinfoseg().getBalance();
                        // 61银行承兑汇票和 51信用证--余额*（1-保证金比例）合计
                        if (guarTransBusiSubdi != null && (Constants.BS_BUS_TYPE_51.equals(guarTransBusiSubdi) || Constants.BS_BUS_TYPE_61.equals(guarTransBusiSubdi))) {
                            //保证金比例 ED04AQ01
                            BigDecimal maginRatio = edgbGuarbasicinfoseg.getMaginRatio();
                            Integer maginRatioInt = (maginRatio == null) ? 0 : maginRatio.intValue();
                            if (balance != null) {
                                credTotalBalan = credTotalBalan.add(balance.multiply((new BigDecimal(1 - (float) maginRatioInt / 100).setScale(2, BigDecimal.ROUND_HALF_UP))));
                            }
                        } else {
                            if (balance != null) {
                                credTotalBalan = credTotalBalan.add(balance);
                            }
                        }
                        credBizNum += 1;
                        //贸易融资担保 03
                        if (guarTransBusiSubdi != null && Constants.BS_BUS_TYPE_03.equals(guarTransBusiSubdi)) {
                            if (balance != null) {
                                unSettMyrzExposure = unSettMyrzExposure.add(balance);
                                unSettMyrzCount += 1;
                            }
                        }
                        String busManagerCode = edgbGuarbasicinfoseg.getBusManagerCode();//业务管理机构代码
                        orgNumberAllSet.add(busManagerCode);
                    }
                    //在保责任信息段 ED04B
                    EdgrGuarresponinfoseg edgrGuarresponinfoseg = guarAccoInfoBo.getEdgrGuarresponinfoseg();
                    if (edgrGuarresponinfoseg != null) {
                        String fiveLevelClass = edgrGuarresponinfoseg.getFiveLevelClass();
                        String accActivStatus = edgrGuarresponinfoseg.getAccActivStatus();
                        if (fiveLevelClass != null && accActivStatus != null) {
                            //1.未结清 五级分类不为 1.正常 2.关注
                            if (!Constants.FIF_TYPE_1.equals(fiveLevelClass) && !Constants.FIF_TYPE_2.equals(fiveLevelClass)) {
                                if (edgbGuarbasicinfoseg != null) {
                                    BigDecimal amount = edgbGuarbasicinfoseg.getAmount();
                                    if (amount != null) {
                                        unSettBadClTotalAmt = unSettBadClTotalAmt.add(amount);
                                    }
                                }
                            }
                        }
                    }
//-------------------------------------------------------------------------------------------------------------------
                    if (edgrGuarresponinfoseg != null && edgbGuarbasicinfoseg != null) {
                        //担保交易业务种类细分
                        String guarTransBusiSubdi = edgbGuarbasicinfoseg.getGuarTransBusiSubdi();
                        if (guarTransBusiSubdi != null) {
                            if (Constants.BS_BUS_TYPE_61.equals(guarTransBusiSubdi)) {  //61 银行承兑汇票
                                BigDecimal balance = edgrGuarresponinfoseg.getBalance();//余额
                                BigDecimal maginRatio = edgbGuarbasicinfoseg.getMaginRatio();//保证金比例
                                Integer maginRatioInt = (maginRatio == null) ? 0 : maginRatio.intValue();
                                if ((balance != null)) {
                                    unSettCdhpExposure = unSettCdhpExposure.add(balance.multiply(new BigDecimal(1 - (float) maginRatioInt / 100).setScale(2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP)));
                                }
                                unSettCdhpCount += 1;
                            }
                            if (Constants.BS_BUS_TYPE_51.equals(guarTransBusiSubdi)) { //51 信用证
                                BigDecimal creditBalance = edgrGuarresponinfoseg.getBalance();//余额
                                BigDecimal maginRatio = edgbGuarbasicinfoseg.getMaginRatio();//保证金比例
                                Integer maginRatioInt = (maginRatio == null) ? 0 : maginRatio.intValue();
                                if (creditBalance != null) {
                                    unSettXyzExposure = unSettXyzExposure.add(creditBalance.multiply(new BigDecimal(1 - (float) maginRatioInt / 100).setScale(2, BigDecimal.ROUND_HALF_UP).setScale(2, BigDecimal.ROUND_HALF_UP)));
                                }
                                unSettXyzCount += 1;
                            }
                            //71   72  银行保函    //todo 及其他业务
                            if (Constants.BS_BUS_TYPE_71.equals(guarTransBusiSubdi) || Constants.BS_BUS_TYPE_72.equals(guarTransBusiSubdi)) {
                                BigDecimal maginRatio = edgbGuarbasicinfoseg.getMaginRatio(); //保证金比例
                                int maginRatioInt = (maginRatio == null) ? 0 : maginRatio.intValue();
                                BigDecimal balance = edgrGuarresponinfoseg.getBalance(); //余额
                                if (balance != null) {
                                    unsettBaohExposure = unsettBaohExposure.add(balance.multiply(new BigDecimal(1 - (float) maginRatioInt / 100).setScale(2, BigDecimal.ROUND_HALF_UP)));
                                }
                            }
                            Date endDate = edgbGuarbasicinfoseg.getEndDate();
                            if (endDate != null && futureDate != null) {
                                if (createTime.before(endDate) && futureDate.after(endDate)) {
                                    BigDecimal balance = edgrGuarresponinfoseg.getBalance();//余额
                                    BigDecimal maginRatio = edgbGuarbasicinfoseg.getMaginRatio();//保证金比例
                                    int maginRatioInt = (maginRatio == null) ? 0 : maginRatio.intValue();
                                    if (balance != null) {
                                        interestBalanSum = interestBalanSum.add(balance.multiply(new BigDecimal(1 - (float) maginRatioInt / 100).setScale(2, BigDecimal.ROUND_HALF_UP)));
                                    }
                                    interestAccountSum += 1;
                                }
                            }
                        }
//-------------------------------------------------------------------------------------------------------------------
                        if (!Constants.ORG_TYPE_00.equals(edgbGuarbasicinfoseg.getBusManagerCode())) {
                            BigDecimal balance = edgrGuarresponinfoseg.getBalance();//账户余额
                            if (balance != null) {
                                othBankCredAmt = othBankCredAmt.add(balance);
                            }
                        }
                    }
                }
//-------------------------------------------------------------------------------------------------------------------
                //借贷账户信息 EDA -- 欠息信息单元 ED03
                List<EdiiInterestinfo> list = ceqReportBo.getDebitAccountInformDataBo().getEdiiInterestinfoList();
                BigDecimal diTotalAmt = new BigDecimal("0.00");
                int currDebitInterestCount = 0;
                if (null != list && list.size() > 0) {
                    currDebitInterestCount = list.size();
                    for (EdiiInterestinfo ediiInterestinfo : list) {
                        BigDecimal interestBalan = ediiInterestinfo.getInterestBalan();
                        if (null != interestBalan) {
                            diTotalAmt = diTotalAmt.add(interestBalan);
                        }
                    }
                }
                // 87.信贷记录明细--1 未结清信贷-- (欠息--欠息余额加总+到期日是6个月内的（中长期借款，短期借款余额加总+循环透支余额加总+贴现金额加总+
                // （银行承兑汇票和信用证+银行保函及其他业务--（余额*（1-保证金比例））加总）   ）  )
                BigDecimal f6monExpiredBizAmt = interestBalanSum.add(diTotalAmt);
                companyZBInfoPo.setF6monExpiredBizAmt(String.valueOf(f6monExpiredBizAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
                //88. 信贷记录明细--1 未结清信贷--到期日6个月内的账户总数
                companyZBInfoPo.setF6monExpiredBizCount(String.valueOf(interestAccountSum + currDebitInterestCount));
//-------------------------------------------------------------------------------------------------------------------
                //79.信贷记录明细--未结清信贷--授信机构去重加总
                companyZBInfoPo.setUnSettLoanRzbankNum(String.valueOf(orgNumberAllSet.size()));
                //125.信贷记录明细-- 1 未结清信贷--账户的“授信机构”去重总数
                companyZBInfoPo.setUnsettCredBusiOrgNum(String.valueOf(orgNumberAllSet.size()));
                //127.信贷记录明细-- 1 未结清信贷--账户的“授信机构”去重总数
                companyZBInfoPo.setUnSettCreditOrgNum(String.valueOf(orgNumberAllSet.size()));
                //81.信贷记录明细--未结清信贷--银行承兑汇票和信用证的信贷明细--业务种类为“银行承兑汇票”--各账户（余额*（1-保证金比例））加总
                companyZBInfoPo.setUnSettCdhpExposure(String.valueOf(unSettCdhpExposure.setScale(2, BigDecimal.ROUND_HALF_UP)));
                //82.信贷记录明细--未结清信贷--银行承兑汇票和信用证的信贷明细--业务种类为“银行承兑汇票”--账户数
                companyZBInfoPo.setUnSettCdhpCount(String.valueOf(unSettCdhpCount));
                //83.信贷记录明细--未结清信贷--银行承兑汇票和信用证的信贷明细--业务种类为“信用证”--各账户（余额*（1-保证金比例））加总
                companyZBInfoPo.setUnSettXyzExposure(String.valueOf(unSettXyzExposure.setScale(2, BigDecimal.ROUND_HALF_UP)));
                //84.信贷记录明细--未结清信贷--银行承兑汇票和信用证的信贷明细--业务种类为“信用证”--账户数
                companyZBInfoPo.setUnSettXyzCount(String.valueOf(unSettXyzCount));
                //110. 信贷记录明细-- 未结清业务--3  银行保函及 9其他业务--（余额*（1-保证金比例））加总
                companyZBInfoPo.setUnsettBaohExposure(String.valueOf(unsettBaohExposure.setScale(2, BigDecimal.ROUND_HALF_UP)));
                //134 .信贷记录明细--1未结清信贷--授信机构不为本机构的各帐余额总合
                companyZBInfoPo.setOthBankCredAmt(String.valueOf(othBankCredAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
//-------------------------------------------------------------------------------------------------------------------
                //85.信贷记录明细--未结清贸易融资余额加总 {1 未结清信贷--D1中长期借款+R1短期借款+R4循环透支--业务种类为“12 贸易融资”--所有账户余额加总}
                companyZBInfoPo.setUnSettMyrzExposure(String.valueOf(unSettMyrzExposure));
                //86.信贷记录明细--未结清贸易融资账户总数 {1 未结清信贷--D1中长期借款+R1短期借款+R4循环透支--业务种类为“12 贸易融资”--账户总数}
                companyZBInfoPo.setUnSettMyrzCount(String.valueOf(unSettMyrzCount));

                //28.信贷记录明细--未结清信贷--欠息--欠息余额加总
                //companyZBInfoPo.setDiTotalAmt(String.valueOf(diTotalAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
                //96.信贷记录明细--未结清信贷--欠息--欠息总笔数
//                companyZBInfoPo.setCurrDebitInterestCount(String.valueOf(currDebitInterestCount));
                //29.di_total_amt + un_sett_bad_cl_total_amt 拖欠总金额=欠息总额+未结清不良信贷信用总余额
                BigDecimal number = unSettBadClTotalAmt.add(diTotalAmt);
                companyZBInfoPo.setOdTotalAmt(String.valueOf(number.setScale(2, BigDecimal.ROUND_HALF_UP)));
                //15. 未结清信贷--五级分类不为"正常和关注"的账户的借款金额， 金额 总和
                companyZBInfoPo.setUnSettBadClTotalAmt(String.valueOf(unSettBadClTotalAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
                //8.未结清信贷--中长期借款 + 短期借款 + 循环透支--业务种类为“贷款、保理融资、票据贴现、贸易融资”
                // --借款金额总和 + 未结清信贷--银行承兑汇票和信用证--金额合计 + 银行保函及其他业务--金额总和
                companyZBInfoPo.setCredTotalAmt(String.valueOf(credTotalAmt));
                //9.未结清信贷--中长期借款 + 短期借款 + 循环透支--业务种类为“贷款、保理融资、票据贴现、贸易融资”
                // --余额总和 + 未结清信贷--银行承兑汇票和信用证--余额*（1-保证金比例）合计 + 银行保函及其他业务--余额总和
                companyZBInfoPo.setCredTotalBalan(String.valueOf(credTotalBalan));
                // 11. 信贷记录明细：（未结清信贷--中长期借款+短期借款+循环透支--借款金额总合）/（cred_total_amt）
                BigDecimal lUnSettClTotalAmtRate = new BigDecimal("0.00");
                if (!unSettLoanTotalAmt.equals("0.00")) {
                    lUnSettClTotalAmtRate = unSettLoanTotalAmt.divide(credTotalAmt, 10, BigDecimal.ROUND_HALF_UP);
                }
                companyZBInfoPo.setLUnSettClTotalAmtRate(String.valueOf(lUnSettClTotalAmtRate.setScale(3, BigDecimal.ROUND_HALF_UP)));
                // 12. 信贷记录明细：（未结清信贷--中长期借款+短期借款+循环透支--余额总合）/（cred_total_balan）
                BigDecimal balaRate = new BigDecimal("0.00");
                if (!credTotalBalan.equals("0.00")) {
                    balaRate = oneBalanceSum.divide(credTotalBalan, 10, BigDecimal.ROUND_HALF_UP);
                }
                companyZBInfoPo.setLUnSettClTotalBalaRate(String.valueOf(balaRate.setScale(3, BigDecimal.ROUND_HALF_UP)));

                // 10.未结清信贷--中长期借款 + 短期借款 + 循环透支--业务种类为“11贷款、13保理融资、21票据贴现、12贸易融资”
                // --账户总数 + 未结清信贷--银行承兑汇票和信用证--账户总数 + 银行保函及其他业务--账户总数
                companyZBInfoPo.setCredBizNum(String.valueOf(credBizNum));

                // 14. 未结清信贷--中长期借款+短期借款+循环透支--借款金额总合/信息概要--未结清信贷及授信信息概要--1中长期借款+2短期借款+3循环透支--合计--账户数相加
                BigDecimal loanAverAmt = new BigDecimal("0.00");
                //其他借贷交易分类汇总信息
                List<EbboOthloantrasortsumu> ebboOthloantrasortsumuList = ceqReportBo.getLoanTransaDataBo().getLoanTransaBo().getEbbuUnloantransa().getEbboOthloantrasortsumuList();
                if (null != ebboOthloantrasortsumuList && ebboOthloantrasortsumuList.size() > 0) {
                    // 0-合计
                    List<EbboOthloantrasortsumu> lists = ebboOthloantrasortsumuList.stream().filter(e -> Constants.QUAL_SORT_0.equals(e.getAssQualSort()) && (Constants.BUS_TYPE_1.equals(e.getBusType())
                            || Constants.BUS_TYPE_2.equals(e.getBusType()) || Constants.BUS_TYPE_3.equals(e.getBusType()))).collect(Collectors.toList());
                    if (null != lists && lists.size() > 0) {
                        int tempAccountSum = 0;//信息概要--未结清信贷及授信信息概要--1中长期借款+2短期借款+3循环透支----账户数相合计加
                        for (EbboOthloantrasortsumu ebboOthloantrasortsumu : lists) {
                            BigDecimal accountNum = ebboOthloantrasortsumu.getAccountNum();
                            int accountNumInt = (accountNum == null) ? 0 : accountNum.intValue();
                            tempAccountSum += accountNumInt;
                        }
                        if (tempAccountSum > 0) {
                            loanAverAmt = unSettLoanTotalAmt.divide(new BigDecimal(tempAccountSum), 10, BigDecimal.ROUND_HALF_UP);
                        }
                    }
                }
                companyZBInfoPo.setLoanAverAmt(String.valueOf(loanAverAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            }
        } catch (Exception e) {
            log.error("CompanyZBUnClearedCreditDetailServiceImpl", e);
            throw new Exception(e);
        }
        return companyZBInfoPo;
    }
}

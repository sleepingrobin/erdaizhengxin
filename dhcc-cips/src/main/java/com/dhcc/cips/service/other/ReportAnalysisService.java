package com.dhcc.cips.service.other;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Service
@FeignClient(value = "person-query")
public interface ReportAnalysisService {

    /**
     * 报告解析接口
     * @param sourceReport 报告字符串
     * @return 报告BO
     */
    @PostMapping(value = "/alert/reportAnalysis")
    String reportAnalysis(@RequestBody String sourceReport);

    /**
     * 报告转换接口
     * @param sourceReport 原报告
     * @param sourceType 源格式
     * @param targetType 目标格式
     * @return 报告字符串
     */
    @PostMapping("/alert/reportConvert")
    String reportConvert(@RequestParam("sourceReport") String sourceReport, @RequestParam("sourceType") String sourceType, @RequestParam("targetType") String targetType);
}

package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import com.dhcc.cips.bo.person.PersonAlarmZBBo;
import com.dhcc.cips.bo.person.PersonLoanInfoBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.other.AlarmInfoService;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.FileUtil;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

/**
 * @author StivenYang
 * @description 个人指标计算处理类
 * @date 2019年6月11日14:36:13
 */
@Service
public class PersonZBCalculateProcessor {
    @Autowired
    private AlarmInfoService alarmInfoService;
    @Autowired
    private PersonOtherZBCalcServiceImpl personOtherZBCalcService;


    private static ExecutorService executorService = new ThreadPoolExecutor(5, 7, 5, TimeUnit.SECONDS, new LinkedBlockingDeque<>(200));


    /**
     * 计算指标信息
     */
    public PersonZBInfoPo caculate(CpqReportBo cpqReportBo) throws Exception {

        try {

            if (cpqReportBo == null) {
                throw new Exception("个人征信报告对象不能为空");
            }

            //个人指标对象
            PersonZBInfoPo personZBInfoPo = new PersonZBInfoPo();

            //工厂类获取指标计算服务线程list
            List<PersonZBCalculteThreadService> personZBCalculteThreadServiceList = PersonZBCalculateFactory.getPersonZBCalculteThreadServiceList();
            ArrayList<Future<PersonZBInfoPo>> submitList = Lists.newArrayList();
            Future<PersonZBInfoPo> submit;
            PersonZBCalculateTask personZBCalculateTask;
            //多线程处理指标计算
            for (int i = 0; i < personZBCalculteThreadServiceList.size(); i++) {
                personZBCalculateTask = new PersonZBCalculateTask();
                personZBCalculateTask.setPersonZBCalculteThreadService(personZBCalculteThreadServiceList.get(i));
                personZBCalculateTask.setCpqReportBo(cpqReportBo);
                submit = executorService.submit(personZBCalculateTask);
                submitList.add(submit);
            }


            for (Future<PersonZBInfoPo> future : submitList) {
                if (future != null) {
                    //临时指标对象
                    PersonZBInfoPo personZBInfoPoTmp = future.get();
                    //汇总指标信息
                    if (personZBInfoPoTmp != null) {
                        FileUtil.copyProperties(personZBInfoPoTmp, personZBInfoPo, null);
                    }
                }
            }

            personZBCalculateTask = null;
            submitList = null;


            //计算其他有依赖的指标
            personOtherZBCalcService.calculate(cpqReportBo, personZBInfoPo);

            //计算预警信息
            PersonAlarmZBBo personAlarmZBBo = new PersonAlarmZBBo();
            if (StringUtils.isNotEmpty(personZBInfoPo.getRelaBankMonNum())) {
                personAlarmZBBo.setRelaBankMonNum(Integer.parseInt(personZBInfoPo.getRelaBankMonNum()));//与银行建立信贷关系月数
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getCurrCredBankNum())) {
                personAlarmZBBo.setCurrCredBankNum(Integer.parseInt(personZBInfoPo.getCurrCredBankNum()));//现有信用卡授信银行数
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getCurrLoanBankNum())) {
                personAlarmZBBo.setCurrLoanBankNum(Integer.parseInt(personZBInfoPo.getCurrLoanBankNum()));//现有贷款授信银行数
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getCredBizBankNum())) {
                personAlarmZBBo.setCredBizBankNum(Integer.parseInt(personZBInfoPo.getCredBizBankNum()));//有信用业务的银行数
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getLateMonEnquCount())) {
                personAlarmZBBo.setLateMonEnquCount(Integer.parseInt(personZBInfoPo.getLateMonEnquCount()));//最近一个月内的查询次数
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getLateMonYearEnquRate())) {
                personAlarmZBBo.setLateMonYearEnquRate(Double.parseDouble(personZBInfoPo.getLateMonYearEnquRate()));//最近1个月内的查询次数/最近1年内的查询次数（比率）
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getClProdEnquRecoRate())) {
                personAlarmZBBo.setClProdEnquRecoRate(Double.valueOf(personZBInfoPo.getClProdEnquRecoRate()));//信用产品数和查询记录数比率
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getL2YOd2TimeCredNum())) {
                personAlarmZBBo.setL2YOd2TimeCredNum(Integer.parseInt(personZBInfoPo.getL2YOd2TimeCredNum()));//过去两年内逾期三期以上（包括三期）信用卡张数
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getLate12MonMinRpNum())) {
                personAlarmZBBo.setLate12MonMinRpNum(Integer.parseInt(personZBInfoPo.getLate12MonMinRpNum()));//贷记卡12月内未还最低还款额次数
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getCCredCardUseFrequ())) {
                personAlarmZBBo.setCCredCardUseFrequ(Double.valueOf(personZBInfoPo.getCCredCardUseFrequ()));//当前信用卡额度使用率
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getCredCardUseFrequ())) {
                personAlarmZBBo.setCredCardUseFrequ(Double.valueOf(personZBInfoPo.getCredCardUseFrequ()));//信用卡使用率
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getLate2yJyxLoanOdNum())) {
                personAlarmZBBo.setL2YJyxLoanOd2TimeNum(Integer.parseInt(personZBInfoPo.getLate2yJyxLoanOdNum()));//过去两年内经营性贷款逾期两期以上（包括两期）贷款笔数
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getF2MUnSettJyxLoanNum())) {
                personAlarmZBBo.setF2MUnSettJyxLoanNum(Integer.parseInt(personZBInfoPo.getF2MUnSettJyxLoanNum()));//将来2个月未结清经营性贷款到期笔数
            }
            if (StringUtils.isNotEmpty(personZBInfoPo.getCOdAcctNum())) {
                personAlarmZBBo.setCOdAcctNum(Integer.parseInt(personZBInfoPo.getCOdAcctNum()));//当前逾期账户数
            }

            String alarmTxt = "";//预警信息
            Set<String> alarmSet = alarmInfoService.getAlarmInfos(personAlarmZBBo, Constants.USER_TYPE_PERSON);
            if (alarmSet != null && alarmSet.size() > 0) {
                int i = 1;
                for (String alarmName : alarmSet) {
                    alarmTxt = alarmTxt + i + ":" + alarmName;
                    i++;
                }
            }
            //预警明细信息
            personZBInfoPo.setEarlyWarningInfoContext(alarmTxt);
            personZBInfoPo.setEarlyWarningInfoNum(String.valueOf(String.valueOf(alarmSet == null ? 0 : alarmSet.size())));

            //设置贷款信息数组，等相关信息
            //personOtherZBCalcService.setOtherInfo(cpqReportBo, personZBInfoPo);

            //其他信息放到loaninfo字段
            return personZBInfoPo;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}

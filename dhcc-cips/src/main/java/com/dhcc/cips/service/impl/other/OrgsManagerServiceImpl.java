package com.dhcc.cips.service.impl.other;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.dhcc.cips.bo.other.*;
import com.dhcc.cips.mapper.other.OrgMapper;
import com.dhcc.cips.po.other.SystemOrgPo;
import com.dhcc.cips.service.other.EsbService;
import com.dhcc.cips.service.other.EsbWebService;
import com.dhcc.cips.service.other.OrgClientService;
import com.dhcc.cips.service.other.OrgsManagerService;
import com.dhcc.cips.util.XmlUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import javax.jws.WebService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author YangJiaheng
 * @date 2019年5月15日14:35:48
 * @description 组织结构管理对接soap服务类
 */
@Slf4j
@Service
@WebService
public class OrgsManagerServiceImpl implements OrgsManagerService {

    @Autowired
    private OrgMapper orgMapper;
    @Autowired
    private OrgClientService orgClientService;

    private static ResultsResBo resultsResBo = new ResultsResBo();
    /**
     * 存放法人机构关系map
     */
    private static HashMap<String, String> relateMap = Maps.newHashMap();

    @Override
    public String createOrgsService(String xml) {
        //业务系统返回列表
        ArrayList<OrgResBo> orgResBoList = Lists.newArrayList();

        try {
            if (StringUtils.isEmpty(xml)) {
                log.error("参数接收异常 request param ={}", xml);
                throw new Exception("参数异常");
            }
            OrgsReqBo orgsReqBo = XmlUtil.convertXml2JavaBean(OrgsReqBo.class, xml, new Class[]{OrgsReqBo.class, OrgReqBo.class});
            if (orgsReqBo != null) {
                //原组织机构列表
                List<OrgReqBo> orgReqBoList = orgsReqBo.getOrgReqBoList();
                if (orgReqBoList != null) {
                    //所有组织机构不进行同步
                    for (OrgReqBo orgReqBo : orgReqBoList) {
                        //设置返回值到返回值列表
                        OrgResBo orgResBo = new OrgResBo();
                        orgResBo.setOrgCode(orgReqBo.getOrgCode());
                        orgResBo.setDimCode(orgReqBo.getDimCode());
                        orgResBo.setCode("210");
                        orgResBo.setMsg("组织机构暂不同步");
                        orgResBoList.add(orgResBo);
                    }
                    resultsResBo.setOrgResultInfoList(orgResBoList);
                }
            }
            return XmlUtil.converJavaBean2Xml(resultsResBo, resultsResBo.getClass());
        } catch (Exception e) {
            {
                e.printStackTrace();
                OrgResBo orgResBo = new OrgResBo();
                orgResBo.setOrgCode("");
                orgResBo.setDimCode("WD1");
                orgResBo.setCode("600");
                orgResBo.setMsg("组织机构新增异常");
                resultsResBo.setOrgResultInfoList(orgResBoList);
                return XmlUtil.converJavaBean2Xml(resultsResBo, resultsResBo.getClass());
            }
        }
    }

    /**
     * 递归插入除法人机构外的其余组织机构
     *
     * @param orgResBoList    最终返回对象列表
     * @param orgOtherReqList 要入库的组织机构列表
     * @param isGet           是否可以取上级机构id作为法人机构id  isGet为false
     * @param orgReqList      原组织机构列表，传递该参数用来减少该列表的大小，减少循环次数
     * @return 新增的组织机构的org_id列表
     */
    private List<String> legalChildPerson(ArrayList<OrgResBo> orgResBoList, List<OrgReqBo> orgOtherReqList, boolean isGet, List<OrgReqBo> orgReqList) {
        if (orgOtherReqList.size() > 0) {
            List<SystemOrgPo> childOrgList = new ArrayList<>();
            List<String> newChildOrgList = new ArrayList<>();
            for (OrgReqBo org : orgOtherReqList) {
                if (org != null) {
                    // 组织机构对象
                    SystemOrgPo systemOrg = new SystemOrgPo();
                    String orgCode = org.getOrgCode();
                    String parentCode = org.getParentCode();
                    newChildOrgList.add(orgCode);
                    systemOrg.setOrgId(orgCode);
                    systemOrg.setOrgName(org.getOrgName());
                    systemOrg.setRecordStopFlag(org.getStatus());
                    systemOrg.setPostCode(org.getPostalCode());
                    systemOrg.setUporg(parentCode);
                    systemOrg.setCorporateFlag("0");
                    //如果上级不是法人机构，递归获取法人机构id
                    systemOrg.setCorporateId(getCorporateIdMethod(orgCode));
                    relateMap.put(orgCode, parentCode);
                    childOrgList.add(systemOrg);
                    orgReqList.remove(org);
                }
            }
            // 发送批量创建请求到前置系统
            String resp = orgClientService.createList(JSON.toJSONString(childOrgList));
            log.info("resp:{}", resp);
            setResultBo(resp, orgResBoList);
            return newChildOrgList;
        }
        return legalChildPerson(orgResBoList, orgOtherReqList, isGet, orgReqList);
    }

    /**
     * 递归获取法人机构id
     *
     * @param orgCode 获取该orgCode的法人机构id
     * @return orgCode对应机构的法人机构id
     */
    private String getCorporateIdMethod(String orgCode) {
        String corporateId = relateMap.get(orgCode);
        if (StringUtils.isBlank(corporateId)) {
            return orgCode;
        }
        return getCorporateIdMethod(corporateId);
    }

    /**
     * 返回值String转换为对象
     *
     * @param legalResp    前置系统返回字符串
     * @param orgResBoList 转换后的对象列表
     */
    private void setResultBo(String legalResp, ArrayList<OrgResBo> orgResBoList) {
        List<OrgAcceptParamBo> list = JSONArray.parseArray(legalResp, OrgAcceptParamBo.class);
        for (OrgAcceptParamBo orgAcceptParamBo : list) {
            OrgResBo orgResBo = new OrgResBo();
            orgResBo.setDimCode("WD1");
            //状态
            if ("success".equals(orgAcceptParamBo.getCode())) {
                orgResBo.setCode("201");
                orgResBo.setMsg("组织机构创建成功");
            } else {
                orgResBo.setCode("209");
                orgResBo.setMsg("组织机构已存在");
            }
            //机构id
            orgResBo.setOrgCode(orgAcceptParamBo.getOrg_id());
            orgResBoList.add(orgResBo);
        }
        resultsResBo.setOrgResultInfoList(orgResBoList);
    }

    @Override
    public String revokeOrgsService(String xml) {
        ResultsResBo resultsResBo = new ResultsResBo();
        ArrayList<OrgResBo> orgResBoList = Lists.newArrayList();
        if (StringUtils.isNotEmpty(xml)) {
            // 解析xml参数，解析为javabean
            OrgsReqBo orgsReqBo = XmlUtil.convertXml2JavaBean(OrgsReqBo.class, xml, new Class[]{OrgsReqBo.class, OrgReqBo.class});
            if (orgsReqBo != null) {
                //删除成功
                List<OrgReqBo> orgReqBoList = orgsReqBo.getOrgReqBoList();
                orgMapper.deleteOrgsByIds(orgReqBoList);
                for (OrgReqBo org : orgReqBoList) {
                    String orgCode = org.getOrgCode();
                    if (orgCode == null) {
                        // 组织结构代码为null
                        OrgResBo orgResBo = new OrgResBo();
                        orgResBo.setOrgCode(org.getOrgCode());
                        orgResBo.setDimCode(org.getDimCode());
                        orgResBo.setCode("600");
                        orgResBo.setMsg("组织机构代码不能为空");
                        orgResBoList.add(orgResBo);
                    } else {
                        // 否则删除对应的组织机构代码
                        orgMapper.deleteOrgByOrgCode(orgCode);
                        OrgResBo orgResBo = new OrgResBo();
                        orgResBo.setOrgCode(org.getOrgCode());
                        orgResBo.setDimCode(org.getDimCode());
                        orgResBo.setCode("203");
                        orgResBo.setMsg("组织机构删除成功");
                        orgResBoList.add(orgResBo);
                    }
                }
            } else {
                //不存在机构
                OrgResBo orgResBo = new OrgResBo();
                orgResBo.setOrgCode(null);
                orgResBo.setDimCode(null);
                orgResBo.setCode("600");
                orgResBo.setMsg("orgs参数格式不正确");
                orgResBoList.add(orgResBo);
            }
        } else {
            // 参数不能为空
            OrgResBo orgResBo = new OrgResBo();
            orgResBo.setOrgCode(null);
            orgResBo.setDimCode(null);
            orgResBo.setCode("600");
            orgResBo.setMsg("参数不能为空");
            orgResBoList.add(orgResBo);
        }

        resultsResBo.setOrgResultInfoList(orgResBoList);

        return XmlUtil.converJavaBean2Xml(resultsResBo, resultsResBo.getClass());
    }

    @Override
    public String deleteOrgsService(String xml) {
        return revokeOrgsService(xml);
    }

    @Override
    public String updateOrgsService(String xml) {
        ResultsResBo resultsResBo = new ResultsResBo();
        ArrayList<OrgResBo> orgResBoList = Lists.newArrayList();
        if (StringUtils.isNotEmpty(xml)) {
            // 解析xml参数，解析为javabean
            OrgsReqBo orgsReqBo = XmlUtil.convertXml2JavaBean(OrgsReqBo.class, xml, new Class[]{OrgsReqBo.class, OrgReqBo.class});
            if (orgsReqBo != null && orgsReqBo.getOrgReqBoList() != null) {
                //修改涉及到其他验证，需要调用远程接口
                List<OrgReqBo> orgReqBoList = orgsReqBo.getOrgReqBoList();
                for (OrgReqBo org : orgReqBoList) {
                    //调用远程update接口
                    LinkedMultiValueMap<String, String> formdata = new LinkedMultiValueMap<>();
                    formdata.add("orgId", org.getOrgCode());
                    formdata.add("orgName", org.getOrgName());
                    formdata.add("uporg", org.getParentCode());
                    formdata.add("recordStopFlag", org.getStatus());
                    formdata.add("postCode", org.getPostalCode());

                    String result = WebClient.create()
                            .post()
                            .uri("http://10.4.146.65:7000/platform/user/systemOrg/update")
                            .contentType(MediaType.APPLICATION_JSON_UTF8)
                            .body(BodyInserters.fromFormData(formdata))
                            .retrieve()
                            .bodyToMono(String.class)
                            .block();
                    log.info("result:{}", result);
                    //转换json为map，方便取值
                    ObjectMapper objectMapper = new ObjectMapper();
                    try {
                        Map map = objectMapper.readValue(result, Map.class);
                        String code = (String) map.get("code");
                        if ("00000000".equals(code)) {
                            OrgResBo orgResBo = new OrgResBo();
                            orgResBo.setOrgCode(org.getOrgCode());
                            orgResBo.setDimCode(org.getDimCode());
                            orgResBo.setCode("201");
                            orgResBo.setMsg("组织机构修改成功");
                            orgResBoList.add(orgResBo);
                        } else {
                            OrgResBo orgResBo = new OrgResBo();
                            orgResBo.setOrgCode(org.getOrgCode());
                            orgResBo.setDimCode(org.getDimCode());
                            orgResBo.setCode("600");
                            orgResBo.setMsg((String) map.get("msg"));
                            orgResBoList.add(orgResBo);
                        }
                    } catch (IOException e) {
                        OrgResBo orgResBo = new OrgResBo();
                        orgResBo.setOrgCode(org.getOrgCode());
                        orgResBo.setDimCode(org.getDimCode());
                        orgResBo.setCode("600");
                        orgResBo.setMsg("前置系统返回参数异常");
                        orgResBoList.add(orgResBo);
                    }
                }
            } else {
                OrgResBo orgResBo = new OrgResBo();
                orgResBo.setOrgCode(orgResBo.getOrgCode());
                orgResBo.setDimCode(orgResBo.getDimCode());
                orgResBo.setCode("600");
                orgResBo.setMsg("修改接口参数转换失败");
                orgResBoList.add(orgResBo);
            }
        } else {
            // 参数不能为空
            OrgResBo orgResBo = new OrgResBo();
            orgResBo.setOrgCode("");
            orgResBo.setDimCode("");
            orgResBo.setCode("600");
            orgResBo.setMsg("修改接口参数不能为空");
            orgResBoList.add(orgResBo);
        }

        resultsResBo.setOrgResultInfoList(orgResBoList);

        String retXml = XmlUtil.converJavaBean2Xml(resultsResBo, resultsResBo.getClass());
        return retXml;
    }
}
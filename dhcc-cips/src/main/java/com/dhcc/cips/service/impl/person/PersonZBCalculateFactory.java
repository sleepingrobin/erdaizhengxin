package com.dhcc.cips.service.impl.person;

import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author StivenYang
 * @description 计算方法注册类
 * @date 2019年6月11日14:31:56
 */
@Component
@Data
public class PersonZBCalculateFactory {

    private static List<PersonZBCalculteThreadService> personZBCalculteThreadServiceList = new ArrayList<>();

    /**
     * 注册指标计算服务
     */
    @PostConstruct
    public void init() {
        //个人基础指标计算
        //基础指标计算（贷款明细信息部分）
        personZBCalculteThreadServiceList.add(new BasicZBCalculateCreditInfoDkServiceImpl());
        //基础指标计算（信用卡明细信息部分）
        personZBCalculteThreadServiceList.add(new BasicZBCalculateCreditInfoXykServiceImpl());
        //基础指标计算（信息概要-信贷交易信息提示部分）
        personZBCalculteThreadServiceList.add(new BasicZBCalculateSummaryInfoPartOneServiceImpl());
        //基础指标计算（信息概要-信贷交易违约信息概要部分）
        personZBCalculteThreadServiceList.add(new BasicZBCalculateSummaryInfoPartTwoServiceImpl());
        //基础指标计算（信息概要-信贷交易授信及负债信息概要部分）
        personZBCalculteThreadServiceList.add(new BasicZBCalculateSummaryInfoPartThreeServiceImpl());

        //业务系统需要的个人指标计算
        //信息概要（信贷交易信息提示）
        personZBCalculteThreadServiceList.add(new ZBCalculateSummaryInfoPartOneServiceImpl());
        //信息概要（信贷交易违约信息概要）
        personZBCalculteThreadServiceList.add(new ZBCalculateSummaryInfoPartTwoServiceImpl());
        //信息概要（信贷交易授信及负债信息概要）
        personZBCalculteThreadServiceList.add(new ZBCalculateSummaryInfoPartThreeServiceImpl());
        //信息概要（非信贷交易信息概要）
        personZBCalculteThreadServiceList.add(new ZBCalculateSummaryInfoPartFourServiceImpl());
        //信息概要（公共信息概要）
        personZBCalculteThreadServiceList.add(new ZBCalculateSummaryInfoPartFiveServiceImpl());
        //信息概要（查询记录概要）
        personZBCalculteThreadServiceList.add(new ZBCalculateSummaryInfoPartSixServiceImpl());
        //信贷交易信息明细（贷款）
        personZBCalculteThreadServiceList.add(new ZBCalculateCreditInfoDkServiceImpl());
        //信贷交易信息明细（贷记卡）
        personZBCalculteThreadServiceList.add(new ZBCalculateCreditInfoDjkServiceImpl());
        //信贷交易信息明细（准贷记卡）
        personZBCalculteThreadServiceList.add(new ZBCalculateCreditInfoZdjkServiceImpl());
        //信贷交易信息明细（信用卡）
        personZBCalculteThreadServiceList.add(new ZBCalculateCreditInfoXykServiceImpl());
        //信贷交易信息明细（贷款+信用卡）
        personZBCalculteThreadServiceList.add(new ZBCalculateCreditInfoDkAndXykServiceImpl());
        //信贷交易信息明细（相关还款责任）
        personZBCalculteThreadServiceList.add(new ZBCalculateCreditInfoXghkzrServiceImpl());
        //查询记录
        personZBCalculteThreadServiceList.add(new ZBCalculateQueryRecordServiceImpl());
    }

    static List<PersonZBCalculteThreadService> getPersonZBCalculteThreadServiceList(){
        return personZBCalculteThreadServiceList;
    }
}

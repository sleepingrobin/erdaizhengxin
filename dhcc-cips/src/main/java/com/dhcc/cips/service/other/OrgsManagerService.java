package com.dhcc.cips.service.other;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author StivenYang
 * @date 2019年5月17日09:49:43
 * @description 组织机构webservice接口
 */
@WebService
public interface OrgsManagerService {

    /**
     * 批量或单个创建组织机构
     * @param param 下游系统接口接收的参数为字符串型，并符合以下xml格式
     * <orgs>
     * 	<org>
     * < dimCode> value </ dimCode>
     * < orgCode > value </ orgCode >
     *
     * </ org>
     * <org>
     * < dimCode> value </ dimCode>
     * < orgCode > value </ orgCode >
     *
     * </ org>
     * </orgs>
     * 1.<org>标签主要记录组织机构的属性信息，其中orgCode机构号（由于人力系统机构树增加维度概念，机构主键
     *              由原机构ID变为机构ID+维度），其它属性字段以附件三：组织机构属性定义表中的字段为主。
     *              一个参数中可以包括多个<org>标签，每个标签表示一个组织机构。
     *
     * @return 下游系统接口在完成操作后返回结果，结果为字符串型，其格式定义如下：
     * <results>
     * 	<org>
     * 		<orgCode></ orgCode >
     *         <dimCode></ dimCode>
     * 		<code>附件一中返回值</ code >
     * <msg>错误提示信息</ msg >
     * </org>
     *    		 <org>
     * 			<orgCode></ orgCode >
     * <dimCode></ dimCode>
     * 			<code>附件一中返回值</ code >
     * <msg>错误提示信息</ msg >
     * </org>
     * </results>
     * 1.<org>表示一个组织机构，在返回结果集中可以有多个组织机构，其中orgCode表示机构号，code表示返回结果，
     * 返回结果以附件一:返回结果代码表中的返回值表示, 另外msg使用附件一中的中文，如果附件一中不包含，业务系可
     * 以自行定义错误的中文信息作为排错参考。
     */
    @WebMethod(operationName = "createOrgs")
    String createOrgsService(String param);

    /**
     * 批量或单个删除组织机构
     * @param param
     * @return
     */
    @WebMethod(operationName = "revokeOrgs")
    String revokeOrgsService(String param);

    /**
     * 2.2.2.	修改组织
     * @param param 下游系统接口接收的参数为字符串型，并符合以下xml格式
     * 	报文同机构新增
     * @return 下游系统接口在完成操作后返回结果，结果为字符串型，其格式定义如下：
     * 报文同机构新增
     */
    @WebMethod(operationName = "updateOrgs")
    String updateOrgsService(String param);

    /**
     * 2.2.4.	删除组织（新增接口）
     * @param param 下游系统接口接收的参数为字符串型，并符合以下xml格式
     * 报文同修改
     * @return 下游系统接口在完成操作后返回结果，结果为字符串型，其格式定义如下：
     * 报文同机构新增
     */
    @WebMethod(operationName = "deleteOrgs")
    String deleteOrgsService(String param);
}

package com.dhcc.cips.service.impl.other;

import com.dhcc.cips.bo.base.*;
import com.dhcc.cips.bo.other.CooperationReqBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.CpqResultInfoPo;
import com.dhcc.cips.service.other.CooperationService;
import com.dhcc.cips.service.person.CooperationPersonParseService;
import com.dhcc.cips.util.DateUtil;
import com.dhcc.cips.util.XmlUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * <p>
 * 合作行征信报告解析服务类
 * </p>
 *
 * @author limin
 * @since 2019-05-30
 */
@Slf4j
@Service
public class CooperationServiceImpl implements CooperationService {

    @Autowired
    private CooperationPersonParseService cooperationPersonParseService;

    /**
     * 征信报告解析
     * @param request
     * @return String
     */

    @Override
    public String reportParse(Request request) {
        //返回交易信息
        String retCode;//交易返回码
        String retMesaage;//交易返回信息
        String retStatus;//交易返回状态
        try {

            //获取请求体参数
            RequestAppHead requestAppHead = request.getRequestAppHead();
            RequestSysHead requestSysHead = request.getRequestSysHead();
            RequestBody requestBody = request.getRequestBody();
            String customerName = requestBody.getClientName();//客户名称
            String globalType = requestBody.getGlobalType();//证件类型
            String globalId = requestBody.getGlobalId();//证件号码
            String userId = requestSysHead.getUserId();//员工号
            String orgCode = requestSysHead.getBranchId();//机构号
            String htmlStr = requestBody.getHtmlStr();//征信报告字符串
            String queryDate = requestBody.getQueryDate();//查询时间
            String PreEvalSeqNo = requestBody.getPreEvalSeqNo();//预评估流水号
            String BussSeqNo = requestAppHead.getBussSeqNo();//业务流水号
            String corpCode = requestSysHead.getCorpCode();//法人机构号
            String sourceType = requestSysHead.getSourceType();//渠道类型

            if (StringUtils.isEmpty(customerName)) {
                throw new Exception(Constants.CLIENT_NAME_NULL_ANO);//客户名称不能为空
            }
            if (StringUtils.isEmpty(globalType)) {
                throw new Exception(Constants.GLOBAL_TYPE_NULL_ANO);//证件类型不能为空
            }
            if (StringUtils.isEmpty(globalId)) {
                throw new Exception(Constants.GLOBAL_ID_NULL_ANO);//证件号码不能为空
            }
            if (StringUtils.isEmpty(htmlStr)) {
                throw new Exception(Constants.REPORT_STRING_NULL_ANO);//征信报告不能为空
            }
            if (StringUtils.isEmpty(queryDate)) {
                throw new Exception(Constants.QUERY_DATE_NULL_ANO);//查询日期不能为空
            }
            if (StringUtils.isEmpty(PreEvalSeqNo)) {
                throw new Exception(Constants.PRE_EVAL_SEQ_NO_NULL_ANO);//预评估流水号不能为空
            }

            CooperationReqBo cooperationReqBo = new CooperationReqBo();
            cooperationReqBo.setCustomerName(customerName);//客户名称
            cooperationReqBo.setGlobalType(globalType);//证件类型
            cooperationReqBo.setGlobalId(globalId);//证件号码
            cooperationReqBo.setUserId(userId);//员工号
            cooperationReqBo.setOrgCode(orgCode);//机构号
            cooperationReqBo.setHtmlStr(htmlStr);//征信报告html字符串
            cooperationReqBo.setQueryDate(DateUtil.toDate(queryDate, "yyyyMMdd hh:mm:ss"));//查询时间
            cooperationReqBo.setPreEvalSeqNo(PreEvalSeqNo);//预评估流水号
            cooperationReqBo.setBussSeqNo(BussSeqNo);//业务流水号
            cooperationReqBo.setCorpCode(corpCode);//法人机构号
            cooperationReqBo.setSourceType(sourceType);//渠道类型

            //新启线程处理征信报告解析
            cooperationPersonParseService.parseWithHtml(cooperationReqBo);

            //交易返回码
            retCode = Constants.SUCCESS_CODE;
            //交易返回信息
            retMesaage = Constants.SUCCESS_MASSAGE;
            //交易返回状态
            retStatus = Constants.SUCCESS_S;
        } catch (Exception e) {
            log.error("", e);
            //交易返回码
            retCode = Constants.FAIL_CODE;
            //交易返回信息
            retMesaage = Constants.FAIL_MASSAGE + e.getMessage();
            //交易返回状态
            retStatus = Constants.FAIL_F;
        }

        //填充返回头
        ResponseHead responseHead = new ResponseHead();
        ResponseSysHead responseSysHead = new ResponseSysHead();
        BeanUtils.copyProperties(request.getRequestSysHead(), responseSysHead);
        //返回状态码
        responseSysHead.setRetStatus(retStatus);
        responseHead.setResponseSysHead(responseSysHead);

        ResponseSysHeadRet responseSysHeadRet = new ResponseSysHeadRet();
        //交易返回码
        responseSysHeadRet.setRetCode(retCode);
        //交易返信息
        responseSysHeadRet.setRetMsg(retMesaage);
        responseHead.getResponseSysHead().setResponseSysHeadRet(responseSysHeadRet);

        //转换成xml报文
        String xml = XmlUtil.converJavaBean2Xml(responseHead, responseHead.getClass());

        return xml;
    }
}
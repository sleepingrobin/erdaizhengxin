package com.dhcc.cips.service.impl.company;

import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.CeqReportBo;
import com.dhcc.cips.bo.company.CompanyAlarmZBBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.dhcc.cips.service.company.CompanyZBThreadService;
import com.dhcc.cips.service.other.AlarmInfoService;
import com.dhcc.cips.util.FileUtil;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

/**
 * @author StivenYang
 * @description 企业统一指标计算类
 * @date 2019年6月11日14:36:13
 */
@Slf4j
@Service
public class CompanyZBCalculateProcessor {
    @Autowired
    AlarmInfoService alarmInfoService;

    private static ExecutorService executorService = new ThreadPoolExecutor(5, 7, 3, TimeUnit.SECONDS, new ArrayBlockingQueue<>(10));

    /**
     * 指标计算
     *
     * @param ceqReportBo
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public CompanyZBInfoPo calculate(CeqReportBo ceqReportBo) throws Exception {
        List<CompanyZBThreadService> companyZBThreadServices = CompanyCalculationFactory.getIndexCalculates();
        ArrayList<Future<CompanyZBInfoPo>> submitList = Lists.newArrayList();
        Future<CompanyZBInfoPo> submit;
        CompanyZBCalculateTask companyZBCalculateTask;
        for (int i = 0; i < companyZBThreadServices.size(); i++) {
            companyZBCalculateTask = new CompanyZBCalculateTask();
            companyZBCalculateTask.setCompanyZBThreadService(companyZBThreadServices.get(i));
            companyZBCalculateTask.setCeqReportBo(ceqReportBo);
            submit = executorService.submit(companyZBCalculateTask);
            submitList.add(submit);
        }
        CompanyZBInfoPo companyZBInfoPo = new CompanyZBInfoPo();
        for (Future<CompanyZBInfoPo> resultFuture : submitList) {
            CompanyZBInfoPo zxCZbInfo = resultFuture.get();
            FileUtil.copyProperties(zxCZbInfo, companyZBInfoPo, null);
        }
        companyZBCalculateTask = null;
        submitList = null;
        //计算预警信息
        CompanyAlarmZBBo companyAlarmZBBo = new CompanyAlarmZBBo();
        if (StringUtils.isNotEmpty(companyZBInfoPo.getCredtLoanCount())) {
            companyAlarmZBBo.setCredtLoanCount(Integer.parseInt(companyZBInfoPo.getCredtLoanCount()));//贷款笔数
        }
        if (StringUtils.isNotEmpty(companyZBInfoPo.getAttentionCount())) {
            companyAlarmZBBo.setAttentionCount(Integer.parseInt(companyZBInfoPo.getAttentionCount()));//未结清关注类业务笔数
        }
        if (StringUtils.isNotEmpty(companyZBInfoPo.getCredBizNum())) {
            companyAlarmZBBo.setCredBizNum(Integer.parseInt(companyZBInfoPo.getCredBizNum()));//信用业务笔数（未结清信贷业务）
        }
        if (StringUtils.isNotEmpty(companyZBInfoPo.getUnSettBadClBizNum())) {
            companyAlarmZBBo.setUnSettBadClBizNum(Integer.parseInt(companyZBInfoPo.getUnSettBadClBizNum()));//未结清不良信用业务笔数
        }
        if (StringUtils.isNotEmpty(companyZBInfoPo.getSettBadBizNum())) {
            companyAlarmZBBo.setSettBadBizNum(Integer.parseInt(companyZBInfoPo.getSettBadBizNum()));//已结清不良信用业务笔数
        }
        if (StringUtils.isNotEmpty(companyZBInfoPo.getUnSettBadClTotalBalan())) {
            companyAlarmZBBo.setUnSettBadClTotalBalan(Double.parseDouble(companyZBInfoPo.getUnSettBadClTotalBalan()));//未结清不良信贷信用总余额
        }

        int i = 1;
        String alarmTxt = "";//预警信息
        Set<String> alarmSet = alarmInfoService.getAlarmInfos(companyAlarmZBBo, Constants.USER_TYPE_COMPANY);
        if (alarmSet != null && alarmSet.size() > 0) {

            for (String alarmName : alarmSet) {
                alarmTxt = alarmTxt + i + ":" + alarmName;
                i++;
            }
        }
        //明细信息
        companyZBInfoPo.setEarlyWarningInfoContext(alarmTxt);
        companyZBInfoPo.setEarlyWarningInfoNum(String.valueOf(alarmSet == null ? 0 : alarmSet.size()));

        return companyZBInfoPo;
    }
}

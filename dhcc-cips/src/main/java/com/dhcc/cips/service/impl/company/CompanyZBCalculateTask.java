package com.dhcc.cips.service.impl.company;

import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.CeqReportBo;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.dhcc.cips.service.company.CompanyZBThreadService;
import lombok.Data;

import java.util.concurrent.Callable;

/**
 * @description 企业统一指标计算任务
 */

@Data
public class CompanyZBCalculateTask implements Callable<CompanyZBInfoPo> {

    private volatile CompanyZBInfoPo companyZBInfoPo;
    private CeqReportBo ceqReportBo;
    private CompanyZBThreadService companyZBThreadService;

    /**
     *
     * @return
     * @throws Exception
     */
    @Override
    public CompanyZBInfoPo call() throws Exception {
        companyZBInfoPo = companyZBThreadService.companyCalculate(ceqReportBo);
        return companyZBInfoPo;
    }
}

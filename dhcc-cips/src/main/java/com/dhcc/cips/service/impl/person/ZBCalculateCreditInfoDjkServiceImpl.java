package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcabBasicinfoseg;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcanFirinfseg;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcatTwentyfourpaymentstat;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcatTwentyfourpaymentstatsSub;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author StivenYang
 * @description 三（信贷交易信息明细）-五
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateCreditInfoDjkServiceImpl implements PersonZBCalculteThreadService {

    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();

        try {

            /*******************************指标声明start************************/
            //26. 贷记卡12月内未还最低还款额次数
            int late12MonMinRpNum = 0;
            //162. 最近2年贷记卡最长逾期月数
            int late2yDjkMaxOdNum = 0;
            //214. 贷记卡状态为“呆账”或“止付”或“冻结”账户数
            int djkDzOrZfOrDjAccNum = 0;
            /*******************************指标声明end************************/

            if (cpqReportBo != null
                    && cpqReportBo.getBorAccInfBo() != null
                    && cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList() != null) {
                List<BorAccInfUnitBo> borAccInfUnitBoList = cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList();
                if (null != borAccInfUnitBoList && borAccInfUnitBoList.size() > 0) {
                    for (BorAccInfUnitBo borAccInfUnitBo : borAccInfUnitBoList) {
                        /***************************账户公共变量开始**************************/
                        //基本信息段
                        PcabBasicinfoseg pcabBasicinfoseg = borAccInfUnitBo.getPcabBasicinfoseg();
                        //账户类型
                        String accountType = null;
                        if (null != pcabBasicinfoseg) {
                            accountType = pcabBasicinfoseg.getAccountType();
                        }
                        //最新表现字段
                        PcanFirinfseg pcanFirinfseg = borAccInfUnitBo.getPcanFirinfseg();
                        //最近两年还款状态
                        PcatTwentyfourpaymentstat pcatTwentyfourpaymentstat = borAccInfUnitBo.getPcatTwentyfourpaymentstat();
                        //最近两年支付列表
                        List<PcatTwentyfourpaymentstatsSub> twentyfourpaymentstatsSubList = null;
                        if (pcatTwentyfourpaymentstat != null) {
                            twentyfourpaymentstatsSubList = pcatTwentyfourpaymentstat.getSub();
                        }
                        //账户状态
                        String accStatus = null;
                        if (pcanFirinfseg != null) {
                            accStatus = pcanFirinfseg.getAccStatus();
                        }
                        /***************************账户公共变量结束**************************/


                        if (StringUtils.isNotEmpty(accountType)
                                && Constants.ACC_TYPE_R2.equals(accountType)) {
                            if (twentyfourpaymentstatsSubList != null) {
                                Boolean judge=true;
                                for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                    String paymentStats = subList.getPaymentStats();
                                    if (StringUtils.isNotEmpty(paymentStats)) {
                                        if (Constants.OVERDUE.contains(paymentStats)&&judge) {
                                            //26. 贷记卡12月内未还最低还款额次数
                                            //各账户24个月还款记录所有出现数字的个数总和
                                            late12MonMinRpNum = late12MonMinRpNum + 1;
                                            judge=false;
                                        }
                                        if (Constants.OVERDUE_STATUS.contains(paymentStats)
                                                && late2yDjkMaxOdNum < Integer.parseInt(paymentStats)) {
                                            //162. 最近2年贷记卡最长逾期月数
                                            //所有账户24个月还款记录取最大值
                                            late2yDjkMaxOdNum = Integer.parseInt(paymentStats);
                                        }
                                    }
                                }
                            }

                            //214. 贷记卡状态为“呆账”或“止付”或“冻结”账户数
                            //                    //账户状态为“呆账、支付、冻结”的账户总数
                            if (StringUtils.isNotEmpty(accStatus)
                                    && Constants.ACC_STAT_5.equals(accStatus)
                                    || Constants.ACC_STAT_3.equals(accStatus)
                                    || Constants.ACC_STAT_2.equals(accStatus)) {
                                djkDzOrZfOrDjAccNum = djkDzOrZfOrDjAccNum + 1;
                            }
                        }
                    }
                }
            }
            //26. 贷记卡12月内未还最低还款额次数
            zxPZbInfo.setLate12MonMinRpNum(String.valueOf(late12MonMinRpNum));
            //162. 最近2年贷记卡最长逾期月数
            zxPZbInfo.setLate2yDjkMaxOdNum(String.valueOf(late2yDjkMaxOdNum));
            //214. 贷记卡状态为“呆账”或“止付”或“冻结”账户数
            zxPZbInfo.setDjkDzOrZfOrDjAccNum(String.valueOf(djkDzOrZfOrDjAccNum));

        } catch (Exception e) {
            throw new Exception(e);
        }

        return zxPZbInfo;
    }
}

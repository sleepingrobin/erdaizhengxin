/**
 * 
 */
package com.dhcc.cips.service.impl.other;


import com.dc.eai.data.CompositeData;
import com.dc.eai.data.Field;
import com.dc.eai.data.FieldAttr;
import com.dc.eai.data.FieldType;
import com.dcfs.esb.utils.TLSimRepTools;

/**
 * Esb 报文公共方法
 * @author
 * @version
 */
public class EsbParentMessageServiceImpl {
    
    /**
     * string 的 xml 转化成cd报文
     * @param str
     * @return
     * @throws Exception
     */
    protected CompositeData xmlToCd(String str) throws Exception {
        
        CompositeData  cd = TLSimRepTools.xmlToCD(str.getBytes(), "UTF-8");
        //cd报文转化失败不会抛出异常，人工判断
        if(cd.size()==0){
            throw new ClassCastException("转化成CD报文异常！");
        }
        return cd;
    }

    /**
     * 添加Filed到CD
     * @param cd     添加目标CD对象
     * @param name   添加字段名称
     * @param type   添加字段类型
     * @param length 添加字段长度
     * @param value  添加字段值
     */
    protected void addField(CompositeData cd, String name, String type, int length, String value){
    	Field field= new Field(new FieldAttr(getType(type), length));
        field.setValue(value);
        cd.addField(name, field);
    }
    
    /**
     * 获取数据类型
     */
    protected FieldType getType(String type){
        if("int".equalsIgnoreCase(type))
            return FieldType.FIELD_INT;
        else if("double".equalsIgnoreCase(type))
            return FieldType.FIELD_DOUBLE;
        else if("image".equalsIgnoreCase(type))
            return FieldType.FIELD_IMAGE;
        else
            return FieldType.FIELD_STRING;
    }
}

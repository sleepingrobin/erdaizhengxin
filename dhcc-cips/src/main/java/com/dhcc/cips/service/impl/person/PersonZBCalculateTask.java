package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import lombok.Data;

import java.util.concurrent.Callable;

/**
 * 个人指标计算任务类
 */
@Data
public class PersonZBCalculateTask implements Callable<PersonZBInfoPo> {

    private volatile PersonZBInfoPo personZBInfoPo;
    private CpqReportBo cpqReportBo;
    private PersonZBCalculteThreadService personZBCalculteThreadService;

    @Override
    public PersonZBInfoPo call() throws Exception {
        personZBInfoPo = personZBCalculteThreadService.calculate(cpqReportBo);
        return personZBInfoPo;
    }
}

package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcabBasicinfoseg;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcanFirinfseg;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PiccCreditaccinfo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PicnNoloaninfo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PicrRelepaysuminfo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PicrRelepaysuminfosub;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.identityinfdata.PbibBasicproinfo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.qryrecord.PiqqQueryrecordsum;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.repayres.PcrpPayliainf;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfUnitBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.credittransinfdata.entity.CreTraInfSummaryUnitBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.repayres.entity.RelReiLiaInfBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.repayres.entity.RelReiLiaInfUnitBo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dhcc.cips.bo.person.PersonExtGuarantyInfoBo;
import com.dhcc.cips.bo.person.PersonLoanInfoBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonBaseInfoPo;
import com.dhcc.cips.po.person.PersonQueryRecordSumPo;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * @author StivenYang
 * @date 2019年6月15日16:31:47
 * @description 依赖指标计算实现类
 */
@Service
public class PersonOtherZBCalcServiceImpl {

    /**
     * 计算对其他指标有依赖的指标结果
     *
     * @param cpqReportBo    报告对象
     * @param personZBInfoPo 个人指标信息
     * @return
     * @throws Exception
     */
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo, PersonZBInfoPo personZBInfoPo) throws Exception {

        try {
            if (cpqReportBo != null
                    && cpqReportBo.getCreTraInfSummaryBo() != null
                    && cpqReportBo.getCreTraInfSummaryBo().getCreTraInfSummaryUnitBo() != null) {
                CreTraInfSummaryUnitBo creTraInfSummaryUnitBo = cpqReportBo.getCreTraInfSummaryBo().getCreTraInfSummaryUnitBo();

                //非循环贷账户授信总额
                BigDecimal picnNoloaninfoTotalCredit = new BigDecimal(0);
                //贷记卡账户授信总额
                BigDecimal piccCreditaccinfoTotalCredit = new BigDecimal(0);
                //贷记卡账户汇总信息段
                PiccCreditaccinfo piccCreditaccinfo = creTraInfSummaryUnitBo.getPiccCreditaccinfo();
                if (piccCreditaccinfo != null) {
                    piccCreditaccinfoTotalCredit = piccCreditaccinfo.getTotalCredit() == null ? new BigDecimal(0) : new BigDecimal(piccCreditaccinfo.getTotalCredit());
                }

                //非循环贷账户汇总信息段
                PicnNoloaninfo picnNoloaninfo = creTraInfSummaryUnitBo.getPicnNoloaninfo();
                if (picnNoloaninfo != null) {
                    picnNoloaninfoTotalCredit = picnNoloaninfo.getTotalCredit() == null ? new BigDecimal(0) : new BigDecimal(picnNoloaninfo.getTotalCredit());
                }

                //bigdecimal格式化
                DecimalFormat formatRate = new DecimalFormat("0.000");
                formatRate.setRoundingMode(RoundingMode.HALF_UP);

                DecimalFormat formatTwoPoint = new DecimalFormat("0.00");
                formatRate.setRoundingMode(RoundingMode.HALF_UP);

                if (personZBInfoPo != null) {
                    //147. 近两年信用卡逾期+消费性贷款逾期+经营性贷款逾期次数
                    int late2yAllOdNum = 0;
                    int late2yCardOdNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLate2yCardOdNum())) {
                        late2yCardOdNum = Integer.valueOf(personZBInfoPo.getLate2yCardOdNum());
                    }
                    int late2yJyxLoanOdNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLate2yJyxLoanOdNum())) {
                        late2yJyxLoanOdNum = Integer.valueOf(personZBInfoPo.getLate2yJyxLoanOdNum());
                    }
                    int late2yXfxLoanOdNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLate2yXfxLoanOdNum())) {
                        late2yXfxLoanOdNum = Integer.valueOf(personZBInfoPo.getLate2yXfxLoanOdNum());
                    }
                    late2yAllOdNum = late2yCardOdNum + late2yJyxLoanOdNum + late2yXfxLoanOdNum;
                    personZBInfoPo.setLate2yAllOdNum(String.valueOf(late2yAllOdNum));

                    //177. 最近3个月审批通过机构比例
                    BigDecimal late3mApprovePassRate = new BigDecimal(0);
                    int late3monOrgPassNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLate3monOrgPassNum())) {
                        late3monOrgPassNum = Integer.valueOf(personZBInfoPo.getLate3monOrgPassNum());
                    }
                    int late3monEnquOrgNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLate3monEnquOrgNum())) {
                        late3monEnquOrgNum = Integer.valueOf(personZBInfoPo.getLate3monEnquOrgNum());
                    }
                    if (late3monEnquOrgNum == 0) {
                        //除数不为0
                        late3mApprovePassRate = new BigDecimal(0);
                    } else {
                        late3mApprovePassRate = new BigDecimal(late3monOrgPassNum / late3monEnquOrgNum);
                    }
                    personZBInfoPo.setLate3mApprovePassRate(formatRate.format(late3mApprovePassRate));

                    //197. 近3个月新增授信比例
                    // （LATE_SEA_ADD_AMT - LATE_SEA_END_SETT_AMT）
                    // /
                    // （（二--三--非循环贷账户信息汇总 + 贷记卡账户信息汇总--授信总额） - LATE_SEA_ADD_AMT + LATE_SEA_END_SETT_AMT）
                    //初始化汇总信息段数据
                    BigDecimal lateSeaAddAmtRate = new BigDecimal(0);
                    BigDecimal lateSeaAddAmt = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLateSeaAddAmt())) {
                        lateSeaAddAmt = new BigDecimal(personZBInfoPo.getLateSeaAddAmt());
                    }
                    BigDecimal lateSeaEndSettAmt = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLateSeaEndSettAmt())) {
                        lateSeaEndSettAmt = new BigDecimal(personZBInfoPo.getLateSeaEndSettAmt());
                    }
                    //分母
                    BigDecimal lateSeaAddAmtRateDeno = piccCreditaccinfoTotalCredit.add(picnNoloaninfoTotalCredit).subtract(lateSeaAddAmt).add(lateSeaEndSettAmt);
                    //除数不为0
                    if (lateSeaAddAmtRateDeno.compareTo(new BigDecimal(0)) == 0) {
                        lateSeaAddAmtRate = new BigDecimal(0);
                    } else {
                        lateSeaAddAmtRate = lateSeaAddAmt.subtract(lateSeaEndSettAmt).divide(lateSeaAddAmtRateDeno, 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setLateSeaAddAmtRate(formatRate.format(lateSeaAddAmtRate));

                    //200. 近3个月新增银行合作家数比例
                    //个人指标（196 - 197）/ (127 - 196 + 197)  即
                    //(LATE_SEA_ADD_BANK_NUM-LATE_SEA_END_BANK_NUM)/(c_bank_service_num)
                    BigDecimal lateSeaAddBankRate = new BigDecimal(0);
                    int lateSeaAddBankNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLateSeaAddBankNum())) {
                        lateSeaAddBankNum = Integer.valueOf(personZBInfoPo.getLateSeaAddBankNum());
                    }
                    int lateSeaEndBankNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLateSeaEndBankNum())) {
                        lateSeaEndBankNum = Integer.valueOf(personZBInfoPo.getLateSeaEndBankNum());
                    }
                    int cBankServiceNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getCBankServiceNum())) {
                        cBankServiceNum = Integer.valueOf(personZBInfoPo.getCBankServiceNum());
                    }
                    if (cBankServiceNum == 0) {
                        lateSeaAddBankRate = new BigDecimal(0);
                    } else {
                        lateSeaAddBankRate = new BigDecimal((lateSeaAddBankNum - lateSeaEndBankNum) / cBankServiceNum);
                    }
                    personZBInfoPo.setLateSeaAddBankRate(formatRate.format(lateSeaAddBankRate));

                    //146. 当前逾期总额
                    //三--二、三、四--各账户“当前逾期总额”总和 + 三--五、六--各账户“当前逾期总额”总和
                    //c_od_total_amt+c_od_amt
                    BigDecimal currUnsettOdBalan = new BigDecimal(0);
                    BigDecimal cOdTotalAmt = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getCOdTotalAmt())) {
                        cOdTotalAmt = new BigDecimal(personZBInfoPo.getCOdTotalAmt());
                    }
                    BigDecimal cOdAmt = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getCOdAmt())) {
                        cOdAmt = new BigDecimal(personZBInfoPo.getCOdAmt());
                    }
                    currUnsettOdBalan = cOdTotalAmt.add(cOdAmt);
                    personZBInfoPo.setCurrUnsettOdBalan(formatTwoPoint.format(currUnsettOdBalan));

                    //31. 未结清经营性贷款笔数/未结清贷款笔数
                    //un_settl_jyx_loan_num / un_settl_loan_num
                    //（基础指标38/个人指标26）
                    BigDecimal unSettJyxLoanNumRate = new BigDecimal(0);
                    int unSettlJyxLoanNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getUnSettlJyxLoanNum())) {
                        unSettlJyxLoanNum = Integer.valueOf(personZBInfoPo.getUnSettlJyxLoanNum());
                    }
                    int unSettlLoanNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getUnSettlLoanNum())) {
                        unSettlLoanNum = Integer.valueOf(personZBInfoPo.getUnSettlLoanNum());
                    }
                    if (unSettlLoanNum != 0) {
                        unSettJyxLoanNumRate = new BigDecimal(unSettlJyxLoanNum / unSettlLoanNum);
                    }
                    personZBInfoPo.setUnSettJyxLoanNumRate(formatRate.format(unSettJyxLoanNumRate));

                    //38. 平均贷款余额（未结清）
                    //loan_total_balan / un_settl_loan_num
                    BigDecimal loanAverBalan = new BigDecimal(0);
                    BigDecimal loanTotalBalan = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLoanTotalBalan())) {
                        loanTotalBalan = new BigDecimal(personZBInfoPo.getLoanTotalBalan());
                    }
                    int unSettlLoanNum1 = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getUnSettlLoanNum())) {
                        unSettlLoanNum1 = Integer.valueOf(personZBInfoPo.getUnSettlLoanNum());
                    }
                    if (unSettlLoanNum1 != 0) {
                        loanAverBalan = loanTotalBalan.divide(new BigDecimal(unSettlLoanNum1), 2, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setLoanAverBalan(formatTwoPoint.format(loanAverBalan));

                    //39. 过去2个月贷款发放数/总未结清贷款数
                    //基础指标--(40+41+42 )/ un_settl_loan_num    即
                    //(late_2mon_fxhd_re_open_num+late_2mon_xhf_re_open_num+late_2mon_xhd_re_open_num)/un_settl_loan_num
                    BigDecimal l2MLUnSettLNumRate = new BigDecimal(0);
                    int late2monFxhdReOpenNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLate2monFxhdReOpenNum())) {
                        late2monFxhdReOpenNum = Integer.valueOf(personZBInfoPo.getLate2monFxhdReOpenNum());
                    }
                    int late2monXhfReOpenNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLate2monXhfReOpenNum())) {
                        late2monXhfReOpenNum = Integer.valueOf(personZBInfoPo.getLate2monXhfReOpenNum());
                    }
                    int late2monXhdReOpenNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLate2monXhdReOpenNum())) {
                        late2monXhdReOpenNum = Integer.valueOf(personZBInfoPo.getLate2monXhdReOpenNum());
                    }
                    int unSettlLoanNum2 = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getUnSettlLoanNum())) {
                        unSettlLoanNum2 = Integer.valueOf(personZBInfoPo.getUnSettlLoanNum());
                    }
                    if (unSettlLoanNum2 != 0) {
                        l2MLUnSettLNumRate = new BigDecimal(late2monFxhdReOpenNum + late2monXhfReOpenNum + late2monXhdReOpenNum).divide(new BigDecimal(unSettlLoanNum2), 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setL2MLUnSettLNumRate(formatRate.format(l2MLUnSettLNumRate));

                    //43. 贷款逾期总额占合同金额比例
                    //c_od_total_amt / loan_total_amt
                    BigDecimal odSumContrSumAmtRate = new BigDecimal(0);
                    BigDecimal cOdTotalAmt1 = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getCOdTotalAmt())) {
                        cOdTotalAmt1 = new BigDecimal(personZBInfoPo.getCOdTotalAmt());
                    }
                    BigDecimal loanTotalAmt = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getLoanTotalAmt())) {
                        loanTotalAmt = new BigDecimal(personZBInfoPo.getLoanTotalAmt());
                    }
                    if (loanTotalAmt.doubleValue() != 0) {
                        odSumContrSumAmtRate = cOdTotalAmt1.divide(loanTotalAmt, 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setOdSumContrSumAmtRate(formatRate.format(odSumContrSumAmtRate));


                    //44. 贷款逾期账户数占总信贷账户比例
                    //(基础指标--49+50+51) / un_settl_loan_num  即
                    //(c_fxhd_loan_due_sum+c_xhf_loan_due_sum+c_xhd_loan_due_sum)/un_settl_loan_num
                    BigDecimal odAcctClAcctRate = new BigDecimal(0);
                    int cFxhdLoanDueSum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getCFxhdLoanDueSum())) {
                        cFxhdLoanDueSum = Integer.valueOf(personZBInfoPo.getCFxhdLoanDueSum());
                    }
                    int cXhfLoanDueSum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getCXhfLoanDueSum())) {
                        cXhfLoanDueSum = Integer.valueOf(personZBInfoPo.getCXhfLoanDueSum());
                    }
                    int cXhdLoanDueSum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getCXhdLoanDueSum())) {
                        cXhdLoanDueSum = Integer.valueOf(personZBInfoPo.getCXhdLoanDueSum());
                    }
                    int unSettlLoanNum3 = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getUnSettlLoanNum())) {
                        unSettlLoanNum3 = Integer.valueOf(personZBInfoPo.getUnSettlLoanNum());
                    }
                    if (unSettlLoanNum3 != 0) {
                        odAcctClAcctRate = new BigDecimal(cFxhdLoanDueSum + cXhfLoanDueSum + cXhdLoanDueSum).divide(new BigDecimal(unSettlLoanNum3), 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setOdAcctClAcctRate(formatRate.format(odAcctClAcctRate));

                    //46. 质押担保笔数占比（贷款、非结清）
                    //(基础指标--52+53+54) / un_settl_loan_num  即
                    //(ass_fxhd_dy_count_num+ass_xhf_dy_count_num+ass_xhd_dy_count_num)/un_settl_loan_num
                    BigDecimal loanZyGuanNumRate = new BigDecimal(0);
                    int assFxhdDyCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssFxhdDyCountNum())) {
                        assFxhdDyCountNum = Integer.valueOf(personZBInfoPo.getAssFxhdDyCountNum());
                    }
                    int assXhfDyCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhfDyCountNum())) {
                        assXhfDyCountNum = Integer.valueOf(personZBInfoPo.getAssXhfDyCountNum());
                    }
                    int assXhdDyCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhdDyCountNum())) {
                        assXhdDyCountNum = Integer.valueOf(personZBInfoPo.getAssXhdDyCountNum());
                    }
                    int unSettlLoanNum4 = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getUnSettlLoanNum())) {
                        unSettlLoanNum4 = Integer.valueOf(personZBInfoPo.getUnSettlLoanNum());
                    }
                    if (unSettlLoanNum4 != 0) {
                        loanZyGuanNumRate = new BigDecimal(assFxhdDyCountNum + assXhfDyCountNum + assXhdDyCountNum).divide(new BigDecimal(unSettlLoanNum4), 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setLoanZyGuanNumRate(formatRate.format(loanZyGuanNumRate));

                    //47. 抵押担保笔数占比（贷款、未结清）
                    //(基础指标--55+56+57) /un_settl_loan_num  即
                    //(ass_fxhd_zy_count_num+ass_xhf_zy_count_num+ass_xhd_zy_count_num)/un_settl_loan_num
                    BigDecimal loanDyGuanNumRate = new BigDecimal(0);
                    int assFxhdZyCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssFxhdZyCountNum())) {
                        assFxhdZyCountNum = Integer.valueOf(personZBInfoPo.getAssFxhdZyCountNum());
                    }
                    int assXhfZyCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhfZyCountNum())) {
                        assXhfZyCountNum = Integer.valueOf(personZBInfoPo.getAssXhfZyCountNum());
                    }
                    int assXhdZyCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhdZyCountNum())) {
                        assXhdZyCountNum = Integer.valueOf(personZBInfoPo.getAssXhdZyCountNum());
                    }
                    if (unSettlLoanNum != 0) {
                        loanDyGuanNumRate = new BigDecimal(assFxhdZyCountNum + assXhfZyCountNum + assXhdZyCountNum).divide(new BigDecimal(unSettlLoanNum), 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setLoanDyGuanNumRate(formatRate.format(loanDyGuanNumRate));

                    //48. 自然人保证笔数占比（贷款、未结清）
                    //(基础指标--58+59+60) /un_settl_loan_num  即
                    //(ass_fxhd_bz_count_num+ass_xhf_bz_count_num+ass_xhd_bz_count_num)/un_settl_loan_num
                    BigDecimal loanPnGuanNumRate = new BigDecimal(0);
                    int assFxhdBzCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssFxhdBzCountNum())) {
                        assFxhdBzCountNum = Integer.valueOf(personZBInfoPo.getAssFxhdBzCountNum());
                    }
                    int assXhfBzCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhfBzCountNum())) {
                        assXhfBzCountNum = Integer.valueOf(personZBInfoPo.getAssXhfBzCountNum());
                    }
                    int assXhdBzCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhdBzCountNum())) {
                        assXhdBzCountNum = Integer.valueOf(personZBInfoPo.getAssXhdBzCountNum());
                    }
                    if (unSettlLoanNum != 0) {
                        loanPnGuanNumRate = new BigDecimal(assFxhdBzCountNum + assXhfBzCountNum + assXhdBzCountNum).divide(new BigDecimal(unSettlLoanNum), 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setLoanPnGuanNumRate(formatRate.format(loanPnGuanNumRate));

                    //49. 其他担保笔数占比（贷款、未结清）
                    //(基础指标--61+62+63) /un_settl_loan_num  即
                    //(ass_fxhd_credit_count_num+ass_xhf_credit_count_num+ass_xhd_credit_count_num)/un_settl_loan_num
                    BigDecimal loanOtGuanNumRate = new BigDecimal(0);
                    int assFxhdCreditCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssFxhdCreditCountNum())) {
                        assFxhdCreditCountNum = Integer.valueOf(personZBInfoPo.getAssFxhdCreditCountNum());
                    }
                    int assXhfCreditCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhfCreditCountNum())) {
                        assXhfCreditCountNum = Integer.valueOf(personZBInfoPo.getAssXhfCreditCountNum());
                    }
                    int assXhdCreditCountNum = 0;
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhdCreditCountNum())) {
                        assXhdCreditCountNum = Integer.valueOf(personZBInfoPo.getAssXhdCreditCountNum());
                    }
                    if (unSettlLoanNum != 0) {
                        loanOtGuanNumRate = new BigDecimal(assFxhdCreditCountNum + assXhfCreditCountNum + assXhdCreditCountNum).divide(new BigDecimal(unSettlLoanNum), 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setLoanOtGuanNumRate(formatRate.format(loanOtGuanNumRate));

                    //50. 质押担保金额占比（贷款、未结清）
                    //(基础指标--64+65+66) / loan_total_amt 即
                    //(ass_fxhd_zy_loan_money+ass_xhf_zy_loan_money+ass_xhd_zy_loan_money)/loan_total_amt
                    BigDecimal loanZyGuanAmtRate = new BigDecimal(0);
                    BigDecimal assFxhdZyLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssFxhdZyLoanMoney())) {
                        assFxhdZyLoanMoney = new BigDecimal(personZBInfoPo.getAssFxhdZyLoanMoney());
                    }
                    BigDecimal assXhfZyLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhfZyLoanMoney())) {
                        assXhfZyLoanMoney = new BigDecimal(personZBInfoPo.getAssXhfZyLoanMoney());
                    }
                    BigDecimal assXhdZyLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhdZyLoanMoney())) {
                        assXhdZyLoanMoney = new BigDecimal(personZBInfoPo.getAssXhdZyLoanMoney());
                    }
                    if (loanTotalAmt.doubleValue() != 0) {
                        loanZyGuanAmtRate = assFxhdZyLoanMoney.add(assXhfZyLoanMoney).add(assXhdZyLoanMoney).divide(loanTotalAmt, 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setLoanZyGuanAmtRate(formatRate.format(loanZyGuanAmtRate));

                    //51. 抵押担保金额占比（贷款、未结清）
                    //(基础指标--67+68+69) / loan_total_amt 即
                    //(ass_fxhd_dy_loan_money+ass_xhf_dy_loan_money+ass_xhd_dy_loan_money)/loan_total_amt
                    BigDecimal loanDyGuanAmtRate = new BigDecimal(0);
                    BigDecimal assFxhdDyLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssFxhdDyLoanMoney())) {
                        assFxhdDyLoanMoney = new BigDecimal(personZBInfoPo.getAssFxhdDyLoanMoney());
                    }
                    BigDecimal assXhfDyLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhfDyLoanMoney())) {
                        assXhfDyLoanMoney = new BigDecimal(personZBInfoPo.getAssXhfDyLoanMoney());
                    }
                    BigDecimal assXhdDyLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhdDyLoanMoney())) {
                        assXhdDyLoanMoney = new BigDecimal(personZBInfoPo.getAssXhdDyLoanMoney());
                    }
                    if (loanTotalAmt.doubleValue() != 0) {
                        loanDyGuanAmtRate = assFxhdDyLoanMoney.add(assXhfDyLoanMoney).add(assXhdDyLoanMoney).divide(loanTotalAmt, 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setLoanDyGuanAmtRate(formatRate.format(loanDyGuanAmtRate));

                    //52. 自然人保证金额占比（贷款、未结清）
                    //(基础指标--70+71+72) / loan_total_amt  即
                    //(ass_fxhd_bz_loan_money+ass_xhf_bz_loan_money+ass_xhd_bz_loan_money)/loan_total_amt
                    BigDecimal loanPnGuanAmtRate = new BigDecimal(0);
                    BigDecimal assFxhdBzLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssFxhdBzLoanMoney())) {
                        assFxhdBzLoanMoney = new BigDecimal(personZBInfoPo.getAssFxhdBzLoanMoney());
                    }
                    BigDecimal assXhfBzLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhfBzLoanMoney())) {
                        assXhfBzLoanMoney = new BigDecimal(personZBInfoPo.getAssXhfBzLoanMoney());
                    }
                    BigDecimal assXhdBzLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhdBzLoanMoney())) {
                        assXhdBzLoanMoney = new BigDecimal(personZBInfoPo.getAssXhdBzLoanMoney());
                    }
                    if (loanTotalAmt.doubleValue() != 0) {
                        loanPnGuanAmtRate = assFxhdBzLoanMoney.add(assXhfBzLoanMoney).add(assXhdBzLoanMoney).divide(loanTotalAmt, 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setLoanPnGuanAmtRate(formatRate.format(loanPnGuanAmtRate));

                    //53. 其他担保金额占比（贷款、未结清）
                    //(基础指标--73+74+75) / loan_total_amt  即
                    //(ass_fxhd_credit_loan_money+ass_xhf_credit_loan_money+ass_xhd_credit_loan_money)/loan_total_amt
                    BigDecimal loanOtGuanAmtRate = new BigDecimal(0);
                    BigDecimal assFxhdCreditLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssFxhdCreditLoanMoney())) {
                        assFxhdCreditLoanMoney = new BigDecimal(personZBInfoPo.getAssFxhdCreditLoanMoney());
                    }
                    BigDecimal assXhfCreditLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhfCreditLoanMoney())) {
                        assXhfCreditLoanMoney = new BigDecimal(personZBInfoPo.getAssXhfCreditLoanMoney());
                    }
                    BigDecimal assXhdCreditLoanMoney = new BigDecimal(0);
                    if (StringUtils.isNotEmpty(personZBInfoPo.getAssXhdCreditLoanMoney())) {
                        assXhdCreditLoanMoney = new BigDecimal(personZBInfoPo.getAssXhdCreditLoanMoney());
                    }
                    if (loanTotalAmt.doubleValue() != 0) {
                        loanOtGuanAmtRate = assFxhdCreditLoanMoney.add(assXhfCreditLoanMoney).add(assXhdCreditLoanMoney).divide(loanTotalAmt, 3, RoundingMode.HALF_UP);
                    }
                    personZBInfoPo.setLoanOtGuanAmtRate(formatRate.format(loanOtGuanAmtRate));

                    //59. 申请月信用卡最大使用的额度比
                    //hist_cred_card_use_frequ    即  个人指标19
                    String histCredCardUseFrequ = personZBInfoPo.getHistCredCardUseFrequ();
                    personZBInfoPo.setMuCc(histCredCardUseFrequ);

                    //64. 非正常账户最差状态
                    //D1最差状态
                    HashSet objectD1 = worstAccountStatus(cpqReportBo, Constants.ACC_TYPE_D1);
                    HashMap<String, String> hashMap = new HashMap<>();
                    if (objectD1.contains(Constants.ACC_STAT_4)) {
                        //呆账
                        hashMap.put(Constants.ACC_STAT_4, Constants.Accmount5);
                    } else if (objectD1.contains(Constants.ACC_STAT_8)) {
                        //司法追偿
                        hashMap.put(Constants.ACC_STAT_8, Constants.Accmount8);
                    } else if (objectD1.contains(Constants.ACC_STAT_6)) {
                        //担保物不足
                        hashMap.put(Constants.ACC_STAT_6, Constants.Accmount6);

                    } else if (objectD1.contains(Constants.ACC_STAT_7)) {
                        //强制平仓
                        hashMap.put(Constants.ACC_STAT_7, Constants.Accmount7);
                    } else if (objectD1.contains(Constants.ACC_STAT_2)) {
                        //逾期
                        hashMap.put(Constants.ACC_STAT_7, Constants.Accmount9);
                    }

                    //R1最差状态
                    HashSet objectR1 = worstAccountStatus(cpqReportBo, Constants.ACC_TYPE_D1);
                    if (objectR1.contains(Constants.ACC_STAT_4)) {
                        //呆账
                        hashMap.put(Constants.ACC_STAT_4, Constants.Accmount5);
                    } else if (objectR1.contains(Constants.ACC_STAT_8)) {
                        //司法追偿
                        hashMap.put(Constants.ACC_STAT_8, Constants.Accmount8);
                    } else if (objectR1.contains(Constants.ACC_STAT_6)) {
                        //担保物不足
                        hashMap.put(Constants.ACC_STAT_6, Constants.Accmount6);
                    } else if (objectR1.contains(Constants.ACC_STAT_5)) {
                        //银行止付
                        hashMap.put(Constants.ACC_STAT_5, Constants.Accmount31);
                    } else if (objectR1.contains(Constants.ACC_STAT_2)) {
                        //逾期
                        hashMap.put(Constants.ACC_STAT_2, Constants.Accmount9);
                    }

                    //R2账户
                    HashSet objectR2 = worstAccountStatus(cpqReportBo, Constants.ACC_TYPE_R2);
                    if (objectR2.contains(Constants.ACC_STAT_5)) {
                        //呆账
                        hashMap.put(Constants.ACC_STAT_5, Constants.Accmount5);
                    } else if (objectR2.contains(Constants.ACC_STAT_8)) {
                        //司法追偿
                        hashMap.put(Constants.ACC_STAT_8, Constants.Accmount8);
                    } else if (objectR2.contains(Constants.ACC_STAT_2)) {
                        //冻结
                        hashMap.put(Constants.ACC_STAT_2, Constants.Accmount2);
                    } else if (objectR2.contains(Constants.ACC_STAT_31)) {
                        //银行止付
                        hashMap.put(Constants.ACC_STAT_31, Constants.Accmount31);
                    } else if (objectR2.contains(Constants.ACC_STAT_3)) {
                        //止付
                        hashMap.put(Constants.ACC_STAT_3, Constants.Accmount3);
                    }

                    //R3账户
                    HashSet objectR3 = worstAccountStatus(cpqReportBo, Constants.ACC_TYPE_R3);
                    if (objectR3.contains(Constants.ACC_STAT_5)) {
                        //呆账
                        hashMap.put(Constants.ACC_STAT_5, Constants.Accmount5);
                    } else if (objectR3.contains(Constants.ACC_STAT_8)) {
                        //司法追偿
                        hashMap.put(Constants.ACC_STAT_8, Constants.Accmount8);
                    } else if (objectR3.contains(Constants.ACC_STAT_2)) {
                        //冻结
                        hashMap.put(Constants.ACC_STAT_2, Constants.Accmount2);
                    } else if (objectR3.contains(Constants.ACC_STAT_31)) {
                        //银行止付
                        hashMap.put(Constants.ACC_STAT_31, Constants.Accmount31);
                    } else if (objectR3.contains(Constants.ACC_STAT_3)) {
                        //止付
                        hashMap.put(Constants.ACC_STAT_3, Constants.Accmount3);
                    }

                    HashSet objectR4 = worstAccountStatus(cpqReportBo, Constants.ACC_TYPE_R4);
                    if (objectR4.contains(Constants.ACC_STAT_4)) {
                        //呆账
                        hashMap.put(Constants.ACC_STAT_4, Constants.Accmount5);
                    } else if (objectR4.contains(Constants.ACC_STAT_8)) {
                        //司法追偿
                        hashMap.put(Constants.ACC_STAT_8, Constants.Accmount8);
                    } else if (objectR4.contains(Constants.ACC_STAT_6)) {
                        //担保物不足
                        hashMap.put(Constants.ACC_STAT_6, Constants.Accmount6);
                    } else if (objectR4.contains(Constants.ACC_STAT_2)) {
                        //逾期
                        hashMap.put(Constants.ACC_STAT_2, Constants.Accmount9);
                    }

                    if (hashMap.containsValue(Constants.Accmount5)) {
                        personZBInfoPo.setWas(Constants.ACC_STAT_5);
                    } else if (hashMap.containsValue(Constants.Accmount8)) {
                        personZBInfoPo.setWas(Constants.ACC_STAT_8);
                    } else if (hashMap.containsValue(Constants.Accmount6)) {
                        personZBInfoPo.setWas(Constants.ACC_STAT_6);
                    } else if (hashMap.containsValue(Constants.Accmount7)) {
                        personZBInfoPo.setWas(Constants.ACC_STAT_7);
                    } else if (hashMap.containsValue(Constants.Accmount2)) {
                        personZBInfoPo.setWas(Constants.ACC_STAT_2);
                    } else if (hashMap.containsValue(Constants.Accmount31)) {
                        personZBInfoPo.setWas(Constants.ACC_STAT_31);
                    } else if (hashMap.containsValue(Constants.Accmount3)) {
                        personZBInfoPo.setWas(Constants.ACC_STAT_3);
                    } else if (hashMap.containsValue(Constants.Accmount9)) {
                        personZBInfoPo.setWas(Constants.ACC_STAT_9);
                    }
                }
            }
        } catch (Exception e) {
            throw new Exception(e);
        }

        return personZBInfoPo;
    }

    //账户最差状态
    private HashSet worstAccountStatus(CpqReportBo reportbo, String account) throws Exception {
        HashSet<String> objects = new HashSet<>();
        try {
            List<BorAccInfUnitBo> borAccInfUnitBoList = reportbo.getBorAccInfBo().getBorAccInfUnitBoList();
            if (borAccInfUnitBoList != null) {
                for (BorAccInfUnitBo list : borAccInfUnitBoList) {
                    if (list.getPcabBasicinfoseg().getAccountType().equals(account)) {
                        if (list.getPcanFirinfseg() != null && list.getPcanFirinfseg().getAccStatus() != null) {
                            String accStatus = list.getPcanFirinfseg().getAccStatus();
                            if (!Constants.ACC_STAT_1.equals(list.getPcanFirinfseg().getFiveAssort())) {
                                if (list.getPcamMonthInformation() != null && list.getPcamMonthInformation().getOverdueAmount() != null) {
                                    Double overdueAmount = Double.parseDouble(list.getPcamMonthInformation().getOverdueAmount());
                                    if (overdueAmount < 300) {
                                        accStatus = Constants.ACC_STAT_1;
                                    }
                                }
                            }
                            objects.add(accStatus);
                        } else {
                            if (list.getPcamMonthInformation() != null) {
                                String accountStatus1 = list.getPcamMonthInformation().getAccountStatus();
                                if (!Constants.ACC_STAT_1.equals(list.getPcanFirinfseg().getFiveAssort())) {
                                    if (list.getPcamMonthInformation() != null && list.getPcamMonthInformation().getOverdueAmount() != null) {
                                        Double overdueAmounts = Double.parseDouble(list.getPcamMonthInformation().getOverdueAmount());
                                        if (overdueAmounts < 300) {
                                            accountStatus1 = Constants.ACC_STAT_1;
                                        }
                                    }
                                    objects.add(accountStatus1);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new Exception(e);
        }
        return objects;
    }

    /**
     * 取bo中的信息设置到personinfo中，方便后期取指标对象所有的信息
     *
     * @param cpqReportBo    报告对象
     * @param personZBInfoPo 个人指标信息对象
     */
    public void setOtherInfo(CpqReportBo cpqReportBo, PersonZBInfoPo personZBInfoPo) {
        JSONObject otherInfoObject = new JSONObject();
        //设置贷款信息数组
        ArrayList<PersonLoanInfoBo> loanInfoList = Lists.newArrayList();
        //设置担保合同信息数组
        ArrayList<PersonExtGuarantyInfoBo> grnyCtrList = Lists.newArrayList();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if (cpqReportBo != null
                && cpqReportBo.getBorAccInfBo() != null
                && cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList() != null) {
            List<BorAccInfUnitBo> borAccInfUnitBoList = cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList();
            //1. 获取贷款信息数组设置到返回对象
            for (BorAccInfUnitBo borAccInfUnitBo : borAccInfUnitBoList) {
                //个人贷款信息数组段
                PersonLoanInfoBo personLoanInfoBo = new PersonLoanInfoBo();
                //最新标新信息段
                PcanFirinfseg pcanFirinfseg = borAccInfUnitBo.getPcanFirinfseg();
                //基本信息段
                PcabBasicinfoseg pcabBasicinfoseg = borAccInfUnitBo.getPcabBasicinfoseg();

                //银行名称
                personLoanInfoBo.setBankName(pcabBasicinfoseg.getOrgCode());
                //担保方式
                personLoanInfoBo.setEnsureMode(pcabBasicinfoseg.getGuaranteeMode());
                //贷款金额
                personLoanInfoBo.setLoanAmt(pcabBasicinfoseg.getLoanAmount());
                //贷款余额
                personLoanInfoBo.setLoanBalance(pcanFirinfseg.getBalance());
                //贷款发放日期
                if (pcabBasicinfoseg.getIssuanceDate() != null) {
                    personLoanInfoBo.setLoanDateGrant(dateFormat.format(pcabBasicinfoseg.getIssuanceDate()));
                }
                //贷款到期日期
                if (pcabBasicinfoseg.getDueDate() != null) {
                    personLoanInfoBo.setLoanDateMature(dateFormat.format(pcabBasicinfoseg.getDueDate()));
                }
                //查询日期
                if (pcanFirinfseg.getReportInfdate() != null) {
                    personLoanInfoBo.setQueryDate(dateFormat.format(pcanFirinfseg.getReportInfdate()));
                }
                //信贷业务种类数
                personLoanInfoBo.setLoanBussVrtyCnt(pcabBasicinfoseg.getBusinessType());
                //总期数
                personLoanInfoBo.setTotPrdCnt(pcabBasicinfoseg.getRepayPeriods());
                //币种
                personLoanInfoBo.setCcy(pcabBasicinfoseg.getCurrency());

                loanInfoList.add(personLoanInfoBo);
            }
        }
        otherInfoObject.put("LOAN_INFO_ARRAY", loanInfoList);

        //2. 获取对外担保信息数组
        if (cpqReportBo != null
                && cpqReportBo.getRelReiLiaInfBo() != null
                && cpqReportBo.getRelReiLiaInfBo().getRelReiLiaInfUnitBoList() != null) {
            List<RelReiLiaInfUnitBo> relReiLiaInfUnitBoList = cpqReportBo.getRelReiLiaInfBo().getRelReiLiaInfUnitBoList();

            for (RelReiLiaInfUnitBo relReiLiaInfUnitBo : relReiLiaInfUnitBoList) {
                PcrpPayliainf payliainf = relReiLiaInfUnitBo.getPayliainf();

                PersonExtGuarantyInfoBo personExtGuarantyInfoBo = new PersonExtGuarantyInfoBo();
                //授信机构名称
                personExtGuarantyInfoBo.setCrdtOrgNm(payliainf.getBussupIns());
                //担保总金额
                personExtGuarantyInfoBo.setWrntBal(payliainf.getReiliaRelamount());
                //担保到期日
                if (payliainf.getExpireDate() != null) {
                    personExtGuarantyInfoBo.setWrntEndDt(dateFormat.format(payliainf.getExpireDate()));
                }
                //担保余额
                personExtGuarantyInfoBo.setWrntTotAmt(payliainf.getMoney());

                grnyCtrList.add(personExtGuarantyInfoBo);
            }
        }
        otherInfoObject.put("GRNY_CTR_ARRAY", grnyCtrList);

        //3. 设置其他字段的信息
        //根据报告编号查询有差异的部分
        //征信评分
        otherInfoObject.put("custCredLevel", "");


        //最近三个月机构审批通过比率
        String late3mApprovePassRate = personZBInfoPo.getLate3mApprovePassRate();
        otherInfoObject.put("late3MAprovePassRate", late3mApprovePassRate);

        //信用卡透支张数
        String credCardUsedNum = personZBInfoPo.getCredCardUsedNum();
        otherInfoObject.put("credCardUsedNum", credCardUsedNum);

        //最近五年内贷款最长逾期月数
        String loanHisMaxOdMonNum = personZBInfoPo.getLoanHisMaxOdMonNum();
        otherInfoObject.put("lOdMaxMonNum", loanHisMaxOdMonNum);

        //未来1-50月需还贷款金额
        //待开发
        otherInfoObject.put("MonLoanAmt", "");

        //近2年经营性贷款逾期次数
        String late2yJyxLoanOdNum = personZBInfoPo.getLate2yJyxLoanOdNum();
        otherInfoObject.put("LATE_2Y_JYX_LOAN_OD_NUM", late2yJyxLoanOdNum);

        //近2年贷款最长逾期月数
        String late2yLoanMaxOdNum = personZBInfoPo.getLate2yLoanMaxOdNum();
        otherInfoObject.put("LATE_2Y_LOAN_MAX_OD_NUM", late2yLoanMaxOdNum);

        //个人基本信息
        //查询次数信息
        PiqqQueryrecordsum piqqQueryrecordsum  = null;
        if (cpqReportBo != null) {
            piqqQueryrecordsum = cpqReportBo.getQueryRecSummaryBo().getQueryRecSummaryUnitBo().getPiqqQueryrecordsum();
            //近一个月本人自主查询次数
            otherInfoObject.put("late_mon_self_query_time", piqqQueryrecordsum.getLastmonQurnum());
            //最近2年内担保资格审查查询次数
            otherInfoObject.put("last_2_y_dbzg_en_num", piqqQueryrecordsum.getLastyexaQrnum());

            PbibBasicproinfo pbibBasicproinfo = cpqReportBo.getIdentityInfoBo().getIdentityInfoUnitBo().getPbibBasicproinfo();
            //性别
            otherInfoObject.put("sex", pbibBasicproinfo.getSex());
            //最高学历
            otherInfoObject.put("high_educ", pbibBasicproinfo.getEducation());
        }
        personZBInfoPo.setPerLoanInfo("");
    }
}

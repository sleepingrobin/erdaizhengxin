package com.dhcc.cips.service.company;

import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.CeqReportBo;
import com.dhcc.cips.po.company.CompanyZBInfoPo;

/**
 * 企业指标计算接口
 */
public interface CompanyZBThreadService {
    /**
     * 指标计算
     *
     * @param ceqReportBo
     * @return
     */
    CompanyZBInfoPo companyCalculate(CeqReportBo ceqReportBo) throws Exception;
}

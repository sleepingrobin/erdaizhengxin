package com.dhcc.cips.service.impl.company;

import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.CeqReportBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.credithintinform.EstcCreditAlertInform;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.guarantsum.EsscClearInform;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.guarantsum.EsscClearInformSub;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.guarantsum.EssuUnclearInformSub;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.lendtranssuminf.EbbcCloseloantransa;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.lendtranssuminf.EbboOthloantrasortsumc;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.lendtranssuminf.EbboOthloantrasortsumu;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.lendtranssuminf.EbbuUnloantransa;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.dhcc.cips.service.company.CompanyZBThreadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 企业基本信息、概要信息
 */
@Service
@Slf4j
public class ZBCalculateSummaryInfoServiceImpl implements CompanyZBThreadService {

    /**
     * 概要信息
     *
     * @param ceqReportBo
     */
    @Override
    public CompanyZBInfoPo companyCalculate(CeqReportBo ceqReportBo) throws Exception {
        CompanyZBInfoPo companyZBInfoPo = new CompanyZBInfoPo();
        try {
            //借贷交易汇总信息 EBB --借贷交易汇总信息单元 EB02 -- 未结清借贷交易汇总信息 EB02A 1
            EbbuUnloantransa ebbuUnloantransa = ceqReportBo.getLoanTransaDataBo().getLoanTransaBo().getEbbuUnloantransa();
            int assDispBusAccNum = 0;
            BigDecimal assDispBusBlance = new BigDecimal("0.00");
            int currAdvanCount = 0;
            BigDecimal currAdvanAmt = new BigDecimal("0.00");
            if (ebbuUnloantransa != null) {
                BigDecimal assDispBusAccNum1 = ebbuUnloantransa.getAssDispBusAccNum();
                //有资产管理公司处置的债务--账户数
                assDispBusAccNum = assDispBusAccNum1 == null ? 0 : assDispBusAccNum1.intValue();
                //有资产管理公司处置的债务--余额
                assDispBusBlance = ebbuUnloantransa.getAssDispBusBlance();
                //垫款--余额
                currAdvanAmt = ebbuUnloantransa.getAdvanceBusBlance();
                BigDecimal advanceBusAccNum = ebbuUnloantransa.getAdvanceBusAccNum();
                //垫款--账户数
                currAdvanCount = advanceBusAccNum == null ? 0 : advanceBusAccNum.intValue();
            }
            //120 .信息概要--未结清信贷及授信信息概要--有资产管理公司处置的债务--账户数
            companyZBInfoPo.setZcAssetNum(String.valueOf(assDispBusAccNum));
            //121 .信息概要--未结清信贷及授信信息概要--有资产管理公司处置的债务--余额
            companyZBInfoPo.setZcAssetBalan(String.valueOf(assDispBusBlance));

            //97. 信息概要--1 未结清信贷及授权信息概要--垫款--余额
            companyZBInfoPo.setCurrAdvanAmt(String.valueOf(currAdvanAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //98. 信息概要--1 未结清信贷及授权信息概要--垫款--账户数
            companyZBInfoPo.setCurrAdvanCount(String.valueOf(currAdvanCount));

            //借贷交易汇总信息 EBB --借贷交易汇总信息单元 EB02 -- 未结清借贷交易汇总信息 EB02A 1 --其他借贷交易分类汇总信息 EB02AH
            List<EbboOthloantrasortsumu> ebboOthloantrasortsumuList = ebbuUnloantransa.getEbboOthloantrasortsumuList();
            //担保交易汇总信息 EBC --担保交易汇总信息单元 EB03 -- 未结清担保交易汇总信息 EB03A 1 --未结清担保交易汇总信息 EB03AH
            List<EssuUnclearInformSub> essuUnclearInformSubList = ceqReportBo.getSecTransInformUnitDataBo().getSecTransInformUnitBo().getEssuUnclearInform().getEssuUnclearInformSubList();
            //不良类
            int totalBadAccount = 0;
            //关注类
            int totalAcAccount = 0;
            //所有
            int allTotalAccount = 0;
            //其他借贷交易分类汇总信息
            if (ebboOthloantrasortsumuList != null && ebboOthloantrasortsumuList.size() > 0) {
                //业务类型  0合计 （中长期借款、短期借款、循环透支和贴现业务的汇总）
                List<EbboOthloantrasortsumu> list = ebboOthloantrasortsumuList.stream().filter(e -> e.getBusType().equals("0")).collect(Collectors.toList());
                for (EbboOthloantrasortsumu ebboOthloantrasortsumu : list) {
                    //账户数
                    BigDecimal accountNum = ebboOthloantrasortsumu.getAccountNum();
                    int accountNumInt = (accountNum == null) ? 0 : accountNum.intValue();
                    String assQualSort = ebboOthloantrasortsumu.getAssQualSort();
                    //不良类
                    if (Constants.FIF_TYPE_3.equals(assQualSort)) {//3不良类
                        totalBadAccount += accountNumInt;
                    }
                    if (Constants.FIF_TYPE_2.equals(assQualSort)) { //2关注类
                        totalAcAccount += accountNumInt;
                    }
                    if (Constants.QUAL_SORT_0.equals(assQualSort)) { //0合计类
                        allTotalAccount += accountNumInt;
                    }
                }
            }
            //未结清担保交易汇总信息
            if (essuUnclearInformSubList != null && essuUnclearInformSubList.size() > 0) {
                //业务类型  1-银行承兑汇票  2-信用证  3-银行保函  9-其他
                for (EssuUnclearInformSub essuUnclearInformSub : essuUnclearInformSubList) {
                    BigDecimal accountNum = essuUnclearInformSub.getAccountNum();
                    int accountNumInt = (accountNum == null) ? 0 : accountNum.intValue();
                    String aqClass = essuUnclearInformSub.getAqClass();
                    if (aqClass != null) {
                        if (aqClass.equals("3")) { //不良类
                            totalBadAccount += accountNumInt;
                        }
                        if (aqClass.equals("2")) { //关注类
                            totalAcAccount += accountNumInt;
                        }
                        if (aqClass.equals("0")) { //合计
                            allTotalAccount += accountNumInt;
                        }
                    }
                }
            }
            //借贷交易信息提示段
            EstcCreditAlertInform estcCreditAlertInform = ceqReportBo.getCreinformDataBo().getCreinformBo().getEstcCreditAlertInform();
            BigDecimal totalBadBalance = new BigDecimal("0.00");
            BigDecimal totalAcBalance = new BigDecimal("0.00");
            BigDecimal totalBadBalanceAll = new BigDecimal("0.00");
            BigDecimal totalBalanceSum = new BigDecimal("0.00"); //113 信息概要--借贷交易--关注类余额+不良类余额
            if (estcCreditAlertInform != null) {
                BigDecimal bcBalance = estcCreditAlertInform.getBcBalance();//不良类担保交易余额
                BigDecimal bcLtBalance = estcCreditAlertInform.getBcLtBalance();//不良类借贷交易余额
                if (bcBalance != null && bcLtBalance != null) {
                    totalBadBalance = bcBalance.add(bcLtBalance);
                }
//------------------------------------------------------------------------------------------------------
                BigDecimal acGtBalance = estcCreditAlertInform.getAcGtBalance();//关注类担保交易余额
                BigDecimal acLtBalance = estcCreditAlertInform.getAcLtBalance();//关注类借贷交易余额
                if (acGtBalance != null && bcLtBalance != null) {
                    totalAcBalance = acGtBalance.add(acLtBalance);
                }
//------------------------------------------------------------------------------------------------------
                BigDecimal gtBalance = estcCreditAlertInform.getGtBalance();//担保交易余额
                BigDecimal ltBalance = estcCreditAlertInform.getLtBalance();//借贷交易余额
                if (bcBalance != null && bcLtBalance != null) {
                    totalBadBalanceAll = gtBalance.add(ltBalance);
                }
                if (bcLtBalance != null && acLtBalance != null) {
                    totalBalanceSum = bcLtBalance.add(acLtBalance);
                }
            }
            //14. 信息概要--未结清信贷--不良类余额--合计加总
            companyZBInfoPo.setUnSettBadClTotalBalan(String.valueOf(totalBadBalance.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //15. 信息概要--未结清信贷--不良类账户数--合计加总
            companyZBInfoPo.setUnSettBadClBizNum(String.valueOf(totalBadAccount));

            //79. 信息概要--未结清信贷及授信信息概要--不良类余额合计
            companyZBInfoPo.setUnSettBadLoanAmt(String.valueOf(totalBadBalance.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //80. 信息概要--未结清信贷及授信信息概要--不良类账户数合计信息概要--不良类账户数合计
            companyZBInfoPo.setUnSettBadLoanCount(String.valueOf(totalBadAccount));

            //81.信息概要--未结清信贷及授信信息概要--关注类余额合计
            companyZBInfoPo.setUnSettConcernLoanAmt(String.valueOf(totalAcBalance.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //82.信息概要--未结清信贷及授信信息概要--关注类账户数合计
            companyZBInfoPo.setUnSettConcernLoanCount(String.valueOf(totalAcAccount));

            //113.信息概要--借贷交易--关注类余额+
            companyZBInfoPo.setUnsettBadBusiBalan(String.valueOf(totalBalanceSum.setScale(2, BigDecimal.ROUND_HALF_UP)));

            //16. un_sett_bad_cl_total_balan/信息概要--未结清信贷及授信信息概要--合计--余额加总
            BigDecimal resultBalance = new BigDecimal("0.000");
            if (!totalBadBalanceAll.equals("0.00")) {
                resultBalance = totalBadBalance.divide(totalBadBalanceAll, 10, BigDecimal.ROUND_HALF_UP);
            }
            companyZBInfoPo.setUnSettBadBalanRate(String.valueOf(resultBalance.setScale(3, BigDecimal.ROUND_HALF_UP)));
            //17. un_sett_bad_cl_biz_num / 信息概要--未结清信贷及授信信息概要--合计--账户数加总
            BigDecimal resultAccountRate = new BigDecimal("0.000");
            if (allTotalAccount > 0) {
                resultAccountRate = new BigDecimal((float) totalBadAccount / allTotalAccount);
            }
            companyZBInfoPo.setUnSettBadNumRate(String.valueOf(resultAccountRate.setScale(3, BigDecimal.ROUND_HALF_UP)));

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

            int allAccountSum = 0;//已结清信用业务笔数
            //借贷交易汇总信息 EBB --借贷交易汇总信息单元 EB02 -- 已结清借贷交易汇总信息 EB02B
            EbbcCloseloantransa ebbcCloseloantransa = ceqReportBo.getLoanTransaDataBo().getLoanTransaBo().getEbbcCloseloantransa();
            //122. 信息概要--强制执行记录条数
            BigDecimal enforceRecordNums = ceqReportBo.getCreinformDataBo().getCreinformBo().getEstnNotCreditTran().getEnforceRecordNum();//强制执行记录条数
            int enforceRecordNumsInt = (enforceRecordNums == null) ? 0 : enforceRecordNums.intValue();
            companyZBInfoPo.setForceDoRecordNum(String.valueOf(enforceRecordNumsInt));
            //116.已结清信贷信息概要--垫款--金额
            BigDecimal advanceBusMoney = ebbcCloseloantransa.getAdvanceBusMoney();
            String advanceBusMoneyResult = String.valueOf(advanceBusMoney == null ? new BigDecimal("0.00") : advanceBusMoney.setScale(2, BigDecimal.ROUND_HALF_UP));
            companyZBInfoPo.setSettAdvanAllAmt(advanceBusMoneyResult);
            //117.已结清信贷信息概要--垫款--账户数
            BigDecimal advanceBusAccNum = ebbcCloseloantransa.getAdvanceBusAccNum();
            int advanceBusMoneyInt = (advanceBusAccNum == null) ? 0 : advanceBusAccNum.intValue();
            companyZBInfoPo.setSettAdvanAllNum(String.valueOf(advanceBusMoneyInt));
            // 已结清信贷信息概要--垫款--账户数
            allAccountSum += advanceBusMoneyInt;
            // 借贷交易汇总信息 EBB --借贷交易汇总信息单元 EB02 -- 已结清借贷交易汇总信息 EB02B --其他借贷交易分类汇总信息 EB02BH
            List<EbboOthloantrasortsumc> ebboOthloantrasortsumcList = ebbcCloseloantransa.getEbboOthloantrasortsumcList();
            if (ebboOthloantrasortsumcList != null && ebboOthloantrasortsumcList.size() > 0) {
                List<EbboOthloantrasortsumc> list = ebboOthloantrasortsumcList.stream().filter(e -> Constants.BUS_TYPE_0.equals(e.getBusType()) && Constants.QUAL_SORT_0.equals(e.getAssQualSort())).collect(Collectors.toList());
                for (EbboOthloantrasortsumc ebboOthloantrasortsumc : list) {
                    //账户数
                    BigDecimal accountNum = ebboOthloantrasortsumc.getAccountNum();
                    Integer accountNumInt = (accountNum == null) ? 0 : accountNum.intValue();
                    allAccountSum += accountNumInt;
                }
            }

            //担保交易汇总信息 EBC --担保交易汇总信息单元 EB03 -- 已结清担保交易汇总信息 EB03B  --已结清担保交易汇总信息EB03BH
            EsscClearInform esscClearInform = ceqReportBo.getSecTransInformUnitDataBo().getSecTransInformUnitBo().getEsscClearInform();
            List<EsscClearInformSub> esscClearInformSubList = esscClearInform.getEsscClearInformSubList();
            if (esscClearInformSubList != null && esscClearInformSubList.size() > 0) {
                List<EsscClearInformSub> list = esscClearInformSubList.stream().filter(e -> Constants.QUAL_SORT_0.equals(e.getAqClass())).collect(Collectors.toList());
                for (EsscClearInformSub esscClearInformSub : list) {
                    //账户数
                    BigDecimal accountNum = esscClearInformSub.getAccountNum();
                    Integer accountNumInt = (accountNum == null) ? 0 : accountNum.intValue();
                    allAccountSum += accountNumInt;
                }
            }
            //19.已结清信贷信息概要--垫款--账户数  + 已结清信贷信息概要--合计之和
            companyZBInfoPo.setSettClTotalNum(String.valueOf(allAccountSum));
            return companyZBInfoPo;
        } catch (Exception e) {
            log.error("CompanyZBSummaryServiceImpl", e);
            throw new Exception(e);
        }
    }
}

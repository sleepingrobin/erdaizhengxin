package com.dhcc.cips.service.other;

import com.dhcc.cips.response.EntityResponse;

import java.util.List;
import java.util.Map;

/**
 * 短信服务
 * @author Yangjiaheng
 * @date 2019-5-6 09:51:58
 */
public interface MsgService {

    /**
     * 发送短信
     * @param username 用户名
     * @param msgContent 短信内容
     * @return 短信发送是否成功
     */
    EntityResponse sendMessage(String username, String msgContent);
    /**
     * 根据业务系统传过来的userId判断用户在前置系统中是否存在
     * @param userId 用户id
     * @return 0：不存在   1：存在，但不包含查询岗  2. 存在，但且是角色包含查询岗
     */
    int queryUserByUserId(String userId);

    /**
     * 查找组织机构用户列表
     * @return 联系人map
     */
    List<Map> selectUserListByOrg(String orgId);
}

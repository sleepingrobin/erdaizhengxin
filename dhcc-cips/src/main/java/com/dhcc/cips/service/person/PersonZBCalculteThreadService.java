package com.dhcc.cips.service.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import com.dhcc.cips.po.person.PersonZBInfoPo;

/**
 * 指标计算接口
 */
public interface PersonZBCalculteThreadService {
    /**
     * 指标计算
     * @param cpqReportBo
     * @return
     */
    PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception;
}

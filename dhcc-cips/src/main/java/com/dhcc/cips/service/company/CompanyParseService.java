package com.dhcc.cips.service.company;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "dhcc-companyparse")
public interface CompanyParseService {
    @RequestMapping(value = "/companyparse/parsehtml", method = RequestMethod.POST)
    Object analysisEnterpriseReport(@RequestBody String htmlStr);
}

package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.*;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.*;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfUnitBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.credittransinfdata.entity.CreTraInfSummaryBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.credittransinfdata.entity.CreTraInfSummaryUnitBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.repayres.entity.RelReiLiaInfBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.repayres.entity.RelReiLiaInfUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 基础指标计算（信息概要-信贷交易授信及负债信息概要部分）
 */
@Service
public class BasicZBCalculateSummaryInfoPartThreeServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();
        try {
            //借贷账户信息(PDA)
            BorAccInfBo borAccInfBo = cpqReportBo.getBorAccInfBo();
            //借贷账户信息单元(PD01)
            List<BorAccInfUnitBo> borAccInfUnitBoList = null;
            if (borAccInfBo != null && borAccInfBo.getBorAccInfUnitBoList() != null) {
                borAccInfUnitBoList = borAccInfBo.getBorAccInfUnitBoList();
            }
            //信贷交易信息概要(PCO)
            CreTraInfSummaryBo creTraInfSummaryBo = cpqReportBo.getCreTraInfSummaryBo();
            //信贷交易信息概要信息单元(PC02)
            CreTraInfSummaryUnitBo creTraInfSummaryUnitBo = null;
            if (creTraInfSummaryBo != null) {
                creTraInfSummaryUnitBo = creTraInfSummaryBo.getCreTraInfSummaryUnitBo();
            }

            //报告日期
            Date reportDate = cpqReportBo.getRepheadBo().getRepheadUnitBo().getPchrPchrreportinfo().getReportTime();

            //7未结清非循环贷账户授信总额
            BigDecimal un_sett_fxh_credit_amt = new BigDecimal("0.00");
            //8未结清循环额度下分账户授信总额
            BigDecimal un_sett_xhf_credit_amt = new BigDecimal("0.00");
            //9未销户循环贷账户授信总额
            BigDecimal un_clo_xh_credit_amt = new BigDecimal("0.00");
            //10未销户贷记卡账户授信总额
            BigDecimal un_clo_dj_credit_amt = new BigDecimal("0.00");
            //11未销户准贷记卡账户总授信额度
            BigDecimal un_clo_zdj_credit_amt = new BigDecimal("0.00");
            //12未结清非循环贷账户总余额
            BigDecimal un_sett_fxhd_all_balan = new BigDecimal("0.00");
            //13未结清循环额度下分账户总余额
            BigDecimal un_sett_xhf_all_balan = new BigDecimal("0.00");
            //14未销户循环贷账户总本金余额
            BigDecimal un_sett_xhd_prin_balan = new BigDecimal("0.00");
            //15未销户贷记卡账户总已用额度
            BigDecimal un_clo_dj_used_amt = new BigDecimal("0.00");
            //16未销户准贷记卡账户总已用额度
            BigDecimal un_clo_zdj_used_amt = new BigDecimal("0.00");
            //非循环贷授信账户数
            int un_sett_fxhd_account_num = 0;
            //循环额度下分账户账户数
            int un_sett_xhf_account_num = 0;
            //循环贷账户账户数
            int un_sett_xhd_account_num = 0;
            //贷记卡账户数
            int un_clo_dj_account_num = 0;
            //准贷记卡账户数
            int un_clo_zdj_account_num = 0;
            //22他行信用卡最高额度
            BigDecimal othe_bank_cred_max_amt = new BigDecimal("0.00");
            //23他行信用卡最低额度
            BigDecimal othe_bank_cred_min_amt = new BigDecimal("0.00");
            BigDecimal accountQuotaNumss = new BigDecimal("0.00");

            //基础指标计算（信息概要-信贷交易授信及负债信息概要部分）
            if (creTraInfSummaryBo != null && creTraInfSummaryUnitBo != null) {
                //非循环贷账户汇总信息段
                PicnNoloaninfo picnNoloaninfo = creTraInfSummaryUnitBo.getPicnNoloaninfo();
                if (picnNoloaninfo != null) {
                    String totalCredit = picnNoloaninfo.getTotalCredit();
                    if (StringUtils.isNotEmpty(totalCredit)) {
                        //未结清非循环贷账户授信总额
                        un_sett_fxh_credit_amt = new BigDecimal(totalCredit);
                    }

                    String balance = picnNoloaninfo.getBalance();
                    if (StringUtils.isNotEmpty(balance)) {
                        //未结清非循环贷账户总余额
                        un_sett_fxhd_all_balan = new BigDecimal(balance);
                    }

                    String accountsNum = picnNoloaninfo.getAccountNum();
                    if (StringUtils.isNotEmpty(accountsNum)) {
                        //非循环贷授信账户数
                        un_sett_fxhd_account_num = Integer.parseInt(accountsNum);
                    }

                }


                //循环额度下分账户汇总信息段
                PicsSubacctinfo picsSubacctinfo = creTraInfSummaryUnitBo.getPicsSubacctinfo();
                if (picsSubacctinfo != null) {
                    String totalCredit = picsSubacctinfo.getTotalCredit();
                    if (StringUtils.isNotEmpty(totalCredit)) {
                        //未结清循环额度下分账户授信总额
                        un_sett_xhf_credit_amt = new BigDecimal(totalCredit);
                    }
                    String balance = picsSubacctinfo.getBalance();
                    if (StringUtils.isNotEmpty(balance)) {
                        //未结清循环额度下分账户总余额
                        un_sett_xhf_all_balan = new BigDecimal(balance);
                    }

                    String accountsNum1 = picsSubacctinfo.getAccountNum();
                    if (StringUtils.isNotEmpty(accountsNum1)) {
                        //循环额度下分账户账户数
                        un_sett_xhf_account_num = Integer.parseInt(accountsNum1);
                    }
                }


                //循环贷账户汇总信息段
                PicrRecreditinfo picrRecreditinfo = creTraInfSummaryUnitBo.getPicrRecreditinfo();
                if (picrRecreditinfo != null) {
                    String totalCredit = picrRecreditinfo.getTotalCredit();
                    if (StringUtils.isNotEmpty(totalCredit)) {
                        //未销户循环贷账户授信总额
                        un_clo_xh_credit_amt = new BigDecimal(totalCredit);
                    }

                    String balance = picrRecreditinfo.getBalance();
                    if (StringUtils.isNotEmpty(balance)) {
                        //未销户循环贷账户总本金余额
                        un_sett_xhd_prin_balan = new BigDecimal(balance);
                    }

                    String accountsNum2 = picrRecreditinfo.getAccountNum();
                    if (StringUtils.isNotEmpty(accountsNum2)) {
                        //循环贷账户账户数
                        un_sett_xhd_account_num = Integer.parseInt(accountsNum2);
                    }
                }


                //未销户贷记卡账户授信总额
                PiccCreditaccinfo piccCreditaccinfo = creTraInfSummaryUnitBo.getPiccCreditaccinfo();
                if (piccCreditaccinfo != null) {
                    String totalCredit = piccCreditaccinfo.getTotalCredit();
                    if (StringUtils.isNotEmpty(totalCredit)) {
                        //未销户贷记卡账户授信总额
                        un_clo_dj_credit_amt = new BigDecimal(totalCredit);
                    }

                    String quotaUsed = piccCreditaccinfo.getQuotaUsed();
                    if (StringUtils.isNotEmpty(quotaUsed)) {
                        //15未销户贷记卡账户总已用额度
                        un_clo_dj_used_amt = new BigDecimal(quotaUsed);
                    }

                    String accountsNum3 = piccCreditaccinfo.getAccountNum();
                    if (StringUtils.isNotEmpty(accountsNum3)) {
                        //贷记卡账户数
                        un_clo_dj_account_num = Integer.parseInt(accountsNum3);
                    }

                    String sinbankMaxcre = piccCreditaccinfo.getSinbankMaxcre();
                    if (StringUtils.isNotEmpty(sinbankMaxcre)) {
                        //22他行信用卡最高额度
                        othe_bank_cred_max_amt = new BigDecimal(sinbankMaxcre);
                    }

                    String sinbankMincre = piccCreditaccinfo.getSinbankMincre();
                    if (StringUtils.isNotEmpty(sinbankMincre)) {
                        //23他行信用卡最低额度
                        othe_bank_cred_min_amt = new BigDecimal(piccCreditaccinfo.getSinbankMincre());
                    }
                }

                //未销户准贷记卡账户总授信额度
                PicqQucreditaccinfo picqQucreditaccinfo = creTraInfSummaryUnitBo.getPicqQucreditaccinfo();
                if (picqQucreditaccinfo != null) {
                    String totalCredit = picqQucreditaccinfo.getTotalCredit();
                    if (StringUtils.isNotEmpty(totalCredit)) {
                        //未销户准贷记卡账户总授信额度
                        un_clo_zdj_credit_amt = new BigDecimal(totalCredit);
                    }

                    String quotaUsed = picqQucreditaccinfo.getQuotaUsed();
                    if (StringUtils.isNotEmpty(quotaUsed)) {
                        //未销户准贷记卡账户总已用额度
                        un_clo_zdj_used_amt = new BigDecimal(quotaUsed);
                    }
                    String accountsNum4 = picqQucreditaccinfo.getAccountNum();
                    if (StringUtils.isNotEmpty(accountsNum4)) {
                        //准贷记卡账户数
                        un_clo_zdj_account_num = Integer.parseInt(accountsNum4);
                    }
                }
            }

            //未结清非循环贷账户授信总额
            zxPZbInfo.setUnSettFxhCreditAmt(String.valueOf(un_sett_fxh_credit_amt));
            //未结清循环额度下分账户授信总额
            zxPZbInfo.setUnSettXhfCreditAmt(String.valueOf(un_sett_xhf_credit_amt));
            //未销户循环贷账户授信总额
            zxPZbInfo.setUnCloXhCreditAmt(String.valueOf(un_clo_xh_credit_amt));
            //未销户贷记卡账户授信总额
            zxPZbInfo.setUnCloDjCreditAmt(String.valueOf(un_clo_dj_credit_amt));
            //未销户准贷记卡账户总授信额度
            zxPZbInfo.setUnCloZdjCreditAmt(String.valueOf(un_clo_zdj_credit_amt));
            //未结清非循环贷账户总余额
            zxPZbInfo.setUnSettFxhdAllBalan(String.valueOf(un_sett_fxhd_all_balan));
            //未结清循环额度下分账户总余额
            zxPZbInfo.setUnSettXhfAllBalan(String.valueOf(un_sett_xhf_all_balan));
            //未销户循环贷账户总本金余额
            zxPZbInfo.setUnSettXhdPrinBalan(String.valueOf(un_sett_xhd_prin_balan));
            //未销户贷记卡账户总已用额度
            zxPZbInfo.setUnCloDjUsedAmt(String.valueOf(un_clo_dj_used_amt));
            //未销户准贷记卡账户总已用额度
            zxPZbInfo.setUnCloZdjUsedAmt(String.valueOf(un_clo_zdj_used_amt));
            //19现有信用产品总数
            int curr_cl_prod_num = un_sett_fxhd_account_num + un_sett_xhf_account_num + un_sett_xhd_account_num + un_clo_dj_account_num + un_clo_zdj_account_num;
            zxPZbInfo.setCurrClProdNum(String.valueOf(curr_cl_prod_num));
            //20未销户贷记卡账户数
            zxPZbInfo.setUnCloDjAccountNum(String.valueOf(un_clo_dj_account_num));
            //21未销户准贷记卡账户数
            zxPZbInfo.setUnCloZdjAccountNum(String.valueOf(un_clo_zdj_account_num));
            //22他行信用卡最高额度
            zxPZbInfo.setOtheBankCredMaxAmt(String.valueOf(othe_bank_cred_max_amt));
            //23他行信用卡最低额度
            zxPZbInfo.setOtheBankCredMinAmt(String.valueOf(othe_bank_cred_min_amt));
            //个人指标5 授信余额（信用总余额）
            BigDecimal credTotalBalan = un_clo_dj_credit_amt.add(un_clo_zdj_credit_amt).add(un_sett_fxhd_all_balan).add(un_sett_xhf_all_balan).add(un_sett_xhd_prin_balan);
            BigDecimal cred_total_balan = credTotalBalan.setScale(2, BigDecimal.ROUND_HALF_UP);
            zxPZbInfo.setCredTotalBalan(String.valueOf(cred_total_balan));
            //个人指标28 未结清贷款笔数
            int un_settl_loan_num = un_sett_fxhd_account_num + un_sett_xhf_account_num + un_sett_xhd_account_num;
            zxPZbInfo.setUnSettlLoanNum(String.valueOf(un_settl_loan_num));


            //相关还款责任信息(PCR)
            RelReiLiaInfBo relReiLiaInfBo = cpqReportBo.getRelReiLiaInfBo();
            //相关还款责任信息单元(PD03)
            List<RelReiLiaInfUnitBo> relReiLiaInfUnitBoList = null;
            if (relReiLiaInfBo != null) {
                relReiLiaInfUnitBoList = relReiLiaInfBo.getRelReiLiaInfUnitBoList();
            }

            //得到前3月的时间
            Date dBefore = DateUtil.getAimMonthDate(reportDate, -3);
            //前2个月
            Date afts = DateUtil.getAimMonthDate(reportDate, -2);

            //24 3个月内人民币信用卡发卡数
            int late_3mon_issue_rmb_card_num = 0;
            //25正在使用信用卡账户数
            int use_credit_card_num = 0;
            //个人基础指标 40未结清经营性贷款笔数
            int un_settl_jyx_loan_num = 0;
            //个人基础指标 41未结清经营性贷款借款金额合计
            BigDecimal un_sett_jyx_loan_money = new BigDecimal("0.00");
            //单张信用卡额度使用率
            StringBuilder per_cred_card_used_rate = new StringBuilder();
            //个人指标58 平均月份间隔（未销户、已激活信用卡及未结清贷款）
            int monthTotal = 0;
            //个人指标60 不良余额比例
            BigDecimal bitdilmt = new BigDecimal("0.000");
            //个人指标79 贷款组合担保笔数占比
            BigDecimal loan_zh_guan_num_rate = new BigDecimal("0.000");
            //个人指标82  贷款组合担保金额占比
            BigDecimal loan_zh_guan_amt_rate = new BigDecimal("0.000");
            //个人指标80 贷款农户联保笔数占比
            BigDecimal loan_lb_guan_num_rate = new BigDecimal("0.000");
            //个人指标83 贷款组合农户金额占比
            BigDecimal loan_lb_guan_amt_rate = new BigDecimal("0.000");
            //个人基础指标81 贷款信用/免担保笔数占比
            BigDecimal loan_xy_guan_num_rate = new BigDecimal("0.000");
            //个人基础指标84 贷款信用/免担保金额占比
            BigDecimal loan_xy_guan_amt_rate = new BigDecimal("0.000");

            //定义变量
            int accStatusn = 0;
            int accStatusn1 = 0;
            int accStatusn2 = 0;
            BigDecimal labanNums = new BigDecimal("0.00");
            BigDecimal labanNums1 = new BigDecimal("0.00");
            BigDecimal labanNums2 = new BigDecimal("0.00");
            int accSta = 0;
            int accSta1 = 0;
            int accSta2 = 0;
            BigDecimal labanNum = new BigDecimal("0.00");
            BigDecimal labanNum1 = new BigDecimal("0.00");
            BigDecimal labanNum2 = new BigDecimal("0.00");
            int accNum = 0;
            int accNum1 = 0;
            int accNum2 = 0;
            BigDecimal labanNumss = new BigDecimal("0.00");
            BigDecimal labanNumss1 = new BigDecimal("0.00");
            BigDecimal labanNumss2 = new BigDecimal("0.00");


            if (borAccInfUnitBoList != null && borAccInfUnitBoList.size() > 0) {
                for (BorAccInfUnitBo list : borAccInfUnitBoList) {
                    //基本信息段(PD01A)
                    PcabBasicinfoseg pcabBasicinfoseg = list.getPcabBasicinfoseg();

                    //担保方式
                    String guaranteeMode = null;
                    if (pcabBasicinfoseg != null && pcabBasicinfoseg.getGuaranteeMode() != null) {
                        guaranteeMode = pcabBasicinfoseg.getGuaranteeMode();
                    }

                    //账户类型(PD01AD01)
                    String accountType = null;
                    if (pcabBasicinfoseg != null && pcabBasicinfoseg.getAccountType() != null) {
                        accountType = pcabBasicinfoseg.getAccountType();
                    }
                    //最新表现字段(PD01B)
                    PcanFirinfseg pcanFirinfseg = list.getPcanFirinfseg();
                    //最近一个月度表现信息段
                    PcamMonthInformation pcamMonthInformation = list.getPcamMonthInformation();

                    if (pcabBasicinfoseg != null && accountType != null) {
                        if (Constants.ACC_TYPE_R2.equals(accountType)) {
                            if (relReiLiaInfUnitBoList != null && relReiLiaInfUnitBoList.size() > 0) {
                                for (RelReiLiaInfUnitBo listRel : relReiLiaInfUnitBoList) {
                                    Date openDate = listRel.getPayliainf().getOpenDate();
                                    if (openDate != null) {
                                        if (reportDate.after(openDate) && openDate.after(dBefore)) {
                                            //3个月内人民币信用卡发卡数
                                            late_3mon_issue_rmb_card_num = late_3mon_issue_rmb_card_num + 1;
                                        }
                                    }
                                }
                            }

                            //正在使用信用卡账户数
                            String balance = pcanFirinfseg.getBalance();
                            if (pcanFirinfseg != null && balance != null) {
                                if (Integer.parseInt(balance) > 0) {
                                    //信用卡账户数
                                    use_credit_card_num = use_credit_card_num + 1;
                                }
                            }
                        }

                        if (Constants.ACC_TYPE_D1.equals(accountType)) {
                            if ((Constants.BUS_TYPE_41).equals(pcabBasicinfoseg.getBusinessType())) {
                                String accStatus = pcanFirinfseg.getAccStatus();
                                if (pcanFirinfseg != null && accStatus != null) {
                                    if ((Constants.ACC_STAT_1).equals(accStatus)) {
                                        //未结清经营性贷款笔数
                                        un_settl_jyx_loan_num = un_settl_jyx_loan_num + 1;
                                        //未结清经营性贷款借款金额合计
                                        String loanAmount = pcabBasicinfoseg.getLoanAmount();
                                        if (StringUtils.isNotEmpty(loanAmount)) {
                                            un_sett_jyx_loan_money = un_sett_jyx_loan_money.add(new BigDecimal(loanAmount));

                                        }
                                    }
                                }
                            }
                        }
                        if (pcabBasicinfoseg != null && pcamMonthInformation != null) {
                            if (accountType.equals(Constants.ACC_TYPE_R2) || accountType.equals(Constants.ACC_TYPE_R3)) {
                                BigDecimal singleCreditRate = new BigDecimal("0.000");
                                BigDecimal accountQuota2 = new BigDecimal("0.00");
                                BigDecimal usedQuota2 = new BigDecimal("0.00");
                                //授信总额度
                                String accountQuota = pcabBasicinfoseg.getAccountQuota();
                                if (null != pcabBasicinfoseg && null != accountQuota) {
                                    accountQuota2 = new BigDecimal(accountQuota);
                                }
                                //已用额度
                                String quotaUsed = pcamMonthInformation.getQuotaUsed();
                                if (pcamMonthInformation != null && quotaUsed != null) {
                                    usedQuota2 = new BigDecimal(quotaUsed);
                                }
                                if (accountQuota2.compareTo(BigDecimal.ZERO) > 0) {
                                    singleCreditRate = usedQuota2.divide(accountQuota2, 3, RoundingMode.HALF_UP);
                                }
                                //单张信用卡额度使用率
                                per_cred_card_used_rate.append(singleCreditRate).append("/");
                            }
                        }
                    /*    BigDecimal balansNum = new BigDecimal("0.00");
                        if (pcanFirinfseg != null && pcabBasicinfoseg != null) {
                            if (accountType.equals(Constants.ACC_TYPE_R2) || accountType.equals(Constants.ACC_TYPE_R3)) {
                                PcapRecfivehisinfo pcapRecfivehisinfo = list.getPcapRecfivehisinfo();
                                if (pcapRecfivehisinfo != null) {
                                    String endYear = pcapRecfivehisinfo.getEndYears();
                                    Date endYears = null;
                                    if (endYears != null) {
                                        endYears = DateUtil.toDate(endYear, "yyyy-MM");
                                    }

                                    List<PcapRecfivehisinfoSub> pcapRecfivehisinfoSubList = pcapRecfivehisinfo.getPcapRecfivehisinfoSubList();
                                    if (pcapRecfivehisinfoSubList != null) {
                                        for (PcapRecfivehisinfoSub pcapList : pcapRecfivehisinfoSubList) {
                                            String month = pcapList.getMonth();
                                            if (StringUtils.isNotBlank(month) && endYears != null) {
                                                Date month1 = DateUtil.toDate(month, "yyyy-MM");
                                                if (afts.before(month1) && month1.before(endYears)) {
                                                    if (Constants.OVERDUE.contains(pcapList.getRepayStatus())) {
                                                        String overdueMoney = pcapList.getOverdueMoney();
                                                        if (overdueMoney != null) {
                                                            BigDecimal money = new BigDecimal(overdueMoney);
                                                            if (null != money) {
                                                                balansNum = balansNum.add(money);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (cred_total_balan.compareTo(BigDecimal.ZERO) == 1)
                                        //不良余额比例
                                        bitdilmt = balansNum.divide(cred_total_balan, 3, RoundingMode.HALF_UP);
                                }
                            }
                        }*/

                        //79个人指标 贷款组合担保笔数占比
                        if (accountType.equals(Constants.ACC_TYPE_R1) || accountType.equals(Constants.ACC_TYPE_R4) || accountType.equals(Constants.ACC_TYPE_D1)) {
                            if (pcabBasicinfoseg != null && guaranteeMode != null) {
                                if ((Constants.GUARANTY_5).equals(guaranteeMode) || (Constants.GUARANTY_6).equals(guaranteeMode)) {
                                    if (pcanFirinfseg.getAccStatus() != null) {
                                        if ((Constants.ACC_STAT_3) != pcanFirinfseg.getAccStatus()) {
                                            accStatusn1 = accStatusn1 + 1;
                                            //贷款组合担保金额占比
                                            if (pcabBasicinfoseg.getLoanAmount() != null) {
                                                BigDecimal bigDecimal = new BigDecimal(pcabBasicinfoseg.getLoanAmount());
                                                if (null != bigDecimal) {
                                                    labanNums1 = labanNums1.add(bigDecimal);
                                                }
                                            }
                                        }
                                    }
                                }
                                if ((Constants.GUARANTY_7).equals(guaranteeMode)) {
                                    if (pcanFirinfseg.getAccStatus() != null) {
                                        if ((Constants.ACC_STAT_3) != pcanFirinfseg.getAccStatus()) {
                                            accSta1 = accSta1 + 1;
                                            //贷款农户担保金额占比
                                            if (pcabBasicinfoseg.getLoanAmount() != null) {
                                                BigDecimal bigDecimal = new BigDecimal(pcabBasicinfoseg.getLoanAmount());
                                                if (null != bigDecimal) {
                                                    labanNum1 = labanNum1.add(bigDecimal);
                                                }
                                            }
                                        }
                                    }
                                }
                                if ((Constants.GUARANTY_4).equals(guaranteeMode)) {
                                    if (!(Constants.ACC_STAT_3).equals(pcanFirinfseg.getAccStatus()) && !(Constants.ACC_STAT_5).equals(pcanFirinfseg.getAccStatus())) {
                                        accNum1 = accNum1 + 1;
                                        //贷款信用/免担保金额占比
                                        if (pcabBasicinfoseg.getLoanAmount() != null) {
                                            BigDecimal bigDecimal = new BigDecimal(pcabBasicinfoseg.getLoanAmount());
                                            if (null != bigDecimal) {
                                                labanNumss1 = labanNumss1.add(bigDecimal);
                                            }
                                        }
                                    }
                                }
                                if ((Constants.GUARANTY_4).equals(guaranteeMode) && accountType.equals(Constants.ACC_TYPE_R1)) {
                                    if (pcabBasicinfoseg.getAccountQuota() != null) {
                                        BigDecimal decimal = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                        accountQuotaNumss = accountQuotaNumss.add(decimal);

                                    }
                                }
                            }
                        }

                        if (accountType.equals(Constants.ACC_TYPE_D1)) {
                            if (pcabBasicinfoseg != null) {
                                if ((Constants.GUARANTY_5).equals(guaranteeMode) || (Constants.GUARANTY_6).equals(guaranteeMode)) {
                                    if (pcanFirinfseg.getAccStatus() != null) {
                                        if ((Constants.ACC_STAT_3) != pcanFirinfseg.getAccStatus() || (Constants.ACC_STAT_5) != pcanFirinfseg.getAccStatus()) {
                                            accStatusn2 = accStatusn2 + 1;
                                            if (pcabBasicinfoseg.getLoanAmount() != null) {
                                                BigDecimal decimal = new BigDecimal(pcabBasicinfoseg.getLoanAmount());
                                                if (null != decimal) {
                                                    labanNums2 = labanNums2.add(decimal);
                                                }
                                            }
                                        }

                                        if ((Constants.GUARANTY_7).equals(guaranteeMode)) {
                                            if (pcanFirinfseg.getAccStatus() != null) {
                                                if ((Constants.ACC_STAT_3) != pcanFirinfseg.getAccStatus() || (Constants.ACC_STAT_5) != pcanFirinfseg.getAccStatus()) {
                                                    accSta2 = accSta2 + 1;
                                                    if (pcabBasicinfoseg.getLoanAmount() != null) {
                                                        BigDecimal decimal = new BigDecimal(pcabBasicinfoseg.getLoanAmount());
                                                        if (null != decimal) {
                                                            labanNum2 = labanNum2.add(decimal);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if ((Constants.GUARANTY_4).equals(guaranteeMode)) {
                                            if (!(Constants.ACC_STAT_3).equals(pcanFirinfseg.getAccStatus()) && !(Constants.ACC_STAT_5).equals(pcanFirinfseg.getAccStatus())) {
                                                accNum2 = accNum2 + 1;
                                                if (pcabBasicinfoseg.getLoanAmount() != null) {
                                                    BigDecimal decimal = new BigDecimal(pcabBasicinfoseg.getLoanAmount());
                                                    if (null != decimal) {
                                                        labanNumss2 = labanNumss2.add(decimal);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


            if (null != labanNums1 && null != labanNums2) {
                labanNums = labanNums1.add(labanNums2);
            }
            accStatusn = accStatusn1 + accStatusn2;
            BigDecimal accStat = new BigDecimal(accStatusn);
            BigDecimal unSettl = new BigDecimal(un_settl_loan_num);
            if (unSettl.compareTo(BigDecimal.ZERO) == 1) {
                loan_zh_guan_num_rate = accStat.divide(unSettl, 3, RoundingMode.HALF_UP);
                loan_zh_guan_amt_rate = labanNums.divide(unSettl, 3, RoundingMode.HALF_UP);
            }
            //3个月内人民币信用卡发卡数
            zxPZbInfo.setLate3monIssueRmbCardNum(String.valueOf(late_3mon_issue_rmb_card_num));
            //正在使用信用卡账户数
            zxPZbInfo.setUseCreditCardNum(String.valueOf(use_credit_card_num));
            //未结清经营性贷款笔数
            zxPZbInfo.setUnSettlJyxLoanNum(String.valueOf(un_settl_jyx_loan_num));
            //未结清经营性贷款借款金额合计
            zxPZbInfo.setUnSettJyxLoanMoney(String.valueOf(un_sett_jyx_loan_money));
            //单张信用卡额度使用率
            zxPZbInfo.setPerCredCardUsedRate(String.valueOf(per_cred_card_used_rate));
            //34未结清非循环贷账户数
            zxPZbInfo.setUnSettFxhdAccountNum(String.valueOf(un_sett_fxhd_account_num));
            //35未结清循环额度下分账账户户数
            zxPZbInfo.setUnSettXhfAccountNum(String.valueOf(un_sett_xhf_account_num));
            //36未结清循环贷账户数
            zxPZbInfo.setUnSettXhdAccountNum(String.valueOf(un_sett_xhd_account_num));
            //不良余额比例
            zxPZbInfo.setBitdilmt(String.valueOf(bitdilmt));


            //个人指标4 授信总额（信用总额）
            BigDecimal credTotalAmt = un_sett_fxh_credit_amt.add(un_sett_xhf_credit_amt).add(un_clo_xh_credit_amt).add(un_clo_dj_credit_amt).add(un_clo_zdj_credit_amt);
            BigDecimal cred_total_amt = credTotalAmt.setScale(2, BigDecimal.ROUND_HALF_UP);
            zxPZbInfo.setCredTotalAmt(String.valueOf(cred_total_amt));


            //个人指标14 信用卡张数
            int cred_card_num = un_clo_dj_account_num + un_clo_zdj_account_num;
            zxPZbInfo.setCredCardNum(String.valueOf(cred_card_num));

            //个人指标15 信用卡总授信额度
            BigDecimal credCardTotalAmt = un_clo_dj_credit_amt.add(un_clo_zdj_credit_amt);
            BigDecimal cred_card_total_amt = credCardTotalAmt.setScale(2, BigDecimal.ROUND_HALF_UP);
            zxPZbInfo.setCredCardTotalAmt(String.valueOf(cred_card_total_amt));


            //个人基础指标19 最近3个月信用卡发数/总发卡数
            BigDecimal late_mon_cred_sum_rate = new BigDecimal("0.000");
            BigDecimal cred_card_use_frequ = new BigDecimal("0.000");
            BigDecimal useCredit = new BigDecimal(use_credit_card_num);
            BigDecimal lateMon = new BigDecimal(late_3mon_issue_rmb_card_num);
            BigDecimal credCard = new BigDecimal(cred_card_num);
            if (0 != cred_card_num) {
                if (credCard.compareTo(BigDecimal.ZERO) == 1) {
                    late_mon_cred_sum_rate = lateMon.divide(credCard, 3, RoundingMode.HALF_UP);
                    cred_card_use_frequ = useCredit.divide(credCard, 3, RoundingMode.HALF_UP);
                }
            }
            zxPZbInfo.setLateMonCredSumRate(String.valueOf(late_mon_cred_sum_rate));

            //个人基础指标20 信用卡使用率
            zxPZbInfo.setCredCardUseFrequ(String.valueOf(cred_card_use_frequ));


            //个人指标32 未结清经营性贷金额/未结清贷款金额
            BigDecimal loan_total_amt = un_sett_fxh_credit_amt.add(un_sett_xhf_credit_amt).add(un_clo_xh_credit_amt);
            BigDecimal un_sett_jyx_loan_amt_rate = new BigDecimal("0.00");
            if (loan_total_amt.compareTo(BigDecimal.ZERO) == 1) {
                un_sett_jyx_loan_amt_rate = un_sett_jyx_loan_money.divide(loan_total_amt, 2, RoundingMode.HALF_UP);
            }
            zxPZbInfo.setUnSettJyxLoanAmtRate(String.valueOf(un_sett_jyx_loan_amt_rate));

            //个人指标36 贷款总授信额度（未结清）
            zxPZbInfo.setLoanTotalAmt(String.valueOf(loan_total_amt));

            //个人指标37 贷款总余额
            BigDecimal loan_total_balan = un_sett_fxhd_all_balan.add(un_sett_xhf_all_balan).add(un_sett_xhd_prin_balan);
            zxPZbInfo.setLoanTotalBalan(String.valueOf(loan_total_balan));


            int addToal = un_sett_fxhd_account_num + un_sett_xhf_account_num + un_sett_xhd_account_num + un_clo_dj_account_num + un_clo_zdj_account_num;
            BigDecimal addToals = new BigDecimal(addToal);
            BigDecimal monthTotals = new BigDecimal(monthTotal);
            BigDecimal amfaot = new BigDecimal("0.000");
            if (monthTotals.compareTo(BigDecimal.ZERO) == 1) {
                amfaot = monthTotals.divide(addToals, 3, RoundingMode.HALF_UP);
            }

            zxPZbInfo.setAmfaot(String.valueOf(amfaot));


            //个人指标16 信用卡总余额
            BigDecimal cred_card_total_balan = un_clo_dj_used_amt.add(un_clo_zdj_used_amt);
            zxPZbInfo.setCredCardTotalBalan(String.valueOf(cred_card_total_balan));


            //个人基础指标17 当前信用卡额度使用率
            BigDecimal c_cred_card_use_frequ = new BigDecimal("0.000");
            if (cred_card_total_amt.compareTo(BigDecimal.ZERO) == 1) {
                c_cred_card_use_frequ = new BigDecimal(1.0).subtract(cred_card_total_balan.divide(cred_card_total_amt, 3, RoundingMode.HALF_UP));
            }
            zxPZbInfo.setCCredCardUseFrequ(String.valueOf(c_cred_card_use_frequ));


            //个人基础指标18 信用卡平均额度
            BigDecimal credCardNum = new BigDecimal(cred_card_num);
            BigDecimal cred_card_aver_amt = new BigDecimal("0.000");
            if (credCardNum.compareTo(BigDecimal.ZERO) == 1) {
                cred_card_aver_amt = cred_card_total_amt.divide(credCardNum, 3, RoundingMode.HALF_UP);
            }
            zxPZbInfo.setCredCardAverAmt(String.valueOf(cred_card_aver_amt));

            //个人指标79 贷款组合担保笔数占比
            zxPZbInfo.setLoanZhGuanNumRate(String.valueOf(loan_zh_guan_num_rate));

            //个人指标82  贷款组合担保金额占比
            zxPZbInfo.setLoanZhGuanAmtRate(String.valueOf(loan_zh_guan_amt_rate));


            //个人指标80 贷款农户联保笔数占比
            if (null != labanNum1 && null != labanNum2) {
                labanNum = labanNum1.add(labanNum2);
            }
            accSta = accSta1 + accSta2;
            BigDecimal accStas = new BigDecimal(accSta);
            BigDecimal unSettlLoan = new BigDecimal(un_settl_loan_num);
            if (unSettlLoan.compareTo(BigDecimal.ZERO) == 1) {
                loan_lb_guan_num_rate = accStas.divide(unSettlLoan, 3, RoundingMode.HALF_UP);
            }
            zxPZbInfo.setLoanLbGuanNumRate(String.valueOf(loan_lb_guan_num_rate));


            //个人指标83 贷款组合农户金额占比
            if (loan_total_amt.compareTo(BigDecimal.ZERO) == 1) {
                loan_lb_guan_amt_rate = labanNum.divide(loan_total_amt, 3, RoundingMode.HALF_UP);
            }
            //个人指标83 贷款组合农户金额占比
            zxPZbInfo.setLoanLbGuanAmtRate(String.valueOf(loan_lb_guan_amt_rate));


            //个人基础指标81 贷款信用/免担保笔数占比
            if (null != labanNumss1 && null != accountQuotaNumss) {
                labanNumss = labanNumss1.add(accountQuotaNumss);
            }
            accNum = accNum1 + accNum2;
            BigDecimal accNums = new BigDecimal(accNum);
            BigDecimal unSettlLoanNum = new BigDecimal(un_settl_loan_num);
            if (unSettlLoanNum.compareTo(BigDecimal.ZERO) == 1) {
                loan_xy_guan_num_rate = accNums.divide(unSettlLoanNum, 3, RoundingMode.HALF_UP);
            }
            zxPZbInfo.setLoanXyGuanNumRate(String.valueOf(loan_xy_guan_num_rate));


            //个人基础指标84 贷款信用/免担保金额占比
            if (loan_total_amt.compareTo(BigDecimal.ZERO) > 0) {
                loan_xy_guan_amt_rate = labanNumss.divide(loan_total_amt, 3, RoundingMode.HALF_UP);
            }
            zxPZbInfo.setLoanXyGuanAmtRate(String.valueOf(loan_xy_guan_amt_rate));

        } catch (Exception e) {
            throw new Exception(e);
        }
        return zxPZbInfo;
    }
}

package com.dhcc.cips.service.other;

import java.util.Set;

/**
 * <p>
 * 预警处理服务类
 * </p>
 *
 * @author limin
 * @since 2019-06-4
 */
public interface AlarmInfoService {

    /**
     * 预警信息查询
     * @param zb
     * @param userType
     * @return
     * @throws Exception
     */

    Set<String> getAlarmInfos(Object zb,String userType) throws Exception;
}

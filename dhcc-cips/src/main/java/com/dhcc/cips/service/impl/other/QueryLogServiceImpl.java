package com.dhcc.cips.service.impl.other;

import com.dhcc.cips.bo.base.Request;
import com.dhcc.cips.bo.base.ResponseHead;
import com.dhcc.cips.bo.base.ResponseSysHead;
import com.dhcc.cips.bo.base.ResponseSysHeadRet;
import com.dhcc.cips.bo.other.QueryLogResBo;
import com.dhcc.cips.bo.other.QueryLogResBodyBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.mapper.company.CeqResultInfoMapper;
import com.dhcc.cips.mapper.person.CpqResultInfoMapper;
import com.dhcc.cips.po.company.CeqResultInfoPo;
import com.dhcc.cips.po.person.CpqResultInfoPo;
import com.dhcc.cips.service.other.QueryLogService;
import com.dhcc.cips.util.ConvertUtil;
import com.dhcc.cips.util.XmlUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;

/**
 * @author limin
 * @Description 查询记录服务实现类
 * @Date 2019-3-27 16:28
 */
@Slf4j
@Service
public class QueryLogServiceImpl implements QueryLogService {

    @Resource
    private CpqResultInfoMapper cpqResultInfoMapper;
    @Resource
    private CeqResultInfoMapper ceqResultInfoMapper;


    @Override
    public String exec(Request request){

        //返回报文
        String retXml;
        //返回报文对象
        QueryLogResBo queryLogResBo = new QueryLogResBo();
        QueryLogResBodyBo queryLogResBodyBo = new QueryLogResBodyBo();

        //返回交易信息
        String retCode;//交易返回码
        String retMesaage;//交易返回信息
        String retStatus;//交易返回状态
        try {
            //获取请求参数
            String globalId = request.getRequestBody().getGlobalId();//证件号码
            String globalType = request.getRequestBody().getGlobalType();//证件类型
            //请求参数校验
            if (StringUtils.isEmpty(globalType)) {
                throw new Exception(Constants.GLOBAL_TYPE_NULL_ANO);//证件类型不能为空
            }
            if (StringUtils.isEmpty(globalId)) {
                throw new Exception(Constants.GLOBAL_ID_NULL_ANO);//证件号码不能为空
            }

            //根据证件号码，证件类型查询最新记录
            HashMap<Object, Object> map = Maps.newHashMap();
            if (globalType.startsWith("B")) {//个人
                map.put("globalType", ConvertUtil.getPersonGlobalType(globalType));
                map.put("globalId", globalId);
                CpqResultInfoPo cpqResultInfoPo = cpqResultInfoMapper.selectByCertId(map);
                if (cpqResultInfoPo == null) {
                    throw new Exception(Constants.QUERY_RECORD_NULL);//此客户无征信查询记录
                }
                //填充返回体
                queryLogResBodyBo.setUserId(cpqResultInfoPo.getOperator());//客户经理工号
                queryLogResBodyBo.setUserName("");//客户经理名称
                queryLogResBodyBo.setOrgId(cpqResultInfoPo.getOperOrg());//客户经理机构号
                queryLogResBodyBo.setOrgName(cpqResultInfoPo.getOperOrgName());//客户经理机构名称
                queryLogResBodyBo.setAccount(cpqResultInfoPo.getPassword());//查询人口令
                queryLogResBodyBo.setQueryDate(new Date());//查询日期  --待处理
                queryLogResBodyBo.setQueryReson(cpqResultInfoPo.getQueryreasonId());//查询原因

            } else if (globalType.startsWith("A")) {//企业
                //中证码
                String loancardCode = request.getRequestBody().getGlobalId();
                map.put("loancardCode", loancardCode);
                CeqResultInfoPo ceqResultInfoPo = ceqResultInfoMapper.selectByLoancardNo(map);
                if (ceqResultInfoPo == null) {
                    throw new Exception(Constants.QUERY_RECORD_NULL);//此客户无征信查询记录
                }
                //填充返回体
                queryLogResBodyBo.setUserId(ceqResultInfoPo.getOperator());//客户经理工号
                queryLogResBodyBo.setUserName("");//客户经理名称
                queryLogResBodyBo.setOrgId(ceqResultInfoPo.getOperOrg());//客户经理机构号
                queryLogResBodyBo.setOrgName(ceqResultInfoPo.getOperOrgName());//客户经理机构名称
                queryLogResBodyBo.setAccount(ceqResultInfoPo.getPassword());//查询人口令
                queryLogResBodyBo.setQueryDate(new Date());//查询日期  --待处理
                queryLogResBodyBo.setQueryReson(ceqResultInfoPo.getQueryreasonId());//查询原因s
            } else {
                throw new Exception("证件类型：" + globalType + "非法");
            }
            //交易返回码
            retCode = Constants.SUCCESS_CODE;
            //交易返回信息
            retMesaage = "交易成功";
            //交易返回状态
            retStatus = Constants.SUCCESS_S;
        } catch (Exception e) {
            log.error("", e);
            //交易返回码
            retCode = Constants.FAIL_CODE;
            //交易返回信息
            retMesaage = "交易失败，" + e.getMessage();
            //交易返回状态
            retStatus = Constants.FAIL_F;
        }


        //设置返回对象参数
        ResponseHead responseHead = new ResponseHead();
        ResponseSysHead responseSysHead = new ResponseSysHead();
        BeanUtils.copyProperties(request.getRequestSysHead(), responseSysHead);
        //返回状态码
        responseSysHead.setRetStatus(retStatus);
        responseHead.setResponseSysHead(responseSysHead);
        ResponseSysHeadRet responseSysHeadRet = new ResponseSysHeadRet();
        //交易返回码
        responseSysHeadRet.setRetCode(retCode);
        //交易返信息
        responseSysHeadRet.setRetMsg(retMesaage);
        responseHead.getResponseSysHead().setResponseSysHeadRet(responseSysHeadRet);

        //填充返回体
        queryLogResBo.setQueryLogResBodyBo(queryLogResBodyBo);

        //转xml
        retXml = XmlUtil.converJavaBean2Xml(queryLogResBo, queryLogResBo.getClass());

        return retXml;
    }
}

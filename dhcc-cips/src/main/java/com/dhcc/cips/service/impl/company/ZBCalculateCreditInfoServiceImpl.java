package com.dhcc.cips.service.impl.company;

import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.CeqReportBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.debaccountinform.EsalLeAccInform;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.debaccountinform.EsalLeAccRepayInform;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.debaccountinform.EsalLeAccRepayInformSub;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.guaaccount.EdgbGuarbasicinfoseg;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.guaaccount.EdgrGuarresponinfoseg;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.debaccountinform.entity.DebitAccountInformDataBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.debaccountinform.entity.LeAccInformUnitBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.guaaccount.entity.GuarAccoInfoBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.guaaccount.entity.GuarAccoInfoDataBo;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dhcc.cips.bo.company.CompanyExtGuarantyInfoBo;
import com.dhcc.cips.bo.company.CompanyLoanInfoBo;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.dhcc.cips.service.company.CompanyZBThreadService;
import com.dhcc.cips.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 信贷记录明细--未结清+已结清信贷
 */
@Service
@Slf4j
public class ZBCalculateCreditInfoServiceImpl implements CompanyZBThreadService {

    @Override
    public CompanyZBInfoPo companyCalculate(CeqReportBo ceqReportBo) throws Exception {
        CompanyZBInfoPo companyZBInfoPo = new CompanyZBInfoPo();
        try {
            if (null == ceqReportBo) {
                return null;
            }
            //开立时间为最近6个月的账户的“授信机构”去重总数
            Set<String> late6monOrgPassNum = new HashSet<>();
            //开立时间为最近12个月的账户的“授信机构”去重总数
            Set<String> late12monOrgPassNum = new HashSet<>();
            //信贷记录明细--所有账户授信机构不为“本机构”的机构数去重
            Set<String> othBankBusiOrgNum = new HashSet<>();
            //业务种类去重总数
            Set<String> credLoanTypeNum = new HashSet<>();
            //报告日期
            Date createTime = ceqReportBo.getRepheadBo().getRepheadUnitBo().getEhhrReportIdentity().getReportTime();
            Date before6MonthDate = null;
            Date before12MonthDate = null;
            if (createTime != null) {
                before6MonthDate = DateUtil.getAimMonthDate(createTime, -6);//报告日期向前推6个月
                before12MonthDate = DateUtil.getAimMonthDate(createTime, -12);//报告日期向前推12个月
            }
            DebitAccountInformDataBo debitAccountInformDataBo = ceqReportBo.getDebitAccountInformDataBo();
            GuarAccoInfoDataBo guarAccoInfoDataBo = ceqReportBo.getGuarAccoInfoDataBo();
            if (null != debitAccountInformDataBo) {
                List<LeAccInformUnitBo> leAccInformUnitBoList = debitAccountInformDataBo.getLeAccInformUnitBoList();
                //贷款信息入库json
//            JSONObject loanInfoJsonObject = new JSONObject();
//            List<CompanyLoanInfoBo> companyLoanInfoBoList = new ArrayList<>();
                if (leAccInformUnitBoList != null && leAccInformUnitBoList.size() > 0) {
                    for (LeAccInformUnitBo leAccInformUnitBo : leAccInformUnitBoList) {
                        //基本信息段  ED01A
                        EsalLeAccInform esalLeAccInform = leAccInformUnitBo.getEsalLeAccInform();
//                    CompanyLoanInfoBo companyLoanInfoBo = new CompanyLoanInfoBo();
                        if (esalLeAccInform != null) {
                            //业务管理 机构代码  //银行名称
                            String bmOrgNumber = esalLeAccInform.getBmOrgNumber();
//                        companyLoanInfoBo.setBankName(bmOrgNumber);
                            //贷款金额
//                        BigDecimal loanAmount = esalLeAccInform.getLoanAmount();
//                        companyLoanInfoBo.setLoanAmt(String.valueOf(loanAmount));
                            //开立时间  //贷款发放日期
                            Date openDate = esalLeAccInform.getOpenDate();
//                        companyLoanInfoBo.setLoanDateGrant(DateUtil.dateYmdFormat(openDate));
                            //贷款到期日期
//                        Date dueDate = esalLeAccInform.getDueDate();
//                        companyLoanInfoBo.setLoanDateMature(DateUtil.dateYmdFormat(dueDate));
                            //查询日期
//                        Date reportInformDate = esalLeAccInform.getReportInformDate();
//                        companyLoanInfoBo.setQueryDate(DateUtil.dateYmdFormat(reportInformDate));
                            //业务种类去重总数 //信贷业务种类数
                            String categoryMax = esalLeAccInform.getCategoryMax();
//                        companyLoanInfoBo.setLoanBussVrtyCnt(categoryMax);
                            //币种
//                        String currency = esalLeAccInform.getCurrency();
//                        companyLoanInfoBo.setCcy(currency);
//------------------------------------------------------------------------------------------------------------------------
                            credLoanTypeNum.add(categoryMax);
                            if (openDate != null) {
                                //报告日期-6 < before 关闭日期
                                //报告日期向前推6个月
                                if (before6MonthDate != null && before6MonthDate.before(openDate)) {
                                    late6monOrgPassNum.add(bmOrgNumber);
                                }
                                //报告日期-12 < before 关闭日期
                                //报告日期向前推12个月
                                if (before12MonthDate != null && before12MonthDate.before(openDate)) {
                                    late12monOrgPassNum.add(bmOrgNumber);
                                }
                            }
                            if (StringUtils.isNotBlank(bmOrgNumber)) {
                                othBankBusiOrgNum.add(bmOrgNumber);
                            }
                        }
//------------------------------------------------------------------------------------------------------------------------
                        //还款表现信息段  ED01B
//                    EsalLeAccRepayInform esalLeAccRepayInform = leAccInformUnitBo.getEsalLeAccRepayInform();
//                    if (esalLeAccRepayInform != null) {
//                        List<EsalLeAccRepayInformSub> esalLeAccRepayInformSubList = esalLeAccRepayInform.getEsalLeAccRepayInformSubList();
//                        if (esalLeAccRepayInformSubList != null) {
//                            //根据日期逆序
//                            List<EsalLeAccRepayInformSub> list = esalLeAccRepayInformSubList.stream().sorted(Comparator.comparing(EsalLeAccRepayInformSub::getReportInformDate).reversed()).collect(Collectors.toList());
//                            EsalLeAccRepayInformSub esalLeAccRepayInformSub = list.get(0);
//                            BigDecimal balance = esalLeAccRepayInformSub.getBalance();//贷款余额
//                            companyLoanInfoBo.setLoanBalance(String.valueOf(balance));
//                        }
//                    }
//                    companyLoanInfoBoList.add(companyLoanInfoBo);
                    }
//                loanInfoJsonObject.put("companyLoanInfoBoList", companyLoanInfoBoList);
                    //担保类信息
                    if (null != guarAccoInfoDataBo) {
                        List<GuarAccoInfoBo> guarAccoInfoBoList = guarAccoInfoDataBo.getGuarAccoInfoBoList();
//                      List<CompanyExtGuarantyInfoBo> companyExtGuarantyInfoBoList = new ArrayList<>();
                        if (guarAccoInfoBoList != null && guarAccoInfoBoList.size() > 0) {
                            for (GuarAccoInfoBo guarAccoInfoBo : guarAccoInfoBoList) {
                                CompanyExtGuarantyInfoBo companyExtGuarantyInfoBo = new CompanyExtGuarantyInfoBo();
                                EdgbGuarbasicinfoseg edgbGuarbasicinfoseg = guarAccoInfoBo.getEdgbGuarbasicinfoseg();
                                EdgrGuarresponinfoseg edgrGuarresponinfoseg = guarAccoInfoBo.getEdgrGuarresponinfoseg();
                                if (null != edgrGuarresponinfoseg) {
                                    //担保余额
                                    BigDecimal balance = edgrGuarresponinfoseg.getBalance();
                                    companyExtGuarantyInfoBo.setWrntBal(String.valueOf(balance));
                                }
                                if (null != edgbGuarbasicinfoseg) {
                                    //业务种类去重总数
                                    String guarTransBusiSubdi = edgbGuarbasicinfoseg.getGuarTransBusiSubdi();
                                    credLoanTypeNum.add(guarTransBusiSubdi);
                                    Date openDate = edgbGuarbasicinfoseg.getOpenDate();
                                    String busManagerCode = edgbGuarbasicinfoseg.getBusManagerCode();//业务管理机构代码
                                    //到期日期
//                            Date endDate = edgbGuarbasicinfoseg.getEndDate();
                                    //companyExtGuarantyInfoBo.setWrntEndDt(DateUtil.dateYmdFormat(endDate));
                                    //担保金额
//                            BigDecimal amount = edgbGuarbasicinfoseg.getAmount();
                                    // companyExtGuarantyInfoBo.setWrntTotAmt(String.valueOf(amount));
                                    //companyExtGuarantyInfoBoList.add(companyExtGuarantyInfoBo);
                                    if (null != openDate && StringUtils.isNotBlank(busManagerCode)) {
                                        //报告日期-6 < before 关闭日期
                                        //报告日期向前推6个月
                                        if (before6MonthDate != null && before6MonthDate.before(openDate)) {
                                            late6monOrgPassNum.add(busManagerCode);
                                        }
                                        //报告日期-12 < before 关闭日期
                                        //报告日期向前推12个月
                                        //update by StivenYang 2019年5月24日09:24:53  最近12个月应该是在开立日期之后
                                        if (before12MonthDate != null && before12MonthDate.before(openDate)) {
                                            late12monOrgPassNum.add(busManagerCode);
                                        }
                                    }
                                    if (StringUtils.isNotBlank(busManagerCode)) {
                                        othBankBusiOrgNum.add(busManagerCode);
                                    }
                                }
                            }
                        }
                    }
//                loanInfoJsonObject.put("companyExtGuarantyInfoBoList", companyExtGuarantyInfoBoList);
                    //贷款信息
//                companyZBInfoPo.setComLoanInfo("");
                    //7.信贷业务种类数  信贷记录明细--未结清信贷 + 已结清信贷--业务种类去重总数
                    companyZBInfoPo.setCredLoanTypeNum(String.valueOf(credLoanTypeNum.size()));
                    // 112. 信贷记录明细--所有账户授信机构不为“本机构”的机构数去重
                    companyZBInfoPo.setOthBankBusiOrgNum(String.valueOf(othBankBusiOrgNum.size()));
                    // 123.信贷记录明细-- 1 未结清信贷+已结清信贷--开立时间为最近6个月的账户的“授信机构”去重总数
                    companyZBInfoPo.setLate6monOrgPassNum(String.valueOf(late6monOrgPassNum.size()));
                    //124.信贷记录明细-- 1 未结清信贷+已结清信贷--开立时间为最近12个月的账户的“授信机构”去重总数
                    companyZBInfoPo.setLate12monOrgPassNum(String.valueOf(late12monOrgPassNum.size()));
                }
            }
        } catch (Exception e) {
            log.error("CompanyZBCreditDetailServiceImpl", e);
            throw new Exception(e);
        }
        return companyZBInfoPo;
    }
}

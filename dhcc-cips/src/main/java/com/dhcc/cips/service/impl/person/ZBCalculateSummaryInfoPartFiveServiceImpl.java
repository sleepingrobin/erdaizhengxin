package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PiccCominformation;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PiccCominformationsub;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.publicinformsum.entity.PubInfSummaryBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author StivenYang
 * @description 二（信息概要）-五
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateSummaryInfoPartFiveServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo reportbo) throws Exception {
        PersonZBInfoPo personZBInfoPo = new PersonZBInfoPo();
        try {

            //公共信息概要(PPO)
            PubInfSummaryBo pubInfSummaryBo = reportbo.getPubInfSummaryBo();
            //公共信息概要信息单元(PC04)
            PiccCominformation piccCominformation = null;
            if (pubInfSummaryBo != null) {
                piccCominformation = pubInfSummaryBo.getPiccCominformation();
            }

            //167 强制执行记录条数
            int force_do_record_num = 0;
            //168 欠税记录数
            int owing_taxes_record_num = 0;
            //169 民事判决记录数
            int civil_judgement_record_num = 0;
            //170 行政处罚记录数
            int admin_punish_record_num = 0;


            //信息概要
            if (null != piccCominformation) {
                List<PiccCominformationsub> piccCominformationsubList = piccCominformation.getPiccCominformationsubList();
                if (piccCominformationsubList != null && piccCominformationsubList.size() > 0) {
                    for (PiccCominformationsub subList : piccCominformationsubList) {
                        String pubinfoType = subList.getPubinfoType();
                        if ( null != pubinfoType) {
                            String recordNum = subList.getRecordNum();
                            if (StringUtils.isNotEmpty(recordNum)) {
                                if ((Constants.PUBLICINFO_3).equals(pubinfoType)) {
                                    force_do_record_num = Integer.parseInt(subList.getRecordNum());
                                }
                                //欠税信息
                                if ((Constants.PUBLICINFO_1).equals(pubinfoType)) {
                                    owing_taxes_record_num = Integer.parseInt(subList.getRecordNum());
                                }
                                //民事判决记录数
                                if ((Constants.PUBLICINFO_2).equals(pubinfoType)) {
                                    civil_judgement_record_num = Integer.parseInt(subList.getRecordNum());
                                }
                                if ((Constants.PUBLICINFO_4).equals(pubinfoType)) {
                                    admin_punish_record_num = Integer.parseInt(subList.getRecordNum());
                                }
                            }
                        }
                    }
                }
            }
            //强制执行信息
            personZBInfoPo.setForceDoRecordNum(String.valueOf(force_do_record_num));
           //欠税记录数
            personZBInfoPo.setOwingTaxesRecordNum(String.valueOf(owing_taxes_record_num));
            //民事判决记录数
            personZBInfoPo.setCivilJudgementRecordNum(String.valueOf(civil_judgement_record_num));
            // 行政处罚记录数
            personZBInfoPo.setAdminPunishRecordNum(String.valueOf(admin_punish_record_num));
        } catch (Exception e) {
            throw new Exception(e);
        }
        return personZBInfoPo;
    }
}

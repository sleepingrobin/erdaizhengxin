package com.dhcc.cips.service.impl.other;

import com.dhcc.cips.bo.base.*;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.service.company.CompanyReportFileService;
import com.dhcc.cips.service.company.CompanyReportQueryService;
import com.dhcc.cips.service.company.CompanyZBInfoService;
import com.dhcc.cips.service.other.CooperationService;
import com.dhcc.cips.service.other.EsbService;
import com.dhcc.cips.service.other.QueryLogService;
import com.dhcc.cips.service.person.PersonReportFileService;
import com.dhcc.cips.service.person.PersonReportQueryService;
import com.dhcc.cips.service.person.PersonZBInfoService;
import com.dhcc.cips.util.ValidateUtil;
import com.dhcc.cips.util.XmlUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebService;

@Slf4j
@Service
@WebService
public class EsbServiceImpl implements EsbService {

    @Autowired
    private PersonZBInfoService personZBInfoService;
    @Autowired
    private CompanyZBInfoService companyZBInfoService;
    @Autowired
    private PersonReportQueryService personReportQueryService;
    @Autowired
    private CompanyReportQueryService companyReportQueryService;
    @Autowired
    private QueryLogService queryLogService;
    @Autowired
    private PersonReportFileService personReportFileService;
    @Autowired
    private CompanyReportFileService companyReportFileService;
    @Autowired
    private CooperationService cooperationService;

    @Override
    public String exec(String esbXml) {
        //请求对象
        Request request = null;
        //返回报文
        String retXml = "";

        try {
            log.info("接收报文：" + esbXml);

            //请求参数合法性校验
            request = XmlUtil.convertXml2JavaBean(Request.class, esbXml, new Class[]{Request.class, RequestHead.class});
            if (request == null) {
                throw new Exception("请求报文不能为空");
            }
            if (request.getRequestSysHead() == null) {
                throw new Exception("请求报文SYS_HEAD不能为空");
            }
            if (request.getRequestAppHead() == null) {
                throw new Exception("请求报文APP_HEAD不能为空");
            }
            if (request.getRequestBody() == null) {
                throw new Exception("请求报文体不能为空");
            }

            //校验错误信息
            String validMessage;
            //校验SYS_HEA
            validMessage = ValidateUtil.commonValidate(request.getRequestSysHead());
            if (!StringUtils.isEmpty(validMessage)) {
                throw new Exception(validMessage);
            }
            //校验APP_HEAD
            validMessage = ValidateUtil.commonValidate(request.getRequestAppHead());
            if (!StringUtils.isEmpty(validMessage)) {
                throw new Exception(validMessage);
            }

            //服务编码
            String serviceCode = request.getRequestSysHead().getServiceCode();
            //场景码
            String serviceScene = request.getRequestSysHead().getServiceScene();

            if (StringUtils.isEmpty(serviceCode)) {
                throw new Exception("服务编码不能为空");
            }
            if (StringUtils.isEmpty(serviceScene)) {
                throw new Exception("场景码不能为空");
            }

            //ESB服务路由
            if ("13002000008".equals(serviceCode) && "01".equals(serviceScene)) {
                //征信系统登录(13002000008)
                //个人征信系统登录查询(01)
                retXml = personReportQueryService.exec(request);
            }
            if ("13002000008".equals(serviceCode) && "02".equals(serviceScene)) {
                //征信系统登录(13002000008)
                //企业征信系统登录查询(02)
                retXml = companyReportQueryService.exec(request);
            }
            if ("13003000007".equals(serviceCode) && "01".equals(serviceScene)) {
                //客户征信信息查询(13003000007)
                //个人征信指标查询(01)
                retXml = personZBInfoService.exec(request);
            }
            if ("13003000007".equals(serviceCode) && "04".equals(serviceScene)) {
                //客户征信信息查询
                //企业征信指标查询(04)(13003000007)
                retXml = companyZBInfoService.exec(request);
            }
            if ("11003000058".equals(serviceCode) && "01".equals(serviceScene)) {
                //查询记录查询(11003000058)
                //客户经理查询客户征信报告最新记录查询(01)
                retXml = queryLogService.exec(request);
            }
            if ("13005000001".equals(serviceCode) && "01".equals(serviceScene)) {
                //征信文件传输(13005000001)
                //个人征信报告下传(01)
                retXml = personReportFileService.download(request);
            }
            if ("13005000001".equals(serviceCode) && "02".equals(serviceScene)) {
                //征信文件传输(13005000001)
                //企业征信报告下传(02)
                retXml = companyReportFileService.download(request);
            }
            if ("13002000008".equals(serviceCode) && "99".equals(serviceScene)) {
                //合作行个人征信报告解析服务（金融云直连，未经过ESB）
                retXml = cooperationService.reportParse(request);
            }

        } catch (Exception e) {
            log.error("", e);
            //交易返回对象
            ResponseHead responseHead = new ResponseHead();
            ResponseSysHead responseSysHead = new ResponseSysHead();
            //填充返回头信息
            BeanUtils.copyProperties(responseSysHead, request.getRequestSysHead());

            ResponseSysHeadRet responseSysHeadRet = new ResponseSysHeadRet();
            //交易返回码
            responseSysHeadRet.setRetCode(Constants.FAIL_CODE);
            //交易返回信息
            responseSysHeadRet.setRetMsg(e.getMessage());
            responseHead.getResponseSysHead().setResponseSysHeadRet(responseSysHeadRet);
            //交易返回状态
            responseSysHead.setRetStatus(Constants.FAIL_F);
            responseHead.setResponseSysHead(responseSysHead);
            retXml = XmlUtil.converJavaBean2Xml(responseHead, responseHead.getClass());
        }

        log.info("反馈报文：" + retXml);
        return retXml;
    }
}

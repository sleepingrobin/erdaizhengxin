package com.dhcc.cips.service.impl.other;


import com.dhcc.cips.bo.other.AlarmInfoBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.mapper.other.AlarmConfigInfoMapper;
import com.dhcc.cips.mapper.other.AlarmItemInfoMapper;
import com.dhcc.cips.po.other.AlarmConfigInfoPo;
import com.dhcc.cips.po.other.AlarmItemInfoPo;
import com.dhcc.cips.service.other.AlarmInfoService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * 预警处理服务实现类
 */
@Slf4j
@Service
public class AlarmInfoServiceImpl implements AlarmInfoService {

    @Resource
    AlarmConfigInfoMapper alarmConfigInfoMapper;
    @Resource
    AlarmItemInfoMapper alarmItemInfoMapper;


    @Override
    public Set<String> getAlarmInfos(Object zb,String userType) {

        try {
            //
            List<AlarmInfoBo> alarmInfoBoList = new ArrayList<>();
            //
            List<AlarmConfigInfoPo> alarmConfigInfoList = alarmConfigInfoMapper.selectByUserType(userType);

            if (alarmConfigInfoList.isEmpty()) {
                throw new Exception("取预警信息异常");
            }

            Object obj;//指标PO对象（个人和企业）
            Set<String> alarmSet = new HashSet<>();//已触发预警规则

            //初始化预警信息
            for (AlarmConfigInfoPo alarmConfigInfoPo : alarmConfigInfoList) {
                List<AlarmItemInfoPo> alarmItemInfoPoList = alarmItemInfoMapper.selectByCId(alarmConfigInfoPo.getCId());
                if (alarmItemInfoPoList.isEmpty()) {
                    throw new Exception("取预警规则异常");
                }
                //预警信息
                AlarmInfoBo alarmInfoBo = new AlarmInfoBo();
                alarmInfoBo.setAlarmConfigInfoPo(alarmConfigInfoPo);
                alarmInfoBo.setAlarmItemInfoPoList(alarmItemInfoPoList);
                alarmInfoBoList.add(alarmInfoBo);

                for (AlarmItemInfoPo alarmItemInfoPo : alarmItemInfoPoList) {

                    obj = getFieldValue(zb, alarmItemInfoPo.getAlarmItem());

                    if (compare(obj, alarmItemInfoPo.getAlarmCond(), alarmItemInfoPo.getAlarmValue())) {

                        alarmSet.add(alarmConfigInfoPo.getAlarmName());
                    }
                }
            }

            return alarmSet;

        } catch (Exception e) {
            log.error("getAlarmInfos", e);
            return null;
        }
    }


    /**
     * 根据个人（企业）PO的字段名得到该字段的值；
     *
     * @param field
     * @return Object - ‘值’对象, 如果是null, 转成Double 0;
     */
    private Object getFieldValue(Object object, String field){

        if(object == null || StringUtils.isEmpty(field)) {
            return null;
        }

        Method method;
        Object retObject = null;
        String getMethod = null;

        try {
            Class<?> c =  object.getClass();

            /*
             * 该字段的第二字母 — 如果是大写，直接相加得到get方法； 如果是小写，则需首字母大写后相加得到。
             */
            char ch = field.charAt(0);

            if (Character.isUpperCase(ch)){
                getMethod = "get" + field;
            }else{
                getMethod = "get" + field.substring(0,1).toUpperCase() + field.substring(1);
            }

            method = (Method) c.getDeclaredMethod(getMethod, new Class[] {});
            retObject = method.invoke(object, new Object[] {});

            if(retObject == null)
                retObject = new Double(0);

        } catch (Exception e) {
            log.error("[getFieldValue] is error", e);
        }

        return retObject;
    }

    /**
     * 根据预警规则判断是否预警
     *
     * @param obj
     * @param alarmCond
     * @param alarmValue
     * @return boolean  true - 预警；
     * 					false - 不预警。
     */
    private boolean compare(Object obj, String alarmCond, String alarmValue){

        if(StringUtils.isEmpty(alarmValue) || StringUtils.isEmpty(alarmCond))
            return false;

        if(obj != null && obj.toString().contains(Constants.NO_ALARM_VALUE))
            return false;

        if(obj instanceof Double){
            Double d = (Double) obj;

            if(Constants.ALARM_COND_0.equals(alarmCond)){
                return d.compareTo(new Double(alarmValue)) < 0;
            }
            if(Constants.ALARM_COND_1.equals(alarmCond)){
                return d.compareTo(new Double(alarmValue)) == 0;
            }
            if(Constants.ALARM_COND_2.equals(alarmCond)){
                return d.compareTo(new Double(alarmValue)) > 0;
            }
            if(Constants.ALARM_COND_21.equals(alarmCond)){
                return d.compareTo(new Double(alarmValue)) >= 0;
            }
            if(Constants.ALARM_COND_10.equals(alarmCond)){
                return d.compareTo(new Double(alarmValue)) <= 0;
            }
        }
        if(obj instanceof Integer){
            Integer in = (Integer) obj;

            if(Constants.ALARM_COND_0.equals(alarmCond)){
                return in < Integer.valueOf(alarmValue);
            }
            if(Constants.ALARM_COND_1.equals(alarmCond)){
                return in == Integer.valueOf(alarmValue);
            }
            if(Constants.ALARM_COND_2.equals(alarmCond)){
                return in > Integer.valueOf(alarmValue);
            }
            if(Constants.ALARM_COND_21.equals(alarmCond)){
                return in >= Integer.valueOf(alarmValue);
            }
            if(Constants.ALARM_COND_10.equals(alarmCond)){
                return in <= Integer.valueOf(alarmValue);
            }
        }

        return false;
    }
}

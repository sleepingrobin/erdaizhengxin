package com.dhcc.cips.service.impl.company;

import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.CeqReportBo;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.dhcc.cips.service.company.CompanyParseService;
import com.dhcc.cips.service.company.CompanyReportParseService;
import com.dhcc.cips.service.other.AlarmInfoService;
import com.dhcc.cips.service.other.CompanyReportAnalysisService;
import com.dhcc.cips.service.other.PersonReportAnalysisService;
import com.dhcc.cips.util.XmlUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * <p>
 * 企业征信报告解析服务实现类
 * </p>
 *
 * @author limin
 * @since 2018-05-31
 */
@Service
@Slf4j
public class CompanyReportParseServiceImpl implements CompanyReportParseService {
    @Autowired
    private CompanyZBCalculateProcessor companyZBCalculateProcessor;
    @Autowired
    private CompanyReportAnalysisService companyReportAnalysisService;

    /**
     * 解析xml版征信报告
     *
     * @param xmlStr
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void parseWithXml(String xmlStr) throws Exception {
        try {
            xmlStr = xmlStr.replaceAll("<EH010R01/>", "");
            CeqReportBo ceqReportBo = XmlUtil.convertXml2JavaBean(CeqReportBo.class, xmlStr, new Class[]{CeqReportBo.class});
            //指标计算
            zbCalculate(ceqReportBo);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    /**
     * 解析html版征信报告
     *
     * @param htmlStr
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void parseWithHtml(String htmlStr) throws Exception {
        try {
//            CeqReportBo ceqReportbo = (CeqReportBo) companyParseService.analysisEnterpriseReport(htmlStr);
            String s = companyReportAnalysisService.reportAnalysis(htmlStr);
            parseWithXml(s.replaceAll("--", "0"));
        } catch (Exception e) {
            throw new Exception(e);
        }
    }


    /**
     * 企业指标计算入库
     *
     * @param ceqReportBo
     */
    private void zbCalculate(CeqReportBo ceqReportBo) throws Exception {
        try {
            //企业报告编号
            String reportNo = ceqReportBo.getRepheadBo().getRepheadUnitBo().getEhhrReportIdentity().getReportId();
            CompanyZBInfoPo companyZBInfoPo = companyZBCalculateProcessor.calculate(ceqReportBo);
            companyZBInfoPo.setReportNo(reportNo);
            //指标信息入库
            companyZBInfoPo.insert();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
    }

}
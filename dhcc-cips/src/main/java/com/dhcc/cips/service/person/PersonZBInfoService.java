package com.dhcc.cips.service.person;

import com.dhcc.cips.bo.base.Request;

/**
 * @date 2019-3-27
 * @author YangJiaheng
 * @description 个人征信指标查询接口
 */
public interface PersonZBInfoService {

    /**
     * 个人征信指标查询，业务场景代码为：13003000007
     * @param param 场景代码
     * @return 返回xml字符串
     * @throws Exception 1
     */
    String exec(Request param);

}

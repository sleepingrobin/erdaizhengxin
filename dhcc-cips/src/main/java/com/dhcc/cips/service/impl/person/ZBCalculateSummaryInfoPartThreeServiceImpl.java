package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.*;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditproinform.entity.CreditProInfBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.credittransinfdata.entity.CreTraInfSummaryBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.credittransinfdata.entity.CreTraInfSummaryUnitBo;
import com.baomidou.mybatisplus.generator.config.IFileCreate;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

/**
 * @author StivenYang
 * @description 二（信息概要）-三
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateSummaryInfoPartThreeServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo reportbo) throws Exception {
        PersonZBInfoPo personZBInfoPo = new PersonZBInfoPo();
        try {
            //信贷交易信息概要(PCO)
            CreTraInfSummaryBo creTraInfSummaryBo = reportbo.getCreTraInfSummaryBo();
            //信贷交易信息概要信息单元(PC02)
            CreTraInfSummaryUnitBo creTraInfSummaryUnitBo=null;
            if (creTraInfSummaryBo!=null&&creTraInfSummaryBo.getCreTraInfSummaryUnitBo()!=null){
                creTraInfSummaryUnitBo = creTraInfSummaryBo.getCreTraInfSummaryUnitBo();
            }


            //信用卡最近6个月平均透支率
            BigDecimal c_c_6m_avg_use_rate = new BigDecimal("0.000");
            //144 保证人代偿笔数
            int gu_comp_num = 0;
            //145保证人代偿余额
            BigDecimal gu_comp_balan = new BigDecimal("0.00");
            //72 贷款最近6个月平均还款金额
            BigDecimal loan_6_mon_aver_rp_amt = new BigDecimal("0.00");
            //73信用卡最近6个月使用金额
            BigDecimal cc_l_6_mon_aver_use_amt = new BigDecimal("0.00");
            //78对外担保笔数
            int ext_guar_num = 0;
            //7 对外担保金额
            BigDecimal guar_contr_amt = new BigDecimal("0.00");
            //8 对外担保本金余额
            BigDecimal guar_contr_balan = new BigDecimal("0.00");


            //定义变量
            BigDecimal meanSix=new BigDecimal("0.00");//贷记卡最近6个月平均使用额度
            BigDecimal cardSix=new BigDecimal("0.00");//准贷记卡最近6个月平均透支额度
            BigDecimal fxhAveBalance = new BigDecimal("0.00");//非循环账户最近6个月平均还贷款
            BigDecimal xhfAveBalance = new BigDecimal("0.00");//循环额度分账户最近6个月平均贷款
            BigDecimal xhdAveBalance = new BigDecimal("0.00");//循环贷款账户最近6个月平均贷款
            BigDecimal quasi=new BigDecimal("0.00");//准贷记卡授信总额
            BigDecimal credits=new BigDecimal("0.00");//贷记卡授信总额
            BigDecimal djAveBalance = new BigDecimal("0.00");//贷记卡账户已用额度
            BigDecimal zdjAveBalance = new BigDecimal("0.00");//准贷记卡账户已用额度
            BigDecimal fxhAveAccount = new BigDecimal("0.00");//非循环贷账户数
            BigDecimal xhfAveAccount = new BigDecimal("0.00");//循环额度下分账户账户数
            BigDecimal xhdAveAccount = new BigDecimal("0.00");//循环贷账户账户数
            BigDecimal cardAccount = new BigDecimal("0.00");//准贷记卡账户数
            BigDecimal meanAccount = new BigDecimal("0.00");//贷记卡账户数


            if (creTraInfSummaryBo!= null && creTraInfSummaryUnitBo!= null) {
                //贷记卡账户汇总信息段(PC02H)
                PiccCreditaccinfo piccCreditaccinfo = creTraInfSummaryUnitBo.getPiccCreditaccinfo();
                //准贷记卡账户汇总信息段(PC02I)
                PicqQucreditaccinfo picqQucreditaccinfo = creTraInfSummaryUnitBo.getPicqQucreditaccinfo();
                //非循环账户汇总信息段(PC02E)
                PicnNoloaninfo picnNoloaninfo = creTraInfSummaryUnitBo.getPicnNoloaninfo();
                //循环额度分账户最近6个月平均贷款(PC02F)
                PicsSubacctinfo picsSubacctinfo = creTraInfSummaryUnitBo.getPicsSubacctinfo();
                //循环贷款账户最近6个月平均贷款(PC02G)
                PicrRecreditinfo picrRecreditinfo = creTraInfSummaryUnitBo.getPicrRecreditinfo();
                //相关还款责任汇总信息段(PC02K)
                PicrRelepaysuminfo picrRelepaysuminfo = creTraInfSummaryUnitBo.getPicrRelepaysuminfo();

                //贷记卡账户
                if (piccCreditaccinfo!=null) {
                        //贷记卡最近6个月平均使用额度
                         meanSix = new BigDecimal(piccCreditaccinfo.getRecentsixRepay());
                        //贷记卡授信总额
                         credits = new BigDecimal(piccCreditaccinfo.getTotalCredit());
                    }

                 //准贷记卡账户
                if (picqQucreditaccinfo!=null) {
                        //准贷记卡最近6个月平均透支额度
                        cardSix = new BigDecimal(picqQucreditaccinfo.getRecentsixRepay());
                        //准贷记卡授信总额
                         quasi = new BigDecimal(picqQucreditaccinfo.getTotalCredit());
                    }
                        c_c_6m_avg_use_rate = (meanSix.add(cardSix)).divide((credits.add(quasi)), 3, BigDecimal.ROUND_HALF_UP);


                    //非循环账户
                    if (null != picnNoloaninfo) {
                        String recentsixRepay = picnNoloaninfo.getRecentsixRepay();
                        String accountNum = picnNoloaninfo.getAccountNum();
                        if (recentsixRepay != null) {
                            //非循环账户最近6个月应还贷款
                            fxhAveBalance = new BigDecimal(recentsixRepay);
                        }
                        if (accountNum!=null){
                            //非循环贷账户数
                            fxhAveAccount=new BigDecimal(accountNum);
                        }
                    }

                //循环额度分账户
                    if (null != picsSubacctinfo) {
                        String recentsixRepay = picsSubacctinfo.getRecentsixRepay();
                        String accountNum = picsSubacctinfo.getAccountNum();
                        if (recentsixRepay != null) {
                            //循环额度分账户最近6个月平均贷款
                            xhfAveBalance = new BigDecimal(recentsixRepay);
                        }
                        if (accountNum!=null){
                            //循环额度下分账户账户数
                            xhfAveAccount = new BigDecimal(accountNum);
                        }
                    }


                    //循环贷款账户
                    if (null != picrRecreditinfo) {
                        String recentsixRepay = picrRecreditinfo.getRecentsixRepay();
                        String accountNum = picrRecreditinfo.getAccountNum();
                        if (recentsixRepay != null) {
                            //循环贷款账户最近6个月平均贷款
                            xhdAveBalance = new BigDecimal(recentsixRepay);
                        }
                        if (accountNum !=null){
                            //循环贷账户账户数
                            xhdAveAccount = new BigDecimal(accountNum);
                        }
                    }


                    //贷记卡账户
                    if (null != piccCreditaccinfo) {
                        String quotaUsed = piccCreditaccinfo.getQuotaUsed();
                        String accountNum = piccCreditaccinfo.getAccountNum();
                        if (quotaUsed != null) {
                            //贷记卡账户已用额度
                            djAveBalance = new BigDecimal(quotaUsed);
                        }
                        if (accountNum != null){
                            //贷记卡账户数
                            meanAccount  = new BigDecimal(accountNum);
                        }
                    }


                    //准贷记卡账户
                    if (null != picqQucreditaccinfo) {
                        String quotaUsed = picqQucreditaccinfo.getQuotaUsed();
                        String accountNum = picqQucreditaccinfo.getAccountNum();
                        if (quotaUsed != null) {
                            //准贷记卡账户透支余额
                            zdjAveBalance = new BigDecimal(quotaUsed);
                        }
                        if (accountNum!=null){
                            //准贷记卡账户数
                            cardAccount =new BigDecimal(accountNum);
                        }
                    }


                //相关还款责任汇总信息段
                if (null != creTraInfSummaryUnitBo && null != picrRelepaysuminfo) {
                    List<PicrRelepaysuminfosub> picrRelepaysuminfosubList = picrRelepaysuminfo.getPicrRelepaysuminfosubList();
                    if (picrRelepaysuminfosubList != null) {
                        for (PicrRelepaysuminfosub picrRelepaysuminfosub : picrRelepaysuminfosubList) {
                            if ((Constants.RepayResponse_1).equals(picrRelepaysuminfosub.getRelepayType())) {
                                //保证人代偿笔数
                                gu_comp_num = gu_comp_num + Integer.parseInt(picrRelepaysuminfosub.getAccountNum());
                                //保证人代偿余额
                                gu_comp_balan = gu_comp_balan.add(new BigDecimal((picrRelepaysuminfosub.getBalance())));
                            }

                            if (StringUtils.isNotEmpty(picrRelepaysuminfosub.getAccountNum())) {
                                //对外担保笔数
                                ext_guar_num = ext_guar_num + Integer.parseInt(picrRelepaysuminfosub.getAccountNum());
                            }
                            if (StringUtils.isNotEmpty(picrRelepaysuminfosub.getRepayment())) {
                                BigDecimal repayment = new BigDecimal(picrRelepaysuminfosub.getRepayment());
                                //还款责任为担保金额
                                if (Constants.RepayResponse_1.equals(picrRelepaysuminfosub.getRelepayType())) {
                                    guar_contr_amt = guar_contr_amt.add(repayment);
                                }
                            }
                            BigDecimal balance1 = new BigDecimal(picrRelepaysuminfosub.getBalance());
                            if (null != balance1) {
                                //为个人为企业余额加总
                                guar_contr_balan = guar_contr_balan.add(balance1);
                            }

                        }
                    }
                }
            }


            //信用卡最近6个月平均透支率
            personZBInfoPo.setCC6mAvgUseRate(String.valueOf(c_c_6m_avg_use_rate));


            //121 最近6个月负债额度
            BigDecimal decimal1 = new BigDecimal(6);
            BigDecimal f_6mon_rp_amt = (fxhAveBalance.multiply(decimal1)).add(xhfAveBalance.multiply(decimal1)).add(xhdAveBalance.multiply(decimal1)).add(djAveBalance).add(zdjAveBalance).setScale(2,RoundingMode.HALF_UP);//得到总数
            personZBInfoPo.setF6monRpAmt(String.valueOf(f_6mon_rp_amt));


            //125 信用卡总透支率
            BigDecimal totleUsedCredit = djAveBalance.add(zdjAveBalance);
            BigDecimal totalCredit = quasi.add(credits);
            BigDecimal c_c_all_use_rate = new BigDecimal("0.000");
            if (totalCredit.compareTo(BigDecimal.ZERO) > 0) {
                c_c_all_use_rate = totleUsedCredit.divide(totalCredit, 3, RoundingMode.HALF_UP);
            }
            personZBInfoPo.setCCAllUseRate(String.valueOf(c_c_all_use_rate.setScale(3, BigDecimal.ROUND_HALF_UP)));

            //保证人代偿笔数
            personZBInfoPo.setGuCompNum(String.valueOf(gu_comp_num));
            //保证人代偿余额
            personZBInfoPo.setGuCompBalan(String.valueOf(gu_comp_balan));


            //158 最近6个月信用卡余额使用率(不含未激活)
            BigDecimal totleUseBalance = meanSix.add(cardSix);  //最近6个月平均使用额度+最近6个月平均透支余额
            BigDecimal totleCredit = quasi.add(credits);  //授信总额+授信额度
            BigDecimal cred_card_6m_use_rate = new BigDecimal(0);
            if (totleCredit.doubleValue() != 0) {
                cred_card_6m_use_rate = totleUseBalance.divide(totleCredit, 3, RoundingMode.HALF_UP);//信用卡余额使用率
            }
            personZBInfoPo.setCredCard6mUseRate(String.valueOf(cred_card_6m_use_rate.setScale(3, BigDecimal.ROUND_HALF_UP)));


            //159 信用卡使用额度
            BigDecimal cred_card_use_amt = djAveBalance.add(zdjAveBalance);//信用卡使用额度
            personZBInfoPo.setCredCardUseAmt(String.valueOf(cred_card_use_amt.setScale(3,BigDecimal.ROUND_HALF_UP)));


            //181 信用卡授信总金额（不含未激活）
            BigDecimal  cred_card_total_amt_out_wjq= quasi.add(credits);
            personZBInfoPo.setCredCardTotalAmtOutWjq(String.valueOf(cred_card_total_amt_out_wjq.setScale(3,BigDecimal.ROUND_HALF_UP)));


            //182 授信总余额（不含未激活）
            BigDecimal subtract = djAveBalance.add(zdjAveBalance);//已用额度
            BigDecimal cred_total_balan_out_wjq = cred_card_total_amt_out_wjq.subtract(subtract);
            personZBInfoPo.setCredTotalBalanOutWjq(String.valueOf(cred_total_balan_out_wjq.setScale(3,BigDecimal.ROUND_HALF_UP)));

            //贷款最近6个月平均还款金额
            BigDecimal add = fxhAveAccount.add(xhfAveAccount).add(xhdAveAccount);
            if (add.compareTo(BigDecimal.ZERO)>0){
                loan_6_mon_aver_rp_amt = (fxhAveBalance.multiply(fxhAveAccount).add(xhfAveBalance.multiply(xhfAveAccount)).add(xhdAveBalance.multiply(xhdAveAccount))).divide((fxhAveAccount.add(xhfAveAccount).add(xhdAveAccount)), 2, RoundingMode.HALF_UP);
            }
             personZBInfoPo.setLoan6MonAverRpAmt(String.valueOf(loan_6_mon_aver_rp_amt.setScale(2, BigDecimal.ROUND_HALF_UP)));


            //信用卡最近6个月使用金额
            BigDecimal adds = meanAccount.add(cardAccount);
            if (adds.compareTo(BigDecimal.ZERO)>0){
                BigDecimal multiplys = meanSix.multiply(meanAccount).add((cardSix.multiply(cardAccount)));
                cc_l_6_mon_aver_use_amt = (multiplys.setScale(2,RoundingMode.HALF_UP)).divide(adds.setScale(2,RoundingMode.HALF_UP),2,RoundingMode.HALF_UP);

            }
            personZBInfoPo.setCcL6MonAverUseAmt(String.valueOf(cc_l_6_mon_aver_use_amt));

            //对外担保笔数
            personZBInfoPo.setExtGuarNum(String.valueOf(ext_guar_num));
            //对外担保金额
            personZBInfoPo.setGuarContrAmt(String.valueOf(guar_contr_amt));
            //对外担保本金余额
            personZBInfoPo.setGuarContrBalan(String.valueOf(guar_contr_balan));



        } catch (Exception e) {
            throw new Exception(e);
        }
        return personZBInfoPo;
    }
}

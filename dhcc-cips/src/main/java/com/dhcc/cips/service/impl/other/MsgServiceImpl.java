package com.dhcc.cips.service.impl.other;

import com.dc.eai.data.CompositeData;
import com.dhcc.cips.bo.base.RequestHead;
import com.dhcc.cips.bo.other.MsgReqBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.esbclient.TCPClient;
import com.dhcc.cips.mapper.other.MsgMapper;
import com.dhcc.cips.response.EntityResponse;
import com.dhcc.cips.service.other.MsgService;
import com.dhcc.cips.util.ESBUtil;
import com.dhcc.cips.util.XmlUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 短信服务实现类
 * @author YangJiaheng
 * @date 2019-5-6 09:51:31
 */
@Service
@Slf4j
public class MsgServiceImpl implements MsgService {
    @Resource
    private MsgMapper msgMapper;

    @Override
    public EntityResponse<?> sendMessage(String username, String msgContent) {
        try {
            //根据用户名查找对应的手机号
            String phoneNum = msgMapper.selectPhoneByUsername(username);
            if (StringUtils.isBlank(phoneNum)) {
                log.debug("获取用户手机号：" + phoneNum);
                return EntityResponse.error(Constants.FAIL_CODE, "获取用户工作手机号失败");
            }
            //短信发送请求对象
            MsgReqBo msgReqBo = new MsgReqBo();
            //初始化Head
            //服务代码
            msgReqBo.getRequestSysHead().setServiceCode("11002000003");
            //服务应用场景
            msgReqBo.getRequestSysHead().setServiceScene("01");
            //交易日期
            msgReqBo.getRequestSysHead().setTranDate(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
            //交易时间
            msgReqBo.getRequestSysHead().setTranTimestamp(LocalTime.now().format(DateTimeFormatter.ofPattern("hhmmss")));
            //法人代码
            msgReqBo.getRequestSysHead().setCorpCode("9999");
            //发送方机构号
            msgReqBo.getRequestSysHead().setBranchId("0000");
            //服务请求者身份
            msgReqBo.getRequestSysHead().setUserId("BERS");
            //请求系统流水号
            msgReqBo.getRequestSysHead().setConsumerSeqNo(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) + "00187468");
            //渠道类型
            msgReqBo.getRequestSysHead().setSourceType("");
            //请求系统ID
            msgReqBo.getRequestSysHead().setConsumerId(Constants.SYSTEM_ID_CIPS);
            //提供系统ID
            msgReqBo.getRequestSysHead().setProviderId(Constants.SYSTEM_ID_SMS);
            //业务流水号
            msgReqBo.getRequestAppHead().setBussSeqNo("1555470624320");

            //初始化Body
            //账号
            //msgReqBo.getMsgReqBody().setAcctNo("zxcxqzxt");
            //账号
            msgReqBo.getMsgReqBody().setAcctNo("yqdz");
            //短信内容
            msgReqBo.getMsgReqBody().setMsgContext(msgContent);
            //电话号码
            msgReqBo.getMsgReqBody().setPhoneNo(phoneNum);
            //服务ID
            msgReqBo.getMsgReqBody().setServiceId("xtfs");

            //1. 将对象转换为xml
            String requestXml = XmlUtil.converJavaBean2Xml(msgReqBo, new Class[]{msgReqBo.getClass(), RequestHead.class});
            //2. 将xml转换为compositeData
            CompositeData request = ESBUtil.xmlToCD(requestXml.getBytes(), "utf-8");
            //3. 调用esb短信接口发送短信
            TCPClient tcpClient = new TCPClient();
            tcpClient.doTransaction(request);
            CompositeData resp_syshead = tcpClient.getResp_syshead();
            //获取交易返回码
            String retCode = resp_syshead.getArray("RET").getStruct(0).getField("RET_CODE").strValue();
            //获取交易返回信息
            String retMsg = resp_syshead.getArray("RET").getStruct(0).getField("RET_MSG").strValue();

            //对返回代码判断，设置返回对象
            if (Constants.SUCCESS_CODE.equals(retCode)) {
                return EntityResponse.ok(retMsg);
            } else {
                return EntityResponse.error(Constants.FAIL_CODE, retMsg);
            }

        } catch (DocumentException e) {
            log.error("TCPClient初始化失败");
            return EntityResponse.error(Constants.FAIL_CODE, "TCPClient初始化失败");
        } catch (Exception e) {
            log.error("短信发送异常");
            e.printStackTrace();
            return EntityResponse.error(Constants.FAIL_CODE, "短信发送异常");
        }
    }

    @Override
    public int queryUserByUserId(String userId) {
        return 0;
    }

    @Override
    public List<Map> selectUserListByOrg(String orgId) {
        HashMap<String, String> map = Maps.newHashMap();
        map.put("orgId", orgId);
        List<Map> result = msgMapper.selectUserList(map);
        return result;
    }
}

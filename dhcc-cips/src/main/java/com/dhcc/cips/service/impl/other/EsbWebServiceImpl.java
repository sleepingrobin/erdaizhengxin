package com.dhcc.cips.service.impl.other;

import com.dhcc.cips.service.other.EsbService;
import com.dhcc.cips.service.other.EsbWebService;
import com.dhcc.cips.util.SpringContextUtils;
import org.springframework.stereotype.Service;

import javax.jws.WebService;

@Service
@WebService
public class EsbWebServiceImpl implements EsbWebService {

    @Override
    public String exec(String esbXml) {
        EsbService esbService = (EsbService) SpringContextUtils.getBean(EsbServiceImpl.class);
        return esbService.exec(esbXml);
    }
}

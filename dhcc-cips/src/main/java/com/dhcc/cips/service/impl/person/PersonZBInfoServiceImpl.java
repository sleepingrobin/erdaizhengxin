package com.dhcc.cips.service.impl.person;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dhcc.cips.bo.base.Request;
import com.dhcc.cips.bo.base.ResponseHead;
import com.dhcc.cips.bo.base.ResponseSysHead;
import com.dhcc.cips.bo.base.ResponseSysHeadRet;
import com.dhcc.cips.bo.company.CompanyExtGuarantyInfoBo;
import com.dhcc.cips.bo.company.CompanyLoanInfoBo;
import com.dhcc.cips.bo.person.PersonExtGuarantyInfoBo;
import com.dhcc.cips.bo.person.PersonLoanInfoBo;
import com.dhcc.cips.bo.person.PersonZBInfoResBo;
import com.dhcc.cips.bo.person.PersonZBInfoResBodyBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.mapper.person.*;
import com.dhcc.cips.po.person.CpqResultInfoPo;
import com.dhcc.cips.po.person.PersonBaseInfoPo;
import com.dhcc.cips.po.person.PersonQueryRecordSumPo;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBInfoService;
import com.dhcc.cips.util.ConvertUtil;
import com.dhcc.cips.util.XmlUtil;
import com.google.common.collect.Maps;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @author Yang Jiaheng
 * @date 2019年3月28日
 * @description 个人征信指标接口服务类
 */
@Slf4j
@Service
public class PersonZBInfoServiceImpl implements PersonZBInfoService {

    @Resource
    private PersonZBInfoMapper personZBInfoMapper;
    @Resource
    private CpqResultInfoMapper cpqResultInfoMapper;

    @Override
    public String exec(Request request) {
        //返回报文
        String retXml;
        //返回报文对象
        PersonZBInfoResBo personZBInfoResBo = new PersonZBInfoResBo();
        //返回报文body对象
        PersonZBInfoResBodyBo personZBInfoResBodyBo = new PersonZBInfoResBodyBo();

        //返回交易信息
        String retCode;//交易返回码
        String retMesaage;//交易返回信息
        String retStatus;//交易返回状态
        try {

            //获取请求参数
            String globalType = request.getRequestBody().getGlobalType();//证件类型
            String globalId = request.getRequestBody().getGlobalId();//取证件号码
            //请求参数校验
            if (StringUtils.isEmpty(globalType)) {
                throw new Exception(Constants.GLOBAL_TYPE_NULL_ANO);//证件类型不能为空
            }
            if (StringUtils.isEmpty(globalId)) {
                throw new Exception(Constants.GLOBAL_ID_NULL_ANO);//证件号码不能为空
            }

            //根据证件号码和证件类型查询报告编号
            HashMap<Object, Object> map = Maps.newHashMap();
            map.put("globalType", ConvertUtil.getPersonGlobalType(globalType));
            map.put("globalId", globalId);
            CpqResultInfoPo cpqResultInfoPo = cpqResultInfoMapper.selectByCertId(map);
            if (cpqResultInfoPo == null) {
                throw new Exception(Constants.ZB_NULL);
            }
            String reportNo = cpqResultInfoPo.getReportId();
            //无征信指标信息
            if (StringUtils.isEmpty(reportNo)) {
                throw new Exception(Constants.ZB_NULL);
            }

            //根据报告编号查询指标
            map.clear();
            map.put("reportNo", reportNo);
            PersonZBInfoPo personZBInfoPo = personZBInfoMapper.selectPersonZBInfoByReportNo(map);
            //无征信指标信息
            if (personZBInfoPo == null) {
                throw new Exception(Constants.ZB_NULL);
            }
            //填充返回体
            personZBInfoResBodyBo.setPersonZBInfoPo(personZBInfoPo);

            //获取json对象
            String perLoanInfo = personZBInfoPo.getPerLoanInfo();

            if (StringUtils.isNotBlank(perLoanInfo)) {
                JSONObject perLoanInfoObject = JSONObject.parseObject(perLoanInfo);
                //贷款信息数组
                String loan = String.valueOf(perLoanInfoObject.get("LOAN_INFO_ARRAY"));
                if (StringUtils.isNotBlank(loan)) {
                    List<PersonLoanInfoBo> personLoanInfoBoList = JSONArray.parseArray(loan, PersonLoanInfoBo.class);
                    personZBInfoResBodyBo.setPersonLoanInfoBoList(personLoanInfoBoList);
                }

                //对外担保信息数组
                String ext = String.valueOf(perLoanInfoObject.get("GRNY_CTR_ARRAY"));
                if (StringUtils.isNotBlank(ext)) {
                    List<PersonExtGuarantyInfoBo> personExtGuarantyInfoBoList = JSONArray.parseArray(ext, PersonExtGuarantyInfoBo.class);
                    personZBInfoResBodyBo.setPersonExtGuarantyInfoBoList(personExtGuarantyInfoBoList);
                }
                //征信评分
                personZBInfoResBodyBo.setCustCreditLevel("custCredLevel");

                //未来1-50月需还贷款金额
                personZBInfoResBodyBo.setAmtString("MonLoanAmt");

                //近一个月本人自主查询次数
                personZBInfoResBodyBo.setLastMonQueryTms("late_mon_self_query_time");

                //最近2年内担保资格审查查询次数
                personZBInfoResBodyBo.setLtyWrntQualfQueryTms("last_2_y_dbzg_en_num");

                //性别
                personZBInfoResBodyBo.setGender("sex");

                //最高学历
                personZBInfoResBodyBo.setEducation("high_educ");
            }

            //交易返回码
            retCode = Constants.SUCCESS_CODE;
            //交易返回信息
            retMesaage = "交易成功";
            //交易返回状态
            retStatus = Constants.SUCCESS_S;

        } catch (Exception e) {
            log.error("", e);
            //交易返回码
            retCode = Constants.FAIL_CODE;
            //交易返回信息
            retMesaage = "交易失败，" + e.getMessage();
            //交易返回状态
            retStatus = Constants.FAIL_F;
        }

        //设置返回值状态码
        ResponseSysHeadRet responseSysHeadRet = new ResponseSysHeadRet();
        //设置返回对象参数
        ResponseHead responseHead = new ResponseHead();
        ResponseSysHead responseSysHead = new ResponseSysHead();
        BeanUtils.copyProperties(request.getRequestSysHead(), responseSysHead);
        //返回状态码
        responseSysHead.setRetStatus(retStatus);
        responseHead.setResponseSysHead(responseSysHead);
        //交易返回码
        responseSysHeadRet.setRetCode(retCode);
        //交易返信息
        responseSysHeadRet.setRetMsg(retMesaage);
        responseHead.getResponseSysHead().setResponseSysHeadRet(responseSysHeadRet);

        //填充返回体
        personZBInfoResBo.setPersonZBInfoResBodyBo(personZBInfoResBodyBo);

        //转xml
        retXml = XmlUtil.converJavaBean2Xml(personZBInfoResBo, personZBInfoResBo.getClass());

        return retXml;
    }
}

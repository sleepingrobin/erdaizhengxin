package com.dhcc.cips.service.company;

import com.dhcc.cips.bo.base.Request;

/**
 * 企业征信查询服务类
 */
public interface CompanyReportQueryService {

    String exec(Request requestParam);

}
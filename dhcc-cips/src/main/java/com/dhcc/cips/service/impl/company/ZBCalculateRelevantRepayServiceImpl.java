package com.dhcc.cips.service.impl.company;

import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.CeqReportBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.informsum.EsrsLrTransaction;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.informsum.EsrsLrTransactionsSub;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.informsum.EsrsSrTransaction;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.informsum.EsrsSrTransactionsSub;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.repayinformunit.entity.RePayInformUnitBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.repayinformunit.entity.RePayInformUnitBoData;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.dhcc.cips.service.company.CompanyZBThreadService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 相关还款责任信息
 */
@Service
@Slf4j
public class ZBCalculateRelevantRepayServiceImpl implements CompanyZBThreadService {

    /**
     * 相关还款责任信息
     *
     * @param ceqReportBo
     */
    @Override
    public CompanyZBInfoPo companyCalculate(CeqReportBo ceqReportBo) throws Exception {
        CompanyZBInfoPo companyZBInfoPo = new CompanyZBInfoPo();
        try {
            if (null == ceqReportBo) {
                return null;
            }
            Integer totalAccountNumber = 0;
            BigDecimal totalMoneySum = new BigDecimal("0.00");
            BigDecimal normalBalanceSum = new BigDecimal("0.00");//正常类保证担保合同余额
            BigDecimal totalFollowBalanceSum = new BigDecimal("0.00");
            BigDecimal totalBadBalanceSum = new BigDecimal("0.00");
            int allAccountSum = 0;//对外担保金额笔数
            BigDecimal allBalanceSum = new BigDecimal("0.00");//对外担保金额
            RePayInformUnitBoData rePayInformUnitBoData = ceqReportBo.getRePayInformUnitBoData();
            if (null != rePayInformUnitBoData) {
                //相关还款责任汇总信息EBH --相关还款责任汇总信息单元EB05 --担保交易相关还款责任汇总信息段 EB05B
                RePayInformUnitBo rePayInformUnitBo = rePayInformUnitBoData.getRePayInformUnitBo();
                //担保交易相关
                if (rePayInformUnitBo != null) {
                    EsrsLrTransaction esrsLrTransaction = rePayInformUnitBo.getEsrsLrTransaction();
                    if (null != esrsLrTransaction) {
                        List<EsrsLrTransactionsSub> esrsLrTransactionsSubList = esrsLrTransaction.getEsrsLrTransactionsSubList();
                        if (esrsLrTransactionsSubList != null && esrsLrTransactionsSubList.size() > 0) {
                            for (EsrsLrTransactionsSub esrsLrTransactionsSub : esrsLrTransactionsSubList) {
                                String liabilyType = esrsLrTransactionsSub.getLiabilyType();
                                if (liabilyType != null && liabilyType.equals("2")) {  //责任类型-保证人
                                    BigDecimal accountNum = esrsLrTransactionsSub.getAccountNum();
                                    BigDecimal repayAmount = esrsLrTransactionsSub.getRepayAmount();
                                    Integer accountNumInt = (accountNum == null) ? 0 : accountNum.intValue();
                                    totalAccountNumber += accountNumInt;
                                    if (repayAmount != null) {
                                        totalMoneySum = totalMoneySum.add(repayAmount);
                                    }
                                    //（担保交易--余额-关注类余额-不良类余额）
                                    BigDecimal balance = esrsLrTransactionsSub.getBalance();//余额
                                    BigDecimal attBalance = esrsLrTransactionsSub.getAttBalance();//关注类余额
                                    BigDecimal badBalance = esrsLrTransactionsSub.getBadBalance();//不良类余额
                                    if (balance != null && attBalance != null && badBalance != null) {
                                        normalBalanceSum = normalBalanceSum.add(balance).subtract(attBalance).subtract(badBalance);
                                        totalFollowBalanceSum = totalFollowBalanceSum.add(attBalance);
                                        totalBadBalanceSum = totalBadBalanceSum.add(badBalance);
                                    }
                                }
                            }
                        }
                    }
                    //借贷交易相关
                    EsrsSrTransaction esrsSrTransaction = rePayInformUnitBo.getEsrsSrTransaction();
                    if (null != esrsSrTransaction) {
                        List<EsrsSrTransactionsSub> esrsSrTransactionsSubList = esrsSrTransaction.getEsrsSrTransactionsSubList();
                        if (esrsSrTransactionsSubList != null && esrsSrTransactionsSubList.size() > 0) {
                            for (EsrsSrTransactionsSub esrsSrTransactionsSub : esrsSrTransactionsSubList) {
                                String liabilyTyoe = esrsSrTransactionsSub.getLiabilyTyoe();
                                if (liabilyTyoe != null && liabilyTyoe.equals("2")) {
                                    //账户数 被追偿账户 其他借贷交易
                                    BigDecimal othAccNum = esrsSrTransactionsSub.getOthAccNum();
                                    BigDecimal accRecNum = esrsSrTransactionsSub.getAccRecNum();
                                    Integer othAccNumInt = (othAccNum == null) ? 0 : othAccNum.intValue();
                                    Integer accRecNumInt = (accRecNum == null) ? 0 : accRecNum.intValue();
                                    totalAccountNumber = totalAccountNumber + othAccNumInt + accRecNumInt;
                                    //金额  被追偿账户 其他借贷交易
                                    BigDecimal othAmount = esrsSrTransactionsSub.getOthAmount();
                                    BigDecimal repayRecAccAmount = esrsSrTransactionsSub.getRepayRecAccAmount();
                                    if (othAmount != null && repayRecAccAmount != null) {
                                        totalMoneySum = totalMoneySum.add(othAmount).add(repayRecAccAmount);
                                    }
                                    //67.信息概要--相关还款责任信息概要--保证人/反担保人--被追偿业务--余额
                                    BigDecimal accRecBalance = esrsSrTransactionsSub.getAccRecBalance();
                                    //其他借贷交易--余额-关注类余额-不良类余额
                                    BigDecimal othAccBalance = esrsSrTransactionsSub.getOthAccBalance();//余额
                                    BigDecimal othAccAttBalance = esrsSrTransactionsSub.getOthAccAttBalance();//关注类余额
                                    BigDecimal othAccBadBalance = esrsSrTransactionsSub.getOthAccBadBalance();//不良类余额
                                    if (othAccBalance != null && othAccAttBalance != null && othAccBadBalance != null) {
                                        normalBalanceSum = normalBalanceSum.add(accRecBalance).add(othAccBalance).subtract(othAccAttBalance).subtract(othAccBadBalance);
                                    }
                                    if (othAccAttBalance != null) {
                                        totalFollowBalanceSum = totalFollowBalanceSum.add(othAccAttBalance);
                                    }
                                    if (othAccBadBalance != null) {
                                        totalBadBalanceSum = totalBadBalanceSum.add(othAccBadBalance);
                                    }
                                }
                            }
                        }
                    }
                    if (null != esrsSrTransaction) {
                        //借贷
                        List<EsrsSrTransactionsSub> esrsSrTransactionsSubList = esrsSrTransaction.getEsrsSrTransactionsSubList();
                        if (esrsSrTransactionsSubList != null && esrsSrTransactionsSubList.size() > 0) {
                            //责任类型 0 合计  余额
                            List<EsrsSrTransactionsSub> list = esrsSrTransactionsSubList.stream().
                                    filter(e -> StringUtils.isNotBlank(e.getLiabilyTyoe()) && Constants.QUAL_SORT_0.equals(e.getLiabilyTyoe())).collect(Collectors.toList());
                            if (list != null && list.size() > 0) {
                                for (EsrsSrTransactionsSub esrsSrTransactionsSub : list) {
                                    BigDecimal accRecBalance = esrsSrTransactionsSub.getAccRecBalance();//被追偿账户余额
                                    BigDecimal accRecNum = esrsSrTransactionsSub.getAccRecNum();
                                    BigDecimal othAccBalance = esrsSrTransactionsSub.getOthAccBalance();//其他借贷交易账户余额
                                    BigDecimal othAccNum = esrsSrTransactionsSub.getOthAccNum();
                                    Integer accRecNumInt = (accRecNum == null) ? 0 : accRecNum.intValue();
                                    Integer othAccNumInt = (othAccNum == null) ? 0 : othAccNum.intValue();
                                    if (accRecBalance != null && othAccBalance != null) {
                                        allBalanceSum = allBalanceSum.add(accRecBalance).add(othAccBalance);
                                    }
                                    allAccountSum += accRecNumInt + othAccNumInt;
                                }
                            }
                        }
                    }
                    if (null != esrsLrTransaction) {
                        //担保
                        List<EsrsLrTransactionsSub> esrsLrTransactionsSubList = esrsLrTransaction.getEsrsLrTransactionsSubList();
                        if (esrsLrTransactionsSubList != null && esrsLrTransactionsSubList.size() > 0) {
                            List<EsrsLrTransactionsSub> list = esrsLrTransactionsSubList.stream().
                                    filter(e -> StringUtils.isNotBlank(e.getLiabilyType()) && Constants.QUAL_SORT_0.equals(e.getLiabilyType())).collect(Collectors.toList());
                            for (EsrsLrTransactionsSub esrsLrTransactionsSub : list) {
                                BigDecimal balance = esrsLrTransactionsSub.getBalance();//担保类余额
                                if (balance != null) {
                                    allBalanceSum = allBalanceSum.add(balance);
                                }
                                BigDecimal accountNum = esrsLrTransactionsSub.getAccountNum();
                                Integer accountNumInt = (accountNum == null) ? 0 : accountNum.intValue();
                                allAccountSum += accountNumInt;
                            }
                        }
                    }
                }
            }
            //30.信息概要--相关还款责任信息概要--保证人/反担保人--账户数合计
            companyZBInfoPo.setCOBzGuraContrNum(String.valueOf(totalAccountNumber));
            //31.信息概要--相关还款责任信息概要--保证人/反担保人--还款责任金额合计
            companyZBInfoPo.setCOBzGuraContrAmt(String.valueOf(totalMoneySum));
            //32.对外担保保证合同单笔平均合同金额 c_o_bz_gura_contr_amt/c_o_bz_gura_contr_num
            BigDecimal result = new BigDecimal("0.00");
            if (totalAccountNumber > 0) {
                result = totalMoneySum.divide(new BigDecimal(totalAccountNumber), 2, BigDecimal.ROUND_HALF_UP);
            }
            companyZBInfoPo.setCOBzGuraAverContrAmt(String.valueOf(result));
            // 55.信息概要--相关还款责任信息概要--保证人/反担保人--被追偿业务--余额 +（其他借贷交易--余额-关注类余额-不良类余额）+（担保交易--余额-关注类余额-不良类余额）
            companyZBInfoPo.setCBzGuraTotalZcBalan(String.valueOf(normalBalanceSum));
            // 58.信息概要--相关还款责任信息概要--保证人/反担保人--其他借贷交易--关注类余额 +（担保交易--关注类余额）
            companyZBInfoPo.setCBzGuraTotalGzBalan(String.valueOf(totalFollowBalanceSum));
            // 61.信息概要--相关还款责任信息概要--保证人/反担保人--其他借贷交易--不良类余额 +（担保交易--不良类余额）
            companyZBInfoPo.setCBzGuraTotalBlBalan(String.valueOf(totalBadBalanceSum));

            //89.信息概要--相关还款责任信息概要--被追偿业务+其他借贷交易+担保交易--合计--余额的加总
            companyZBInfoPo.setGuarContrAmt(String.valueOf(allBalanceSum));
            //90.信息概要--相关还款责任信息概要--被追偿业务+其他借贷交易+担保交易--合计--账户数加总
            companyZBInfoPo.setGuarContrCount(String.valueOf(allAccountSum));
        } catch (
                Exception e) {
            log.error("CompanyZBRelevantRepayServiceImpl", e);
            throw new Exception(e);
        }
        return companyZBInfoPo;
    }
}

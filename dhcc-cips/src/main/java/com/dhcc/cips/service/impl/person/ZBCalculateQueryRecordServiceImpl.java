package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.queryrecord.PqqqRecordUnit;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.queryrecord.entity.QueryRecordBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.DateUtil;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author StivenYang
 * @description 六（查询记录）
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateQueryRecordServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo reportbo) throws Exception {
        PersonZBInfoPo personZBInfoPo = new PersonZBInfoPo();
        try {
            //报告时间
            Date reportTime = reportbo.getRepheadBo().getRepheadUnitBo().getPchrPchrreportinfo().getReportTime();

            //最新6个月
            Date aimMonthDateSix = DateUtil.getAimMonthDate(reportTime, -6);
            //最近3个月贷款审核查询次数
            Date aimMonthDateThree = DateUtil.getAimMonthDate(reportTime, -3);
            //最近1个月
            Date aimMonthDateOne = DateUtil.getAimMonthDate(reportTime, -1);
            //最近1年贷款审核查询记录
            Date aimYearDateOne = DateUtil.getAimYearDate(reportTime, -1);

            //查询记录(POQ)
            QueryRecordBo queryRecordBo = reportbo.getQueryRecordBo();
            //查询记录信息单元(PH01)
            List<PqqqRecordUnit> pqqqRecordUnitList = null;
            if (queryRecordBo != null) {
                pqqqRecordUnitList = queryRecordBo.getPqqqRecordUnitList();
            }


            //166 最近6个月审批查询金融机构家数（信用卡审批和贷款审批）
            TreeSet<Object> arrayList = new TreeSet<>();
            //65过多征信查询
            int lmbne = 0;
            //86最近一个月担保资格查询记录
            int late_mon_guan_count = 0;
            //3个月贷款审核查询记录数
            int late_sea_loan_count = 0;
            //3个月担保审核查询记录数
            int late_sea_guan_count = 0;
            //3个月信用卡审批查询记录数
            int late_sea_card_count = 0;
            //6个月贷款审核查询记录数
            int late_hly_loan_count = 0;
            //6个月担保审核查询记录数
            int late_hly_guan_count = 0;
            //6个月信用卡审批查询记录数
            int late_hly_card_count = 0;
            //12个月贷款审核查询记录数
            int late_year_loan_count = 0;
            //12个月担保审核查询记录数
            int late_year_guan_count = 0;
            //12个月信用卡审批查询记录数
            int late_year_card_count = 0;
            //121 最近3个月审核查询机构家数
            Set<String> aset = new TreeSet<>();


            //（查询记录）
            if (pqqqRecordUnitList != null && pqqqRecordUnitList.size() > 0) {
                for (PqqqRecordUnit pqqqList : pqqqRecordUnitList) {
                    //查询日期
                    Date queryDate = null;
                    //查询原因
                    String queryReason=null;
                    if (pqqqList.getQueryDate() != null) {
                        queryDate = pqqqList.getQueryDate();
                    }
                    if (pqqqList.getQueryReason()!=null){
                        queryReason =  pqqqList.getQueryReason();
                    }

                    if (queryDate!=null&&queryReason!=null) {
                        //3个月内
                        if (reportTime.after(queryDate) && queryDate.after(aimMonthDateThree)) {
                            String queryOrg = pqqqList.getQueryOrg();
                            aset.add(queryOrg);
                        }

                        //6个月内
                        if (reportTime.after(queryDate) && queryDate.after(aimMonthDateSix)) {
                            //查询原因为贷款审批或者信用卡审批
                            if (Constants.QueryCause_03.equals(queryReason) || Constants.QueryCause_02.equals(queryReason)) {
                                //金融机构
                                String queryOrgType = pqqqList.getQueryOrgType();
                                //银行机构
                                String queryOrg = pqqqList.getQueryOrg();
                                arrayList.add(queryOrgType + queryOrg);
                            }
                            //查询次数
                            lmbne = lmbne + 1;
                        }
                    }
                       //查询原因为担保
                        if ((Constants.QueryCause_08).equals(queryReason)) {
                            //一年内
                            if (aimMonthDateOne.before(queryDate) && queryDate.before(reportTime)) {
                                //查询记录
                                late_mon_guan_count = late_mon_guan_count + 1;
                            }
                            //最近3个月担保资格查询记录
                            if (aimMonthDateThree.before(queryDate) && queryDate.before(reportTime)) {
                                late_sea_guan_count = late_sea_guan_count + 1;
                            }
                            //最近6个月担保资格查询记录
                            if (aimMonthDateSix.before(queryDate) && queryDate.before(reportTime)) {
                                late_hly_guan_count = late_hly_guan_count + 1;
                            }
                            //最近12个月担保资格查询记录
                            if (aimYearDateOne.before(queryDate) && queryDate.before(reportTime)) {
                                late_year_guan_count = late_year_guan_count + 1;
                            }

                        }

                        //查询原因为贷款审批
                    if ((Constants.QueryCause_02).equals(queryReason)) {
                        if (aimMonthDateThree.before(queryDate) && queryDate.before(reportTime)) {
                            late_sea_loan_count = late_sea_loan_count + 1;
                        }
                        //最近6个月贷款审核查询记录
                        if (aimMonthDateSix.before(queryDate)&& queryDate.before(reportTime)) {
                            late_hly_loan_count = late_hly_loan_count + 1;
                        }

                        //最近12个月贷款审核查询记录
                        if (aimYearDateOne.before(queryDate) && queryDate.before(reportTime)) {
                            late_year_loan_count = late_year_loan_count + 1;
                        }
                    }

                    //查询原因为信用卡审批
                    if ((Constants.QueryCause_03).equals(queryReason)) {
                        //最近3个月信用卡审批查询记录
                        if (aimMonthDateThree.before(queryDate) && queryDate.before(reportTime)) {
                            late_sea_card_count = late_sea_card_count + 1;
                        }
                        //最近6个月信用卡审批查询记录
                        if (aimMonthDateSix.before(queryDate) && queryDate.before(reportTime)) {
                            late_hly_card_count = late_hly_card_count + 1;
                        }
                        //最近12个月信用卡审批查询记录
                        if (aimYearDateOne.before(queryDate)&& queryDate.before(reportTime)) {
                            late_year_card_count = late_year_card_count + 1;
                        }
                    }
                }
            }
            //审批查询金融机构去重家数
            int late_6mon_enqu_org_num = arrayList.size();
            personZBInfoPo.setLate6monEnquOrgNum(String.valueOf(late_6mon_enqu_org_num));
            //过多征信查询
            personZBInfoPo.setLmbne(String.valueOf(lmbne));
            //最近一个月担保资格查询记录
            personZBInfoPo.setLateMonGuanCount(String.valueOf(late_mon_guan_count));
            //最近3个月贷款审核查询次数
            personZBInfoPo.setLateSeaLoanCount(String.valueOf(late_sea_loan_count));
            //最近3个月担保资格查询记录
            personZBInfoPo.setLateSeaGuanCount(String.valueOf(late_sea_guan_count));
            //最近3个月信用卡审批查询记录
            personZBInfoPo.setLateSeaCardCount(String.valueOf(late_sea_card_count));
            //最近6个月贷款审核查询记录
            personZBInfoPo.setLateHlyLoanCount(String.valueOf(late_hly_loan_count));
            //最近6个月担保资格查询记录
            personZBInfoPo.setLateHlyGuanCount(String.valueOf(late_hly_guan_count));
            //最近6个月信用卡审批查询记录
            personZBInfoPo.setLateHlyCardCount(String.valueOf(late_hly_card_count));
            //最近12个月贷款审核查询记录
            personZBInfoPo.setLateYearLoanCount(String.valueOf(late_year_loan_count));
            //最近12个月担保资格查询记录
            personZBInfoPo.setLateYearGuanCount(String.valueOf(late_year_guan_count));
            //最近12个月信用卡审批查询记录
            personZBInfoPo.setLateYearCardCount(String.valueOf(late_year_card_count));
            //查询机构家数
            int late_3mon_enqu_org_num = aset.size();
            personZBInfoPo.setLate3monEnquOrgNum(String.valueOf(late_3mon_enqu_org_num));
        } catch (Exception e) {
            throw new Exception(e);
        }
        return personZBInfoPo;
    }
}

package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.*;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 基础指标计算（贷款明细信息部分）
 */
@Service
public class BasicZBCalculateCreditInfoDkServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {

        PersonZBInfoPo personZBInfoPo = new PersonZBInfoPo();
        try {

            //借贷账户信息对象
            BorAccInfBo borAccInfBo = cpqReportBo.getBorAccInfBo();
            if (borAccInfBo == null) {
                return null;
            }
            //借贷账户信息list
            List<BorAccInfUnitBo> borAccInfUnitBoList = borAccInfBo.getBorAccInfUnitBoList();

            //报告日期
            Date reportDate = cpqReportBo.getRepheadBo().getRepheadUnitBo().getPchrPchrreportinfo().getReportTime();

            BigDecimal f_2mon_fxhd_expire_balan_sum = new BigDecimal("0.00");//36 将来两个月非循环贷账户到期余额总和
            BigDecimal f_2mon_xhd_expire_balan_sum = new BigDecimal("0.00");//37将来两个月循环贷账户到期余额总和
            BigDecimal f_2mon_xhf_expire_balan_sum = new BigDecimal("0.00");//38将来两个月循环额度下分账户到期余额总和
            int late_2mon_fxhd_re_open_num = 0;//41非循环贷账户开立日期距离报告期2个月内的账户数
            int late_2mon_xhf_re_open_num = 0;//42循环额度下分账户开立日期距离报告期2个月内的账户数
            int late_2mon_xhd_re_open_num = 0;//43循环贷账户开立日期距离报告期2个月内的账户数
            BigDecimal c_fxhd_loan_due_amt = new BigDecimal("0.00");//44当前非循环贷账户逾期总额
            BigDecimal c_xhf_loan_due_amt = new BigDecimal("0.00");//45当前循环额度下分账户逾期总额
            BigDecimal c_xhd_loan_due_amt = new BigDecimal("0.00");//46当前循环贷账户逾期总额
            int c_fxhd_loan_max_due_num = 0;//47当前非循环贷账户最大逾期期数
            int c_xhf_loan_max_due_num = 0;//48当前循环贷额度下分账户最大逾期期数
            int c_xhd_loan_max_due_num = 0;//49当前循环贷账户最大逾期期数
            int c_fxhd_loan_due_sum = 0;//50当前非循环贷款账户逾期账户总数
            int c_xhf_loan_due_sum = 0;//51当前循环额度下分账户逾期账户总数
            int c_xhd_loan_due_sum = 0;//52当前循环贷账户逾期账户总数
            int ass_fxhd_dy_count_num = 0; //53非循环贷账户担保方式为质押的账户数
            int ass_xhf_zy_count_num = 0; //54循环额度下分账户担保方式为质押的账户数
            int ass_xhd_dy_count_num = 0;  //55循环贷账户担保方式为质押的账户数
            int ass_fxhd_zy_count_num = 0; //56非循环贷账户担保方式为抵押的账户数
            int ass_xhf_dy_count_num = 0; //57循环额度下分账户担保方式为抵押的账户数
            int ass_xhd_zy_count_num = 0; //58循环贷账户担保方式为抵押的账户数
            int ass_fxhd_bz_count_num = 0; //59非循环贷账户担保方式为保证的账户数
            int ass_xhf_bz_count_num = 0; //60循环额度下分账户担保方式为保证的账户数
            int ass_xhd_bz_count_num = 0; //61循环贷账户担保方式为保证的账户数
            int ass_fxhd_credit_count_num = 0; //62非循环贷账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的账户数
            int ass_xhf_credit_count_num = 0; //63循环额度下分账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的账户数
            int ass_xhd_credit_count_num = 0; //64循环贷账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的账户数
            BigDecimal ass_fxhd_zy_loan_money = new BigDecimal("0.00"); //65非循环贷账户担保方式为质押的借款金额总和
            BigDecimal ass_xhf_zy_loan_money = new BigDecimal("0.00"); //66循环额度下分账户担保方式为质押的借款金额的总和
            BigDecimal ass_xhd_zy_loan_money = new BigDecimal("0.00");//67循环贷账户担保方式为质押的借款金额总和
            BigDecimal ass_fxhd_dy_loan_money = new BigDecimal("0.00"); //68非循环贷账户担保方式为抵押的借款金额总和
            BigDecimal ass_xhf_dy_loan_money = new BigDecimal("0.00"); //69循环额度下分账户担保方式为抵押的借款金额的总和
            BigDecimal ass_xhd_dy_loan_money = new BigDecimal("0.00");//70循环贷账户担保方式为抵押的借款金额总和
            BigDecimal ass_fxhd_bz_loan_money = new BigDecimal("0.00"); //71非循环贷账户担保方式为保证的借款金额总和
            BigDecimal ass_xhf_bz_loan_money = new BigDecimal("0.00"); //72循环额度下分账户担保方式为保证的借款金额的总和
            BigDecimal ass_xhd_bz_loan_money = new BigDecimal("0.00");//73循环贷账户担保方式为保证的借款金额总和
            BigDecimal ass_fxhd_credit_loan_money = new BigDecimal("0.00"); // 74非循环贷账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的借款金额总和
            BigDecimal ass_xhf_credit_loan_money = new BigDecimal("0.00"); //75循环额度下分账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的借款金额的总和
            BigDecimal ass_xhd_credit_loan_money = new BigDecimal("0.00"); //76循环贷账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的借款金额总和
            int late_2y_fxhd_due_count_sum = 0;//77非循环贷账户24个月还款记录有逾期情况的账户总数
            int late_2y_xhf_due_count_sum = 0;//78循环额度下分账户24个月还款记录有逾期情况的账户总数
            int late_2y_xhd_due_count_sum = 0;//79循环贷账户24个月还款记录有逾期情况的账户总数


            if (borAccInfUnitBoList != null && borAccInfUnitBoList.size() > 0) {
                //循环借贷账户信息list
                for (BorAccInfUnitBo borAccInfUnitBo : borAccInfUnitBoList) {
                    //借贷账户信息-基本信息段
                    PcabBasicinfoseg pcabBasicinfoseg = borAccInfUnitBo.getPcabBasicinfoseg();
                    //借贷账户信息-最新表现信息段
                    PcanFirinfseg pcanFirinfseg = borAccInfUnitBo.getPcanFirinfseg();
                    //最近一次月度表现信息段
                    PcamMonthInformation pcamMonthInformation = borAccInfUnitBo.getPcamMonthInformation();
                    //最近24个月还款记录信息段
                    PcatTwentyfourpaymentstat pcatTwentyfourpaymentstat = borAccInfUnitBo.getPcatTwentyfourpaymentstat();

                    //基本信息段
                    if (pcabBasicinfoseg != null) {


                        String accountType = pcabBasicinfoseg.getAccountType();//账户类型
                        Date issuanceDate = pcabBasicinfoseg.getIssuanceDate();//开立日期
                        Date dueDate = pcabBasicinfoseg.getDueDate();//到期日期
                        String guaranteeMode = pcabBasicinfoseg.getGuaranteeMode();//担保方式
                        BigDecimal loanAmount = new BigDecimal("0.00");//借款金额
                        BigDecimal recentlyBalance = new BigDecimal("0.00");//最近一个月余额
                        BigDecimal newestBalance = new BigDecimal("0.00");//最新余额

                        if (pcabBasicinfoseg!=null&&pcabBasicinfoseg.getLoanAmount()!=null){
                            loanAmount = new BigDecimal(pcabBasicinfoseg.getLoanAmount());
                        }
                        if (pcamMonthInformation!=null){
                            String balance =pcamMonthInformation.getBalance();
                            if(StringUtils.isNotBlank(balance)) {
                                recentlyBalance = new BigDecimal(balance);
                            }
                        }
                        if (pcanFirinfseg!=null&&pcanFirinfseg.getBalance()!=null){
                            newestBalance = new BigDecimal(pcanFirinfseg.getBalance());
                        }


                        //授信额度
                        BigDecimal accountQuota=new BigDecimal("0.00");
                        if (Constants.ACC_TYPE_R1.equals(accountType)){
                            if (pcabBasicinfoseg.getAccountQuota()!=null){
                                accountQuota = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                            }
                        }



                        //开立日期相关
                        if (issuanceDate != null) {
                            if (issuanceDate.after(DateUtil.getAimMonthDate(reportDate, -2))) {
                                //非循环贷账户
                                if (Constants.ACC_TYPE_D1.equals(accountType)) {
                                    //开立日期距离报告期2个月内的账户数
                                    late_2mon_fxhd_re_open_num++;
                                }
                                //循环额度下分账户
                                if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                    //开立日期距离报告期2个月内的账户数
                                    late_2mon_xhf_re_open_num++;
                                }
                                //循环贷账户
                                if (Constants.ACC_TYPE_R1.equals(accountType)) {
                                    //开立日期距离报告期2个月内的账户数
                                    late_2mon_xhd_re_open_num++;
                                }
                            }
                        }


                        //最新表现信息段

                            String pcanFirinfseg_balance = pcanFirinfseg.getBalance();//账户余额

                            //到期日期
                            if (dueDate != null) {
                                if (dueDate.before(DateUtil.getAimMonthDate(reportDate, 2))) {
                                    if (StringUtils.isNotEmpty(pcanFirinfseg_balance)) {
                                        //非循环贷账户
                                        if (Constants.ACC_TYPE_D1.equals(accountType)) {
                                            //37 将来两个月非循环贷账户到期余额总和
                                            f_2mon_fxhd_expire_balan_sum = new BigDecimal("0.00");//37 将来两个月非循环贷账户到期余额总和
                                            f_2mon_fxhd_expire_balan_sum = f_2mon_fxhd_expire_balan_sum.add(new BigDecimal(pcanFirinfseg_balance));
                                            personZBInfoPo.setF2monFxhdExpireBalanSum(String.valueOf(f_2mon_fxhd_expire_balan_sum));//将来两个月非循环贷账户到期余额总和
                                        }

                                        //循环贷账户
                                        if (Constants.ACC_TYPE_R1.equals(accountType)) {
                                            //38将来两个月循环贷账户到期余额总和
                                            f_2mon_xhd_expire_balan_sum = new BigDecimal("0.00");//38将来两个月循环贷账户到期余额总和
                                            f_2mon_xhd_expire_balan_sum = f_2mon_xhd_expire_balan_sum.add(new BigDecimal(pcanFirinfseg_balance));
                                            personZBInfoPo.setF2monXhdExpireBalanSum(String.valueOf(f_2mon_xhd_expire_balan_sum));//38将来两个月循环贷账户到期余额总和
                                        }

                                        //循环额度下分账户
                                        if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                            //39将来两个月循环额度下分账户到期余额总和
                                            f_2mon_xhf_expire_balan_sum = new BigDecimal("0.00");//39将来两个月循环额度下分账户到期余额总和
                                            f_2mon_xhf_expire_balan_sum = f_2mon_xhf_expire_balan_sum.add(new BigDecimal(pcanFirinfseg_balance));
                                            personZBInfoPo.setF2monXhfExpireBalanSum(String.valueOf(f_2mon_xhf_expire_balan_sum));
                                        }
                                    }
                                }
                            }


                                //非循环贷、质押
                                if (Constants.ACC_TYPE_D1.equals(accountType) && (Constants.GUARANTY_1).equals(guaranteeMode)) {
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        //非循环贷账户担保方式为质押的账户数
                                        ass_fxhd_dy_count_num = ass_fxhd_dy_count_num + 1;
                                        //非循环贷账户担保方式为质押的借款金额总和
                                        ass_fxhd_zy_loan_money = ass_fxhd_zy_loan_money.add(loanAmount);
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0) {
                                        ass_fxhd_dy_count_num = ass_fxhd_dy_count_num + 1;
                                        //非循环贷账户担保方式为质押的借款金额总和
                                        ass_fxhd_zy_loan_money = ass_fxhd_zy_loan_money.add(loanAmount);
                                    }
                                }

                                //非循环贷、抵押
                                if (Constants.ACC_TYPE_D1.equals(accountType) && (Constants.GUARANTY_2).equals(guaranteeMode)) {
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        //非循环贷账户担保方式为抵押的账户数
                                        ass_fxhd_zy_count_num = ass_fxhd_zy_count_num + 1;
                                        //非循环贷账户担保方式为抵押的借款金额总和
                                        ass_fxhd_dy_loan_money = ass_fxhd_dy_loan_money.add(loanAmount);
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0) {
                                        ass_fxhd_zy_count_num = ass_fxhd_zy_count_num + 1;
                                        //非循环贷账户担保方式为抵押的借款金额总和
                                        ass_fxhd_dy_loan_money = ass_fxhd_dy_loan_money.add(loanAmount);
                                    }
                                }

                                //非循环贷、保证
                                if (Constants.ACC_TYPE_D1.equals(accountType) && (Constants.GUARANTY_3).equals(guaranteeMode)) {
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        //非循环贷账户担保方式为保证的账户数
                                        ass_fxhd_bz_count_num = ass_fxhd_bz_count_num + 1;
                                        //非循环贷账户担保方式为保证的借款金额总和
                                        ass_fxhd_bz_loan_money = ass_fxhd_bz_loan_money.add(loanAmount);
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0) {
                                        ass_fxhd_bz_count_num = ass_fxhd_bz_count_num + 1;
                                        //非循环贷账户担保方式为保证的借款金额总和
                                        ass_fxhd_bz_loan_money = ass_fxhd_bz_loan_money.add(loanAmount);
                                    }
                                }

                                //非循环贷、信用/免担保，组合(含保证，不含保证)，农户联保
                            if( guaranteeMode != null){
                                if (Constants.ACC_TYPE_D1.equals(accountType)  && (Constants.GUARANTY_STYLE).contains(guaranteeMode)) {
                                    // 非循环贷账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的借款金额总和
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        ass_fxhd_credit_loan_money = ass_fxhd_credit_loan_money.add(loanAmount);
                                        ass_fxhd_credit_count_num = ass_fxhd_credit_count_num + 1;
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0){
                                        ass_fxhd_credit_loan_money = ass_fxhd_credit_loan_money.add(loanAmount);
                                        ass_fxhd_credit_count_num = ass_fxhd_credit_count_num + 1;
                                    }
                                }
                            }

                                //循环额度下分账户、质押
                                if (Constants.ACC_TYPE_R4.equals(accountType) && Constants.GUARANTY_1.equals(guaranteeMode)) {
                                    //循环额度下分账户担保方式为质押的账户数
                                    ass_xhf_zy_count_num = ass_xhf_zy_count_num + 1;
                                    //循环额度下分账户担保方式为质押的借款金额的总和
                                        ass_xhf_zy_loan_money = ass_xhf_zy_loan_money.add(loanAmount);
                                }

                                //循环额度下分账户、抵押
                                if (Constants.ACC_TYPE_R4.equals(accountType) && Constants.GUARANTY_2.equals(guaranteeMode)) {
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        //循环额度下分账户担保方式为抵押的账户数
                                        ass_xhf_dy_count_num = ass_xhf_dy_count_num + 1;
                                        //循环额度下分账户担保方式为抵押的借款金额的总和
                                        ass_xhf_dy_loan_money = ass_xhf_dy_loan_money.add(loanAmount);
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0){
                                        ass_xhf_dy_count_num = ass_xhf_dy_count_num + 1;
                                        ass_xhf_dy_loan_money = ass_xhf_dy_loan_money.add(loanAmount);
                                    }
                                }

                                //循环额度下分账户、保证
                                if (Constants.ACC_TYPE_R4.equals(accountType) && Constants.GUARANTY_3.equals(guaranteeMode)) {
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        //循环额度下分账户担保方式为保证的账户数
                                        ass_xhf_bz_count_num = ass_xhf_bz_count_num + 1;
                                        //循环额度下分账户担保方式为保证的借款金额的总和
                                        ass_xhf_bz_loan_money = ass_xhf_bz_loan_money.add(loanAmount);
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0){
                                        ass_xhf_bz_count_num = ass_xhf_bz_count_num + 1;
                                        //循环额度下分账户担保方式为保证的借款金额的总和
                                        ass_xhf_bz_loan_money = ass_xhf_bz_loan_money.add(loanAmount);
                                    }
                                }


                                //循环额度下分账户、信用/免担保，组合(含保证，不含保证)，农户联保
                                if (Constants.ACC_TYPE_R4.equals(accountType) && Constants.GUARANTY_STYLE.contains(guaranteeMode)) {
                                    //循环额度下分账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的账户数
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        ass_xhf_credit_count_num = ass_xhf_credit_count_num + 1;
                                        //循环额度下分账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的借款金额的总和
                                        ass_xhf_credit_loan_money = ass_xhf_credit_loan_money.add(loanAmount);
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0){
                                        ass_xhf_credit_count_num = ass_xhf_credit_count_num + 1;
                                        ass_xhf_credit_loan_money = ass_xhf_credit_loan_money.add(loanAmount);
                                    }
                                }

                                //循环贷账户、质押
                                if (Constants.ACC_TYPE_R1.equals(accountType) && Constants.GUARANTY_1.equals(guaranteeMode)) {
                                    //循环贷账户担保方式为质押的借款金额总和
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        ass_xhd_zy_loan_money = ass_xhd_zy_loan_money.add(accountQuota);
                                        //循环贷账户担保方式为质押的账户数
                                        ass_xhd_dy_count_num = ass_xhd_dy_count_num + 1;
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0){
                                        ass_xhd_zy_loan_money = ass_xhd_zy_loan_money.add(accountQuota);
                                        //循环贷账户担保方式为质押的账户数
                                        ass_xhd_dy_count_num = ass_xhd_dy_count_num + 1;
                                    }
                                }

                                //循环贷账户、抵押
                                if (Constants.ACC_TYPE_R1.equals(accountType) && Constants.GUARANTY_2.equals(guaranteeMode)) {

                                    //循环贷账户担保方式为抵押的借款金额总和
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        ass_xhd_dy_loan_money = ass_xhd_dy_loan_money.add(accountQuota);
                                        //循环贷账户担保方式为抵押的账户数
                                        ass_xhd_zy_count_num = ass_xhd_zy_count_num + 1;
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0){
                                        ass_xhd_dy_loan_money = ass_xhd_dy_loan_money.add(accountQuota);
                                        //循环贷账户担保方式为抵押的账户数
                                        ass_xhd_zy_count_num = ass_xhd_zy_count_num + 1;
                                    }
                                }

                                //循环贷账户、保证
                                if (Constants.ACC_TYPE_R1.equals(accountType) && Constants.GUARANTY_3.equals(guaranteeMode)) {
                                    //循环贷账户担保方式为保证的借款金额总和
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        ass_xhd_bz_loan_money = ass_xhd_bz_loan_money.add(accountQuota);
                                        //循环贷账户担保方式为保证的账户数
                                        ass_xhd_bz_count_num = ass_xhd_bz_count_num + 1;
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0){
                                        ass_xhd_bz_loan_money = ass_xhd_bz_loan_money.add(accountQuota);
                                        //循环贷账户担保方式为保证的账户数
                                        ass_xhd_bz_count_num = ass_xhd_bz_count_num + 1;
                                    }
                                }

                                //循环贷账户、信用/免担保，组合(含保证，不含保证)，农户联保
                                if (Constants.ACC_TYPE_R1.equals(accountType) && guaranteeMode != null && Constants.GUARANTY_STYLE.contains(guaranteeMode)) {
                                    //循环贷账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的借款金额总和
                                    if (newestBalance.compareTo(BigDecimal.ZERO)>0) {
                                        //循环贷账户担保方式为信用/免担保，组合1(含保证，不含保证)，农户联保，其他的账户数
                                        ass_xhd_credit_count_num = ass_xhd_credit_count_num + 1;
                                        ass_xhd_credit_loan_money = ass_xhd_credit_loan_money.add(accountQuota);
                                    }else if (recentlyBalance.compareTo(BigDecimal.ZERO)>0){
                                        ass_xhd_credit_count_num = ass_xhd_credit_count_num + 1;
                                        ass_xhd_credit_loan_money = ass_xhd_credit_loan_money.add(accountQuota);
                                    }
                                }


                        //最近一次月度表现信息段
                        if (pcamMonthInformation != null) {

                            String overdueAmount = pcamMonthInformation.getOverdueAmount();//当前逾期总额
                            String overduePeriod = pcamMonthInformation.getOverduePeriod();//当前逾期期数


                            if (StringUtils.isNotEmpty(overdueAmount)) {
                                BigDecimal formationAmount= new BigDecimal("0.00");//最近一个月度变现信息段逾期总额
                                if (StringUtils.isNotEmpty(overdueAmount)) {
                                    formationAmount = new BigDecimal(overdueAmount);
                                }

                                BigDecimal firinfsegLastRepmoney = new BigDecimal("0.00");//最新表现字段还款金额
                                if (pcanFirinfseg != null) {
                                    if (StringUtils.isNotEmpty(pcanFirinfseg.getLastRepmoney())) {
                                        firinfsegLastRepmoney  = new BigDecimal(pcanFirinfseg.getLastRepmoney());
                                    }
                                }

                                //最近一个月度变现信息段逾期总额-最新表现字段还款金额大于等于0
                                if (((formationAmount.subtract(firinfsegLastRepmoney)).compareTo(BigDecimal.ZERO)) > 0) {

                                    //非循环贷账户
                                    if (Constants.ACC_TYPE_D1.equals(accountType)) {

                                        c_fxhd_loan_due_amt = c_fxhd_loan_due_amt.add(new BigDecimal(overdueAmount));


                                        if (overduePeriod != null) {
                                            if (c_fxhd_loan_max_due_num < Integer.parseInt(overduePeriod)) {
                                                c_fxhd_loan_max_due_num = Integer.parseInt(overduePeriod);
                                            }
                                            personZBInfoPo.setCFxhdLoanMaxDueNum(String.valueOf(c_fxhd_loan_max_due_num));//48当前非循环贷账户最大逾期期数
                                        }

                                        if (Double.parseDouble(overdueAmount) > 0) {
                                            c_fxhd_loan_due_sum++;//51当前非循环贷款账户逾期账户总数
                                        }
                                    }

                                    //当前循环额度下分账户
                                    if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                        c_xhf_loan_due_amt = c_xhf_loan_due_amt.add(new BigDecimal(overdueAmount));

                                        if (overduePeriod != null) {
                                            if (c_xhf_loan_max_due_num < Integer.parseInt(overduePeriod)) {
                                                c_xhf_loan_max_due_num = Integer.parseInt(overduePeriod);
                                            }
                                            personZBInfoPo.setCXhfLoanMaxDueNum(String.valueOf(c_xhf_loan_max_due_num));//48当前非循环贷账户最大逾期期数
                                        }

                                        if (Double.parseDouble(pcamMonthInformation.getOverdueAmount()) > 0) {
                                            c_xhf_loan_due_sum++;//51当前非循环贷款账户逾期账户总数
                                        }
                                    }


                                    //当前循环账户
                                    if (Constants.ACC_TYPE_R1.equals(accountType)) {
                                        c_xhd_loan_due_amt = c_xhd_loan_due_amt.add(new BigDecimal(overdueAmount));
                                        personZBInfoPo.setCXhdLoanDueAmt(String.valueOf(c_xhd_loan_due_amt));//47当前循环贷账户逾期总额

                                        if (overduePeriod != null) {
                                            if (c_xhd_loan_max_due_num < Integer.parseInt(overduePeriod)) {
                                                c_xhd_loan_max_due_num = Integer.parseInt(overduePeriod);
                                            }
                                            personZBInfoPo.setCXhdLoanDueSum(String.valueOf(c_xhd_loan_max_due_num));//50当前循环贷账户最大逾期期数
                                        }

                                        if (Double.parseDouble(pcamMonthInformation.getOverdueAmount()) > 0) {
                                            c_xhd_loan_due_sum++;//53当前循环贷账户逾期账户总数
                                        }
                                    }
                                }
                            }
                        }

                        //最近24个月还款记录信息段
                        if (pcatTwentyfourpaymentstat != null) {

                            List<PcatTwentyfourpaymentstatsSub> pcatTwentyfourpaymentstatsSubList = pcatTwentyfourpaymentstat.getSub();
                            if (pcatTwentyfourpaymentstatsSubList != null && pcatTwentyfourpaymentstatsSubList.size() > 0) {

                                if (Constants.ACC_TYPE_D1.equals(accountType) || Constants.ACC_TYPE_R1.equals(accountType) || Constants.ACC_TYPE_R4.equals(accountType)) {
                                    for (PcatTwentyfourpaymentstatsSub pcatTwentyfourpaymentstatsSub : pcatTwentyfourpaymentstatsSubList) {
                                        if (Constants.OVERDUE2.contains(pcatTwentyfourpaymentstatsSub.getPaymentStats())) {//逾期

                                            if (Constants.ACC_TYPE_D1.equals(accountType)) {
                                                late_2y_fxhd_due_count_sum++;//78非循环贷账户24个月还款记录有逾期情况的账户总数
                                            }
                                            if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                                late_2y_xhf_due_count_sum++;//79循环额度下分账户24个月还款记录有逾期情况的账户总数
                                            }
                                            if (Constants.ACC_TYPE_R1.equals(accountType)) {
                                                late_2y_xhd_due_count_sum++;//80循环贷账户24个月还款记录有逾期情况的账户总数
                                            }

                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            personZBInfoPo.setCXhfLoanDueSum(String.valueOf(c_xhf_loan_due_amt));//46当前循环额度下分账户逾期总额
            //42非循环贷账户开立日期距离报告期2个月内的账户数
            personZBInfoPo.setLate2monFxhdReOpenNum(String.valueOf(late_2mon_fxhd_re_open_num));
            //43循环额度下分账户开立日期距离报告期2个月内的账户数
            personZBInfoPo.setLate2monXhfReOpenNum(String.valueOf(late_2mon_xhf_re_open_num));
            //44循环贷账户开立日期距离报告期2个月内的账户数
            personZBInfoPo.setLate2monXhdReOpenNum(String.valueOf(late_2mon_xhd_re_open_num));
            //51当前非循环贷款账户逾期账户总数
            personZBInfoPo.setCFxhdLoanDueSum(String.valueOf(c_fxhd_loan_due_sum));
            //52当前循环额度下分账户逾期账户总数
            personZBInfoPo.setCXhfLoanDueSum(String.valueOf(c_xhf_loan_due_sum));
            //53当前循环贷账户逾期账户总数
            personZBInfoPo.setCXhdLoanDueSum(String.valueOf(c_xhd_loan_due_sum));
            //非循环贷账户担保方式为质押的账户数
            personZBInfoPo.setAssFxhdDyCountNum(String.valueOf(ass_fxhd_dy_count_num));
            //循环额度下分账户担保方式为质押的账户数
            personZBInfoPo.setAssXhfZyCountNum(String.valueOf(ass_xhf_zy_count_num));
            //45当前非循环贷账户逾期总额
            personZBInfoPo.setCFxhdLoanDueAmt(String.valueOf(c_fxhd_loan_due_amt));
            //循环贷账户担保方式为质押的账户数
            personZBInfoPo.setAssXhdDyCountNum(String.valueOf(ass_xhd_dy_count_num));
            //非循环贷账户担保方式为抵押的账户数
            personZBInfoPo.setAssFxhdZyCountNum(String.valueOf(ass_fxhd_zy_count_num));
            //循环额度下分账户担保方式为抵押的账户数
            personZBInfoPo.setAssXhfDyCountNum(String.valueOf(ass_xhf_dy_count_num));
            //循环贷账户担保方式为抵押的账户数
            personZBInfoPo.setAssXhdZyCountNum(String.valueOf(ass_xhd_zy_count_num));
            //非循环贷账户担保方式为保证的账户数
            personZBInfoPo.setAssFxhdBzCountNum(String.valueOf(ass_fxhd_bz_count_num));
            //循环额度下分账户担保方式为保证的账户数
            personZBInfoPo.setAssXhfBzCountNum(String.valueOf(ass_xhf_bz_count_num));
            //循环贷账户担保方式为保证的账户数
            personZBInfoPo.setAssXhdBzCountNum(String.valueOf(ass_xhd_bz_count_num));
            //非循环贷账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的账户数
            personZBInfoPo.setAssFxhdCreditCountNum(String.valueOf(ass_fxhd_credit_count_num));
            //循环额度下分账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的账户数
            personZBInfoPo.setAssXhfCreditCountNum(String.valueOf(ass_xhf_credit_count_num));
            //循环贷账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的账户数
            personZBInfoPo.setAssXhdCreditCountNum(String.valueOf(ass_xhd_credit_count_num));
            //非循环贷账户担保方式为质押的借款金额总和
            personZBInfoPo.setAssFxhdZyLoanMoney(String.valueOf(ass_fxhd_zy_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //循环额度下分账户担保方式为质押的借款金额的总和
            personZBInfoPo.setAssXhfZyLoanMoney(String.valueOf(ass_xhf_zy_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //循环贷账户担保方式为质押的借款金额总和
            personZBInfoPo.setAssXhdZyLoanMoney(String.valueOf(ass_xhd_zy_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //非循环贷账户担保方式为抵押的借款金额总和
            personZBInfoPo.setAssFxhdDyLoanMoney(String.valueOf(ass_fxhd_dy_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //非循环贷账户担保方式为保证的借款金额总和
            personZBInfoPo.setAssFxhdBzLoanMoney(String.valueOf(ass_fxhd_bz_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            // 非循环贷账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的借款金额总和
            personZBInfoPo.setAssFxhdCreditLoanMoney(String.valueOf(ass_fxhd_credit_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //循环额度下分账户担保方式为抵押的借款金额的总和
            personZBInfoPo.setAssXhfDyLoanMoney(String.valueOf(ass_xhf_dy_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //循环额度下分账户担保方式为保证的借款金额的总和
            personZBInfoPo.setAssXhfBzLoanMoney(String.valueOf(ass_xhf_bz_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //循环额度下分账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的借款金额的总和
            personZBInfoPo.setAssXhfCreditLoanMoney(String.valueOf(ass_xhf_credit_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //循环贷账户担保方式为抵押的借款金额总和
            personZBInfoPo.setAssXhdDyLoanMoney(String.valueOf(ass_xhd_dy_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //循环贷账户担保方式为保证的借款金额总和
            personZBInfoPo.setAssXhdBzLoanMoney(String.valueOf(ass_xhd_bz_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //循环贷账户担保方式为信用/免担保，组合(含保证，不含保证)，农户联保，其他的借款金额总和
            personZBInfoPo.setAssXhdCreditLoanMoney(String.valueOf(ass_xhd_credit_loan_money.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //78非循环贷账户24个月还款记录有逾期情况的账户总数
            personZBInfoPo.setLate2yFxhdDueCountSum(String.valueOf(late_2y_fxhd_due_count_sum));
            //79循环额度下分账户24个月还款记录有逾期情况的账户总数
            personZBInfoPo.setLate2yXhfDueCountSum(String.valueOf(late_2y_xhf_due_count_sum));
            //80循环贷账户24个月还款记录有逾期情况的账户总数
            personZBInfoPo.setLate2yXhdDueCountSum(String.valueOf(late_2y_xhd_due_count_sum));



            //个人指标30 将来2个月贷款到期金额数（贷款有无集中到期）
            BigDecimal loanAmt = f_2mon_fxhd_expire_balan_sum.add(f_2mon_xhd_expire_balan_sum).add(f_2mon_xhf_expire_balan_sum);
            BigDecimal f_2_m_un_sett_loan_amt = loanAmt.setScale(2, BigDecimal.ROUND_HALF_UP);
            personZBInfoPo.setF2MUnSettLoanAmt(String.valueOf(f_2_m_un_sett_loan_amt));


            //个人指标41 当前贷款逾期总额
            BigDecimal cOdTotal = c_fxhd_loan_due_amt.add(c_xhf_loan_due_amt).add(c_xhd_loan_due_amt);
            BigDecimal c_od_total_amt = cOdTotal.setScale(2, BigDecimal.ROUND_HALF_UP);
            personZBInfoPo.setCOdTotalAmt(String.valueOf(c_od_total_amt));


            //个人指标55 过去24个月有多少个月存在拖欠账户（贷款）
            int last_24_mon_owe_acct_num = late_2y_fxhd_due_count_sum + late_2y_xhf_due_count_sum + late_2y_xhd_due_count_sum;
            personZBInfoPo.setLast24MonOweAcctNum(String.valueOf(last_24_mon_owe_acct_num));

            //个人指标42 当前贷款最大逾期期数
            BigDecimal max = new BigDecimal(c_fxhd_loan_max_due_num).max(new BigDecimal(c_xhf_loan_max_due_num)).max(new BigDecimal(c_xhd_loan_max_due_num));
            personZBInfoPo.setCOdMaxNum(String.valueOf(max));

            return personZBInfoPo;

        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}
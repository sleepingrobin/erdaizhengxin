package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PiccCreditproinfo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PiccCreditproinfosub;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.credittransinfdata.entity.CreTraInfSummaryBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.credittransinfdata.entity.CreTraInfSummaryUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 基础指标计算（信息概要-信贷交易信息提示部分）
 */
@Service
@Slf4j
public class BasicZBCalculateSummaryInfoPartOneServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {
        PersonZBInfoPo personZBInfoPo = new PersonZBInfoPo();
        try {
            //信贷交易信息概要（PC0）
            CreTraInfSummaryBo creTraInfSummaryBo = cpqReportBo.getCreTraInfSummaryBo();
            if (creTraInfSummaryBo == null) {
                return null;
            }
            //信贷交易信息概要信息单元（PC02）
            CreTraInfSummaryUnitBo creTraInfSummaryUnitBo = creTraInfSummaryBo.getCreTraInfSummaryUnitBo();
            if (creTraInfSummaryUnitBo == null) {
                return null;
            }
            //信贷交易提示信息段（PC02A）
            PiccCreditproinfo piccCreditproinfo = creTraInfSummaryUnitBo.getPiccCreditproinfo();
            if (piccCreditproinfo == null) {
                return null;
            }
            //概要信息-信贷交易信息提示list（PC02AH）
            List<PiccCreditproinfosub> piccCreditproinfosubList = piccCreditproinfo.getPiccCreditproinfosubList();
            if (piccCreditproinfosubList == null) {
                return null;
            }

            //报告日期
            Date reportDate = cpqReportBo.getRepheadBo().getRepheadUnitBo().getPchrPchrreportinfo().getReportTime();

            //3首张贷款发放日期
            Date firstDkReleaseDate = null;
            //4首张贷记卡发卡日期
            Date firstDjkReleaseDate = null;
            //5首张准贷记卡发卡日期
            Date firstZdjkReleaseDate = null;
            //6 首次除贷款和信用卡发生业务日期
            Date firstXzdjkReleaseDate = null;

            if (piccCreditproinfosubList.size() > 0) {
                for (PiccCreditproinfosub piccCreditproinfosub : piccCreditproinfosubList) {
                    //业务类型
                    String businessType  = piccCreditproinfosub.getBusiType();

                    //计算首张贷款发放日期
                    if (Constants.BUSINESS_11.equals(businessType) || Constants.BUSINESS_12.equals(businessType) || Constants.BUSINESS_99.equals(businessType)) {
                        //首笔业务发放月份取最小
                        if (piccCreditproinfosub.getFirstbusiMonth() != null) {
                            if (firstDkReleaseDate == null) {
                                firstDkReleaseDate = piccCreditproinfosub.getFirstbusiMonth();
                            } else {
                                if (firstDkReleaseDate.after(piccCreditproinfosub.getFirstbusiMonth())) {
                                    firstDkReleaseDate = piccCreditproinfosub.getFirstbusiMonth();
                                }
                            }
                        }
                    }

                    //计算首张贷记卡发卡日期
                    if (Constants.BUSINESS_21.equals(businessType)) {
                        firstDjkReleaseDate = piccCreditproinfosub.getFirstbusiMonth();
                    }

                    //计算首张准贷记卡发卡日期
                    if (Constants.BUSINESS_22.equals(businessType)) {
                        firstZdjkReleaseDate = piccCreditproinfosub.getFirstbusiMonth();
                    }

                    //计算首次除贷款和信用卡发生业务日期
                    if (Constants.BUSINESS_99.equals(businessType)) {
                        firstXzdjkReleaseDate = piccCreditproinfosub.getFirstbusiMonth();
                    }
                }
            }
            personZBInfoPo.setFirstDkReleaseDate(DateUtil.monthFormat(firstDkReleaseDate));//首张贷款发放日期
            personZBInfoPo.setFirstDjkReleaseDate(DateUtil.monthFormat(firstDjkReleaseDate));//首张贷记卡发卡日期
            personZBInfoPo.setFirstZdjkReleaseDate(DateUtil.monthFormat(firstZdjkReleaseDate));//首张准贷记卡发卡日期
            personZBInfoPo.setFirstXzdjkReleaseDate(DateUtil.monthFormat(firstXzdjkReleaseDate));//首次除贷款和信用卡发生业务日期


            //与银行建立信贷关系月数
            //贷款、贷记卡、准贷记卡首比发放日期取最小值
            Date minDate = DateUtil.getEarliestDate(firstDkReleaseDate, firstDjkReleaseDate, firstZdjkReleaseDate);
            //计算报告日期与最小日期之间相隔的月数
            personZBInfoPo.setRelaBankMonNum(String.valueOf(DateUtil.getDateOverMonth(minDate, reportDate)));

            return personZBInfoPo;

        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}
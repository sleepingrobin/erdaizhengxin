package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.PcabBasicinfoseg;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * 基础指标计算（信息概要-信贷交易违约信息概要部分）
 */
@Service
public class BasicZBCalculateSummaryInfoPartTwoServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo reportbo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();
        try {
            //借贷账户信息(PDA)
            BorAccInfBo borAccInfBo = reportbo.getBorAccInfBo();
            //借贷账户信息单元(PD01)
            List<BorAccInfUnitBo> borAccInfUnitBoList = null;
            if (borAccInfBo != null && borAccInfBo.getBorAccInfUnitBoList() != null) {
                borAccInfUnitBoList = borAccInfBo.getBorAccInfUnitBoList();
            }

            Set<String> orgCodeList = new TreeSet<String>();
            Set<String> accountTypeList = new TreeSet<String>();
            if (borAccInfUnitBoList != null && borAccInfUnitBoList.size() > 0) {
                for (BorAccInfUnitBo list : borAccInfUnitBoList) {
                    //借贷账户信息单元(PD01A)
                    PcabBasicinfoseg pcabBasicinfoseg = list.getPcabBasicinfoseg();
                    //账户类型(PD01AD01)
                    String accountType = null;
                    if (pcabBasicinfoseg != null) {
                        accountType = pcabBasicinfoseg.getAccountType();
                    }
                    if (Constants.ACC_TYPE_R1.equals(accountType) || Constants.ACC_TYPE_R4.equals(accountType) || Constants.ACC_TYPE_D1.equals(accountType)) {
                        //17现有贷款授信银行数
                        String orgCode = null;
                        if (pcabBasicinfoseg!=null&&pcabBasicinfoseg.getOrgCode() != null) {
                            orgCode = pcabBasicinfoseg.getOrgCode();
                        }
                        //银行数去重
                        if (StringUtils.isNotBlank(orgCode)) {
                            orgCodeList.add(orgCode);
                        }
                    }

                    //18现有信用卡授信银行数
                    if (Constants.ACC_TYPE_R2.equals(accountType) || Constants.ACC_TYPE_R3.equals(accountType)) {
                        String orgCode = null;
                        if (pcabBasicinfoseg!=null&&pcabBasicinfoseg.getOrgCode() != null) {
                            orgCode = pcabBasicinfoseg.getOrgCode();
                        }
                        accountTypeList.add(orgCode);
                    }
                }
            }
            int curr_loan_bank_num = orgCodeList.size();
            //17现有贷款授信银行数
            zxPZbInfo.setCurrLoanBankNum(String.valueOf(curr_loan_bank_num));

            int curr_cred_bank_num = accountTypeList.size();
            //18现有信用卡授信银行数
            zxPZbInfo.setCurrCredBankNum(String.valueOf(curr_cred_bank_num));



            //个人指标6 有信用业务的银行数
            int cred_biz_bank_num = curr_loan_bank_num + curr_cred_bank_num;
            zxPZbInfo.setCredBizBankNum(String.valueOf(cred_biz_bank_num));

        } catch (Exception e) {
            throw new Exception(e);
        }
        return zxPZbInfo;
    }
}

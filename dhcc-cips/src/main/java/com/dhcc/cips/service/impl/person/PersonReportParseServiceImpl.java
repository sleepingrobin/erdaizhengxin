package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.other.PersonReportAnalysisService;
import com.dhcc.cips.service.person.PersonParseService;
import com.dhcc.cips.service.person.PersonReportParseService;
import com.dhcc.cips.util.XmlUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * <p>
 * 个人征信报告解析服务实现类
 * </p>
 *
 * @author limin
 * @since 2019-05-31
 */
@Service
@Slf4j
public class PersonReportParseServiceImpl implements PersonReportParseService {

    @Autowired
    private PersonParseService personParseService;
    @Autowired
    private PersonZBCalculateProcessor personZBCalculateProcessor;
    @Autowired
    private PersonReportAnalysisService personReportAnalysisService;
    @Autowired
    private PersonReportParseService personReportParseService;

    /**
     * 解析xml版征信报告
     *
     * @param xmlStr
     * @return
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void parseWithXml(String xmlStr) throws Exception {
        try {
            //个人征信报告对象
            CpqReportBo cpqReportBo = XmlUtil.convertXml2JavaBean(CpqReportBo.class, xmlStr, new Class[]{CpqReportBo.class});
            //指标计算
            zbCalculate(cpqReportBo);

        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    /**
     * 解析html版征信报告
     *
     * @param htmlStr
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void parseWithHtml(String htmlStr) throws Exception {
        try {
//            Object object = personParseService.analysisPersonParseReport(htmlStr);
//            log.error("object:" + object);
//            Map<String, Object> map = (LinkedHashMap) object;
//            Object obj = map.get("CpqReportBo");
//            CpqReportBo cpqReportBo = (CpqReportBo) obj;
            String s = personReportAnalysisService.reportAnalysis(htmlStr);
            personReportParseService.parseWithXml(s.replaceAll("--", "0"));
            //指标计算
//            zbCalculate(cpqReportBo);
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

    /**
     * 个人指标计算入库
     *
     * @param cpqReportBo
     */
    public void zbCalculate(CpqReportBo cpqReportBo) throws Exception {
        try {
            //个人报告编号
            String reportNo = cpqReportBo.getRepheadBo().getRepheadUnitBo().getPchrPchrreportinfo().getReportCode();
            PersonZBInfoPo personZBInfoPo = personZBCalculateProcessor.caculate(cpqReportBo);
            personZBInfoPo.setReportNo(reportNo);
            //指标信息入库
            personZBInfoPo.insert();
        } catch (Exception e) {
            throw new Exception(e);
        }
    }

}
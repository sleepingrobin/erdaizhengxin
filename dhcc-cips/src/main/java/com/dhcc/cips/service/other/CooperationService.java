package com.dhcc.cips.service.other;

import com.dhcc.cips.bo.base.Request;

/**
 * <p>
 * 合作行征信报告解析服务类
 * </p>
 *
 * @author limin
 * @since 2019-05-30
 */
public interface CooperationService {
    /**
     * xml解析指标入库
     *
     * @param
     * @param
     * @return
     */

    String reportParse(Request request);
}

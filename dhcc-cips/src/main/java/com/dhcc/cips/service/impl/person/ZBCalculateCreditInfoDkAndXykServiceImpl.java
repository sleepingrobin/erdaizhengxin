package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.*;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.queryrecord.PqqqRecordUnit;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * @author StivenYang
 * @description 三（信贷交易信息明细）-二、三、四、五、六
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateCreditInfoDkAndXykServiceImpl implements PersonZBCalculteThreadService {

    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();
        try {
            //129. 当前银行服务家数
            int cBankServiceNum = 0;
            //156. 他行授信机构数(不含未激活)
            int othBankBusiOrgNum = 0;
            List<Object> othBankBusiOrgNumList = Lists.newArrayList();
            //157. 他行授信总余额(不含未激活)
            BigDecimal othBankCreditTotalAmt = new BigDecimal(0);
            //165. 最近6个月金融机构审批通过家数
            int late6monOrgPassNum = 0;
            HashSet<Object> late6monOrgPassNumSet = Sets.newHashSet();
            //185. 授信机构数（不含未激活）
            int credBusiOrgOutWjq = 0;
            HashSet<Object> credBusiOrgOutWjqSet = Sets.newHashSet();
            //189. 授信余额(经营性贷款+已激活信用卡)
            BigDecimal creditExtensionBalanOutWjh = new BigDecimal(0);
            //195. 近3个月到期结清授信总额
            BigDecimal lateSeaEndSettAmt = new BigDecimal(0);
            //196. 近3个月新增授信总额
            BigDecimal lateSeaAddAmt = new BigDecimal(0);
            //202. 非正常账户数
            int unNormalAccountNum = 0;
            //9. 历史信贷产品数（5年内历史）
            int clProdL5YNum = 0;
            //13. 信用产品数和查询记录数比率
            BigDecimal clProdEnquRecoRate = new BigDecimal(0);
            //54. 是否存在逾期记录（历史逾期标识） 0：否 1：是
            int odRecoTotalNum = 0;
            //56. 以非结清形式销户数
            int unSettColseAcctNum = 0;
            //57. 当前逾期账户数
            int cOdAcctNum = 0;
            //115. 他行授信总额
            BigDecimal othCredTotalAmt = new BigDecimal(0);
            //61. 近6个月的最大逾期期数
            int wdil6m = 0;
            //62. 近12个月的最大逾期期数
            int wdil12m = 0;
            //63. 近24个月的最大逾期期数
            int wdi24m = 0;
            //67. 信用透支额与贷款每月还款额之和
            BigDecimal oumd = new BigDecimal(0);
            //68. 信用额与担保贷款额之和
            BigDecimal oumue = new BigDecimal(0);
            //74. 历史贷款逾期笔数
            int loanHisOdNum = 0;
            //123. 最近3个月审核通过机构家数
            int late3monOrgPassNum = 0;
            //130. 剔除贷款类别总授信额度
            BigDecimal creditAmt = new BigDecimal(0);

            HashSet<Object> cBankServiceNumSet = Sets.newHashSet();
            HashSet<Object> late3monOrgPassNumSet = Sets.newHashSet();


            //报告日期
            Date reportTime = cpqReportBo.getRepheadBo().getRepheadUnitBo().getPchrPchrreportinfo().getReportTime();
            //前3个月
            Date aimMonthDate3 = DateUtil.getAimMonthDate(reportTime, -3);
            //前6个月
            Date aimMonthDate6 = DateUtil.getAimMonthDate(reportTime, -6);
            //前5年
            Date aimYearDate5 = DateUtil.getAimYearDate(reportTime, -5);

            List<BorAccInfUnitBo> borAccInfUnitBoList = cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList();
            List<PqqqRecordUnit> pqqqRecordUnitList = cpqReportBo.getQueryRecordBo().getPqqqRecordUnitList();
            if (null != borAccInfUnitBoList && borAccInfUnitBoList.size() > 0) {
                for (BorAccInfUnitBo borAccInfUnitBo : borAccInfUnitBoList) {
                    /***************************账户公共变量开始**************************/
                    //基本信息段
                    PcabBasicinfoseg pcabBasicinfoseg = borAccInfUnitBo.getPcabBasicinfoseg();
                    //开立日期
                    Date issuanceDate = null;
                    //账户类型
                    String accountType = null;
                    //交易类型
                    String businessType = null;
                    //组织代码
                    String orgCode = null;
                    //借款金额
                    BigDecimal loanAmount = new BigDecimal(0);
                    //担保方式
                    String guaranteeMode = null;
                    //授信额度
                    BigDecimal accountQuota = new BigDecimal(0);
                    //币种
                    String currency = null;
                    //组织机构类型
                    String orgType = null;
                    if (null != pcabBasicinfoseg) {
                        issuanceDate = pcabBasicinfoseg.getIssuanceDate();
                        accountType = pcabBasicinfoseg.getAccountType();
                        currency = pcabBasicinfoseg.getCurrency();
                        businessType = pcabBasicinfoseg.getBusinessType();
                        orgCode = pcabBasicinfoseg.getOrgCode();
                        guaranteeMode = pcabBasicinfoseg.getGuaranteeMode();
                        if (StringUtils.isNotBlank(pcabBasicinfoseg.getLoanAmount())) {
                            loanAmount = new BigDecimal(pcabBasicinfoseg.getLoanAmount());
                        }
                        if (StringUtils.isNotBlank(pcabBasicinfoseg.getAccountQuota())) {
                            accountQuota = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                        }
                        orgType = pcabBasicinfoseg.getOrgType();
                    }
                    //最新表现字段
                    PcanFirinfseg pcanFirinfseg = borAccInfUnitBo.getPcanFirinfseg();
                    //最近两年还款状态
                    PcatTwentyfourpaymentstat pcatTwentyfourpaymentstat = borAccInfUnitBo.getPcatTwentyfourpaymentstat();
                    //最近两年支付列表
                    List<PcatTwentyfourpaymentstatsSub> twentyfourpaymentstatsSubList = null;
                    if (pcatTwentyfourpaymentstat != null) {
                        twentyfourpaymentstatsSubList = pcatTwentyfourpaymentstat.getSub();
                    }
                    //最近五年表现字段
                    PcapRecfivehisinfo pcapRecfivehisinfo = borAccInfUnitBo.getPcapRecfivehisinfo();
                    //最近五年偿还列表
                    List<PcapRecfivehisinfoSub> pcapRecfivehisinfoSubList = null;
                    if (pcapRecfivehisinfo != null) {
                        pcapRecfivehisinfoSubList = pcapRecfivehisinfo.getPcapRecfivehisinfoSubList();
                    }
                    //账户状态
                    String accStatus = null;
                    //最新表现字段 余额
                    BigDecimal balance = new BigDecimal(0);
                    //关闭日期
                    Date closeDate = null;
                    if (pcanFirinfseg != null) {
                        accStatus = pcanFirinfseg.getAccStatus();
                        if (StringUtils.isNotEmpty(pcanFirinfseg.getBalance())) {
                            balance = new BigDecimal(pcanFirinfseg.getBalance());
                        }
                        closeDate = pcanFirinfseg.getCloseDate();
                    }
                    //最近一个月的账户信息
                    PcamMonthInformation pcamMonthInformation = borAccInfUnitBo.getPcamMonthInformation();
                    //已用额度
                    BigDecimal quotaUsed = new BigDecimal(0);
                    //还款额
                    BigDecimal repayment = new BigDecimal(0);
                    //最近一月段 余额
                    BigDecimal monBalance = new BigDecimal(0);
                    if (pcamMonthInformation != null) {
                        if (StringUtils.isNotBlank(pcamMonthInformation.getQuotaUsed())) {
                            quotaUsed = new BigDecimal(pcamMonthInformation.getQuotaUsed());
                        }
                        if (StringUtils.isNotBlank(pcamMonthInformation.getRepayment())) {
                            repayment = new BigDecimal(pcamMonthInformation.getRepayment());
                        }
                        if (StringUtils.isNotBlank(pcamMonthInformation.getBalance())) {
                            monBalance = new BigDecimal(pcamMonthInformation.getBalance());
                        }
                    }

                    /***************************账户公共变量结束**************************/

                    //129. 当前银行服务家数
                    cBankServiceNumSet.add(orgType);
                    cBankServiceNum = cBankServiceNumSet.size();

                    //开立日期账户总数
                    int openingDate = 0;
                    //查询日期账户总数
                    int query = 0;
                    if (null != borAccInfUnitBoList) {
                        for (BorAccInfUnitBo accInfUnitBoList : borAccInfUnitBoList) {
                            if (accInfUnitBoList.getPcabBasicinfoseg() != null) {
                                if (issuanceDate != null && reportTime.after(issuanceDate)
                                        && issuanceDate.after(aimMonthDate3)) {
                                    //开立日期账户总数
                                    ++openingDate;
                                }
                            }
                        }
                    }
                    if (null != pqqqRecordUnitList) {
                        for (PqqqRecordUnit pqqqRecordList : pqqqRecordUnitList) {
                            Date queryDate = pqqqRecordList.getQueryDate();
                            if (reportTime.after(queryDate)
                                    && queryDate.before(aimMonthDate3)) {
                                //查询日期账户总数
                                ++query;
                            }
                        }
                    }
                    if (query != 0) {
                        clProdEnquRecoRate = new BigDecimal(openingDate / query);
                    }

                    if (twentyfourpaymentstatsSubList != null) {
                        for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                            if ((Constants.REPAY_STAT_G).equals(subList.getPaymentStats())) {
                                unSettColseAcctNum = unSettColseAcctNum + 1;
                            }
                        }
                    }
                    boolean judge = true;
                    if (Constants.ACC_TYPE_R1.equals(accountType)
                            || Constants.ACC_TYPE_R2.equals(accountType)
                            || Constants.ACC_TYPE_R4.equals(accountType)
                            || Constants.ACC_TYPE_D1.equals(accountType)) {
                        //最近一个月度变现信息段逾期总额
                        BigDecimal formationBalance = new BigDecimal("0.00");
                        if (borAccInfUnitBo.getPcamMonthInformation() != null && borAccInfUnitBo.getPcamMonthInformation().getOverdueAmount() != null) {
                            formationBalance = new BigDecimal(borAccInfUnitBo.getPcamMonthInformation().getOverdueAmount());
                        }
                        //最新表现字段还款金额
                        BigDecimal firinfsegBalance = new BigDecimal("0.00");
                        if (borAccInfUnitBo.getPcanFirinfseg() != null && borAccInfUnitBo.getPcanFirinfseg().getLastRepmoney() != null) {
                            firinfsegBalance = new BigDecimal(borAccInfUnitBo.getPcanFirinfseg().getLastRepmoney());
                        }
                        BigDecimal balances = formationBalance.subtract(firinfsegBalance);
                        //最近一个月度变现信息段余额-最新表现字段余额大于等于0
                        if (balances.compareTo(BigDecimal.ZERO) >= 0 && judge) {
                            cOdAcctNum = cOdAcctNum + 1;
                            judge = false;
                        }
                    } else if (Constants.ACC_TYPE_R3.equals(accountType)) {
                        if (twentyfourpaymentstatsSubList != null) {
                            for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                String paymentStats = subList.getPaymentStats();
                                if (StringUtils.isNotEmpty(paymentStats)) {
                                    if (Constants.OVERDUE.contains(paymentStats) && judge) {
                                        cOdAcctNum = cOdAcctNum + 1;
                                        judge = false;
                                    }
                                }
                            }
                        }
                    }

                    if (twentyfourpaymentstatsSubList != null) {
                        Date endTime = pcatTwentyfourpaymentstat.getEndTime();
                        Date payEndMonth6 = DateUtil.getAimMonthDate(endTime, -6);
                        Date payEndMonth12 = DateUtil.getAimMonthDate(endTime, -12);
                        Date payEndMonth24 = DateUtil.getAimMonthDate(endTime, -24);
                        for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                            Date month = subList.getMonth();
                            String paymentStats = subList.getPaymentStats();
                            if (month.after(payEndMonth6)) {
                                if (Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                    if (wdil6m < Integer.parseInt(paymentStats)) {
                                        wdil6m = Integer.parseInt(paymentStats);
                                    }
                                }
                            }
                            if (month.after(payEndMonth12)) {
                                if (Constants.OVERDUE_STATUS.contains(subList.getPaymentStats())) {
                                    if (wdil12m < Integer.parseInt(subList.getPaymentStats())) {
                                        wdil12m = Integer.parseInt(subList.getPaymentStats());
                                    }
                                }
                            }
                            if (month.after(payEndMonth24)) {
                                if (Constants.OVERDUE_STATUS.contains(subList.getPaymentStats())) {
                                    if (wdi24m < Integer.parseInt(subList.getPaymentStats())) {
                                        wdi24m = Integer.parseInt(subList.getPaymentStats());
                                    }
                                }
                            }
                        }
                    }

                    if (pcapRecfivehisinfoSubList != null) {
                        for (PcapRecfivehisinfoSub pcapList : pcapRecfivehisinfoSubList) {
                            if (StringUtils.isNotBlank(pcapList.getRepayStatus()) && Constants.OVERDUE.contains(pcapList.getRepayStatus())) {
                                loanHisOdNum = loanHisOdNum + 1;
                                break;
                            }
                        }
                    }

                    if (null != issuanceDate) {
                        if (reportTime.after(issuanceDate)
                                && issuanceDate.after(aimMonthDate3)) {
                            late3monOrgPassNumSet.add(orgCode);
                        }
                    }
                    late3monOrgPassNum = late3monOrgPassNumSet.size();

                    if (Constants.ACC_TYPE_D1.equals(accountType)
                            || Constants.ACC_TYPE_R1.equals(accountType)
                            || Constants.ACC_TYPE_R4.equals(accountType)) {

                        //156. 他行授信机构数(不含未激活)
                        //所有账户状态不为“未激活”的账户的“管理机构数”不为“浙江泰隆商业银行”的机构总数
                        if (!"浙江泰隆商业银行".equals(orgCode)
                                && !Constants.ACC_STAT_6.equals(accStatus)) {
                            othBankBusiOrgNumList.add(orgCode);
                            //157. 他行授信总余额(不含未激活)
                            othBankCreditTotalAmt = othBankCreditTotalAmt.add(accountQuota);
                        }

                        //165. 最近6个月金融机构审批通过家数
                        if (issuanceDate != null && reportTime.after(issuanceDate)
                                && issuanceDate.after(aimMonthDate6)) {
                            if (StringUtils.isNotEmpty(orgCode)) {
                                late6monOrgPassNumSet.add(orgCode);
                            }
                        }

                        //185. 授信机构数（不含未激活）
                        if (!Constants.ACC_STAT_4.equals(accStatus)) {
                            credBusiOrgOutWjqSet.add(borAccInfUnitBo.getPcabBasicinfoseg().getOrgCode());
                        }

                        //189. 授信余额(经营性贷款+已激活信用卡)
                        if (Constants.BUS_TYPE_41.equals(businessType)
                                || Constants.BUS_TYPE_51.equals(businessType)
                                || Constants.BUS_TYPE_52.equals(businessType)) {
                            if (StringUtils.isNotBlank(pcanFirinfseg.getBalance())) {
                                creditExtensionBalanOutWjh = creditExtensionBalanOutWjh.add(balance);
                            } else {
                                creditExtensionBalanOutWjh = creditExtensionBalanOutWjh.add(monBalance);
                            }
                        }

                        //195. 近3个月到期结清授信总额
                        if (Constants.ACC_STAT_3.equals(accStatus)
                                && closeDate != null
                                && closeDate.after(aimMonthDate3)) {
                            BigDecimal account = new BigDecimal("0.00");
                            if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                if (pcabBasicinfoseg.getAccountQuota() != null) {
                                    account = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                }
                            }
                            lateSeaEndSettAmt = lateSeaEndSettAmt.add(loanAmount).add(account);
                        }

                        //196. 近3个月新增授信总额
                        if (issuanceDate != null && issuanceDate.after(aimMonthDate3)) {
                            BigDecimal account = new BigDecimal("0.00");
                            if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                if (pcabBasicinfoseg.getAccountQuota() != null) {
                                    account = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                }
                            }
                            lateSeaAddAmt = lateSeaAddAmt.add(loanAmount).add(account);
                        }

                        //202. 非正常账户数
                        if (Constants.ACC_STAT_4.equals(accStatus)
                                || Constants.ACC_STAT_6.equals(accStatus)
                                || Constants.ACC_STAT_8.equals(accStatus)) {
                            ++unNormalAccountNum;
                        }

                        //9. 历史信贷产品数（5年内历史）
                        if (issuanceDate != null && aimYearDate5.before(issuanceDate)
                                && issuanceDate.before(reportTime)) {
                            clProdL5YNum = clProdL5YNum + 1;
                        }

                        //54. 是否存在逾期记录（历史逾期标识） 0：否 1：是
                        if (twentyfourpaymentstatsSubList != null) {
                            for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                if (Constants.OVERDUE.contains(subList.getPaymentStats())) {
                                    odRecoTotalNum = 1;
                                }
                            }
                        }

                        //115. 他行授信总额
                        if (!Constants.ACC_STAT_3.equals(accStatus)
                                && !Constants.ACC_STAT_4.equals(accStatus)) {
                            BigDecimal account = new BigDecimal("0.00");
                            if (Constants.ACC_TYPE_R1.equals(accountType)) {
                                if (pcabBasicinfoseg.getAccountQuota() != null) {
                                    account = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                    othCredTotalAmt = othCredTotalAmt.add(account);
                                }
                            } else if (Constants.ACC_TYPE_R4.equals(accountType)) {
                                if (loanAmount != null) {
                                    othCredTotalAmt = othCredTotalAmt.add(loanAmount);
                                }
                            } else {
                                othCredTotalAmt = othCredTotalAmt.add(loanAmount);
                            }
                        }

                        //67. 信用透支额与贷款每月还款额之和
                        oumd = oumd.add(repayment);

                        //68. 信用额与担保贷款额之和
                        //担保方式为“信用/免担保”的各账户余额之和
                        if (StringUtils.isNotEmpty(guaranteeMode) && Constants.ACC_STAT_4.equals(guaranteeMode)) {
                            oumue = oumue.add(balance);
                        }

                        //130. 剔除贷款类别总授信额度
                        if ((Constants.BUS_TYPE_41).equals(businessType)
                                || (Constants.BUS_TYPE_51).equals(businessType)
                                || (Constants.BUS_TYPE_52).equals(businessType)) {
                            if (Constants.ACC_TYPE_R1.equals(accountType)) {
                                //账户类别是四，加账户授信额度
                                creditAmt = creditAmt.add(accountQuota);
                            } else {
                                //账户类别是其他的，则加上借款金额
                                creditAmt = creditAmt.add(loanAmount);
                            }
                        }

                    } else if (Constants.ACC_TYPE_R2.equals(accountType)
                            || Constants.ACC_TYPE_R3.equals(accountType)) {

                        //账户状态不为销户未激活账户授信额度总和
                        if (!Constants.ACC_STAT_6.equals(accStatus)
                                && !Constants.ACC_STAT_4.equals(accStatus)) {
                            if (pcabBasicinfoseg.getAccountQuota() != null) {
                                BigDecimal account = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                                othCredTotalAmt = othCredTotalAmt.add(account);
                            }
                        }


                        //156. 他行授信机构数(不含未激活)
                        //所有账户状态不为“未激活”的账户的“管理机构数”不为“浙江泰隆商业银行”的机构总数
                        if (!"浙江泰隆商业银行".equals(orgCode)
                                && !Constants.ACC_STAT_6.equals(accStatus)) {
                            othBankBusiOrgNumList.add(orgCode);
                            //157. 他行授信总余额(不含未激活)
                            othBankCreditTotalAmt = othBankCreditTotalAmt.add(accountQuota);
                        }

                        //165. 最近6个月金融机构审批通过家数
                        if (issuanceDate != null && reportTime.after(issuanceDate)
                                && issuanceDate.after(aimMonthDate6)) {
                            if (null != orgCode) {
                                late6monOrgPassNumSet.add(orgCode);
                            }
                        }

                        //185. 授信机构数（不含未激活）
                        if (StringUtils.isNotBlank(accStatus)
                                && !Constants.ACC_STAT_4.equals(accStatus)
                                && !Constants.ACC_STAT_6.equals(accStatus)
                                && "CNY".equals(currency)) {
                            credBusiOrgOutWjqSet.add(orgCode);
                        }

                        //189. 授信余额(经营性贷款+已激活信用卡)
                        if (!Constants.ACC_STAT_6.equals(accStatus)) {
                            creditExtensionBalanOutWjh = creditExtensionBalanOutWjh.add(accountQuota);
                        }

                        //195. 近3个月到期结清授信总额
                        if (!Constants.ACC_STAT_4.equals(accStatus)
                                && closeDate != null
                                && closeDate.after(aimMonthDate3)) {
                            lateSeaEndSettAmt = lateSeaEndSettAmt.add(accountQuota);
                        }

                        //196. 近3个月新增授信总额
                        if (issuanceDate != null && issuanceDate.after(aimMonthDate3)) {
                            lateSeaAddAmt = lateSeaAddAmt.add(accountQuota);
                        }

                        //202. 非正常账户数
                        boolean unNormalAccountNumFlag = false;
                        if (twentyfourpaymentstatsSubList != null) {
                            for (PcatTwentyfourpaymentstatsSub pcatTwentyfourpaymentstatsSub : twentyfourpaymentstatsSubList) {
                                if (Constants.OVERDUE_STATUS.contains(pcatTwentyfourpaymentstatsSub.getPaymentStats())) {
                                    unNormalAccountNumFlag = true;
                                }
                            }
                            if (unNormalAccountNumFlag
                                    && Constants.ACC_STAT_2.equals(accStatus)
                                    || Constants.ACC_STAT_3.equals(accStatus)
                                    || Constants.ACC_STAT_5.equals(accStatus)
                                    || Constants.ACC_STAT_6.equals(accStatus)
                                    || Constants.ACC_STAT_8.equals(accStatus)
                            ) {
                                if (balance.compareTo(BigDecimal.ZERO) > 0) {
                                    if (balance.doubleValue() > 300) {
                                        ++unNormalAccountNum;
                                    }
                                } else if (monBalance.doubleValue() > 300) {
                                    ++unNormalAccountNum;
                                }
                            }
                        }


                        //9. 历史信贷产品数（5年内历史）
                        if (issuanceDate != null && aimYearDate5.before(issuanceDate)
                                && issuanceDate.before(reportTime)) {
                            clProdL5YNum = clProdL5YNum + 1;
                        }

                        //54. 是否存在逾期记录（历史逾期标识） 0：否 1：是
                        if (twentyfourpaymentstatsSubList != null) {
                            for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                if (Constants.ACC_TYPE_R3.equals(accountType)) {
                                    if (Constants.REPAY_STAT_N.contains(subList.getPaymentStats())) {
                                        odRecoTotalNum = 1;
                                    }
                                } else {
                                    if (Constants.OVERDUE_STATUS.contains(subList.getPaymentStats())) {
                                        odRecoTotalNum = 1;
                                    }
                                }
                            }
                        }

                        //67. 信用透支额与贷款每月还款额之和
                        oumd = oumd.add(quotaUsed.multiply(new BigDecimal(0.1)));

                        //68. 信用额与担保贷款额之和
                        oumue = oumue.add(accountQuota);

                        //130. 剔除贷款类别总授信额度
                        if (!Constants.ACC_STAT_4.equals(accStatus)) {
                            if ("CNY".equals(currency)) {
                                creditAmt = creditAmt.add(accountQuota);
                            }
                        }
                    }

                    othBankBusiOrgNum = othBankBusiOrgNumList.size();
                    late6monOrgPassNum = late6monOrgPassNumSet.size();
                    credBusiOrgOutWjq = credBusiOrgOutWjqSet.size();
                }
            }

            //保留两位小数点
            DecimalFormat formatTwo = new DecimalFormat("0.00");
            formatTwo.setRoundingMode(RoundingMode.HALF_UP);
            //保留三位小数点
            DecimalFormat formatRate = new DecimalFormat("0.000");
            formatRate.setRoundingMode(RoundingMode.HALF_UP);

            //129. 当前银行服务家数
            zxPZbInfo.setCBankServiceNum(String.valueOf(cBankServiceNum));
            //156. 他行授信机构数(不含未激活)
            zxPZbInfo.setOthBankBusiOrgNum(String.valueOf(othBankBusiOrgNum));
            //157. 他行授信总余额(不含未激活)
            zxPZbInfo.setOthBankCreditTotalAmt(formatTwo.format(othBankCreditTotalAmt));
            //165. 最近6个月金融机构审批通过家数
            zxPZbInfo.setLate6monOrgPassNum(String.valueOf(late6monOrgPassNum));
            //185. 授信机构数（不含未激活）
            zxPZbInfo.setCredBusiOrgOutWjq(String.valueOf(credBusiOrgOutWjq));
            //189. 授信余额(经营性贷款+已激活信用卡)
            zxPZbInfo.setCreditExtensionBalanOutWjh(formatTwo.format(creditExtensionBalanOutWjh));
            //195. 近3个月到期结清授信总额
            zxPZbInfo.setLateSeaEndSettAmt(formatTwo.format(lateSeaEndSettAmt));
            //196. 近3个月新增授信总额
            zxPZbInfo.setLateSeaAddAmt(formatTwo.format(lateSeaAddAmt));
            //202. 非正常账户数
            zxPZbInfo.setUnNormalAccountNum(String.valueOf(unNormalAccountNum));
            //9. 历史信贷产品数（5年内历史）
            zxPZbInfo.setClProdL5YNum(String.valueOf(clProdL5YNum));
            //13. 信用产品数和查询记录数比率
            zxPZbInfo.setClProdEnquRecoRate(formatRate.format(clProdEnquRecoRate));
            //54. 是否存在逾期记录（历史逾期标识） 0：否 1：是hist_cred_card_use_frequ
            zxPZbInfo.setOdRecoTotalNum(String.valueOf(odRecoTotalNum));
            //56. 以非结清形式销户数
            zxPZbInfo.setUnSettColseAcctNum(String.valueOf(unSettColseAcctNum));
            //57. 当前逾期账户数
            zxPZbInfo.setCOdAcctNum(String.valueOf(cOdAcctNum));
            //115. 他行授信总额
            zxPZbInfo.setOthCredTotalAmt(formatTwo.format(othCredTotalAmt));
            //61. 近6个月的最大逾期期数
            zxPZbInfo.setWdil6m(String.valueOf(wdil6m));
            //62. 近12个月的最大逾期期数
            zxPZbInfo.setWdil12m(String.valueOf(wdil12m));
            //63. 近24个月的最大逾期期数
            zxPZbInfo.setWdi24m(String.valueOf(wdi24m));
            //67. 信用透支额与贷款每月还款额之和
            zxPZbInfo.setOumd(formatTwo.format(oumd));
            //68. 信用额与担保贷款额之和
            zxPZbInfo.setOumue(formatTwo.format(oumue));
            //74. 历史贷款逾期笔数
            zxPZbInfo.setLoanHisOdNum(String.valueOf(loanHisOdNum));
            //123. 最近3个月审核通过机构家数
            zxPZbInfo.setLate3monOrgPassNum(String.valueOf(late3monOrgPassNum));
            //130. 剔除贷款类别总授信额度
            zxPZbInfo.setCreditAmt(formatTwo.format(creditAmt));

        } catch (Exception e) {
            throw new Exception(e);
        }

        return zxPZbInfo;
    }
}

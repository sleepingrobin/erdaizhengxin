package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PiccCreditproinfo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PiccCreditproinfosub;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PicoOverdueinfo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.PicoOverdueinfosub;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.credittransinfdata.entity.CreTraInfSummaryUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author StivenYang
 * @description 二（信息概要）-一
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateSummaryInfoPartOneServiceImpl implements PersonZBCalculteThreadService {

    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();
        try {

            //160. 最近5年贷记卡最长逾期月数
            int late5yDjkMaxOdNum = 0;
            //161. 最近5年准贷记卡最长逾期月数
            int late5yZdjkMaxOdNum = 0;
            //183. 信用卡张数（不含未激活）
            int credCardNumOutWjq = 0;

            if (cpqReportBo != null
                    && cpqReportBo.getCreTraInfSummaryBo() != null) {

                /**********************************公共变量start**********************/
                //信贷交易授信及负债信息概要
                CreTraInfSummaryUnitBo creTraInfSummaryUnitBo = cpqReportBo.getCreTraInfSummaryBo().getCreTraInfSummaryUnitBo();
                //信贷交易提示信息段
                PiccCreditproinfo piccCreditproinfo = null;
                //信贷交易提示信息列表
                List<PiccCreditproinfosub> piccCreditproinfosubList = null;
                //逾期信息
                PicoOverdueinfo picoOverdueinfo = null;
                //逾期信息列表
                List<PicoOverdueinfosub> picoOverdueinfosubList = null;

                if (creTraInfSummaryUnitBo != null) {
                    picoOverdueinfo = creTraInfSummaryUnitBo.getPicoOverdueinfo();
                    if (picoOverdueinfo != null) {
                        picoOverdueinfosubList = picoOverdueinfo.getPicoOverdueinfosubList();
                    }
                    piccCreditproinfo = creTraInfSummaryUnitBo.getPiccCreditproinfo();
                    if (piccCreditproinfo != null) {
                        piccCreditproinfosubList = piccCreditproinfo.getPiccCreditproinfosubList();
                    }
                }
                /**********************************公共变量end**********************/


                if (null != picoOverdueinfo) {
                    if (picoOverdueinfosubList != null) {
                        for (PicoOverdueinfosub subList : picoOverdueinfosubList) {
                            if (Constants.Overdue_4.equals(subList.getAccoutType())) {
                                //160. 最近5年贷记卡最长逾期月数
                                late5yDjkMaxOdNum = Integer.parseInt(subList.getMaxoverdueMon());
                            }
                            if (Constants.Overdue_5.equals(subList.getAccoutType())) {
                                //161. 最近5年准贷记卡最长逾期月数
                                late5yZdjkMaxOdNum = Integer.parseInt(subList.getMaxoverdueMon());
                            }
                        }
                    }
                }

                //183. 信用卡张数（不含未激活）
                if (piccCreditproinfosubList != null) {
                    for (PiccCreditproinfosub piccCreditproinfosub : piccCreditproinfosubList) {
                        if ("2".equals(piccCreditproinfosub.getBusiCategory())
                                && "21".equals(piccCreditproinfosub.getBusiType())) {
                            credCardNumOutWjq = Integer.parseInt(piccCreditproinfosub.getAccountNum());
                        }
                    }
                }
            }

            //160. 最近5年贷记卡最长逾期月数
            zxPZbInfo.setLate5yDjkMaxOdNum(String.valueOf(late5yDjkMaxOdNum));
            //161. 最近5年准贷记卡最长逾期月数
            zxPZbInfo.setLate5yZdjkMaxOdNum(String.valueOf(late5yZdjkMaxOdNum));
            //183. 信用卡张数（不含未激活）
            zxPZbInfo.setCredCardNumOutWjq(String.valueOf(credCardNumOutWjq));
        }catch (Exception e) {
            throw new Exception(e);
        }

        return zxPZbInfo;
    }
}

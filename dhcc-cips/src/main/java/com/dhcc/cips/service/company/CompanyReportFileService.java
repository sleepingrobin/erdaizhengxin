package com.dhcc.cips.service.company;


import com.dhcc.cips.bo.base.Request;

public interface CompanyReportFileService {

    String download(Request request);
}

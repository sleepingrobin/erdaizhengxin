package com.dhcc.cips.service.other;

import com.dhcc.cips.bo.base.Request;

/**
 * @author limin
 * @Description 查询记录服务类
 * @Date 2019-5-29
 */
public interface QueryLogService {

    /**
     * 客户经理查询客户征信报告最新记录查询
     */
    String exec(Request personRequestParam);


}

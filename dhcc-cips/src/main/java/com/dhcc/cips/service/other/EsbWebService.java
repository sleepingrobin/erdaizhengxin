package com.dhcc.cips.service.other;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface EsbWebService {

    @WebMethod(operationName = "esbservice")
    String exec(@WebParam(name="esbXml")String esbXml);
}

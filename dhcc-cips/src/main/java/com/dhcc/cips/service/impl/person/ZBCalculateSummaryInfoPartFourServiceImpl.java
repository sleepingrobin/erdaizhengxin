package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.uncredittranssum.PippPostpayarrerinfo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.uncredittranssum.PippPostpayarrerinfosub;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.uncredittranssum.entity.NoCreTraInfSummaryBo;
import com.baomidou.mybatisplus.generator.config.IFileCreate;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author StivenYang
 * @description 二（信息概要）-四
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateSummaryInfoPartFourServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo reportbo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();
        try {
            //电信欠费记录数
            int tele_arrearage_record_num = 0;

            //非信贷交易信息概要(PNO)
            NoCreTraInfSummaryBo noCreTraInfSummaryBo = reportbo.getNoCreTraInfSummaryBo();
            //后付费业务欠费信息汇总
            PippPostpayarrerinfo pippPostpayarrerinfo =null;
            if (noCreTraInfSummaryBo!=null&&noCreTraInfSummaryBo.getPippPostpayarrerinfo()!=null){
                pippPostpayarrerinfo= noCreTraInfSummaryBo.getPippPostpayarrerinfo();
            }

                    if (noCreTraInfSummaryBo!=null&&pippPostpayarrerinfo != null) {
                        List<PippPostpayarrerinfosub> pippPostpayarrerinfosubList = pippPostpayarrerinfo.getPippPostpayarrerinfosubList();
                        if (pippPostpayarrerinfosubList != null) {
                            for (PippPostpayarrerinfosub pippPostpayarrerinfosub : pippPostpayarrerinfosubList) {
                                //欠费记录数相加
                                String arrearsNum = pippPostpayarrerinfosub.getArrearsNum();
                                tele_arrearage_record_num += Integer.parseInt(arrearsNum == null ? "0" : arrearsNum);
                            }
                        }
                    }
            zxPZbInfo.setTeleArrearageRecordNum(String.valueOf(tele_arrearage_record_num));
        } catch (Exception e) {
            throw new Exception(e);
        }

        return zxPZbInfo;
    }
}

package com.dhcc.cips.service.impl.company;

import com.dhcc.cips.service.company.CompanyZBThreadService;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * @author StivenYang
 * @description 企业计算方法注册类
 * @date 2019年6月11日14:31:56
 */
@Component
@Data
public class CompanyCalculationFactory {

    private static List<CompanyZBThreadService> companyZBThreadServices = new ArrayList<>();

    //注册计算方法
    @PostConstruct
    public void init() {
        //基本信息
        companyZBThreadServices.add(new ZBCalculateBasicInfoServiceImpl());
        //信息概要--  1.借贷交易 2.已结清信贷信息概要 3.未结清信贷及授信信息概要
        companyZBThreadServices.add(new ZBCalculateSummaryInfoServiceImpl());
        //相关还款责任
        companyZBThreadServices.add(new ZBCalculateRelevantRepayServiceImpl());
        // 信贷记录明细（借贷，担保）-- 1.已结清信贷+未结清信贷  2.信贷记录明细 + --已结清信贷信息概要
        companyZBThreadServices.add(new ZBCalculateCreditInfoServiceImpl());
        // 信贷记录明细 （借贷，担保） --已结清信贷
        companyZBThreadServices.add(new ZBCalculateSettledCreditInfoServiceImpl());
        // 信贷记录明细（借贷，担保）--未结清信贷
        companyZBThreadServices.add(new ZBCalculateUnsettledCreditInfoServiceImpl());
    }

    public static List<CompanyZBThreadService> getIndexCalculates() {
        return companyZBThreadServices;
    }
}

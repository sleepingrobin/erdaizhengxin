package com.dhcc.cips.service.impl.company;

import com.dhcc.cips.bo.base.*;
import com.dhcc.cips.bo.other.QueryReqBo;
import com.dhcc.cips.bo.other.QueryResBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.mapper.company.CeqResultInfoMapper;
import com.dhcc.cips.mapper.other.SystemDicMapper;
import com.dhcc.cips.po.company.CeqResultInfoPo;
import com.dhcc.cips.service.company.CompanyReportQueryService;
import com.dhcc.cips.service.other.QueryAccountService;
import com.dhcc.cips.util.ConvertUtil;
import com.dhcc.cips.util.DateUtil;
import com.dhcc.cips.util.XmlUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

/**
 * 企业征信报告查询服务实现类
 */
@Slf4j
@Service
@RefreshScope
public class CompanyReportQueryServiceImpl implements CompanyReportQueryService {
    @Value("${companyWsdl}")
    private String companyWsdl;
    @Value("${reportVersion}")
    private String reportVersion;
    @Value("${syncFlag}")
    private String syncFlag;
    @Value("${reportType}")
    private String reportType;

    @Resource
    private SystemDicMapper systemDicMapper;
    @Resource
    private CeqResultInfoMapper ceqResultInfoMapper;
    @Autowired
    private QueryAccountService queryAccountService;


    /**
     * 企业征信查询
     *
     * @param request
     * @return
     * @throws Exception
     */
    @Override
    public String exec(Request request) {

        InputStream is = null;

        //返回信息
        String retCode;
        String retMesaage;
        String retStatus;

        try {
            RequestSysHead requestSysHead = request.getRequestSysHead();
            RequestBody requestBody = request.getRequestBody();
            //获取请求参数
            String loanCardNo = requestBody.getLoanCardNo();                        //贷款卡号
            String password = requestBody.getLoancardPassword();                    //贷款卡密码
            String clientName = requestBody.getClientName();                        //客户名称
            String globalType = requestBody.getGlobalType();                        //证件类型
            String globalId = requestBody.getGlobalId();                            //证件号码
            String reasonCode = requestBody.getReasonCode();                        //查询原因
            String signMd = requestBody.getSignMd();                                //签署方式
            String checkUserId = requestBody.getChckTlr();                           //核查柜员
            String checkUserName = requestBody.getTellerRoleName();                  //柜员角色名称
            String checkTime = requestBody.getOprtTm();                              //审核时间
            String userName = requestBody.getClntMgrNm();                            //客户经理名称
            String orgName = requestBody.getMgrOrgName();                            //客户经理机构名称
            String userId = requestSysHead.getUserId();                               //客户经理工号
            String orgId = requestSysHead.getBranchId();                             //客户经理机构号
            String sourceType = requestSysHead.getSourceType();                      //渠道

            //请求参数校验
            if (StringUtils.isEmpty(loanCardNo)) {
                retMesaage = Constants.LOANCARD_NO_NULL_ANO;//贷款卡号不能为空！
                throw new Exception(retMesaage);
            }
//            if (StringUtils.isEmpty(password)) {
//                retMesaage = Constants.LOANCARD_PASSWORD_NULL_ANO;//贷款卡密码不能为空！
//                throw new Exception(retMesaage);
//            }
//            if (StringUtils.isEmpty(clientName)) {
//                retMesaage = Constants.CLIENT_NAME_NULL_ANO;//客户名称不能为空！
//                throw new Exception(retMesaage);
//            }
            /*if (StringUtils.isEmpty(globalType)) {
                retMessage = Constants.GLOBAL_TYPE_ANO;//证件类型不能为空！
                throw new Exception();
            }
            if (StringUtils.isEmpty(globalId)) {
                retMessage = Constants.GLOBAL_ID_ANO;//证件号码不能为空！
                throw new Exception();
            }*/
            if (StringUtils.isEmpty(reasonCode)) {
                retMesaage = Constants.REASON_CODE_NULL_ANO;//查询原因不能为空！
                throw new Exception(retMesaage);
            }
//            if (StringUtils.isEmpty(signMd)) {
//                retMesaage = Constants.SIGN_MD_NULL_ANO;//签署方式不能为空！
//                throw new Exception(retMesaage);
//            }
            if (StringUtils.isEmpty(userId)) {
                retMesaage = Constants.CLNT_MGR_NM_NULL_ANO; //客户经理工号不能为空！
                throw new Exception(retMesaage);
            }
//            if (StringUtils.isEmpty(userName)) {
//                retMesaage = Constants.CLNT_MGR_NM_NULL_ANO; //客户经理名称不能为空！
//                throw new Exception(retMesaage);
//            }
            if (StringUtils.isEmpty(orgId)) {
                retMesaage = Constants.CLNT_MGR_NO_NULL_ANO; //客户经理机构号不能为空！
                throw new Exception(retMesaage);
            }
//            if (StringUtils.isEmpty(orgName)) {
//                retMesaage = Constants.MGR_ORG_NAME_NULL_ANO; //客户经理机构名称不能为空！
//                throw new Exception(retMesaage);
//            }
            //查询原因为贷后管理时，审核信息不能为空
//            if (Constants.C_REASON_DHGL.equals(reasonCode)) {
//                if (StringUtils.isEmpty(checkUserId)) {
//                    retMesaage = Constants.CHCK_USERID_NULL_ANO; //审核人工号不能为空！
//                    throw new Exception(retMesaage);
//                }
//                if (StringUtils.isEmpty(checkUserName)) {
//                    retMesaage = Constants.CHCK_USERNAME_NULLE_ANO;//审核人名称不能为空！
//                    throw new Exception(retMesaage);
//                }
//                if (StringUtils.isEmpty(checkTime)) {
//                    retMesaage = Constants.CHCK_TM_ANO;//审核时间不能为空，且审核时间格式为:yyyyMMdd hh:mm:ss！
//                    throw new Exception(retMesaage);
//                }
//            }

            //匹配征信查询账号
            String queryAccount = queryAccountService.getQueryUser(orgId, "C");
            //信用报告复用策略  负数:查询本地  0:征信中心  正数:征信报告有效期
            String queryType = queryAccountService.getReportQueryType(orgId, "C");
            String businessLine = systemDicMapper.selectBusinessLine();
            QueryReqBo queryReqBo = new QueryReqBo();
            queryReqBo.setClientName(clientName);//客户名称
            queryReqBo.setGlobalType(Constants.COMPANY_CRETIFICATE_TYPE_A1300);//证件类型（统一传贷款卡）
            queryReqBo.setGlobalId(loanCardNo);//证件号码（统一传贷款卡号）
            queryReqBo.setReasonCode(reasonCode);///查询原因
            queryReqBo.setReportType(reportType);//信用报告格式   H:Html格式  X:Xml格式  HX:Html格式和Xml格式
            queryReqBo.setQueryType(queryType);//信用报告复用策略  负数:查询本地  0:征信中心  正数:征信报告有效期
            queryReqBo.setSyncFlag(syncFlag);//同步异步返回标识 0:同步 1:异步
            queryReqBo.setReportVersion(reportVersion);//信用报告版本  2.0.0:二代信用报告
            queryReqBo.setBusinessLine(businessLine);//业务线
            queryReqBo.setUserId(userId);//查询用户工号
            queryReqBo.setSysCode("1");//系统标识  区分接口调用子系统
            queryReqBo.setQueryAccount(queryAccount);//查询账号

            String requestXml = ConvertUtil.getComanyConvertedRequestXml(queryReqBo);

            //调产品查询接口
            PostMethod postMethod = new PostMethod(companyWsdl);
            byte[] b = requestXml.getBytes("utf-8");
            is = new ByteArrayInputStream(b, 0, b.length);
            RequestEntity requestEntity = new InputStreamRequestEntity(is, b.length, "text/xml;charset=UTF-8");
            postMethod.setRequestEntity(requestEntity);
            HttpClient httpClient = new HttpClient();
            int status = httpClient.executeMethod(postMethod);
            if (status != 200) {//服务不可用
                throw new Exception("服务不可用");

            }
            String convertedResponseXml = ConvertUtil.getConvertedResponseXml(postMethod.getResponseBodyAsString());
            //调用产品查询接口返回报文转换成对象
            QueryResBo queryResBo = XmlUtil.convertXml2JavaBean(QueryResBo.class, convertedResponseXml, new Class[]{QueryResBo.class});
            if (queryResBo != null && Constants.SUCCESS_CODE.equals(queryResBo.getResCode())) {//查询成功
                //更新查询日志表信息
                CeqResultInfoPo ceqResultInfoPo = new CeqResultInfoPo();
                ceqResultInfoPo.setId(queryResBo.getCreditreportNo());
                ceqResultInfoPo.setOperatorName(userName);//申请人姓名
                ceqResultInfoPo.setOperOrgName(orgName);  //申请机构名称
                ceqResultInfoPo.setAuthorizaVesion(signMd);//授权版本
                ceqResultInfoPo.setSourceType(sourceType); //渠道
                ceqResultInfoPo.setRekUser(checkUserId);   //审核人
                // ceqResultInfoPo.setRekOrg();            //审核人机构号
                ceqResultInfoPo.setRekTime(DateUtil.toDate(checkTime, "yyyy-MM-dd HH:mm:ss"));//审核人时间
                ceqResultInfoPo.updateById();
            } else {
                throw new Exception(queryResBo.getResMsg());//查询失败原因
            }

            //交易返回码
            retCode = Constants.SUCCESS_CODE;
            //交易返回信息
            retMesaage = "交易成功";
            //交易返回状态
            retStatus = Constants.SUCCESS_S;
        } catch (Exception e) {
            log.error("", e);
            //交易返回码
            retCode = Constants.FAIL_CODE;
            //交易返回信息
            retMesaage = "交易失败，" + e.getMessage();
            //交易返回状态
            retStatus = Constants.FAIL_F;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    log.debug("CompanyReportQueryServiceImpl.exec", e);
                }
            }
        }
        //设置返回对象参数
        ResponseHead responseHead = new ResponseHead();
        ResponseSysHead responseSysHead = new ResponseSysHead();
        BeanUtils.copyProperties(request.getRequestSysHead(), responseSysHead);
        //返回状态码
        responseSysHead.setRetStatus(retStatus);
        responseHead.setResponseSysHead(responseSysHead);

        ResponseSysHeadRet responseSysHeadRet = new ResponseSysHeadRet();
        //交易返回码
        responseSysHeadRet.setRetCode(retCode);
        //交易返信息
        responseSysHeadRet.setRetMsg(retMesaage);
        responseHead.getResponseSysHead().setResponseSysHeadRet(responseSysHeadRet);

        String xml = XmlUtil.converJavaBean2Xml(responseHead, responseHead.getClass());

        return xml;
    }
}
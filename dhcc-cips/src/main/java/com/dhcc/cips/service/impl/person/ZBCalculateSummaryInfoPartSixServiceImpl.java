package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.qryrecord.PiqqQueryrecordsum;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.queryrecord.PqqqRecordUnit;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.qryrecord.entity.QueryRecSummaryBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.qryrecord.entity.QueryRecSummaryUnitBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.queryrecord.entity.QueryRecordBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

/**
 * @author StivenYang
 * @description 二（信息概要）-六
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateSummaryInfoPartSixServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo reportbo) {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();
        //报告时间
        Date reportTime = reportbo.getRepheadBo().getRepheadUnitBo().getPchrPchrreportinfo().getReportTime();


        //查询记录概要(PQO)
        QueryRecSummaryBo queryRecSummaryBo = reportbo.getQueryRecSummaryBo();
        //查询记录(POQ)
        QueryRecordBo queryRecordBo = reportbo.getQueryRecordBo();
        //查询记录信息单元(PH01)
        List<PqqqRecordUnit> pqqqRecordUnitList = null;
        if (queryRecordBo != null) {
            pqqqRecordUnitList = queryRecordBo.getPqqqRecordUnitList();
        }
        //查询记录概要信息单元(PC05)
        QueryRecSummaryUnitBo queryRecSummaryUnitBo = null;
        if (queryRecSummaryBo != null && queryRecSummaryBo.getQueryRecSummaryUnitBo()!= null) {
            queryRecSummaryUnitBo = queryRecSummaryBo.getQueryRecSummaryUnitBo();
        }
        //查询记录汇总信息段(PC05B)
        PiqqQueryrecordsum piqqQueryrecordsum=null;
        if (queryRecSummaryUnitBo!=null&&queryRecSummaryUnitBo.getPiqqQueryrecordsum()!=null){
             piqqQueryrecordsum = queryRecSummaryUnitBo.getPiqqQueryrecordsum();
        }

        //最近一年
        Date aimYearDateOne = DateUtil.getAimYearDate(reportTime, -1);


        //10最近一个月内查询次数
        int lastoneQunumload = 0;
        int lastoneQunumcre = 0;
        if (piqqQueryrecordsum != null) {
            //贷款审批次数
            String lastoneQunumload1 = piqqQueryrecordsum.getLastoneQunumload();
            if (lastoneQunumload1!=null){
                lastoneQunumload  = Integer.parseInt(lastoneQunumload1);
            }
            //信用卡审批次数
            String lastoneQunumcre1 = piqqQueryrecordsum.getLastoneQunumcre();
            if (lastoneQunumcre1!=null){
                lastoneQunumcre = Integer.parseInt(lastoneQunumcre1);
            }
        }

        int  late_mon_enqu_count = lastoneQunumload + lastoneQunumcre;
        zxPZbInfo.setLateMonEnquCount(String.valueOf(late_mon_enqu_count));


        //11最近一年内的查询次数
        int late_year_enqu_count = 0;
        //查询记录概要
        if (null != pqqqRecordUnitList && pqqqRecordUnitList.size() > 0) {
            for (PqqqRecordUnit pqqqRecordList : pqqqRecordUnitList) {
                String queryReason = null;
                if (StringUtils.isNotEmpty(pqqqRecordList.getQueryReason())) {
                    queryReason = pqqqRecordList.getQueryReason();
                }
                if (queryReason != null) {
                    if (StringUtils.isNotBlank(queryReason) && Constants.QueryCause_02.equals(queryReason) || Constants.QueryCause_03.equals(queryReason)) {

                        if (pqqqRecordList.getQueryDate() != null) {
                            Date  queryDate = pqqqRecordList.getQueryDate();
                            if (queryDate!=null){
                                //一年内
                                if (reportTime.after(queryDate) && aimYearDateOne.before(queryDate)) {
                                    //查询次数
                                    late_year_enqu_count = late_year_enqu_count + 1;
                                }
                            }
                        }
                    }
                }
            }
        }
        zxPZbInfo.setLateYearEnquCount(String.valueOf(late_year_enqu_count));


        //85最近一个月贷款审核查询记录
        String late_mon_loan_count = null;
        String late_mon_card_count = null;
        if (queryRecSummaryBo != null && queryRecSummaryUnitBo != null && piqqQueryrecordsum != null) {
            late_mon_loan_count = piqqQueryrecordsum.getLastoneQunumload();
            //87最近一个月信用卡审批查询记录
            late_mon_card_count = piqqQueryrecordsum.getLastoneQunumcre();
        }
        //最近一个月贷款审核查询记录
        zxPZbInfo.setLateMonLoanCount(String.valueOf(late_mon_loan_count));

        //最近一个月信用卡审批查询记录
        zxPZbInfo.setLateMonCardCount(String.valueOf(late_mon_card_count));


        //个人指标12 最近1个月内的查询次数/最近1年内的查询次数（比率）
        BigDecimal late_mon_year_enqu_rate = new BigDecimal("0.00");
        BigDecimal lateMon = new BigDecimal(late_mon_enqu_count);
        BigDecimal lateYear = new BigDecimal(late_year_enqu_count);
        if (lateYear.compareTo(BigDecimal.ZERO) > 0) {
            late_mon_year_enqu_rate = lateMon.divide(lateYear, 3, RoundingMode.HALF_UP);
        }
        zxPZbInfo.setLateMonYearEnquRate(String.valueOf(late_mon_year_enqu_rate));


        return zxPZbInfo;
    }
}

package com.dhcc.cips.service.other;


/**
 * <p>
 * 获取查询账号服务类
 * </p>
 *
 * @author limin
 * @since 2019-06-04
 */
public interface QueryAccountService {

    /**
     * 获取人行查询账号（页面查询）
     *
     * @return
     */
    String getQueryUser(String branchId, String type) throws Exception;

    /**
     * 获取本地报告有效期
     *
     * @param branchId
     * @param type
     * @return
     * @throws Exception
     */
    String getReportQueryType(String branchId, String type) throws Exception;
}

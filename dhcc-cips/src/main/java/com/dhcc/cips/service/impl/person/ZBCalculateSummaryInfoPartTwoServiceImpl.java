package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.credittransinfdata.*;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.credittransinfdata.entity.CreTraInfSummaryUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

/**
 * @author StivenYang
 * @description 二（信息概要）-二
 * @date 2019年6月11日15:48:34
 */
@Slf4j
@Service
public class ZBCalculateSummaryInfoPartTwoServiceImpl implements PersonZBCalculteThreadService {
    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {
        PersonZBInfoPo personZBInfoPo = new PersonZBInfoPo();
        try {

            //126. 信用卡累计逾期月份数
            int ccSumOdMonNum = 0;
            //132. 最近5年内贷款逾期最长逾期月数
            int loanHisMaxOdMonNum = 0;
            //133. 过去5年内贷款最高逾期期数
            int loanHisSumOdMonNum = 0;
            //134. 信用卡累计逾期月份数(同126)
            //136. 剔除贷款类别逾期贷款金额(同140)
            //137. 当前银行服务家数(同140)
            //138. 剔除贷款类别总授信额度(同140)
            //139. 剔除贷款类别银行家数(同140)
            //140. 呆账笔数
            int dzAcctNum = 0;
            //141. 呆账余额
            BigDecimal dzAcctBalan = new BigDecimal(0);
            //142. 资产处置笔数
            int zcAssetNum = 0;
            //143. 资产处置余额
            BigDecimal zcAssetBalan = new BigDecimal(0);
            //148. 近两年信用卡逾期次数
            int late2yCardOdNum = 0;
            //151. 最近2年最长逾期月数
            int late2yMaxOdNum = 0;
            //24. 历史最大逾期(透支)期数
            int cMaxOdNum = 0;
            //75. 贷款单月最高逾期总额
            BigDecimal loanPerMonOdAmt = new BigDecimal(0);
            //76. 信用卡单月最高逾期金额
            BigDecimal ccPerMonOdAmt = new BigDecimal(0);
            //77. 信用卡最长逾期月数
            int ccMaxOdMonNum = 0;

            if (cpqReportBo != null
                    && cpqReportBo.getCreTraInfSummaryBo() != null) {

                /**********************************公共变量start**********************/
                //信贷交易授信及负债信息概要
                CreTraInfSummaryUnitBo creTraInfSummaryUnitBo = cpqReportBo.getCreTraInfSummaryBo().getCreTraInfSummaryUnitBo();
                //被追偿汇总信息段
                PiccBerecosuminfo piccBerecosuminfo = null;
                //被追偿汇总信息汇总列表
                List<PiccBerecosuminfosub> piccBerecosuminfosubList = null;
                //呆账汇总信息段
                PicbBaddebtinfo picbBaddebtinfo = null;
                //逾期信息
                PicoOverdueinfo picoOverdueinfo = null;
                //逾期信息列表
                List<PicoOverdueinfosub> picoOverdueinfosubList = null;

                if (creTraInfSummaryUnitBo != null) {
                    picbBaddebtinfo = creTraInfSummaryUnitBo.getPicbBaddebtinfo();
                    piccBerecosuminfo = creTraInfSummaryUnitBo.getPiccBerecosuminfo();

                    if (piccBerecosuminfo != null) {
                        piccBerecosuminfosubList = piccBerecosuminfo.getPiccBerecosuminfosubList();
                    }
                    picoOverdueinfo = creTraInfSummaryUnitBo.getPicoOverdueinfo();
                    if (picoOverdueinfo != null) {
                        picoOverdueinfosubList = picoOverdueinfo.getPicoOverdueinfosubList();
                    }
                }
                if (picbBaddebtinfo != null) {
                    dzAcctNum = StringUtils.isEmpty(picbBaddebtinfo.getAccountNum()) ? 0 : Integer.parseInt(picbBaddebtinfo.getAccountNum());
                    dzAcctBalan = StringUtils.isEmpty(picbBaddebtinfo.getBalance()) ? new BigDecimal(0) : new BigDecimal(picbBaddebtinfo.getBalance());
                }
                /**********************************公共变量end**********************/

                //132. 最近5年内贷款逾期最长逾期月数
                //非循环贷账户逾期月数
                int fxMonthNum = 0;
                //循环额度下分账户逾期月数
                int xfMonthNum = 0;
                //循环贷账户逾期月数
                int xdMonthNum = 0;

                //133. 过去5年内贷款最高逾期期数
                //非循环贷账户逾期月数
                int fxMonthNum1 = 0;
                //循环额度下分账户逾期月数
                int xfMonthNum1 = 0;
                //循环贷账户逾期月数
                int xdMonthNum1 = 0;

                //151. 最近2年最长逾期月数
                //准贷记卡账户逾期月数
                int zOverDueMonth = 0;
                //贷记卡账户逾期月数
                int dOverDueMonth = 0;

                if (picoOverdueinfosubList != null) {
                    for (PicoOverdueinfosub picoOverdueinfosub : picoOverdueinfosubList) {
                        //账户类型
                        String accoutType = picoOverdueinfosub.getAccoutType();
                        //逾期期数
                        Integer monthNum = StringUtils.isEmpty(picoOverdueinfosub.getMonthNum()) ? 0 : Integer.parseInt(picoOverdueinfosub.getMonthNum());
                        //最高逾期月数
                        Integer maxoverdueMon = StringUtils.isEmpty(picoOverdueinfosub.getMaxoverdueMon()) ? 0 : Integer.parseInt(picoOverdueinfosub.getMaxoverdueMon());
                        //最高逾期金额
                        BigDecimal maxoverdueSum = StringUtils.isEmpty(picoOverdueinfosub.getMaxoverdueSum()) ? new BigDecimal(0) : new BigDecimal(picoOverdueinfosub.getMaxoverdueSum());

                        if (null != accoutType) {
                            //贷记卡账户累计逾期月份数
                            if (Constants.Overdue_4.equals(accoutType)) {
                                ccSumOdMonNum += monthNum;
                            }
                            //准贷记卡账户累计逾期月份数
                            if (Constants.Overdue_5.equals(accoutType)) {
                                ccSumOdMonNum += monthNum;
                            }
                            //非循环贷账户
                            if (Constants.Overdue_1.equals(accoutType)) {
                                fxMonthNum1 = monthNum;
                            }
                            //循环额度下分账户
                            if (Constants.Overdue_2.equals(accoutType)) {
                                xfMonthNum1 = monthNum;
                            }
                            //循环贷账户
                            if (Constants.Overdue_3.equals(accoutType)) {
                                xdMonthNum1 = monthNum;
                            }
                            //非循环贷账户最长逾期月数
                            if (Constants.Overdue_1.equals(accoutType)) {
                                fxMonthNum = maxoverdueMon;
                            }
                            //循环额度下分账户最长逾期月数
                            if (Constants.Overdue_2.equals(accoutType)) {
                                xfMonthNum = maxoverdueMon;
                            }
                            //循环贷账户最长逾期月数
                            if (Constants.Overdue_3.equals(accoutType)) {
                                xdMonthNum = maxoverdueMon;
                            }

                            if (Constants.Overdue_4.equals(accoutType)) {
                                late2yCardOdNum = monthNum;
                            }

                            if (Constants.Overdue_5.equals(accoutType)) {
                                zOverDueMonth = maxoverdueMon;
                            }

                            if (Constants.Overdue_4.equals(accoutType)) {
                                dOverDueMonth = maxoverdueMon;
                            }

                            if (maxoverdueMon > cMaxOdNum) {
                                cMaxOdNum = maxoverdueMon;
                            }

                            if (Constants.Overdue_1.equals(accoutType)
                                    || Constants.Overdue_2.equals(accoutType)
                                    || Constants.Overdue_3.equals(accoutType)) {
                                if (loanPerMonOdAmt.compareTo(maxoverdueSum) < 0) {
                                    loanPerMonOdAmt = maxoverdueSum;
                                }
                            }

                            //信用卡单月最高逾期金额
                            if (Constants.Overdue_4.equals(accoutType)
                                    || Constants.Overdue_5.equals(accoutType)) {
                                if (ccPerMonOdAmt.compareTo(maxoverdueSum) < 0) {
                                    ccPerMonOdAmt = maxoverdueSum;
                                }

                                //信用卡最长逾期月数
                                if (ccMaxOdMonNum < maxoverdueMon) {
                                    ccMaxOdMonNum = maxoverdueMon;
                                }
                            }
                        }
                    }
                    //132. 最近5年内贷款逾期最长逾期月数
                    if (fxMonthNum > xfMonthNum) {
                        if (fxMonthNum > xdMonthNum) {
                            loanHisMaxOdMonNum = fxMonthNum;
                        } else {
                            loanHisMaxOdMonNum = xdMonthNum;
                        }
                    } else {
                        if (xfMonthNum > xdMonthNum) {
                            loanHisMaxOdMonNum = xfMonthNum;
                        } else {
                            loanHisMaxOdMonNum = xdMonthNum;
                        }
                    }
                    //133 过去5年内贷款最高逾期期数
                    if (fxMonthNum1 > xfMonthNum1) {
                        if (fxMonthNum1 > xdMonthNum1) {
                            loanHisSumOdMonNum = fxMonthNum1;
                        } else {
                            loanHisSumOdMonNum = xdMonthNum1;
                        }
                    } else {
                        if (xfMonthNum1 > xdMonthNum1) {
                            loanHisSumOdMonNum = xfMonthNum1;
                        } else {
                            loanHisSumOdMonNum = xdMonthNum1;
                        }
                    }
                    //151 最近2年最长逾期月数
                    if (zOverDueMonth > dOverDueMonth) {
                        late2yMaxOdNum = zOverDueMonth;
                    } else {
                        late2yMaxOdNum = dOverDueMonth;
                    }

                    //142. 资产处置笔数
                    //143. 资产处置余额
                    if (piccBerecosuminfosubList != null) {
                        for (PiccBerecosuminfosub subList : piccBerecosuminfosubList) {
                            if (Constants.PersonRecovery_1.equals(subList.getBusiType())) {
                                int accountNum = StringUtils.isEmpty(subList.getAccountNum()) ? 0 : Integer.parseInt(subList.getAccountNum());
                                zcAssetNum = subList.getAccountNum() == null ? 0 : accountNum;
                                String balance1 = subList.getBalance();
                                zcAssetBalan = balance1 == null ? new BigDecimal(0) : new BigDecimal(balance1);
                            }
                        }
                    }
                }
            }

            //保留两位小数点
            DecimalFormat formatTwo = new DecimalFormat("0.00");
            formatTwo.setRoundingMode(RoundingMode.HALF_UP);

            //126. 信用卡累计逾期月份数
            personZBInfoPo.setCcSumOdMonNum(String.valueOf(ccSumOdMonNum));
            //132. 最近5年内贷款逾期最长逾期月数
            personZBInfoPo.setLoanHisMaxOdMonNum(String.valueOf(loanHisMaxOdMonNum));
            //133. 过去5年内贷款最高逾期期数
            personZBInfoPo.setLoanHisSumOdMonNum(String.valueOf(loanHisSumOdMonNum));
            //134. 信用卡累计逾期月份数(同126)

            //136. 剔除贷款类别逾期贷款金额(同140)

            //137. 当前银行服务家数(同140)

            //138. 剔除贷款类别总授信额度(同140)

            //139. 剔除贷款类别银行家数(同140)

            //140. 呆账笔数
            personZBInfoPo.setDzAcctNum(String.valueOf(dzAcctNum));
            //141. 呆账余额
            personZBInfoPo.setDzAcctBalan(formatTwo.format(dzAcctBalan));
            //142. 资产处置笔数
            personZBInfoPo.setZcAssetNum(String.valueOf(zcAssetNum));
            //143. 资产处置余额
            personZBInfoPo.setZcAssetBalan(formatTwo.format(zcAssetBalan));
            //148. 近两年信用卡逾期次数
            personZBInfoPo.setLate2yCardOdNum(String.valueOf(late2yCardOdNum));
            //151. 最近2年最长逾期月数
            personZBInfoPo.setLate2yMaxOdNum(String.valueOf(late2yMaxOdNum));
            //24. 历史最大逾期(透支)期数
            personZBInfoPo.setCMaxOdNum(String.valueOf(cMaxOdNum));
            //75. 贷款单月最高逾期总额
            personZBInfoPo.setLoanPerMonOdAmt(formatTwo.format(loanPerMonOdAmt));
            //76. 信用卡单月最高逾期金额
            personZBInfoPo.setCcPerMonOdAmt(formatTwo.format(ccPerMonOdAmt));
            //77. 信用卡最长逾期月数
            personZBInfoPo.setCcMaxOdMonNum(String.valueOf(ccMaxOdMonNum));

            return personZBInfoPo;

        }catch (Exception e) {
            throw new Exception(e);
        }
    }
}

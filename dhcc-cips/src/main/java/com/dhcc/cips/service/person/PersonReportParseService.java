package com.dhcc.cips.service.person;

import org.springframework.transaction.annotation.Transactional;

public interface PersonReportParseService {

    /**
     *  解析xml版征信报告
     * @param xmlStr
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    void parseWithXml(String xmlStr) throws Exception;

    /**
     * 解析html版征信报告
     * @param htmlStr
     * @throws Exception
     */
    @Transactional(rollbackFor = Exception.class)
    void parseWithHtml(String htmlStr) throws Exception;

}

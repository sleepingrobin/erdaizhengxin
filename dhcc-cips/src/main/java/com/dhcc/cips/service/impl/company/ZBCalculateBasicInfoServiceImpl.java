package com.dhcc.cips.service.impl.company;

import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.CeqReportBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.basicinfom.EiccConInformation;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.basicinfom.EiccConInformationSub;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.basicinfom.entity.BasicInfomDataBo;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.dhcc.cips.service.company.CompanyZBThreadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 企业基本信息
 */
@Service
@Slf4j
public class ZBCalculateBasicInfoServiceImpl implements CompanyZBThreadService {

    @Override
    public CompanyZBInfoPo companyCalculate(CeqReportBo ceqReportBo) throws Exception {
        CompanyZBInfoPo companyZBInfoPo = new CompanyZBInfoPo();
        try {

            //基本信息
            BasicInfomDataBo basicInfomDataBo = ceqReportBo.getBasicInfomDataBo();
            // 注册资本及主要出资人信息单元
            EiccConInformation eiccConInformation = basicInfomDataBo.getEiccConInformation();
            // 3. 基本信息-注册资本及主要出资人信息 注册资金
            BigDecimal registAmt = new BigDecimal("0.00");
            if (null != eiccConInformation) {
                registAmt = eiccConInformation.getRegistCapital();
            }
            companyZBInfoPo.setRegistAmt(String.valueOf(registAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            // 4.基本信息-注册资本及主要出资人信息 出资比例最大值
            BigDecimal registMaxAmtRate = new BigDecimal("0.00");
            List<EiccConInformationSub> list = new ArrayList<>();
            if (null != eiccConInformation) {
                list = eiccConInformation.getEiccConInformationSubList();
                if (list.size() > 0) {
                    for (EiccConInformationSub eiccConInformationSub : list) {
                        BigDecimal capRation = eiccConInformationSub.getCapRatio();
                        if (null != capRation) {
                            registMaxAmtRate = capRation.compareTo(registMaxAmtRate) > 0 ? capRation : registMaxAmtRate;
                        }
                    }
                }
            }
            companyZBInfoPo.setRegistMaxAmtRate(String.valueOf(registMaxAmtRate.setScale(2, BigDecimal.ROUND_HALF_UP)));
            // 5.记录总数基本信息-注册资本及主要出资人信息--记录总数
            String registNum = String.valueOf(list.size());
            companyZBInfoPo.setRegistNum(registNum);
            //平均出资金额  regist_amt/regist_num
            BigDecimal registAverAmt = new BigDecimal("0.00");
            if (list.size() > 0) {
                registAverAmt = registAmt.divide(new BigDecimal(registNum), 10, BigDecimal.ROUND_HALF_UP);
            }
            companyZBInfoPo.setRegistAverAmt(String.valueOf(registAverAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //114 基本信息--基本概况信息--登记地址
            String registAddress = basicInfomDataBo.getEisuUnitInformation().getRegistAddress();//登记地址
            companyZBInfoPo.setCompanyRegistAddr(String.valueOf(registAddress));

        } catch (Exception e) {
            log.error("CompanyZBBasicServiceImpl", e);
            throw new Exception(e);
        }
        return companyZBInfoPo;
    }
}

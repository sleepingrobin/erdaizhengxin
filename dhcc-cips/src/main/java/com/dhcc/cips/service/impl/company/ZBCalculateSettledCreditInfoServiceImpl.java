package com.dhcc.cips.service.impl.company;

import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.CeqReportBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.debaccountinform.EsalLeAccInform;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.debaccountinform.EsalLeAccRepayInform;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.debaccountinform.EsalLeAccRepayInformSub;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.guaaccount.EdgbGuarbasicinfoseg;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.guaaccount.EdgrGuarresponinfoseg;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.guarantsum.EssuUnclearInformSub;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.basebo.lendtranssuminf.EbboOthloantrasortsumu;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.debaccountinform.entity.DebitAccountInformDataBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.debaccountinform.entity.LeAccInformUnitBo;
import cn.com.dhcc.creditquery.ent.query.bo.reportanalysis.bo.guaaccount.entity.GuarAccoInfoBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.dhcc.cips.service.company.CompanyZBThreadService;
import com.dhcc.cips.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 信贷记录明细--已结清信贷
 */
@Service
@Slf4j
public class ZBCalculateSettledCreditInfoServiceImpl implements CompanyZBThreadService {

    @Override
    public CompanyZBInfoPo companyCalculate(CeqReportBo ceqReportBo) throws Exception {
        CompanyZBInfoPo companyZBInfoPo = new CompanyZBInfoPo();
        try {
            if (null == ceqReportBo) {
                return null;
            }
            //报告日期
            Date createTime = ceqReportBo.getRepheadBo().getRepheadUnitBo().getEhhrReportIdentity().getReportTime();
            //已结清信贷信息概要--垫款--金额
            BigDecimal advanceBusMoney = ceqReportBo.getLoanTransaDataBo().getLoanTransaBo().getEbbcCloseloantransa().getAdvanceBusMoney();
            if (advanceBusMoney == null) {
                advanceBusMoney = new BigDecimal(0);
            }
            BigDecimal loadDecimalSum = new BigDecimal("0.00");//借款金额总和
            BigDecimal settLoanMaxAmt = new BigDecimal("0.00");//借款金额最大值
            int accountNum = 0;//账户总数
            int settBadBizNum = 0;//账户数
            //关闭日期为4年内--五级分类为“3次级，4可疑，5损失，6违约
            BigDecimal settBadLoanAmt = new BigDecimal(0.00);
            int settBadLoanCount = 0;
            //关闭日期为1年内--五级分类为“2 关注
            BigDecimal settConcernLoanAmt = new BigDecimal(0.00);
            int settConcernLoanCount = 0;
            //五级分类为“3次级，4可疑，5损失，6违约
            BigDecimal settBadLoanAllAmt = new BigDecimal(0.00);
            int settBadLoanAllNum = 0;
            //五级分类为“2 关注
            BigDecimal settConcernLoanAllAmt = new BigDecimal(0.00);
            int settConcernLoanAllNum = 0;
            BigDecimal fiveClassDecimal = new BigDecimal("0.00");
            Date before2Date = null;//报告日期向前推2年
            if (createTime != null) {
                before2Date = DateUtil.getAimYearDate(createTime, -2);//报告日期向前推2年
            }
            int late2yBadBusiNum = 0;//关闭日期或结清日期为2年内--五级分类不为“正常”的账户总数

            DebitAccountInformDataBo debitAccountInformDataBo = ceqReportBo.getDebitAccountInformDataBo();
            //借贷账户信息 EDA --借贷账户信息单元 ED01
            if (null != debitAccountInformDataBo) {
                List<LeAccInformUnitBo> leAccInformUnitBoList = debitAccountInformDataBo.getLeAccInformUnitBoList();
                if (leAccInformUnitBoList != null && leAccInformUnitBoList.size() > 0) {
                    // 2 已结清
                    List<LeAccInformUnitBo> leAccInformUnitBoList1 = leAccInformUnitBoList.stream().filter(e ->
                            StringUtils.isNotBlank(e.getEsalLeAccInform().getAccountState()) && Constants.ACTIV_STATUS_2.equals(e.getEsalLeAccInform().getAccountState())).collect(Collectors.toList());
                    if (leAccInformUnitBoList1 != null && leAccInformUnitBoList1.size() > 0) {
                        for (LeAccInformUnitBo leAccInformUnitBo : leAccInformUnitBoList1) {
                            //借贷账户基本信息段 ED01B
                            EsalLeAccInform esalLeAccInform = leAccInformUnitBo.getEsalLeAccInform();
                            if (esalLeAccInform != null) {
                                String leAccountType = esalLeAccInform.getLeAccountType();
                                //D1中长期借款 + R1短期借款 + R4循环透支
                                if (StringUtils.isNotBlank(leAccountType) && (Constants.ACC_TYPE_D1.equals(leAccountType)
                                        || Constants.ACC_TYPE_R1.equals(leAccountType) || Constants.ACC_TYPE_R4.equals(leAccountType))) {
                                    BigDecimal loanAmount = esalLeAccInform.getLoanAmount();
                                    if (loanAmount != null) {
                                        loadDecimalSum = loadDecimalSum.add(loanAmount);
                                        settLoanMaxAmt = loanAmount.compareTo(settLoanMaxAmt) > 0 ? loanAmount : settLoanMaxAmt;
                                    }
                                    accountNum += 1;
//---------------------------------------------------------------------------------------------------------------------------------
                                    boolean flag1 = false;
                                    boolean flag2 = false;
                                    boolean flag3 = false;
                                    EsalLeAccRepayInform esalLeAccRepayInform = leAccInformUnitBo.getEsalLeAccRepayInform();
                                    if (esalLeAccRepayInform != null) {
                                        List<EsalLeAccRepayInformSub> esalLeAccRepayInformSubList = esalLeAccRepayInform.getEsalLeAccRepayInformSubList();
                                        if (esalLeAccRepayInformSubList != null && esalLeAccRepayInformSubList.size() > 0) {
                                            //根据日期逆序
                                            List<EsalLeAccRepayInformSub> list = esalLeAccRepayInformSubList.stream().sorted(Comparator.comparing(EsalLeAccRepayInformSub::getReportInformDate).reversed()).collect(Collectors.toList());
                                            EsalLeAccRepayInformSub esalLeAccRepayInformSub = list.get(0);
                                            if (esalLeAccRepayInformSub != null) {
                                                String fileClass = esalLeAccRepayInformSub.getFileClass();
                                                BigDecimal overMonthNum = esalLeAccRepayInformSub.getOverMonthNum();//逾期月数
                                                int overMonthNumInt = overMonthNum == null ? 0 : overMonthNum.intValue();
                                                //五级分类为 3次级，4可疑，5损失，6违约  五级分类为空（9 未分类）且逾期天数大于3个月
                                                flag1 = Constants.FIF_TYPE_3.equals(fileClass) || Constants.FIF_TYPE_4.equals(fileClass) || Constants.FIF_TYPE_5.equals(fileClass)
                                                        || Constants.FIF_TYPE_6.equals(fileClass) || (Constants.FIF_TYPE_9.equals(fileClass) && (overMonthNumInt > 3));
                                                //五级分类为 2 关注   五级分类为空（9 未分类）且逾期天数大于3个月
                                                flag2 = Constants.FIF_TYPE_2.equals(fileClass) || (Constants.FIF_TYPE_9.equals(fileClass) && (overMonthNumInt > 3));
                                                //五级分类不为 1.正常
                                                flag3 = !fileClass.equals("1");
                                            }
                                        }
                                    }
                                    if (flag1) {
                                        BigDecimal loanAmounts = esalLeAccInform.getLoanAmount();//借款金额
                                        if (loanAmounts != null) {
                                            settBadLoanAllAmt = settBadLoanAllAmt.add(loanAmounts);
                                        }
                                        settBadLoanAllNum += 1;
                                        if (createTime != null) {
                                            //报告日期向前推4年
                                            Date beforeDate = DateUtil.getAimYearDate(createTime, -4);
                                            Date closeData = esalLeAccInform.getCloseDate();
                                            if (closeData != null && beforeDate != null) {
                                                //报告日期-4 < before 关闭日期
                                                if (beforeDate.before(closeData)) {
                                                    if (loanAmount != null) {
                                                        settBadLoanAmt = settBadLoanAmt.add(loanAmount);
                                                    }
                                                    settBadLoanCount += 1;
                                                }
                                            }
                                        }
                                    }
                                    if (flag2) {
                                        settConcernLoanAllNum += 1;
                                        if (esalLeAccInform != null) {
                                            BigDecimal loanAmounte = esalLeAccInform.getLoanAmount();//借款金额
                                            if (loanAmounte != null) {
                                                settConcernLoanAllAmt = settConcernLoanAllAmt.add(loanAmounte);
                                            }
                                            if (createTime != null) {
                                                //报告日期向前推1年
                                                Date beforeDate = DateUtil.getAimYearDate(createTime, -1);
                                                Date closeDate = esalLeAccInform.getCloseDate();
                                                if (closeDate != null && beforeDate != null) {
                                                    //报告日期-1 < before 关闭日期
                                                    if (beforeDate.before(closeDate)) {
                                                        settConcernLoanAmt = settConcernLoanAmt.add(loanAmount);
                                                        settConcernLoanCount += 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (flag3) {
                                        Date closeDate = esalLeAccInform.getCloseDate();
                                        // 2 已结清  关闭日期 2 年内
                                        if (closeDate != null) {
                                            //报告日期向前推2年
                                            if (closeDate != null && before2Date != null) {
                                                //报告日期-2 < before 关闭日期
                                                if (before2Date.before(closeDate)) {
                                                    late2yBadBusiNum += 1;
                                                }
                                            }
                                        }
                                    }
                                }
//---------------------------------------------------------------------------------------------------------------------------------
                                boolean flag = false;
                                EsalLeAccRepayInform esalLeAccRepayInform = leAccInformUnitBo.getEsalLeAccRepayInform();
                                if (esalLeAccRepayInform != null) {
                                    List<EsalLeAccRepayInformSub> list = esalLeAccRepayInform.getEsalLeAccRepayInformSubList();
                                    if (list != null && list.size() > 0) {
                                        for (EsalLeAccRepayInformSub esalLeAccRepayInformSub : list) {
                                            if (esalLeAccRepayInformSub != null) {
                                                String fileClass = esalLeAccRepayInformSub.getFileClass();
                                                //五级分类 3.次级 4.可疑 5.损失
                                                flag = Constants.FIF_TYPE_3.equals(fileClass) || Constants.FIF_TYPE_4.equals(fileClass) || Constants.FIF_TYPE_5.equals(fileClass);
                                            }
                                        }
                                    }
                                }
                                if (flag) {
                                    BigDecimal loadAmount = esalLeAccInform.getLoanAmount();
                                    if (loadAmount != null) {
                                        fiveClassDecimal = fiveClassDecimal.add(loadAmount);
                                    }
                                    settBadBizNum += 1;
                                }
                            }
                        }
                    }
                }
            }
            //23.信贷记录明细--已结清信贷--中长期借款+短期借款+循环透支--借款金额最大值
            companyZBInfoPo.setSettLoanMaxAmt(String.valueOf(settLoanMaxAmt));
            //24.信贷记录明细--已结清信贷--中长期借款+短期借款+循环透支--借款金额总和/信贷记录明细--已结清信贷--中长期借款+短期借款+循环透支--账户总数
            BigDecimal settLoanAverAmt = loadDecimalSum.divide(new BigDecimal(accountNum), 10, BigDecimal.ROUND_HALF_UP);
            companyZBInfoPo.setSettLoanAverAmt(String.valueOf(settLoanAverAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //103. 信贷记录明细--2 已结清信贷 --中长期借款+短期借款+循环透支--关闭日期为4年内--五级分类为“3次级，4可疑，5损失，6违约；五级分类为空且逾期大于3个月”--借款金额总合
            companyZBInfoPo.setSettBadLoanAmt(String.valueOf(settBadLoanAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //104. 信贷记录明细--2 已结清信贷 --中长期借款+短期借款+循环透支--关闭日期为4年内--五级分类为“次级，可疑，损失，违约；五级分类为空且逾期大于3个月”--账户总数
            companyZBInfoPo.setSettBadLoanCount(String.valueOf(settBadLoanCount));
            //105. 信贷记录明细--2 已结清信贷 --中长期借款+短期借款+循环透支--关闭日期为1年内--五级分类为“2关注；五级分类为空且逾期大于3个月”--借款金额总合
            companyZBInfoPo.setSettConcernLoanAmt(String.valueOf(settConcernLoanAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //106. 信贷记录明细--2 已结清信贷 --中长期借款+短期借款+循环透支--关闭日期为1年内--五级分类为“2关注；五级分类为空且逾期大于3个月”--账户总数
            companyZBInfoPo.setSettConcernLoanCount(String.valueOf(settConcernLoanCount));

            //109. 信贷记录明细-- 2 已结清业务--关闭日期或结清日期为2年内--五级分类不为“正常”的账户总数
            companyZBInfoPo.setLate2yBadBusiNum(String.valueOf(late2yBadBusiNum));

            //118. 信贷记录明细--2 已结清信贷 --中长期借款+短期借款+循环透支--五级分类为“3次级，4可疑，5损失，6违约；五级分类为空且逾期大于3个月”--借款金额总合
            companyZBInfoPo.setSettBadLoanAllAmt(String.valueOf(settBadLoanAllAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //119. 信贷记录明细--2 已结清信贷 --中长期借款+短期借款+循环透支--五级分类为“次级，可疑，损失，违约；五级分类为空且逾期大于3个月”--账户总数
            companyZBInfoPo.setSettBadLoanAllNum(String.valueOf(settBadLoanAllNum));
            //120. 信贷记录明细--2 已结清信贷 --中长期借款+短期借款+循环透支--五级分类为“2关注；五级分类为空且逾期小于3个月”--借款金额总合
            companyZBInfoPo.setSettConcernLoanAllAmt(String.valueOf(settConcernLoanAllAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //121. 信贷记录明细--2 已结清信贷 --中长期借款+短期借款+循环透支--五级分类为“2关注；五级分类为空且逾期小于3个月”--账户总数
            companyZBInfoPo.setSettConcernLoanAllNum(String.valueOf(settConcernLoanAllNum));

            BigDecimal accountMoney = new BigDecimal("0.00");//各账户金额之和
            BigDecimal settAdvanAmt = new BigDecimal("0.00");//金额加总
            int settAdvanCount = 0;//账户总数
            //担保账户信息 EDB --担保类信息账户信息明细单元 ED04
            List<GuarAccoInfoBo> guarAccoInfoBoList = ceqReportBo.getGuarAccoInfoDataBo().getGuarAccoInfoBoList();
            if (guarAccoInfoBoList != null && guarAccoInfoBoList.size() > 0) {
                List<GuarAccoInfoBo> guarAccoInfoBoList1 = guarAccoInfoBoList.stream().filter(a -> Constants.ACTIV_STATUS_2.equals(a.getEdgrGuarresponinfoseg().getAccActivStatus())).collect(Collectors.toList());
                //报告日期向前推4年
                Date beforeDate = null;
                if (createTime != null) {
                    beforeDate = DateUtil.getAimYearDate(createTime, -4);
                }
                for (GuarAccoInfoBo guarAccoInfoBo : guarAccoInfoBoList1) {
                    EdgbGuarbasicinfoseg edgbGuarbasicinfoseg = guarAccoInfoBo.getEdgbGuarbasicinfoseg();
                    if (edgbGuarbasicinfoseg != null) {
                        BigDecimal amount = edgbGuarbasicinfoseg.getAmount();
                        if (amount != null) {
                            accountMoney = accountMoney.add(amount);
                        }
                    }
                    boolean flag = false;
                    EdgrGuarresponinfoseg edgrGuarresponinfoseg = guarAccoInfoBo.getEdgrGuarresponinfoseg();
                    if (edgrGuarresponinfoseg != null) {
                        String fileClass = edgrGuarresponinfoseg.getFiveLevelClass();
                        String compensFlag = edgrGuarresponinfoseg.getCompensFlag();
                        //五级分类 3.次级 4.可疑 5.损失    //已结清 2
                        flag = fileClass != null && compensFlag != null && Constants.FIF_TYPE_3.equals(fileClass) || Constants.FIF_TYPE_4.equals(fileClass) || Constants.FIF_TYPE_5.equals(fileClass);
                        //--2 已结清信贷 --银行承兑汇票和信用证+银行保函及其他业务--1 垫款标志为“是”且关闭日期为4年内的账户
                        Date closeData = edgrGuarresponinfoseg.getCloseDate();//关闭日期
                        if (closeData != null && beforeDate != null && StringUtils.isNotBlank(compensFlag)) {
                            //报告日期-4 < before 关闭日期
                            if (Constants.COMPENS_FLAG_1.equals(compensFlag) && beforeDate.before(closeData)) {
                                BigDecimal amount = guarAccoInfoBo.getEdgbGuarbasicinfoseg().getAmount();//金额
                                if (amount != null) {
                                    settAdvanAmt = settAdvanAmt.add(amount);
                                }
                                settAdvanCount += 1;
                            }
                        }
                    }
                    if (flag) {
                        BigDecimal amount = edgbGuarbasicinfoseg.getAmount();
                        if (amount != null) {
                            fiveClassDecimal = fiveClassDecimal.add(amount);
                        }
                        settBadBizNum += 1;
                    }

                }
            }
            //20. 已结清信贷信息概要--垫款--金额 EB02BJ02 + 信贷记录明细--已结清信贷--各账户金额之和
            BigDecimal settClTotalAmt = advanceBusMoney.add(accountMoney);
            companyZBInfoPo.setSettClTotalAmt(String.valueOf(settClTotalAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //22.信贷记录明细--已结清信贷--中长期借款+短期借款+循环透支--借款金额总和/sett_cl_total_amt
            BigDecimal settLSettClAmtRate = loadDecimalSum.divide(settClTotalAmt, 2, BigDecimal.ROUND_HALF_UP);
            companyZBInfoPo.setSettLSettClAmtRate(String.valueOf(settLSettClAmtRate.setScale(3, BigDecimal.ROUND_HALF_UP)));
            //25.信贷记录明细--已结清信贷--五级分类为“次要，可疑和损失”下垫款总发生额+信贷记录明细--已结清信贷--级分类为“次要，可疑和损失”下各账户金额之和
            companyZBInfoPo.setSettBadBizTotalAmt(String.valueOf(fiveClassDecimal.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //101. 信贷记录明细--2 已结清信贷 --银行承兑汇票和信用证+银行保函及其他业务--1 垫款标志为“是”且关闭日期为4年内的账--金额加总
            companyZBInfoPo.setSettAdvanAmt(String.valueOf(settAdvanAmt.setScale(2, BigDecimal.ROUND_HALF_UP)));
            //102. 信贷记录明细--2 已结清信贷 --银行承兑汇票和信用证+银行保函及其他业务--1 垫款标志为“是”且关闭日期为4年内的账户--账户总数
            companyZBInfoPo.setSettAdvanCount(String.valueOf(settAdvanCount));
            //26.信贷记录明细--已结清信贷--五级分类为“次级、可疑和损失”下垫款账户数 + 信贷记录明细--已结清信贷--五级分类为“次级、可疑和损失”下账户数
            companyZBInfoPo.setSettBadBizNum(String.valueOf(settBadBizNum));
//---------------------------------------------------------------------------------------------------------------------------------

            //不良类
            int unSettBadClBizNum = 0;
            //借贷交易汇总信息 EBB --借贷交易汇总信息单元 EB02 -- 未结清借贷交易汇总信息 EB02A 1 --其他借贷交易分类汇总信息 EB02AH
            List<EbboOthloantrasortsumu> ebboOthloantrasortsumuList = ceqReportBo.getLoanTransaDataBo().getLoanTransaBo().getEbbuUnloantransa().getEbboOthloantrasortsumuList();
            if (ebboOthloantrasortsumuList != null && ebboOthloantrasortsumuList.size() > 0) {
                // 业务类型  0合计 （中长期借款、短期借款、循环透支和贴现业务的汇总）  //不良类 3
                List<EbboOthloantrasortsumu> listu = ebboOthloantrasortsumuList.stream().filter(e -> Constants.BUS_TYPE_0.equals(e.getBusType())
                        && Constants.FIF_TYPE_3.equals(e.getAssQualSort())).collect(Collectors.toList());
                unSettBadClBizNum += listu.size();
            }
            //担保交易汇总信息 EBC --担保交易汇总信息单元 EB03 -- 未结清担保交易汇总信息 EB03A 1 --未结清担保交易汇总信息 EB03AH
            List<EssuUnclearInformSub> essuUnclearInformSubList = ceqReportBo.getSecTransInformUnitDataBo().getSecTransInformUnitBo().getEssuUnclearInform().getEssuUnclearInformSubList();
            //未结清担保交易汇总信息
            if (essuUnclearInformSubList != null && essuUnclearInformSubList.size() > 0) {
                //业务类型  1-银行承兑汇票  2-信用证  3-银行保函  9-其他     //不良类  3
                List<EssuUnclearInformSub> listn = essuUnclearInformSubList.stream().filter(e -> Constants.FIF_TYPE_3.equals(e.getAqClass())).collect(Collectors.toList());
                unSettBadClBizNum += listn.size();
            }
            //27.un_sett_bad_cl_biz_num + sett_bad_biz_num
            int badBizTotalNum = unSettBadClBizNum + settBadBizNum;
            companyZBInfoPo.setBadBizTotalNum(String.valueOf(badBizTotalNum));
        } catch (
                Exception e) {
            log.error("CompanyZBClearedCreditDetailServiceImpl", e);
            throw new Exception(e);
        }
        return companyZBInfoPo;
    }
}

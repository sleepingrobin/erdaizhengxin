package com.dhcc.cips.service.impl.company;

import com.dhcc.cips.bo.base.Request;
import com.dhcc.cips.bo.base.ResponseHead;
import com.dhcc.cips.bo.base.ResponseSysHead;
import com.dhcc.cips.bo.base.ResponseSysHeadRet;
import com.dhcc.cips.bo.other.ReportFileDownloadResBo;
import com.dhcc.cips.bo.other.ReportFileDownloadResBodyBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.mapper.company.CeqResultInfoMapper;
import com.dhcc.cips.po.company.CeqResultInfoPo;
import com.dhcc.cips.service.company.CompanyReportFileService;
import com.dhcc.cips.util.XmlUtil;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;

/**
 * @author limin
 * @date 2019年5月30日
 * @description 企业征信文件传输下载
 */
@Slf4j
@Service
public class CompanyReportFileServiceImpl implements CompanyReportFileService {
    @Resource
    private CeqResultInfoMapper ceqResultInfoMapper;

    @Override
    public String download(Request request){

        //返回报文对象
        ReportFileDownloadResBo reportFileDownloadResBo = new ReportFileDownloadResBo();
        //返回报文体对象
        ReportFileDownloadResBodyBo reportFileDownloadResBodyBo = new ReportFileDownloadResBodyBo();

        //返回信息
        String retCode;//交易返回码
        String retMesaage;//交易返回信息
        String retStatus;//交易返回状态
        try {

            //获取请求参数
            String globalType =  request.getRequestBody().getGlobalType();//证件类型
            String globalId = request.getRequestBody().getGlobalId();//证件号码（企业只接受贷款卡号）
            String corpName = request.getRequestBody().getBranchName();//法人机构名称
            String userId = request.getRequestSysHead().getUserId();//查看人工号

            //请求参数校验
            if (StringUtils.isEmpty(globalType)) {
                throw new Exception(Constants.GLOBAL_TYPE_NULL_ANO);//证件类型不能为空
            }
            if (StringUtils.isEmpty(globalId)) {
                throw new Exception(Constants.GLOBAL_ID_NULL_ANO);//中证码（贷款卡号）不能为空
            }
            if (StringUtils.isEmpty(corpName)) {
                throw new Exception(Constants.CORP_NAME_NULL_ANO);//法人机构名称不能为空
            }
            if (StringUtils.isEmpty(userId)) {
                throw new Exception(Constants.QUERY_MGR_NO_NULL_ANO);//查看人工号不能为空
            }

            HashMap<Object, Object> map = Maps.newHashMap();
            map.put("loancardNo", globalId);
            CeqResultInfoPo ceqResultInfoPo = ceqResultInfoMapper.selectByLoancardNo(map);
            if (ceqResultInfoPo == null) {
                throw new Exception(Constants.REPORT_NULL); //无本地征信报告
            }

            String reportFilePath = ceqResultInfoPo.getHtmlPath();//征信报告绝对路径
            //填充返回体
            reportFileDownloadResBodyBo.setReportFilePath(reportFilePath);

            //交易返回码
            retCode = Constants.SUCCESS_CODE;
            //交易返回信息
            retMesaage = "交易成功";
            //交易返回状态
            retStatus = Constants.SUCCESS_S;
        } catch (Exception e) {
            log.error("", e);
            //交易返回码
            retCode = Constants.FAIL_CODE;
            //交易返回信息
            retMesaage = "交易失败，" + e.getMessage();
            //交易返回状态
            retStatus = Constants.FAIL_F;
        }

        //填充返回头
        ResponseHead responseHead = new ResponseHead();
        ResponseSysHead responseSysHead = new ResponseSysHead();
        BeanUtils.copyProperties(request.getRequestSysHead(), responseSysHead);
        //返回状态码
        responseSysHead.setRetStatus(retStatus);
        responseHead.setResponseSysHead(responseSysHead);

        ResponseSysHeadRet responseSysHeadRet = new ResponseSysHeadRet();
        //交易返回码
        responseSysHeadRet.setRetCode(retCode);
        //交易返信息
        responseSysHeadRet.setRetMsg(retMesaage);
        responseHead.getResponseSysHead().setResponseSysHeadRet(responseSysHeadRet);

        //返回体
        reportFileDownloadResBo.setReportFileDownloadResBodyBo(reportFileDownloadResBodyBo);

        //转换成xml报文
        String xml = XmlUtil.converJavaBean2Xml(reportFileDownloadResBo, reportFileDownloadResBo.getClass());

        return xml;
    }
}

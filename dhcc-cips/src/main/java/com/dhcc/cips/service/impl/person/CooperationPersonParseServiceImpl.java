package com.dhcc.cips.service.impl.person;

import com.dc.eai.data.CompositeData;
import com.dhcc.cips.bo.other.CooperationReqBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.esbclient.TCPClient;
import com.dhcc.cips.mapper.person.CpqResultInfoMapper;
import com.dhcc.cips.po.person.CpqResultInfoPo;
import com.dhcc.cips.service.impl.other.EsbParentMessageServiceImpl;
import com.dhcc.cips.service.impl.other.EsbServiceImpl;
import com.dhcc.cips.service.person.CooperationPersonParseService;
import com.dhcc.cips.service.person.PersonReportParseService;
import com.dhcc.cips.util.DateUtil;
import com.dhcc.cips.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 对外合作个人征信报告解析服务实现类
 */
@Service
@Slf4j
public class CooperationPersonParseServiceImpl extends EsbParentMessageServiceImpl implements CooperationPersonParseService {

    @Autowired
    private PersonReportParseService personReportParseService;

    /**
     *  解析xml版征信报告
     * @param cooperationReqBo
     * @return
     * @throws Exception
     */
    @Async
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void parseWithXml(CooperationReqBo cooperationReqBo) throws Exception {
        try {
            personReportParseService.parseWithXml(cooperationReqBo.getXmlStr());
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    /**
     * 解析html版征信报告
     * @param cooperationReqBo
     * @throws Exception
     */
    @Async
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void parseWithHtml(CooperationReqBo cooperationReqBo) throws Exception {

        CpqResultInfoPo cpqResultInfoPo = new CpqResultInfoPo();
        String parseResult = "";//解析结果
        String parseResultMsg = "";//解析异常信息

        try {
            cpqResultInfoPo.setReqAcceptTime(new Date());//请求接收时间

            personReportParseService.parseWithHtml(cooperationReqBo.getHtmlStr());

            cpqResultInfoPo.setQueryReturnTime(new Date());//请求反馈时间

            parseResult = "P";//征信报告解析成功

        } catch (Exception e) {
            parseResult = "N";//征信报告解析失败
            parseResultMsg = e.getMessage();
            throw new Exception(e);
        } finally {

            //更新resultInfo表信息
            cpqResultInfoPo.setId(StringUtil.getUUID());//主键
            cpqResultInfoPo.setClientName(cooperationReqBo.getCustomerName());//客户名称
            cpqResultInfoPo.setCertType(cooperationReqBo.getGlobalType());//证件号码
            cpqResultInfoPo.setCertNo(cooperationReqBo.getGlobalId());//证件号码
            cpqResultInfoPo.setOperator(cooperationReqBo.getUserId());//员工号
            cpqResultInfoPo.setOperOrg(cooperationReqBo.getOrgCode());//机构号
            cpqResultInfoPo.setQueryTime(cooperationReqBo.getQueryDate());//查询时间
            cpqResultInfoPo.insert();


            //通知信贷系统征信报告解析结果
            String transCode = "";//交易返回代码
            String transMessage = "";//交易返回信息

            TCPClient tcp = new TCPClient("02002000026", "06", Constants.SYSTEM_ID_LOAN);
            tcp.setUserId(cooperationReqBo.getUserId());//经办人
            tcp.setBranchId(cooperationReqBo.getOrgCode());//经办机构
            tcp.setCorpCode(cooperationReqBo.getCorpCode());//法人机构号
            tcp.setBusinessSerialno(cooperationReqBo.getBussSeqNo());//业务全局流水号
            tcp.setSourceType(cooperationReqBo.getSourceType());//渠道类型

            CompositeData req_body = new CompositeData();
            addField(req_body, "GLOBAL_TYPE", "String", 5, cooperationReqBo.getGlobalType());//证件类型
            addField(req_body, "GLOBAL_ID", "String", 60, cooperationReqBo.getGlobalId());//证件号码
            addField(req_body, "ANLYS_RSLT_TP", "String", 10, parseResult);//解析结果
            addField(req_body, "FAIL_RSN", "String", 1024, parseResultMsg);//解析失败原因

            tcp.setReq_body(req_body);
            tcp.doTransaction();
            transCode = tcp.getRetCode();
            transMessage = tcp.getRetMessage();


            TCPClient  tcpRegist = new TCPClient("02002000036", "01", Constants.SYSTEM_ID_LOAN);
            tcp.setUserId(cooperationReqBo.getUserId());//经办人
            tcp.setBranchId(cooperationReqBo.getOrgCode());//经办机构
            tcp.setCorpCode(cooperationReqBo.getCorpCode());//法人机构号
            tcp.setBusinessSerialno(cooperationReqBo.getBussSeqNo());//业务全局流水号
            tcp.setSourceType(cooperationReqBo.getSourceType());//渠道类型
            CompositeData req_body_regist = new CompositeData();

            addField(req_body_regist, "GLOBAL_TYPE", "String", 5, cooperationReqBo.getGlobalType());//证件类型
            addField(req_body_regist, "GLOBAL_ID", "String", 60, cooperationReqBo.getGlobalId());//证件号码
            addField(req_body_regist, "CLIENT_NAME", "String", 200, cooperationReqBo.getCustomerName());//客户名称
            addField(req_body_regist, "PRE_EVAL_SEQ_NO", "String", 32, cooperationReqBo.getPreEvalSeqNo());//预评估流水号
            
            if ("P".equals(parseResult) && Constants.SUCCESS_CODE.equals(transCode)) {//征信解析成功，通知信贷结果成功
                log.error("交易流水号为：" + cooperationReqBo.getBussSeqNo() + "的征信报告解析成功，通知信贷系统征信报告解析结果成功！");
                addField(req_body_regist,"ORI_RET_CODE","String",30,Constants.SUCCESS_CODE);//原交易返回代码
                addField(req_body_regist,"ORI_RET_MSG","String",512,"交易成功");//原交易返回信息
            } else if (!"P".equals(parseResult) && Constants.SUCCESS_CODE.equals(transCode)) {//征信解析失败，通知信贷系统结果成功
                log.error("交易流水号为：" + cooperationReqBo.getBussSeqNo() + "的征信报告解析失败，通知信贷系统征信报告解析结果成功！");
                log.error("交易流水号为：" + cooperationReqBo.getBussSeqNo() + "的征信报告解析失败的原因：" + parseResultMsg);
                addField(req_body_regist,"ORI_RET_CODE","String",30,Constants.FAIL_CODE);//原交易返回代码
                addField(req_body_regist,"ORI_RET_MSG","String",512,"交易失败！征信报告解析失败，通知信贷系统征信报告解析结果成功！征信报告解析失败的原因：" + parseResultMsg);//原交易返回信息
            } else if ("P".equals(parseResult) && !Constants.SUCCESS_CODE.equals(transCode)) {//征信解析成功，通知信贷系统结果失败
                log.error("交易流水号为：" + cooperationReqBo.getBussSeqNo() + "的征信报告解析成功，通知信贷系统征信报告解析结果失败！");
                log.error("交易流水号为：" + cooperationReqBo.getBussSeqNo() + "的征信报告解析结果通知信贷系统失败的原因：" + transMessage);
                addField(req_body_regist,"ORI_RET_CODE","String",30,Constants.FAIL_CODE);//原交易返回代码
                addField(req_body_regist,"ORI_RET_MSG","String",512,"交易失败！征信报告解析成功，通知信贷系统征信报告解析结果失败！征信报告解析结果通知信贷系统失败的原因：" + transMessage);//原交易返回信息
            } else if (!"P".equals(parseResult) && !Constants.SUCCESS_CODE.equals(transCode)) {//征信解析失败，通知信贷系统结果失败
                log.error("交易流水号为：" + cooperationReqBo.getBussSeqNo() + "的征信报告解析失败，通知信贷系统征信报告解析结果失败！");
                log.error("交易流水号为：" + cooperationReqBo.getBussSeqNo() + "的征信报告解析失败的原因：" + parseResultMsg + "；通知信贷系统通知信贷系统征信报告解析结果失败的原因：" + transMessage);
                addField(req_body_regist,"ORI_RET_CODE","String",30,Constants.FAIL_CODE);//原交易返回代码
                addField(req_body_regist,"ORI_RET_MSG","String",512,"交易失败！征信报告解析失败，通知信贷系统征信报告解析结果失败！征信报告解析失败的原因：" + parseResultMsg + "；通知信贷系统通知信贷系统征信报告解析结果失败的原因：" + transMessage);//原交易返回信息
            }

            //对外合作登记簿
            log.error("交易流水号为：" + cooperationReqBo.getBussSeqNo() + "的征信报告处理结果登记到信贷系统！");
            tcpRegist.setReq_body(req_body_regist);
            tcpRegist.doTransaction();

            String transCode_regist = tcpRegist.getRetCode();
            String transMessage_regist = tcpRegist.getRetMessage();
            if (Constants.SUCCESS_CODE.equals(transCode_regist)) {
                log.error("交易流水号为：" + cooperationReqBo.getBussSeqNo() + "征信报告处理结果登记到信贷系统成功！");
            } else {
                log.error("交易流水号为：" + cooperationReqBo.getBussSeqNo() + "征信报告处理结果登记到信贷系统失败！失败原因：" + transMessage_regist);
            }

        }
    }

}

package com.dhcc.cips.service.impl.person;

import com.dhcc.cips.bo.base.*;
import com.dhcc.cips.bo.other.QueryReqBo;
import com.dhcc.cips.bo.other.QueryResBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.mapper.other.SystemDicMapper;
import com.dhcc.cips.mapper.person.CpqResultInfoMapper;
import com.dhcc.cips.po.person.CpqResultInfoPo;
import com.dhcc.cips.service.other.QueryAccountService;
import com.dhcc.cips.service.person.PersonReportQueryService;
import com.dhcc.cips.util.ConvertUtil;
import com.dhcc.cips.util.DateUtil;
import com.dhcc.cips.util.XmlUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;

/**
 * 个人征信查询
 */
@Slf4j
@Service
@RefreshScope
public class PersonReportQueryServiceImpl implements PersonReportQueryService {
    @Value("${personWsdl}")
    private String personWsdl;
    @Value("${reportVersion}")
    private String reportVersion;
    @Value("${syncFlag}")
    private String syncFlag;
    @Value("${reportType}")
    private String reportType;

    @Resource
    private SystemDicMapper systemDicMapper;
    @Resource
    private CpqResultInfoMapper cpqResultInfoMapper;
    @Autowired
    private QueryAccountService queryAccountService;

    /**
     * 个人征信查询
     *
     * @param request
     * @return
     * @throws Exception
     */
    @Override
    public String exec(Request request) {

        InputStream is = null;

        //返回信息
        String retCode;
        String retMessage;
        String retStatus;
        try {
            RequestSysHead requestSysHead = request.getRequestSysHead();
            RequestBody requestBody = request.getRequestBody();
            //获取请求参数
            String clientName = requestBody.getClientName();                         //客户名称
            String globalType = requestBody.getGlobalType();                         //证件类型
            String globalId = requestBody.getGlobalId();                             //证件号码
            String reasonCode = requestBody.getReasonCode();                         //查询原因
            String signMd = requestBody.getSignMd();                                 //签署方式
            String checkUserId = requestBody.getChckTlr();                           //核查柜员
            String checkUserName = requestBody.getTellerRoleName();                  //柜员角色名称
            String checkTime = requestBody.getOprtTm();                              //审核时间
            String userName = requestBody.getClntMgrNm();                            //客户经理名称
            String OrgName = requestBody.getMgrOrgName();                            //客户经理机构名称
            String userId = requestSysHead.getUserId();                              //客户经理工号
            String orgId = requestSysHead.getBranchId();                             //客户经理机构号
            String sourceType = requestSysHead.getSourceType();                      //渠道

            //参数请求校验
//            if (StringUtils.isEmpty(userId)) {
//                retMessage = Constants.CLNT_MGR_NO_NULL_ANO; //客户经理工号不能为空！
//                throw new Exception(retMessage);
//            }
//            if (StringUtils.isEmpty(userName)) {
//                retMessage = Constants.CLNT_MGR_NM_NULL_ANO; //客户经理名称不能为空！
//                throw new Exception(retMessage);
//            }
            if (StringUtils.isEmpty(orgId)) {
                retMessage = Constants.MGR_ORG_NO_NULL_ANO; //客户经理机构号不能为空！
                throw new Exception(retMessage);
            }
//            if (StringUtils.isEmpty(OrgName)) {
//                retMessage = Constants.MGR_ORG_NAME_NULL_ANO; //客户经理机构名称不能为空！
//                throw new Exception(retMessage);
//            }
            if (StringUtils.isEmpty(clientName)) {
                retMessage = Constants.CLIENT_NAME_NULL_ANO; //客户名称不能为空！
                throw new Exception(retMessage);
            }
            if (StringUtils.isEmpty(globalType)) {
                retMessage = Constants.GLOBAL_TYPE_NULL_ANO;//证件类型不能为空！
                throw new Exception(retMessage);
            }
            if (StringUtils.isEmpty(globalId)) {
                retMessage = Constants.GLOBAL_ID_NULL_ANO;//证件号码不能为空！
                throw new Exception(retMessage);
            }
            if (StringUtils.isEmpty(reasonCode)) {
                retMessage = Constants.REASON_CODE_NULL_ANO;//查询原因不能为空！
                throw new Exception(retMessage);
            }
//            if (StringUtils.isEmpty(signMd)) {
//                retMessage = Constants.SIGN_MD_NULL_ANO;//签署方式不能为空！
//                throw new Exception(retMessage);
//            }
            //查询原因为贷后管理时，审核信息不能为空
//            if (Constants.P_REASON_DHGL.equals(reasonCode)) {
//                if (StringUtils.isEmpty(checkUserId)) {
//                    retMessage = Constants.CHCK_USERID_NULL_ANO;//审核人工号不能为空！
//                    throw new Exception(retMessage);
//                }
//                if (StringUtils.isEmpty(checkUserName)) {
//                    retMessage = Constants.CHCK_USERNAME_NULLE_ANO;//审核人名称不能为空！
//                    throw new Exception(retMessage);
//                }
//                if (StringUtils.isEmpty(checkTime) && checkTime.length() != 17) {
//                    retMessage = Constants.CHCK_TM_ANO;//审核时间时间不能为空！
//                    throw new Exception(retMessage);
//                }
//            }

            //获取查询账号
            String queryUser = queryAccountService.getQueryUser(orgId, "P");
            //信用报告复用策略  负数:查询本地  0:征信中心  正数:征信报告有效期
            String queryType = queryAccountService.getReportQueryType(orgId, "P");
            String businessLine = systemDicMapper.selectBusinessLine();
            //组装产品查询个人服务请求报文
            QueryReqBo queryReqBo = new QueryReqBo();
            queryReqBo.setClientName(clientName);//客户名称
            queryReqBo.setGlobalType(globalType);//证件类型
            queryReqBo.setGlobalId(globalId);//证件号码
            queryReqBo.setReasonCode(reasonCode);///查询原因
            queryReqBo.setReportType(reportType);//信用报告格式   H:Html格式  X:Xml格式  HX:Html格式和Xml格式
            queryReqBo.setQueryType(queryType);//信用报告复用策略  负数:查询本地  0:征信中心  正数:征信报告有效期
            queryReqBo.setSyncFlag(syncFlag);//同步异步返回标识 0:同步 1:异步
            queryReqBo.setReportVersion(reportVersion);//信用报告版本  2.0.0:二代信用报告
            queryReqBo.setBusinessLine(businessLine);//业务线
            queryReqBo.setUserId(userId);//查询用户工号
            queryReqBo.setSysCode("1");//系统标识  区分接口调用子系统
            queryReqBo.setQueryAccount(queryUser);//查询账号
            String requestXml = ConvertUtil.getPersonConvertedRequestXml(queryReqBo);
            log.debug("requestXml : " + requestXml);

            //调产品查询接口
            PostMethod postMethod = new PostMethod(personWsdl);
            byte[] b = requestXml.getBytes("utf-8");
            is = new ByteArrayInputStream(b, 0, b.length);
            RequestEntity requestEntity = new InputStreamRequestEntity(is, b.length, "text/xml;charset=UTF-8");//请求对象
            postMethod.setRequestEntity(requestEntity);
            HttpClient httpClient = new HttpClient();
            //人行返回码
            int status = httpClient.executeMethod(postMethod);

            if (status != 200) {//调用服务失败
                throw new Exception("服务不可用");
            }

            String convertedResponseXml = ConvertUtil.getConvertedResponseXml(postMethod.getResponseBodyAsString());

            //调用产品查询接口返回报文转换成对象
            QueryResBo queryResBo = XmlUtil.convertXml2JavaBean(QueryResBo.class, convertedResponseXml, new Class[]{QueryResBo.class});
            if (queryResBo != null && Constants.SUCCESS_CODE.equals(queryResBo.getResCode())) {//查询成功
                //更新查询日志表信息
                CpqResultInfoPo cpqResultInfoPo = new CpqResultInfoPo();
                cpqResultInfoPo.setId(queryResBo.getCreditreportNo());//主键
                cpqResultInfoPo.setOperatorName(userName);//申请人姓名
                cpqResultInfoPo.setOperOrgName(OrgName);//申请机构名称
                cpqResultInfoPo.setAuthorizaVesion(signMd);//签署方式
                cpqResultInfoPo.setSourceType(sourceType); //渠道
                cpqResultInfoPo.setRekUser(checkUserId);  //审核人
                //cpqResultInfoPo.setRekOrg();            //审核人机构号
                cpqResultInfoPo.setRekTime(DateUtil.toDate(checkTime, "yyyy-MM-dd HH:mm:ss"));//审核人时间
                cpqResultInfoPo.updateById();
            } else {
                throw new Exception(queryResBo.getResMsg());//查询失败原因
            }
            //交易返回码
            retCode = Constants.SUCCESS_CODE;
            //交易返回信息
            retMessage = "交易成功";
            //交易返回状态
            retStatus = Constants.SUCCESS_S;
        } catch (Exception e) {
            log.error("", e);
            //交易返回码
            retCode = Constants.FAIL_CODE;
            //交易返回信息
            retMessage = "交易失败，" + e.getMessage();
            //交易返回状态
            retStatus = Constants.FAIL_F;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    log.debug("PersonReportQueryServiceImpl.exec", e);
                }
            }
        }
        //设置返回对象参数
        ResponseHead responseHead = new ResponseHead();
        ResponseSysHead responseSysHead = new ResponseSysHead();
        BeanUtils.copyProperties(request.getRequestSysHead(), responseSysHead);
        //返回状态码
        responseSysHead.setRetStatus(retStatus);
        responseHead.setResponseSysHead(responseSysHead);
        ResponseSysHeadRet responseSysHeadRet = new ResponseSysHeadRet();
        //交易返回码
        responseSysHeadRet.setRetCode(retCode);
        //交易返信息
        responseSysHeadRet.setRetMsg(retMessage);
        responseHead.getResponseSysHead().setResponseSysHeadRet(responseSysHeadRet);
        String xml = XmlUtil.converJavaBean2Xml(responseHead, responseHead.getClass());
        return xml;
    }
}
package com.dhcc.cips.service.other;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "user", path = "/")
public interface OrgClientService {
    /**
     * 机构管理-新增
     */
    @PostMapping(value = "/systemOrg/createList")
    String createList(@RequestBody String sysOrgListJson);
}

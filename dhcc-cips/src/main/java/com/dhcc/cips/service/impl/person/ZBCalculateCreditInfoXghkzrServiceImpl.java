package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.repayres.PcrpPayliainf;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.repayres.entity.RelReiLiaInfUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author StivenYang
 * @description 三（信贷交易信息明细）-七
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateCreditInfoXghkzrServiceImpl implements PersonZBCalculteThreadService {

    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();

        try {

            /*******************************指标声明start************************/
            //127. 最近2年内非正常担保金
            BigDecimal late2yearUnnmGuarAmt = new BigDecimal(0);
            //135. 近5年内对外担保贷款为非正常的金额
            BigDecimal late5yearUnnmGuarAmt = new BigDecimal(0);
            //174. 当前非正常对外贷款担保金额
            BigDecimal currBadExtguanLoanAmt = new BigDecimal(0);
            //207. 非正常贷款对外担保笔数
            int currBadExtguanLoanNum = 0;
            /*******************************指标声明end************************/

            if (cpqReportBo != null
                    && cpqReportBo.getRelReiLiaInfBo() != null
                    && cpqReportBo.getRelReiLiaInfBo().getRelReiLiaInfUnitBoList() != null) {
                //报告日期
                Date reportTime = cpqReportBo.getRepheadBo().getRepheadUnitBo().getPchrPchrreportinfo().getReportTime();
                //前2年
                Date aimYearDate2 = DateUtil.getAimYearDate(reportTime, -2);

                List<RelReiLiaInfUnitBo> relReiLiaInfUnitBoList = cpqReportBo.getRelReiLiaInfBo().getRelReiLiaInfUnitBoList();
                if (null != relReiLiaInfUnitBoList && relReiLiaInfUnitBoList.size() > 0) {
                    for (RelReiLiaInfUnitBo relReiLiaInfUnitBo : relReiLiaInfUnitBoList) {
                        /***************************账户公共变量开始**************************/

                        //相关还款责任信息
                        PcrpPayliainf pcrpPayliainf = relReiLiaInfUnitBo.getPayliainf();
                        //责任人类型
                        String reiresHumtype = null;
                        //五级分类
                        String fiveAssort = null;
                        //开立日期
                        Date openDate = null;
                        //还款责任金额
                        BigDecimal reiliaRelamount = null;
                        if (pcrpPayliainf != null) {
                            reiresHumtype = pcrpPayliainf.getReiresHumtype();
                            fiveAssort = pcrpPayliainf.getFiveAssort();
                            openDate = pcrpPayliainf.getOpenDate();
                            if (pcrpPayliainf.getReiliaRelamount() != null) {
                                reiliaRelamount = new BigDecimal(pcrpPayliainf.getReiliaRelamount());
                            }
                        }

                        /***************************账户公共变量结束**************************/

                        //127. 最近2年内非正常担保金
                        //相关还款责任信息（三--七--开立日期为最近2年且“五级分类”不为“正常”的账户的还款责任金额总值）
                        if (StringUtils.isNotEmpty(fiveAssort) && openDate != null) {
                            if (!Constants.FIF_TYPE_1.equals(fiveAssort)) {
                                if (reportTime.after(openDate)
                                        && openDate.after(aimYearDate2)) {
                                    if (reiliaRelamount != null) {
                                        late2yearUnnmGuarAmt = late2yearUnnmGuarAmt.add(reiliaRelamount);
                                    }
                                }
                            }
                        }

                        //135. 近5年内对外担保贷款为非正常的金额
                        if (StringUtils.isNotBlank(fiveAssort)
                                && StringUtils.isNotBlank(reiresHumtype)
                                && !Constants.FIF_TYPE_1.equals(fiveAssort)
                                && (Constants.RepayPerson_2).equals(reiresHumtype)) {
                            if (reiliaRelamount != null) {
                                late5yearUnnmGuarAmt = late5yearUnnmGuarAmt.add(reiliaRelamount);
                            }
                        }

                        //174. 当前非正常对外贷款担保金额
                        //五级分类不为“正常”的账户的还款责任金额总和
                        if (reiliaRelamount != null && StringUtils.isNotEmpty(fiveAssort)) {
                            if (!Constants.FIF_TYPE_1.equals(fiveAssort)) {
                                currBadExtguanLoanAmt = currBadExtguanLoanAmt.add(reiliaRelamount);
                            }
                        }

                        //207. 非正常贷款对外担保笔数
                        //五级分类不为“正常”的账户总数
                        if (StringUtils.isNotEmpty(fiveAssort) && !Constants.FIF_TYPE_1.equals(fiveAssort)) {
                            ++currBadExtguanLoanNum;
                        }
                    }
                }
            }

            //127. 最近2年内非正常担保金
            zxPZbInfo.setLate2yearUnnmGuarAmt(late2yearUnnmGuarAmt.setScale(2, BigDecimal.ROUND_UP).toString());
            //135. 近5年内对外担保贷款为非正常的金额
            zxPZbInfo.setLate5yearUnnmGuarAmt(late5yearUnnmGuarAmt.setScale(2, BigDecimal.ROUND_UP).toString());
            //174. 当前非正常对外贷款担保金额
            zxPZbInfo.setCurrBadExtguanLoanAmt(currBadExtguanLoanAmt.setScale(2, BigDecimal.ROUND_UP).toString());
            //207. 非正常贷款对外担保笔数
            zxPZbInfo.setCurrBadExtguanLoanNum(String.valueOf(currBadExtguanLoanNum));
        } catch (Exception e) {
            throw new Exception(e);
        }

        return zxPZbInfo;
    }
}

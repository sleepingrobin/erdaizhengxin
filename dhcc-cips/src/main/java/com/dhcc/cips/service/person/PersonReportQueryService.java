package com.dhcc.cips.service.person;

import com.dhcc.cips.bo.base.Request;

/**
 * @date 2019-5-29
 * @author limin
 * @description 个人征信报告查询服务类
 */
public interface PersonReportQueryService {

    /**
     * 个人征信指标查询，业务场景代码为：13003000007
     * @param  request
     * @return 返回xml字符串
     */
    String exec(Request request);

}
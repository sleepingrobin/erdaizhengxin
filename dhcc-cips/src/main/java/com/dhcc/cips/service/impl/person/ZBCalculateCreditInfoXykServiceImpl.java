package com.dhcc.cips.service.impl.person;

import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.CpqReportBo;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.basebo.creditaccountinform.*;
import cn.com.dhcc.creditquery.person.query.bo.reportanalysis.bo.creditaccountinform.entity.BorAccInfUnitBo;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.dhcc.cips.service.person.PersonZBCalculteThreadService;
import com.dhcc.cips.util.DateUtil;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * @author StivenYang
 * @description 三（信贷交易信息明细）-五、六
 * @date 2019年6月11日15:48:34
 */
@Service
public class ZBCalculateCreditInfoXykServiceImpl implements PersonZBCalculteThreadService {

    @Override
    public PersonZBInfoPo calculate(CpqReportBo cpqReportBo) throws Exception {
        PersonZBInfoPo zxPZbInfo = new PersonZBInfoPo();

        try {
            /*******************************指标声明start************************/
            //69. 未激活信用卡张数
            int wjhCcNum = 0;
            //70. 未激活信用卡金额
            BigDecimal wjhCcAmt = new BigDecimal(0);
            //71. 销户信用卡张数
            int xhCcNum = 0;
            //106. 最近1个月新发放信用卡
            int lateMonCardNum = 0;
            //109. 最近3个月新发放信用卡
            int lateSeaCardNum = 0;
            //112. 最近6个月新发放信用卡
            int lateHlyCardNum = 0;
            //154. 近2年信用卡最长逾期月数
            int late2yCardMaxOdNum = 0;
            //176. 信用卡使用张数
            int credCardUsedNum = 0;
            //184. 信用卡授信机构数（不含未激活）
            int credCardOrgnumOutWjq = 0;
            HashSet<Object> credCardOrgnumOutWjqSet = Sets.newHashSet();
            //209. 当前信用卡逾期期数
            int currUnsettCardOdNum = 0;
            //211. 当前信用卡最高逾期金额
            BigDecimal currUnsettCardMaxOdAmt = new BigDecimal(0);
            //215. 近2年信用卡最大连续逾期月份数
            String late2yCardMaxSerialOdNum = "0";
            //216. 近2年信用卡最大同月逾期卡张数
            int late2yCardMaxMonOdNum = 0;
            //219. 近两年信用卡逾期账户数
            int late2yCardOdAccNum = 0;
            //220. 近两年信用卡最高逾期金额
            BigDecimal late2yCardMaxOdAmt = new BigDecimal(0);
            /*******************************指标声明end************************/

            if (cpqReportBo != null && cpqReportBo.getBorAccInfBo() != null && cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList() != null) {
                //报告日期
                Date reportTime = cpqReportBo.getRepheadBo().getRepheadUnitBo().getPchrPchrreportinfo().getReportTime();
                //前1个月
                Date aimMonthDate1 = DateUtil.getAimMonthDate(reportTime, -1);
                //前3个月
                Date aimMonthDate3 = DateUtil.getAimMonthDate(reportTime, -3);
                //前6个月
                Date aimMonthDate6 = DateUtil.getAimMonthDate(reportTime, -6);

                HashMap<Date, Integer> late2yCardMaxMonOdNumMap = Maps.newHashMap();
                List<BorAccInfUnitBo> borAccInfUnitBoList = cpqReportBo.getBorAccInfBo().getBorAccInfUnitBoList();
                if (null != borAccInfUnitBoList && borAccInfUnitBoList.size() > 0) {
                    for (BorAccInfUnitBo borAccInfUnitBo : borAccInfUnitBoList) {
                        /***************************账户公共变量开始**************************/
                        //基本信息段
                        PcabBasicinfoseg pcabBasicinfoseg = borAccInfUnitBo.getPcabBasicinfoseg();
                        //开立日期
                        Date issuanceDate = null;
                        //账户类型
                        String accountType = null;
                        //组织代码
                        String orgCode = null;
                        //账户余额
                        BigDecimal accountQuota = null;
                        //币种
                        String currency = null;
                        if (null != pcabBasicinfoseg) {
                            issuanceDate = pcabBasicinfoseg.getIssuanceDate();
                            currency = pcabBasicinfoseg.getCurrency();
                            accountType = pcabBasicinfoseg.getAccountType();
                            orgCode = pcabBasicinfoseg.getOrgCode();

                            if (StringUtils.isNotEmpty(pcabBasicinfoseg.getAccountQuota())) {
                                accountQuota = new BigDecimal(pcabBasicinfoseg.getAccountQuota());
                            }
                        }
                        //最新表现字段
                        PcanFirinfseg pcanFirinfseg = borAccInfUnitBo.getPcanFirinfseg();
                        //最近两年还款状态
                        PcatTwentyfourpaymentstat pcatTwentyfourpaymentstat = borAccInfUnitBo.getPcatTwentyfourpaymentstat();
                        //最近两年支付列表
                        List<PcatTwentyfourpaymentstatsSub> twentyfourpaymentstatsSubList = Lists.newArrayList();
                        if (pcatTwentyfourpaymentstat != null) {
                            twentyfourpaymentstatsSubList = pcatTwentyfourpaymentstat.getSub();
                        }
                        //最近五年表现字段
                        PcapRecfivehisinfo pcapRecfivehisinfo = borAccInfUnitBo.getPcapRecfivehisinfo();
                        //最近五年偿还列表
                        List<PcapRecfivehisinfoSub> pcapRecfivehisinfoSubList = Lists.newArrayList();
                        if (pcapRecfivehisinfo != null) {
                            pcapRecfivehisinfoSubList = pcapRecfivehisinfo.getPcapRecfivehisinfoSubList();
                        }
                        //五级分类
                        String fiveAssort = null;
                        //账户状态
                        String accStatus = null;
                        //最新表现字段 余额
                        BigDecimal balance = null;
                        if (pcanFirinfseg != null) {
                            fiveAssort = pcanFirinfseg.getFiveAssort();
                            accStatus = pcanFirinfseg.getAccStatus();
                            if (StringUtils.isNotEmpty(pcanFirinfseg.getBalance())) {
                                balance = new BigDecimal(pcanFirinfseg.getBalance());
                            }
                        }
                        //最近一个月的账户信息
                        PcamMonthInformation pcamMonthInformation = borAccInfUnitBo.getPcamMonthInformation();
                        //当前逾期总额
                        BigDecimal overdueAmount = new BigDecimal(0);
                        //当前逾期期数
                        String overduePeriod = null;
                        //已用额度
                        BigDecimal quotaUsed = new BigDecimal(0);
                        //最近一月 余额
                        BigDecimal monBalance = new BigDecimal(0);
                        if (pcamMonthInformation != null) {
                            if (StringUtils.isNotBlank(pcamMonthInformation.getOverdueAmount())) {
                                overdueAmount = new BigDecimal(pcamMonthInformation.getOverdueAmount());
                            }
                            if (StringUtils.isNotBlank(pcamMonthInformation.getOverduePeriod())) {
                                overduePeriod = pcamMonthInformation.getOverduePeriod();
                            }
                            if (StringUtils.isNotBlank(pcamMonthInformation.getQuotaUsed())) {
                                quotaUsed = new BigDecimal(pcamMonthInformation.getQuotaUsed());
                            }
                            if (StringUtils.isNotBlank(pcamMonthInformation.getBalance())) {
                                monBalance = new BigDecimal(pcamMonthInformation.getBalance());
                            }
                        }
                        /***************************账户公共变量结束**************************/

                        if (Constants.ACC_TYPE_R2.equals(accountType)
                                || Constants.ACC_TYPE_R3.equals(accountType)) {

                            //106. 最近1个月新发放信用卡
                            //109. 最近3个月新发放信用卡
                            //112. 最近6个月新发放信用卡
                            //开立日期距离当前6个月内且币种为人民币的账户数
                            if (issuanceDate.before(reportTime)) {
                                if (StringUtils.isNotBlank(currency)
                                        && "CNY".equals(currency)) {
                                    if (aimMonthDate1.before(issuanceDate)) {
                                        //最近一个月新发放信用卡
                                        lateMonCardNum = lateMonCardNum + 1;
                                    }
                                    if (aimMonthDate3.before(issuanceDate)) {
                                        //最近三个月个月新发放信用卡
                                        lateSeaCardNum = lateSeaCardNum + 1;
                                    }
                                    if (aimMonthDate6.before(issuanceDate)) {
                                        //最近六个月个月新发放信用卡
                                        lateHlyCardNum = lateHlyCardNum + 1;
                                    }
                                }
                            }

                            //69. 未激活信用卡张数
                            //70. 未激活信用卡金额
                            if (Constants.ACC_STAT_6.equals(accStatus)) {
                                //未激活信用卡金额
                                wjhCcAmt = wjhCcAmt.add(accountQuota);
                                //未激活信用卡张数
                                wjhCcNum = wjhCcNum + 1;
                            }

                            //71. 销户信用卡张数
                            if (Constants.ACC_STAT_4.equals(accStatus)) {
                                xhCcNum = xhCcNum + 1;
                            }

                            //154. 近2年信用卡最长逾期月数
                            //所有账户24个月还款记录最大值
                            if (null != twentyfourpaymentstatsSubList) {
                                for (PcatTwentyfourpaymentstatsSub subList : twentyfourpaymentstatsSubList) {
                                    String paymentStats = subList.getPaymentStats();
                                    if (null != paymentStats) {
                                        if (Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                            if (late2yCardMaxOdNum < Integer.parseInt(subList.getPaymentStats())) {
                                                late2yCardMaxOdNum = Integer.parseInt(subList.getPaymentStats());
                                            }
                                        }
                                    }
                                }
                            }

                            //176. 信用卡使用张数
                            //已用额度大于0的账户总数 + 透支额度大于0的账户总数
                            if (null != quotaUsed&&Constants.ACC_TYPE_R2.equals(accountType)) {
                                if (quotaUsed.doubleValue() > 0) {
                                    ++credCardUsedNum;
                                }
                            }
                            if (null != balance&&Constants.ACC_TYPE_R3.equals(accountType)) {
                                if (monBalance.doubleValue() > 0) {
                                    ++credCardUsedNum;
                                }
                            }

                            //184. 信用卡授信机构数（不含未激活）
                            //账户状态不为“销户”和“未激活”的“管理机构”的去重总数
                            if (!Constants.ACC_STAT_4.equals(accStatus)
                                    && !Constants.ACC_STAT_6.equals(accStatus)) {
                                credCardOrgnumOutWjqSet.add(orgCode);
                            }
                            credCardOrgnumOutWjq = credCardOrgnumOutWjqSet.size();

                            //209. 当前信用卡逾期期数
                            //各账户“当前逾期期数”之和
                            //最近一个月度变现信息段余额
                            //最近一个月度变现信息段余额-最新表现字段余额大于等于0
                            if (overduePeriod != null) {
                                if (!Constants.ACC_STAT_1.equals(fiveAssort)
                                        && monBalance.compareTo(balance) > 0) {
                                    currUnsettCardOdNum += Integer.parseInt(overduePeriod);
                                }
                            }

                            //211. 当前信用卡最高逾期金额
                            //各账户“当前逾期总额”最大值
                            if (currUnsettCardMaxOdAmt.compareTo(overdueAmount) < 0) {
                                currUnsettCardMaxOdAmt = overdueAmount;
                            }

                            //215. 近2年信用卡最大连续逾期月份数
                            //各账户24月还款状态取最大值（有G取G）
                            if (null != twentyfourpaymentstatsSubList) {
                                for (PcatTwentyfourpaymentstatsSub pcatTwentyfourpaymentstatsSub : twentyfourpaymentstatsSubList) {
                                    String paymentStats = pcatTwentyfourpaymentstatsSub.getPaymentStats();
                                    if (Constants.OVERDUE3.contains(paymentStats)
                                            && late2yCardMaxSerialOdNum.compareTo(paymentStats)<0) {
                                        late2yCardMaxSerialOdNum = paymentStats;
                                    }
                                }
                            }

                            //216. 近2年信用卡最大同月逾期卡张数

                            //24月还款状态中，获取各账户在同一个月出现逾期的账户数，再获取24个数中的最大值
                            if (null != twentyfourpaymentstatsSubList) {
                                for (PcatTwentyfourpaymentstatsSub pcatTwentyfourpaymentstatsSub : twentyfourpaymentstatsSubList) {
                                    if (pcatTwentyfourpaymentstatsSub != null) {
                                        Date month = pcatTwentyfourpaymentstatsSub.getMonth();
                                        String paymentStats = pcatTwentyfourpaymentstatsSub.getPaymentStats();
                                        if (late2yCardMaxMonOdNumMap.get(month) == null
                                                && Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                            late2yCardMaxMonOdNumMap.put(month, 1);
                                        } else if (late2yCardMaxMonOdNumMap.get(month) != null
                                                && Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                            late2yCardMaxMonOdNumMap.put(month, late2yCardMaxMonOdNumMap.get(month) + 1);
                                        }
                                    }
                                }
                            }
                            for (Date date : late2yCardMaxMonOdNumMap.keySet()) {
                                if (late2yCardMaxMonOdNum < late2yCardMaxMonOdNumMap.get(date)) {
                                    late2yCardMaxMonOdNum = late2yCardMaxMonOdNumMap.get(date);
                                }
                            }

                            //219. 近两年信用卡逾期账户数
                            if (twentyfourpaymentstatsSubList != null) {
                                for (PcatTwentyfourpaymentstatsSub pcatTwentyfourpaymentstatsSub : twentyfourpaymentstatsSubList) {
                                    String paymentStats = pcatTwentyfourpaymentstatsSub.getPaymentStats();
                                    if (Constants.OVERDUE_STATUS.contains(paymentStats)) {
                                        ++late2yCardOdAccNum;
                                        break;
                                    }
                                }
                            }

                            //220. 近两年信用卡最高逾期金额
                            //24个月还款状态取第二行最大值
                            if (pcapRecfivehisinfoSubList != null) {
                                int length = pcapRecfivehisinfoSubList.size();
                                length = length > 24 ? 24 : length;
                                for (int i = 0; i < length; i++) {
                                    //24个月
                                    PcapRecfivehisinfoSub pcapRecfivehisinfoSub = pcapRecfivehisinfoSubList.get(i);
                                    if (StringUtils.isNotBlank(pcapRecfivehisinfoSub.getOverdueMoney())) {
                                        BigDecimal overdueMoney = new BigDecimal(pcapRecfivehisinfoSub.getOverdueMoney().replaceAll(",", ""));
                                        if (late2yCardMaxOdAmt.compareTo(overdueMoney) < 0) {
                                            late2yCardMaxOdAmt = overdueMoney;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }

            DecimalFormat formatTwo = new DecimalFormat("0.00");
            formatTwo.setRoundingMode(RoundingMode.HALF_UP);

            //69. 未激活信用卡张数
            zxPZbInfo.setWjhCcNum(String.valueOf(wjhCcNum));
            //70. 未激活信用卡金额
            zxPZbInfo.setWjhCcAmt(formatTwo.format(wjhCcAmt));
            //71. 销户信用卡张数
            zxPZbInfo.setXhCcNum(String.valueOf(xhCcNum));
            //106. 最近1个月新发放信用卡
            zxPZbInfo.setLateMonCardNum(String.valueOf(lateMonCardNum));
            //109. 最近3个月新发放信用卡
            zxPZbInfo.setLateSeaCardNum(String.valueOf(lateSeaCardNum));
            //112. 最近6个月新发放信用卡
            zxPZbInfo.setLateHlyCardNum(String.valueOf(lateHlyCardNum));
            //154. 近2年信用卡最长逾期月数
            zxPZbInfo.setLate2yCardMaxOdNum(String.valueOf(late2yCardMaxOdNum));
            //176. 信用卡使用张数
            zxPZbInfo.setCredCardUsedNum(String.valueOf(credCardUsedNum));
            //184. 信用卡授信机构数（不含未激活）
            zxPZbInfo.setCredCardOrgnumOutWjq(String.valueOf(credCardOrgnumOutWjq));
            //209. 当前信用卡逾期期数
            zxPZbInfo.setCurrUnsettCardOdNum(String.valueOf(currUnsettCardOdNum));
            //211. 当前信用卡最高逾期金额
            zxPZbInfo.setCurrUnsettCardMaxOdAmt(formatTwo.format(currUnsettCardMaxOdAmt));
            //215. 近2年信用卡最大连续逾期月份数
            zxPZbInfo.setLate2yCardMaxSerialOdNum(String.valueOf(late2yCardMaxSerialOdNum));
            //216. 近2年信用卡最大同月逾期卡张数
            zxPZbInfo.setLate2yCardMaxMonOdNum(String.valueOf(late2yCardMaxMonOdNum));
            //219. 近两年信用卡逾期账户数
            zxPZbInfo.setLate2yCardOdAccNum(String.valueOf(late2yCardOdAccNum));
            //220. 近两年信用卡最高逾期金额
            zxPZbInfo.setLate2yCardMaxOdAmt(formatTwo.format(late2yCardMaxOdAmt));
        } catch (Exception e) {
            throw new Exception(e);
        }
        return zxPZbInfo;
    }
}

package com.dhcc.cips.bo.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("SERVICE")
public class Request extends RequestHead implements Serializable  {

    @XStreamAlias("BODY")
    private RequestBody requestBody;
}

package com.dhcc.cips.bo.other;

import com.dhcc.cips.bo.base.RequestAppHead;
import com.dhcc.cips.bo.base.RequestHead;
import com.dhcc.cips.bo.base.RequestLocalHead;
import com.dhcc.cips.bo.base.RequestSysHead;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * @author StivenYang
 * @description 短信接口请求实体类
 * @date 2019-4-26
 */
@Data
@XStreamAlias("SERVICE")
public class MsgReqBo extends RequestHead implements Serializable {

    public MsgReqBo(){
        this.setRequestSysHead(new RequestSysHead());
        this.setRequestAppHead(new RequestAppHead());
        this.setRequestLocalHead(new RequestLocalHead());
        this.setMsgReqBody(new MsgReqBody());
    }

    @XStreamAlias("BODY")
    private MsgReqBody msgReqBody;
}

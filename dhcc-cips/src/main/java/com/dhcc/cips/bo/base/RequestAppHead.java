package com.dhcc.cips.bo.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * @author haokai
 * @date 2019年3月29日
 * @description 公共app头
 */
@Data
public class RequestAppHead implements Serializable {

    /**
     * 业务流水号
     */
    @XStreamAlias("BUSS_SEQ_NO")
    private String bussSeqNo;

}

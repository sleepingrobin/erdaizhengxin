package com.dhcc.cips.bo.other;

import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

/**
 * <p>
 * 征信报告文件下载返回实体body类
 * </p>
 *
 * @author limin
 * @since 2019-05-30
 */
@Data
public class ReportFileDownloadResBodyBo {

    /**
     * 征信报告文件路径查询
     */
    @XStreamImplicit(itemFieldName="FILE_PATH")
    private String reportFilePath;

}

package com.dhcc.cips.bo.other;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
@XStreamAlias("SingleResult")
public class QueryResBo implements Serializable {

    @XStreamAlias("reqID")
    private String reqID;
    @XStreamAlias("resCode")
    private String resCode;
    @XStreamAlias("resMsg")
    private String resMsg;
    @XStreamAlias("xmlStr")
    private String xmlStr;
    @XStreamAlias("htmlStr")
    private String htmlStr;
    @XStreamAlias("reportSource")
    private String reportSource;
    @XStreamAlias("reportVersion")
    private String reportVersion;
    @XStreamAlias("creditreportNo")
    private String creditreportNo;
    @XStreamAlias("xmlMd5")
    private String xmlMd5;
    @XStreamAlias("htmlMd5")
    private String htmlMd5;
    @XStreamAlias("useTime")
    private String useTime;
}

package com.dhcc.cips.bo.company;

import lombok.Data;

/**
 * 企业预警涉及指标bo
 */
@Data
public class CompanyAlarmZBBo {

    /**
     * 贷款笔数
     */
    private Integer credtLoanCount;

    /**
     * 未结清关注类业务笔数
     */
    private Integer attentionCount;

    /**
     * 信用业务笔数（未结清信贷业务）
     */
    private Integer credBizNum;

    /**
     * 未结清不良信用业务笔数
     */
    private Integer unSettBadClBizNum;

    /**
     * 已结清不良信用业务笔数
     */
    private Integer settBadBizNum;


    /**
     * 未结清不良信贷信用总余额
     */
    private double unSettBadClTotalBalan;
}

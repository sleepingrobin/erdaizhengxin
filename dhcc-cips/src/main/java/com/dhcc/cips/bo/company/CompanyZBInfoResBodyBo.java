package com.dhcc.cips.bo.company;

import com.dhcc.cips.po.company.CompanyZBInfoPo;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 企业指标 查询字段返回BO
 * </p>
 *
 * @author limin
 * @since 2019-05-30
 */
@Data
public class CompanyZBInfoResBodyBo {

    /**
     * 企业指标信息对象
     */
    private CompanyZBInfoPo companyZBInfoPo;

    /**
     * 贷款信息数组
     */
    @XStreamImplicit(itemFieldName="LOAN_INFO_ARRAY")
    private List<CompanyLoanInfoBo> companyLoanInfoBoList;

    /**
     * 担保合同信息数组
     */
    @XStreamImplicit(itemFieldName="GRNY_CTR_ARRAY")
    private List<CompanyExtGuarantyInfoBo> companyExtGuarantyInfoBoList;

}

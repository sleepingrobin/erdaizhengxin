package com.dhcc.cips.bo.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author haokai
 * @date 2019年3月29日
 * @description 报告体
 */
@Data
public class RequestBody implements Serializable {
    /**
     * 客户名称
     */
    @XStreamAlias(value = "CLIENT_NAME")
    private String clientName;

    /**
     * 服务代码
     */
    @XStreamAlias(value = "SERVICE_CODE")
    private String serviceCode;

    /**
     * 服务应用场景
     */
    @XStreamAlias(value = "SERVICE_SCENE")
    private String serviceScene;

    /**
     * 证件类型
     */
    @XStreamAlias(value = "GLOBAL_TYPE")
    private String globalType;

    /**
     * 证件号码
     */
    @XStreamAlias(value = "GLOBAL_ID")
    private String globalId;

    /**
     * 原因代码(事由代码)
     */
    @XStreamAlias(value = "REASON_CODE")
    private String reasonCode;
    /**
     * 贷款卡号
     */
    @XStreamAlias(value = "LOAN_CARD_NO")
    private String loanCardNo;

    /**
     * 密码
     */
    @XStreamAlias(value = "PASSWORD")
    private String loancardPassword;
    /**
     * 客户经理机构名称
     */
    @XStreamAlias(value = "MGR_ORG_NAME")
    private String mgrOrgName;

    /**
     * 客户经理姓名
     */
    @XStreamAlias(value = "CLNT_MGR_NM")
    private String clntMgrNm;

    /**
     * 签署方式  01 电子  02 纸质   03 未签署
     */
    @XStreamAlias(value = "SIGN_MD")
    private String signMd;

    /**
     * 审核人工号
     */
    @XStreamAlias(value = "CHCK_TLR")
    private String chckTlr;

    /**
     * 审核人名称
     */
    @XStreamAlias(value = "TELLER_ROLE_NAME")
    private String tellerRoleName;

    /**
     * 审核时间
     */
    @XStreamAlias(value = "OPRT_TM")
    private String oprtTm;

    /**
     * 信贷客户号
     */
    @XStreamAlias(value = "LOAN_CLIENT_NO")
    private String loanClientNo;

    /**
     * 客户经理编号
     */
    @XStreamAlias(value = "CLNT_MGR_NO")
    private String clntMgrNo;

    /**
     * 机构名称
     */
    @XStreamAlias(value = "BRANCH_NAME")
    private String branchName;

    /**
     * 员工号
     */
    @XStreamAlias(value = "EMPLOYEE_ID")
    private String employeeId;


    /**
     * 征信报告(html版)字符串（base64编码）
     */
    @XStreamAlias(value = "HTML_STR")
    private String htmlStr;

    /**
     * 查询时间
     */
    @XStreamAlias(value = "QUERY_DATE")
    private String queryDate;

    /**
     * 预评估流水号
     */
    @XStreamAlias(value = "PRE_EVAL_SEQ_NO")
    private String preEvalSeqNo;
}

package com.dhcc.cips.bo.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * 交易返回信息
 */
@Data
public class ResponseSysHeadRet implements Serializable {

    /**
     * 返回码
     */
    @XStreamAlias("RET_MSG")
    private String retMsg;

    /**
     * 返回信息
     */
    @XStreamAlias("RET_CODE")
    private String retCode;
}

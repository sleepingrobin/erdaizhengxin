package com.dhcc.cips.bo.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author Yang Jiaheng
 * @date 2019年4月8日
 * @description 响应头SysHead
 */
@Data
public class ResponseSysHead implements Serializable {
    /**
     * 响应码实体
     */
    @XStreamAlias("RET")
    private ResponseSysHeadRet responseSysHeadRet;

    /**
     * 服务代码
     */
    @NotNull(message = "服务代码不可为空")
    @XStreamAlias(value ="SERVICE_CODE")
    private String serviceCode;

    /**
     * 服务应用场景
     */
    @NotNull(message = "服务应用场景字段不可为空")
    @XStreamAlias(value ="SERVICE_SCENE")
    private String serviceScene;

    /**
     * 请求系统ID
     */
    @NotNull(message = "请求系统ID不可为空")
    @XStreamAlias(value ="CONSUMER_ID")
    private String consumerId;

    /**
     * 提供系统ID
     */
    @NotNull(message = "提供系统ID不可为空")
    @XStreamAlias(value ="PROVIDER_ID")
    private String providerId;

    /**
     * 交易日期
     */
    @NotNull(message = "交易日期不可为空")
    @XStreamAlias(value ="TRAN_DATE")
    private String tranDate;

    /**
     * 交易时间
     */
    @NotNull(message = "交易时间不可为空")
    @XStreamAlias(value ="TRAN_TIMESTAMP")
    private String tranTimestamp;

    /**
     * 请求系统流水号
     */
    @NotNull(message = "请求系统流水号不可为空")
    @XStreamAlias(value ="CONSUMER_SEQ_NO")
    private String consumerSeqNo;

    /**
     * 文件路径
     */
    @XStreamAlias(value ="FILE_PATH")
    private String filePath;

    /**
     * 返回状态码
     */
    @XStreamAlias("RET_STATUS")
    private String retStatus;

    /**
     * 服务处理返回流水号
     */
    @XStreamAlias("SERV_SEQ_NO")
    private String ServSeqNo;

    /**
     * ESB产生的流水号
     */
    @XStreamAlias("ESB_SEQ_NO")
    private String esbSeqNo;
}

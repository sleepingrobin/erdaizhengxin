package com.dhcc.cips.bo.other;

import lombok.Data;

/**
 * 查询请求参数bo
 */
@Data
public class QueryReqBo {
    private String clientName;//客户名称
    private String globalType;//证件类型
    private String globalId;//证件号码
    private String reasonCode;//查询原因
    private String reportType;//信用报告格式   H:Html格式  X:Xml格式  HX:Html格式和Xml格式
    private String queryType;//信用报告复用策略  负数:查询本地  0:征信中心  正数:征信报告有效期
    private String syncFlag;//同步异步返回标识 0:同步 1:异步
    private String reportVersion;//信用报告版本  2.0.0:二代信用报告
    private String businessLine;//业务线
    private String userId;//查询用户工号
    private String sysCode;//系统标识  区分接口调用子系统
    //实际触发本次产品查询请求和使用查询结果的用户，是发起机构自身业务系统中的用户，由发起机构自行分配管理
    private String queryAccount;
}

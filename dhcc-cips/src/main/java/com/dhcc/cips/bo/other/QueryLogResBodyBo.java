package com.dhcc.cips.bo.other;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author limin
 * @Description 返回体
 * @Date 2019-5-30
 */
@Data
public class QueryLogResBodyBo {

    @XStreamAlias("CUST_MANAGER_ID")
    private String userId;              //客户经理工号

    @XStreamAlias("CUST_MANAGER_NAME")
    private String userName;            //客户经理名称

    @XStreamAlias("MGR_BRANCH_ID")
    private String orgId;               //客户经理机构号

    @XStreamAlias("MGR_ORG_NAME")
    private String orgName;             //客户经理机构名称

    @XStreamAlias("PASSWORD")
    private String account;            //征信中心登录用户id

    @XStreamAlias("QUERY_DATE")
    private Date queryDate;             //查询日期

    @XStreamAlias("REASON_MSG")
    private String queryReson;          //查询原因

}

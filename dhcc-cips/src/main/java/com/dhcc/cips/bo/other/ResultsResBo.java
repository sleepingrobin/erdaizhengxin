package com.dhcc.cips.bo.other;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.util.List;

/**
 * @author StivenYang
 * @date 2019年5月17日10:18:37
 * @description 返回结果代码类
 */
@Data
@XStreamAlias("results")
public class ResultsResBo {

    @XStreamImplicit
    private List<OrgResBo> orgResultInfoList;

    @Override
    public String toString() {
        return "OrgResult{" +
                "orgResultInfoList=" + orgResultInfoList +
                '}';
    }
}

package com.dhcc.cips.bo.person;

import com.dhcc.cips.po.person.PersonZBInfoPo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.util.List;

/**
 * <p>
 * 个人指标 查询字段返回实体类
 * </p>
 *
 * @author Yang Jiaheng
 * @since 2019-03-28
 */
@Data
public class PersonZBInfoResBodyBo {

    private static final long serialVersionUID = 1L;


    /**
     * 个人指标信息
     */
    private PersonZBInfoPo personZBInfoPo;


    /**
     * 征信评分
     */
    @XStreamAlias("CUST_CREDIT_LEVEL")
    private String custCreditLevel;


    /**
     * 未来1-50月需还贷款金额
     */
    @XStreamAlias("AMT_STRING")
    private String amtString;

    /**
     * 近一个月本人自主查询次数
     */
    @XStreamAlias("LAST_MON_QUERY_TMS")
    private String lastMonQueryTms;

    /**
     * 最近2年内担保资格审查查询次数
     */
    @XStreamAlias("LTY_WRNT_QUALF_QUERY_TMS")
    private String ltyWrntQualfQueryTms;

    /**
     * 性别
     */
    @XStreamAlias("GENDER")
    private String gender;

    /**
     * 最高学历
     */
    @XStreamAlias("EDUCATION")
    private String education;

    /**
     * 贷款信息数组
     */
    @XStreamImplicit(itemFieldName="LOAN_INFO_ARRAY")
    private List<PersonLoanInfoBo> personLoanInfoBoList;

    /**
     * 对外担保信息数组
     */
    @XStreamImplicit(itemFieldName="GRNY_CTR_ARRAY")
    private List<PersonExtGuarantyInfoBo> personExtGuarantyInfoBoList;

}

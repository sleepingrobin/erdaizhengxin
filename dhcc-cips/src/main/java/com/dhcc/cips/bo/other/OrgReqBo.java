package com.dhcc.cips.bo.other;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Date;

/**
 * @author StivenYang
 * @date 2019年5月17日09:27:32
 * @description 组织机构实体。使用该实体接收业务系统传递的xml参数
 */
@Data
@XStreamAlias("org")
public class OrgReqBo {

    /**
     * 维度编号
     */
    @XStreamAlias("dimCode")
    private String dimCode;
    /**
     * 维度名称
     */
    @XStreamAlias("dimName")
    private String dimName;
    /**
     * 组织机构代码
     */
    @XStreamAlias("orgCode")
    private String orgCode;
    /**
     * 组织机构名称
     */
    @XStreamAlias("orgName")
    private String orgName;
    /**
     * 上级组织机构代码
     */
    @XStreamAlias("parentCode")
    private String parentCode;
    /**
     * 职能属性
     */
    @XStreamAlias("property")
    private String property;
    /**
     * 该组织机构所属的大类
     */
    @XStreamAlias("mainCategory")
    private String mainCategory;
    /**
     * 所属总分行/村行
     */
    @XStreamAlias("xzLevel")
    private String xzLevel;
    /**
     * 所属总分行/村行名称
     */
    @XStreamAlias("xzLevelCode")
    private String xzLevelCode;
    /**
     * 支行类型
     */
    @XStreamAlias("branchType")
    private String branchType;
    /**
     * 组织机构成立日期
     */
    @XStreamAlias("beginDate")
    private String beginDate;
    /**
     * 组织机构生效日期
     */
    @XStreamAlias("startTime")
    private Date startTime;
    /**
     * 组织机构失效日期
     */
    @XStreamAlias("endTime")
    private Date endTime;
    /**
     * 是否虚拟机构
     */
    @XStreamAlias("isVirtual")
    private String isVirtual;
    /**
     * 组织机构层级编码
     */
    @XStreamAlias("levelCode")
    private String levelCode;
    /**
     * 机构层级名称
     */
    @XStreamAlias("level")
    private String level;
    /**
     * 所属条线
     */
    @XStreamAlias("txType")
    private String txType;

    @XStreamAlias("orgClass")
    private String orgClass;
    /**
     * 机构分类
     */
    @XStreamAlias("class")
    private String clazz;
    /**
     * 营业状态
     */
    @XStreamAlias("busiStatus")
    private String busiStatus;
    /**
     * 营业时间
     */
    @XStreamAlias("ysTime")
    private String ysTime;
    /**
     * 人行金融机构代码
     */
    @XStreamAlias("bankID")
    private String bankID;
    /**
     * 组织机构法人代表
     */
    @XStreamAlias("legalPerson")
    private String legalPerson;
    /**
     * 组织机构的邮政编码
     */
    @XStreamAlias("postalCode")
    private String postalCode;
    /**
     * 组织机构的联系电话
     */
    @XStreamAlias("telephonenumber")
    private String telephonenumber;
    /**
     * 组织机构所在地
     */
    @XStreamAlias("address")
    private String address;
    /**
     * 组织机构的机构地址
     */
    @XStreamAlias("postalAddress")
    private String postalAddress;
    /**
     * 组织机构的传真
     */
    @XStreamAlias("fax")
    private String fax;
    /**
     * 组织机构描述
     */
    @XStreamAlias("description")
    private String description;
    /**
     * 部门负责人
     */
    @XStreamAlias("depLeader")
    private String depLeader;
    /**
     * 部门负责人姓名
     */
    @XStreamAlias("depLeaderName")
    private String depLeaderName;
    /**
     * 部门负责人电话
     */
    @XStreamAlias("depLeaderphone")
    private String depLeaderphone;
    /**
     * 该组织机构的在兄弟机构中的顺序
     */
    @XStreamAlias("displayOrder")
    private String displayOrder;
    /**
     * 最末端机构标志
     */
    @XStreamAlias("isLeaf")
    private String isLeaf;
    /**
     * 状态
     */
    @XStreamAlias("status")
    private String status;
}

package com.dhcc.cips.bo.person;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * @author limin
 * @date 2019年5月28日
 * @description 个人基本信息返回BO
 */
@Data
public class PersonBaseInfoBo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 性别
     */
    @XStreamAlias("GENDER")
    private String gender;

    /**
     * 最高学历
     */
    @XStreamAlias("EDUCATION")
    private String education;


}

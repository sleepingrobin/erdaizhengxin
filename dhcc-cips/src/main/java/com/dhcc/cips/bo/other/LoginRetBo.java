package com.dhcc.cips.bo.other;

import com.dhcc.cips.bo.base.RequestHead;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Yang Jiaheng
 * @date 2019年4月8日
 * @description 个人征信系统登录查询返回BO
 */
@Data
@XStreamAlias("SERVICE")
public class LoginRetBo extends RequestHead implements Serializable {

}

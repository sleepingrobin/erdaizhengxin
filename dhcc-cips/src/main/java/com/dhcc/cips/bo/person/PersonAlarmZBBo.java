package com.dhcc.cips.bo.person;

import lombok.Data;

/**
 * 个人预警设计到的指标bo
 */
@Data
public class PersonAlarmZBBo {

    /**
     * 与银行建立信贷关系月数
     */
    private Integer relaBankMonNum;

    /**
     * 现有信用卡授信银行数
     */
    private Integer currCredBankNum;

    /**
     * 现有贷款授信银行数
     */
    private Integer currLoanBankNum;

    /**
     * 有信用业务的银行数
     */
    private Integer credBizBankNum;

    /**
     * 最近一个月内的查询次数
     */
    private Integer lateMonEnquCount;

    /**
     * 最近1个月内的查询次数/最近1年内的查询次数（比率）
     */
    private double lateMonYearEnquRate;


    /**
     * 信用产品数和查询记录数比率
     */
    private double clProdEnquRecoRate;


    /**
     * 过去两年内逾期三期以上（包括三期）信用卡张数
     */
    private Integer l2YOd2TimeCredNum;

    /**
     * 贷记卡12月内未还最低还款额次数
     */
    private  Integer late12MonMinRpNum;

    /**
     * 当前信用卡额度使用率
     */
    private double cCredCardUseFrequ;

    /**
     * 信用卡使用率
     */
    private double credCardUseFrequ;

    /**
     * 过去两年内经营性贷款逾期两期以上（包括两期）贷款笔数
     */
    private Integer l2YJyxLoanOd2TimeNum;

    /**
     * 将来2个月未结清经营性贷款到期笔数
     */
    private Integer f2MUnSettJyxLoanNum;

    /**
     * 当前逾期账户数
     */
    private Integer cOdAcctNum;

}

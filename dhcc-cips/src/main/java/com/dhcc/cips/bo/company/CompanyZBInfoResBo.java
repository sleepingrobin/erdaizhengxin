package com.dhcc.cips.bo.company;

import com.dhcc.cips.bo.base.ResponseHead;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 企业指标 查询字段返回实体类
 * </p>
 *
 * @author limin
 * @since 2019-05-30
 */
@Data
@XStreamAlias("SERVICE")
public class CompanyZBInfoResBo extends ResponseHead implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 返回体
     */
    @XStreamAlias("BODY")
    private CompanyZBInfoResBodyBo companyZBInfoResBodyBo;

}

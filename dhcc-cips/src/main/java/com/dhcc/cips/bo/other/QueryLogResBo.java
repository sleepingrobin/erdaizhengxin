package com.dhcc.cips.bo.other;

import com.dhcc.cips.bo.base.ResponseHead;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * @author zcg
 * @ClassName PQueryReturnParam
 * @Description TODO
 * @Date 2019-4-2 9:13
 */
@Data
@XStreamAlias("SERVICE")
public class QueryLogResBo extends ResponseHead implements Serializable {

    @XStreamAlias("BODY")
    private QueryLogResBodyBo queryLogResBodyBo;
}

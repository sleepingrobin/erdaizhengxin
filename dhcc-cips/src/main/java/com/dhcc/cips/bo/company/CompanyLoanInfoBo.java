package com.dhcc.cips.bo.company;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * @author YangJiaheng
 * @date 2019年3月29日
 * @description 企业贷款信息数组返回BO
 */
@Data
public class CompanyLoanInfoBo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 银行名称
     */
    @XStreamAlias("BANK_NAME")
    private String bankName;

    /**
     * 贷款金额
     */
    @XStreamAlias("LOAN_AMT")
    private String loanAmt;

    /**
     * 贷款余额
     */
    @XStreamAlias("LOAN_BALANCE")
    private String loanBalance;

    /**
     * 贷款发放日期
     */
    @XStreamAlias("LOAN_DATE_GRANT")
    private String loanDateGrant;

    /**
     * 贷款到期日期
     */
    @XStreamAlias("LOAN_DATE_MATURE")
    private String loanDateMature;

    /**
     * 查询日期
     */
    @XStreamAlias("QUERY_DATE")
    private String queryDate;

    /**
     * 信贷业务种类数
     */
    @XStreamAlias("LOAN_BUSS_VRTY_CNT")
    private String loanBussVrtyCnt;

    /**
     * 币种
     */
    @XStreamAlias("CCY")
    private String ccy;
}

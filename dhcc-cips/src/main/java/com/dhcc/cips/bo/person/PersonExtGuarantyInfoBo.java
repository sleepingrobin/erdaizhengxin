package com.dhcc.cips.bo.person;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * @author YangJiaheng
 * @date 2019年3月29日
 * @description 个人担保合同信息数组
 */
@Data
public class PersonExtGuarantyInfoBo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 授信机构名称
     */
    @XStreamAlias("CRDT_ORG_NM")
    private String crdtOrgNm;

    /**
     * 担保总金额
     */
    @XStreamAlias("WRNT_TOT_AMT")
    private String wrntTotAmt;

    /**
     * 担保余额
     */
    @XStreamAlias("WRNT_BAL")
    private String wrntBal;

    /**
     * 担保到期日
     */
    @XStreamAlias("WRNT_END_DT")
    private String wrntEndDt;
}

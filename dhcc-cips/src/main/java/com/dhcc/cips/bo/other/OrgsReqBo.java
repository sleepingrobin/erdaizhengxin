package com.dhcc.cips.bo.other;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import lombok.Data;

import java.util.List;

@Data
@XStreamAlias("orgs")
public class OrgsReqBo {

    @XStreamImplicit
    private List<OrgReqBo> orgReqBoList;

}

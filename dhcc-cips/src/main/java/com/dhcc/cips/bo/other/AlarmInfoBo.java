package com.dhcc.cips.bo.other;

import com.dhcc.cips.po.other.AlarmConfigInfoPo;
import com.dhcc.cips.po.other.AlarmItemInfoPo;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author limin
 * @description 预警信息Bo
 * @date 2019-6-4
 */
@Data
public class AlarmInfoBo implements Serializable {

    /**
     * 预警信息
     */
    private AlarmConfigInfoPo alarmConfigInfoPo;
    /**
     * 预警信息对应的预警规则集
     */
    private List<AlarmItemInfoPo> alarmItemInfoPoList = new ArrayList<>();
}

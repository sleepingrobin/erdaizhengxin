package com.dhcc.cips.bo.other;

import lombok.Data;

import java.util.Date;

/**
 * 对外合作bo
 */
@Data
public class CooperationReqBo {

    String customerName;//客户名称
    String globalType;//证件类型
    String globalId;//证件号码
    String userId;//员工号
    String userName;//员工名称
    String orgCode;//机构号
    String orgName;//机构名称
    String htmlStr;//征信报告html字符串
    String xmlStr;//征信报告html字符串
    Date queryDate;//查询时间
    String sourceType;//渠道
    String PreEvalSeqNo;//预评估流水号
    String BussSeqNo;//业务流水号
    String corpCode;//法人机构号

}

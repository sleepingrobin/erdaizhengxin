package com.dhcc.cips.bo.person;

import com.dhcc.cips.bo.base.ResponseHead;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 个人指标 查询字段返回实体类
 * </p>
 *
 * @author Yang Jiaheng
 * @since 2019-03-28
 */
@Data
@XStreamAlias("SERVICE")
public class PersonZBInfoResBo extends ResponseHead implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 返回体
     */
    @XStreamAlias("BODY")
    private PersonZBInfoResBodyBo personZBInfoResBodyBo;

}

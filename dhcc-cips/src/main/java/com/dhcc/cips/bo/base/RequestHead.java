package com.dhcc.cips.bo.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * @author haokai
 * @date 2019年3月29日
 * @description 公共请求参数头
 */
@Data
public class RequestHead implements Serializable {
    /**
     * 公共头
     */
    @XStreamAlias("SYS_HEAD")
    private RequestSysHead requestSysHead;

    /**
     * APP头
     */
    @XStreamAlias("APP_HEAD")
    private RequestAppHead requestAppHead;

    /**
     * 本地头
     */
    @XStreamAlias("LOCAL_HEAD")
    private RequestLocalHead requestLocalHead;

}

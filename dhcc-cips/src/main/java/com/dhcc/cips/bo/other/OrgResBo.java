package com.dhcc.cips.bo.other;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 组织机构代码返回信息
 * @author StivenYang
 * @date 2019年5月21日16:23:15
 * @description 返回信息
 */
@Data
@XStreamAlias("org")
public class OrgResBo {

    /**
     * 组织机构代码
     */
    @XStreamAlias("orgCode")
    private String orgCode;
    /**
     * dimCode
     */
    @XStreamAlias("dimCode")
    private String dimCode;
    /**
     * 返回状态码
     */
    @XStreamAlias("code")
    private String code;
    /**
     * 错误消息提示
     */
    @XStreamAlias("msg")
    private String msg;
}

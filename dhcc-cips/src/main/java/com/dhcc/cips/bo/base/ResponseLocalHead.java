package com.dhcc.cips.bo.base;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Yang Jiaheng
 * @date 2019年4月8日
 * @description 响应 本地头
 */
@Data
public class ResponseLocalHead implements Serializable {
}

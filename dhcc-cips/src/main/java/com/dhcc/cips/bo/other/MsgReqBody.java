package com.dhcc.cips.bo.other;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

@Data
public class MsgReqBody implements Serializable {

    /**
     * 账号
     */
    @XStreamAlias("ACCT_NO")
    private String acctNo;

    /**
     * 服务ID
     */
    @XStreamAlias("SERVICE_ID")
    private String serviceId;

    /**
     * 电话号码
     */
    @XStreamAlias("PHONE_NO")
    private String phoneNo;

    /**
     * 备注
     */
    @XStreamAlias("REMARK")
    private String remark;

    /**
     * 短信内容
     */
    @XStreamAlias("MSG_CONTEXT")
    private String msgContext;
}

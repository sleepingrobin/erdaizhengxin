package com.dhcc.cips.bo.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Yang Jiaheng
 * @date 2019年4月8日
 * @description 响应参头
 */
@Data
public class ResponseHead implements Serializable {
    /**
     * 公共头
     */
    @XStreamAlias("SYS_HEAD")
    private ResponseSysHead responseSysHead;

    /**
     * APP头
     */
    @XStreamAlias("APP_HEAD")
    private ResponseAppHead responseAppHead;

    /**
     * 本地头
     */
    @XStreamAlias("LOCAL_HEAD")
    private ResponseLocalHead parentLocalHead;

}

package com.dhcc.cips.bo.base;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author haokai
 * @date 2019年3月29日
 * @description 公共系统头
 */
@Data
public class RequestSysHead implements Serializable {


    /**
     * 服务代码
     */
    @NotNull(message = "服务代码不可为空")
    @XStreamAlias(value ="SERVICE_CODE")
    private String serviceCode;

    /**
     * 服务应用场景
     */
    @NotNull(message = "服务应用场景不可为空")
    @XStreamAlias(value ="SERVICE_SCENE")
    private String serviceScene;

    /**
     * 请求系统ID
     */
    @NotNull(message = "请求系统ID不可为空")
    @XStreamAlias(value ="CONSUMER_ID")
    private String consumerId;

    /**
     * 提供系统ID
     */
    @NotNull(message = "提供系统ID不可为空")
    @XStreamAlias(value ="PROVIDER_ID")
    private String providerId;

    /**
     * 渠道类型
     */
    @XStreamAlias(value ="SOURCE_TYPE")
    private String sourceType;

    /**
     * 交易日期
     */
    @NotNull(message = "交易日期不可为空")
    @XStreamAlias(value ="TRAN_DATE")
    private String tranDate;

    /**
     * 交易时间
     */
    @NotNull(message = "交易时间不可为空")
    @XStreamAlias(value ="TRAN_TIMESTAMP")
    private String tranTimestamp;

    /**
     * 请求系统流水号
     */
    @NotNull(message = "请求系统流水号不可为空")
    @XStreamAlias(value ="CONSUMER_SEQ_NO")
    private String consumerSeqNo;

    /**
     * 发送方机构号
     */
    @NotNull(message = "发送方机构号不可为空")
    @XStreamAlias(value ="BRANCH_ID")
    private String branchId;

    /**
     * 服务请求者身份
     */
    @NotNull(message = "服务请求者身份不可为空")
    @XStreamAlias(value ="USER_ID")
    private String userId;

    /**
     * 文件路径
     */
    @XStreamAlias(value ="FILE_PATH")
    private String filePath;

    /**
     * 法人类型代码
     */
    @XStreamAlias(value ="CORP_CODE")
    private String corpCode;

    /**
     * 组织系统id
     */
    @XStreamAlias("ORG_SYS_ID")
    private String orgSysId;

    /**
     * esb序列号
     */
    @XStreamAlias("ESB_SEQ_NO")
    private String esbSeqNo;

    /**
     * 版本号
     */
    @XStreamAlias("VERSION")
    private String version;

    /**
     * 消息代码
     */
    @XStreamAlias("MESSAGE_CODE")
    private String messageCode;

    /**
     * 客户号
     */
    @XStreamAlias("CLIENT_NO")
    private String clientNo;

    /**
     * 发送优先级
     */
    @XStreamAlias("PRIORITY_LEVEL")
    private String priorityLevel;
}

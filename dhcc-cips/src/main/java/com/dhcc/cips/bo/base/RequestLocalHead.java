package com.dhcc.cips.bo.base;

import lombok.Data;

import java.io.Serializable;

/**
 * @author StivenYang
 * @date 2019年3月29日
 * @description 公共app头
 * @update 2019年6月25日
 */
@Data
public class RequestLocalHead implements Serializable {

}

package com.dhcc.cips.bo.other;

import lombok.Data;

import java.io.Serializable;

@Data
public class OrgAcceptParamBo implements Serializable {

    private String code;
    private String msg;
    private String org_id;
}

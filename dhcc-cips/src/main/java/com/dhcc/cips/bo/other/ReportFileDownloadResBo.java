package com.dhcc.cips.bo.other;

import com.dhcc.cips.bo.base.ResponseHead;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 征信报告文件下载返回实体类
 * </p>
 *
 * @author limin
 * @since 2019-05-30
 */
@Data
@XStreamAlias("SERVICE")
public class ReportFileDownloadResBo extends ResponseHead implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 返回体
     */
    @XStreamAlias("BODY")
    private ReportFileDownloadResBodyBo reportFileDownloadResBodyBo;

}

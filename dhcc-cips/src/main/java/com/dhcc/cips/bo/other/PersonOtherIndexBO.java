package com.dhcc.cips.bo.other;

import lombok.Data;

import java.io.Serializable;

/**
 * @author StivenYang
 * @date 2019年6月23日
 * @description 个人其他指标（包括基本信息，贷款信息数组，担保信息数组）
 */
@Data
public class PersonOtherIndexBO implements Serializable {

    //征信评分

    //最近三个月机构审批通过比率

    //信用卡透支张数

    //最近五年内贷款最长逾期月数

    //未来1-50月需还贷款金额

    //近2年经营性贷款逾期次数

    //近2年贷款最长逾期月数

}

package com.dhcc.cips;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description:    用户服务中心启动类
 * @Author:         haokai
 * @CreateDate:     2019/2/27 14:23
 * @Version:        1.0
 */
@EnableFeignClients
@SpringBootApplication()
@Transactional
public class CipsServiceApplication {

    public static void main(String[] args) {
            SpringApplication.run(CipsServiceApplication.class, args);
        }
}


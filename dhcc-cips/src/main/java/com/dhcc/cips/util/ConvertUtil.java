package com.dhcc.cips.util;

import com.dhcc.cips.bo.other.QueryReqBo;
import com.dhcc.cips.constants.Constants;
import org.apache.commons.lang3.StringUtils;

/**
 * 参数转换工具类
 * 调用东华产品服务涉及到入参和出参的转换
 */
public class ConvertUtil {


    /**
     * 获取个人转换查询请求报文
     *
     * @param queryReqBo
     * @return
     */
    public static String getPersonConvertedRequestXml(QueryReqBo queryReqBo) {
        //转换后的请求报文
        String convertedRequestXml;

        //组装产品查询服务请求报文
        StringBuilder sb = new StringBuilder();
        convertedRequestXml = sb.append(Constants.PERSTART).append("<querierName>").append(queryReqBo.getClientName()).append("</querierName>\n")   //被查询企业名称
                .append("<querierCertype>").append(ConvertUtil.getPersonGlobalType(queryReqBo.getGlobalType())).append("</querierCertype>\n")             //被查询企业证件类型
                .append("<querierCertno>").append(queryReqBo.getGlobalId()).append("</querierCertno>\n")                                            //被查询企业证件号码
                .append("<queryReason>").append(queryReqBo.getReasonCode()).append("</queryReason>\n")                 //查询原因
                .append("<queryFormat>").append(Constants.queryFormat).append("</queryFormat>\n")                                                   //信用报告版式   00:授信机构版
                .append("<reportType>").append(queryReqBo.getReportType()).append("</reportType>\n")                                                //信用报告格式   H:Html格式  X:Xml格式  HX:Html格式和Xml格式
                .append("<queryType>").append(queryReqBo.getQueryType()).append("</queryType>\n")                                                   //信用报告复用策略  负数:查询本地  0:征信中心  正数:征信报告有效期
                .append("<syncFlag>").append(queryReqBo.getSyncFlag()).append("</syncFlag>\n")                                                      //同步异步返回标识 0:同步 1:异步
                .append("<reportVersion>").append(queryReqBo.getReportVersion()).append("</reportVersion>\n")                                          //信用报告版本  2.0.0:二代信用报告
                .append("<businessLine>").append(queryReqBo.getBusinessLine()).append("</businessLine>\n")                                          //业务线
                .append("<queryUser>").append(queryReqBo.getQueryAccount()).append("</queryUser>\n")                                                      //查询用户
                .append("<sysCode>").append(queryReqBo.getSysCode()).append("</sysCode>\n")                                                         //系统标识  区分接口调用子系统
                .append("<callSysUser>").append(queryReqBo.getUserId()).append("</callSysUser>\n")                                          //征信查询用户
                .append(Constants.PEREND).toString();
        return convertedRequestXml;
    }

    /**
     * 获取企业转换查询请求报文
     *
     * @param queryReqBo
     * @return
     */
    public static String getComanyConvertedRequestXml(QueryReqBo queryReqBo) {
        //转换后的请求报文
        String convertedRequestXml;

        //组装企业产品查询服务请求报文
        StringBuilder sb = new StringBuilder();
        convertedRequestXml = sb.append(Constants.ENTSTART).append("<querierName>").append(queryReqBo.getClientName()).append("</querierName>\n")   //被查询企业名称
                .append("<querierCertype>").append(ConvertUtil.getCompanyType(queryReqBo.getGlobalType())).append("</querierCertype>\n")             //被查询企业证件类型
                .append("<querierCertno>").append(queryReqBo.getGlobalId()).append("</querierCertno>\n")                                            //被查询企业证件号码
                .append("<queryReason>").append(ConvertUtil.getCompanyReason(queryReqBo.getReasonCode())).append("</queryReason>\n")                 //查询原因
                .append("<queryFormat>").append(Constants.queryFormat).append("</queryFormat>\n")                                                   //信用报告版式   00:授信机构版
                .append("<reportType>").append(queryReqBo.getReportType()).append("</reportType>\n")                                                //信用报告格式   H:Html格式  X:Xml格式  HX:Html格式和Xml格式
                .append("<queryType>").append(queryReqBo.getQueryType()).append("</queryType>\n")                                                   //信用报告复用策略  负数:查询本地  0:征信中心  正数:征信报告有效期
                .append("<syncFlag>").append(queryReqBo.getSyncFlag()).append("</syncFlag>\n")                                                      //同步异步返回标识 0:同步 1:异步
                .append("<reportVersion>").append(queryReqBo.getReportVersion()).append("</reportVersion>\n")                                          //信用报告版本  2.0.0:二代信用报告
                .append("<businessLine>").append(queryReqBo.getBusinessLine()).append("</businessLine>\n")                                          //业务线
                .append("<queryUser>").append(queryReqBo.getQueryAccount()).append("</queryUser>\n")                                                      //查询用户
                .append("<sysCode>").append(queryReqBo.getSysCode()).append("</sysCode>\n")                                                         //系统标识  区分接口调用子系统
                .append("<callSysUser>").append(queryReqBo.getUserId()).append("</callSysUser>\n")                                          //征信查询用户
                .append(Constants.ENTEND).toString();
        return convertedRequestXml;
    }

    /**
     * 获取转换查询返回报文
     *
     * @param responseXml
     * @return
     */
    public static String getConvertedResponseXml(String responseXml) {
        //转换后的返回报文
        String convertedResponseXml;

        if (StringUtils.isEmpty(responseXml)) {
            return "";
        }

        int begin = responseXml.indexOf("<return>");
        begin = responseXml.indexOf(">", begin);
        int end = responseXml.indexOf("</return>");
        convertedResponseXml = responseXml.substring(begin + 1, end);
        convertedResponseXml = convertedResponseXml.replaceAll("<", "<");
        convertedResponseXml = convertedResponseXml.replaceAll(">", ">");
        return convertedResponseXml;
    }


    /**
     * 返回个人证件类型
     *
     * @param str
     * @return
     */
    public static String getPersonGlobalType(String str) {
        String res;
        if ("B0100".equals(str) || "B0101".equals(str)) {//身份证
            res = "10";                                  //居民身份证及其他以公民身份证号为标识的证件
        } else if ("B0200".equals(str)) {//户口簿
            res = "1";
        } else if ("B0300".equals(str) || str.equals("B0301") || str.equals("B0302")) {//护照
            res = "2";
        } else if ("B1100".equals(str)) {//军官证
            res = "20";                  //军人身份证件
        } else if ("B0400".equals(str)) {//港澳居民来往内地通行证
            res = "5";
        } else if ("B0600".equals(str)) {//台湾同胞来往内地通行证
            res = "6";
        } else if ("B2400".equals(str)) {//外国人居留证/外国人永久居住证
            res = "8";
        } else if ("B1800".equals(str)) {//警官证
            res = "9";
        } else if ("B9900".equals(str) || str.equals("B1000") || str.equals("B0102")) { //其他证件 //士兵证  //临时身份证
            res = "X";
        } else if ("B2600".equals(str)) {//香港身份证
            res = "A";
        } else if ("B2700".equals(str)) {//澳门身份证
            res = "B";
        } else if ("B2800".equals(str)) {//台湾身份证
            res = "C";
        } else {
            res = "0";
        }
        return res;
    }

    /**
     * 返回企业证件类型
     *
     * @param str
     * @return
     */
    public static String getCompanyType(String str) {
        String res = "";
        if ("A0200".equals(str)) {//组织机构代码
            res = "30";
        } else if ("A0100".equals(str)) {//统一社会信用代码
            res = "20";
        } else if ("A1300".equals(str)) {
            res = "10";
        }
        return res;
    }


    /**
     * 获取企业查询原因
     *
     * @param reasonCode
     * @return
     */
    public static String getCompanyReason(String reasonCode) {
        String res;
        if ("01".equals(reasonCode)) {//01-贷前审查
            res = "10";
        } else if ("02".equals(reasonCode)) {//02-贷中操作
            res = "20";
        } else if ("03".equals(reasonCode)) {//03-贷后管理
            res = "30";
        } else if ("05".equals(reasonCode)) {//05-关联查询
            res = "40";
        } else if ("04".equals(reasonCode)) {//04-其他原因
            res = "90";
        } else {
            res = "90";
        }
        return res;
    }
}

package com.dhcc.cips.util;

import java.util.UUID;

/**
 * 字符串工具类
 */
public class StringUtil {

    /**
     * 得到uuid
     *
     * @return
     */
    public static String getUUID() {
        return UUID.randomUUID().toString();
    }
}

package com.dhcc.cips.util;

import com.dhcc.cips.config.CustomDateConverter;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.BigDecimalConverter;
import com.thoughtworks.xstream.converters.basic.StringConverter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.io.xml.XppDriver;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.TimeZone;

/**
 * @author lekang.liu
 * @date 2018年7月31日
 */
@Slf4j
public class XmlUtil {
    private static String TIMEZONE = "GMT+8";
    private static String DEFAULTFORMAT = "yyyy-MM-dd";

    /**
     * @param clazz
     * @param xmlMessage
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> T convertXml2JavaBean(Class<T> clazz, String xmlMessage, Class[] types) {
        try {
            XStream xStream = new XStream(new DomDriver());
            XStream.setupDefaultSecurity(xStream);
            xStream.allowTypes(types);
            xStream.processAnnotations(types);
            String XMLMessage = StringEscapeUtils.unescapeXml(xmlMessage);        //特殊转义字符处理
            xStream.registerConverter(new CustomDateConverter(DEFAULTFORMAT, null, TimeZone.getTimeZone(TIMEZONE)));
            xStream.registerConverter(new BigDecimalConverter() {
                @Override
                public Object fromString(String str) {
                    if (StringUtils.isEmpty(str)) {
                        return null;
                    }
                    return super.fromString(str);
                }
            });
            xStream.registerConverter(new StringConverter() {
                @Override
                public String fromString(String str) {
                    if (StringUtils.isEmpty(str)) {
                        return null;
                    }
                    return super.fromString(str).toString().trim();
                }
            });
            T t = (T) xStream.fromXML(XMLMessage);
            return t;
        } catch (Exception e) {
            log.debug("XmlUtil.convertXml2JavaBean", e);
        }
        return null;
    }

    /**
     * java对象转为xml字符串
     *
     * @param o     要转换的对象
     * @param types 转换对象需要处理注解的类数组
     * @return 转换完的xml
     */
    public static String converJavaBean2Xml(Object o, Class[] types) {
        XStream xStream = new XStream(new XppDriver(new XmlFriendlyNameCoder("_-", "_")));
        XStream.setupDefaultSecurity(xStream);
        xStream.processAnnotations(types);
        return xStream.toXML(o);
    }

    /**
     * java对象转为xml字符串
     *
     * @param o    要转换的对象
     * @param type 转换对象需要处理注解的类数组
     * @return 转换完的xml
     */
    public static String converJavaBean2Xml(Object o, Class type) {
        XStream xStream = new XStream(new XppDriver(new XmlFriendlyNameCoder("_-", "_")));
        XStream.setupDefaultSecurity(xStream);
        xStream.processAnnotations(type);
        return xStream.toXML(o);
    }
}

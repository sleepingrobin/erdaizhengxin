package com.dhcc.cips.util;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 通用类
 *
 * @author haokai
 */
public class DateUtil {
    private static ThreadLocal<DateFormat> threadLocal = new ThreadLocal<DateFormat>();
    private static final String month = "yyyy.MM";
    private static final String datetime = "yyyy-MM-dd HH:mm:ss";
    private static final String day = "yyyy.MM.dd";
    private static final String ymd = "yyyy-MM-dd";
    private static Calendar c = Calendar.getInstance();//获得一个日历的实例

    /**
     * 获取系统当前时间
     *
     * @param str
     * @return
     */
    public static String getNowTime(String str) {
        SimpleDateFormat simpledateformat = new SimpleDateFormat(str);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        return simpledateformat.format(calendar.getTime());
    }

    public static DateFormat getYmdDateFormat() {
        DateFormat monthDateFormat = threadLocal.get();
        if (monthDateFormat == null) {
            monthDateFormat = new SimpleDateFormat(ymd);
            threadLocal.set(monthDateFormat);
        }
        return monthDateFormat;
    }

    public static DateFormat getMonthDateFormat() {
        DateFormat monthDateFormat = threadLocal.get();
        if (monthDateFormat == null) {
            monthDateFormat = new SimpleDateFormat(month);
            threadLocal.set(monthDateFormat);
        }
        return monthDateFormat;
    }

    public static DateFormat getDayDateFormat() {
        DateFormat dayDateFormat = threadLocal.get();
        if (dayDateFormat == null) {
            dayDateFormat = new SimpleDateFormat(day);
            threadLocal.set(dayDateFormat);
        }
        return dayDateFormat;
    }

    public static DateFormat getDatetimeDateFormat() {
        DateFormat datetimeDateFormat = threadLocal.get();
        if (datetimeDateFormat == null) {
            datetimeDateFormat = new SimpleDateFormat(datetime);
            threadLocal.set(datetimeDateFormat);
        }
        return datetimeDateFormat;
    }
    // ****************************Date转换为String****************************

    /**
     * 转换成日期格式的字符串 格式为yyyy-MM
     *
     * @param date
     * @return String
     */
    public static synchronized String monthFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getMonthDateFormat().format(date);
    }

    /**
     * 转换成日期格式的字符串 格式为yyyy-MM-dd
     *
     * @param date
     * @return String
     */
    public static String dateYmdFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getYmdDateFormat().format(date);
    }

    /**
     * 转换成时间格式的字符串 格式为yyyy-MM-dd hh:mm:ss
     *
     * @param date
     * @return String
     */
    public static String dateTimeFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getDatetimeDateFormat().format(date);
    }

    /**
     * 转换成时间格式的字符串 格式为yyyy-MM-dd hh:mm:ss
     *
     * @param date
     * @return String
     */
    public static Date convertDateFormat(String date) throws ParseException {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        return new SimpleDateFormat(datetime).parse(date);
    }

    /**
     * 字符串转换成date格式 格式为yyyy.MM
     *
     * @param str
     * @return Date
     */
    public static synchronized Date toDate(String str, String format) throws ParseException {
        if (StringUtils.isEmpty(str)) {
            return null;
        }

        return new SimpleDateFormat(format).parse(str);
    }

    //获取日期相差月数
    public static int getMonthSpace(Date date1, Date date2) {

        int result = 0;
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();

        c1.setTime(date1);
        c2.setTime(date2);

        result = c2.get(Calendar.MONTH) - c1.get(Calendar.MONTH);

        return result == 0 ? 1 : Math.abs(result);
    }

    //获取指定日期前(-n)后n个月的日期
    public static synchronized Date getAimMonthDate(Date date, int n) {
        c.setTime(date);
        //可以是天数或月数  数字自定 -6前6个月
        c.add(GregorianCalendar.MONTH, n);
        Date result = c.getTime();
        return result;
    }

    //获取指定日期前(-n)后n年的日期
    public static synchronized Date getAimYearDate(Date date, int n) {
        c.setTime(date);
        //可以是天数或月数  数字自定 -4 前 4年
        c.add(GregorianCalendar.YEAR, n);
        Date result = c.getTime();
        return result;
    }

    /**
     * 获取当前日期后一天
     *
     * @param date
     * @return Date
     */
    public static Date getSpecifiedDayAfter(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int day = c.get(Calendar.DATE);
        c.set(Calendar.DATE, day + 1);
        date = c.getTime();
        return date;
    }

    public static Date getTime(Date date) {
        SimpleDateFormat datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return null;
    }

    /**
     * 算两个日期相差月份
     *
     * @param startDate
     * @param endDate
     * @return
     */
    public static Integer getDateOverMonth(Date startDate, Date endDate) {

        if (startDate == null || endDate == null) {
            return 0;
        }

        // 如果开始日期大于结束日期，返回null值；
        if (startDate.getTime() > endDate.getTime()) {
            return 0;
        }

        Calendar c_start = Calendar.getInstance();
        c_start.setTime(startDate);

        Calendar c_end = Calendar.getInstance();
        c_end.setTime(endDate);

        int result = (c_end.get(1) - c_start.get(1)) * 12
                + (c_end.get(2) - c_start.get(2));

        if (result < 0) {
            return 0;
        }

        return result;
    }


    /**
     * 取两个日期间最小 值
     *
     * @param d1
     * @param d2
     * @return Date - null，当两个参数都为null时；
     * - Date，两个日期的最大值。
     */
    public static Date getEarlierDate(Date d1, Date d2) {

        if (d1 == null)
            return d2;

        if (d2 == null)
            return d1;

        if (d1.after(d2)) {
            return d2;
        } else {
            return d1;
        }

    }

    /**
     * 取三个日期间最小值
     *
     * @param d1
     * @param d2
     * @param d3
     * @return Date - null，当三个参数都为null时；
     * - Date，三个日期的最小值。
     */
    public static Date getEarliestDate(Date d1, Date d2, Date d3) {

        return getEarlierDate(getEarlierDate(d1, d2), d3);
    }
}

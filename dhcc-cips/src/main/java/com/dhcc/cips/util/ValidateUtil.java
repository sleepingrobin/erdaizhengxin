package com.dhcc.cips.util;

import org.hibernate.validator.HibernateValidator;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

/**
 * 统一校验工具类
 */
public class ValidateUtil {

    /**
     * 通用校验，javabean类里通过注解校验
     * @param object
     * @return
     */
    public static String commonValidate(Object object) {
        String validMessage = "";
        Validator validator = Validation.byProvider(HibernateValidator.class).configure().failFast(false).buildValidatorFactory().getValidator();
        Set<ConstraintViolation<Object>> validators = validator.validate(object);
        for (ConstraintViolation<Object> constraintViolation : validators) {
            validMessage = validMessage + constraintViolation.getMessage() + ";";
        }
        return validMessage;
    }

}
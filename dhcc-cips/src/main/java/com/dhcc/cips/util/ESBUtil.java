package com.dhcc.cips.util;

import com.dc.eai.data.*;
import com.dcfs.esb.security.Base64;
import com.dcfs.esb.server.global.DataUtils;
import com.dhcc.cips.bo.base.Request;
import lombok.extern.slf4j.Slf4j;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.Iterator;

/**
 * ESB相关工具类
 */
@Slf4j
public class ESBUtil {

    private static String[] HEAD = new String[]{"SYS_HEAD", "APP_HEAD", "LOCAL_HEAD", "BODY"};

    /**
     *
     * @param xmlBytes
     * @param encoding
     * @return
     * @throws DocumentException
     */
    public static CompositeData xmlToCD(byte[] xmlBytes, String encoding) throws DocumentException {
        String xmlStr = null;

        try {
            xmlStr = new String(xmlBytes, encoding);
        } catch (UnsupportedEncodingException var9) {
            ;
        }

        CompositeData data = new CompositeData();

        try {
            Document doc = DocumentHelper.parseText(xmlStr);
            Element root = null;
            System.out.println(root);

            for(int i = 0; i < HEAD.length; ++i) {
                String node = HEAD[i];
                root = doc.getRootElement().element(node);
                if (root != null) {
                    CompositeData data_struct = new CompositeData();
                    data.addStruct(node, parseXml(root, data_struct));
                } else if (log.isWarnEnabled()) {
                    log.warn("节点[ " + node + " ]不包含元素");
                }
            }
        } catch (DocumentException var10) {
            if (log.isErrorEnabled()) {
                log.error("解析xml出错", var10);
            }
        }

        return data;
    }

    /**
     *
     * @param root
     * @param dataCD
     * @return
     */
    private static CompositeData parseXml(Element root, CompositeData dataCD) {
        Element elem = null;
        String dataType = null;
        Iterator i = root.elementIterator();

        while(true) {
            while(true) {
                while(i.hasNext()) {
                    elem = (Element)i.next();
                    dataType = elem.attributeValue("type");
                    if (dataType == null) {
                        if (elem.isTextOnly()) {
                            dataCD.addField(elem.getName(), DataUtils.getStringField(elem.getText()));
                        } else {
                            CompositeData dataArray;
                            if (!elem.getName().toUpperCase().endsWith("_ARRAY") && !elem.getName().toUpperCase().endsWith("RET")) {
                                dataArray = new CompositeData();
                                dataArray = parseXml(elem, dataArray);
                                dataCD.addStruct(elem.getName(), dataArray);
                            } else {
                                dataArray = new CompositeData();
                                dataArray = parseXml(elem, dataArray);
                                Array array = dataCD.getArray(elem.getName());
                                if (array == null) {
                                    array = new Array();
                                    array.addStruct(dataArray);
                                    dataCD.addArray(elem.getName(), array);
                                } else {
                                    array.addStruct(dataArray);
                                }
                            }
                        }
                    } else if ("double".equals(dataType)) {
                        String scaleValue = elem.attributeValue("scale");
                        double value = "".equals(elem.getText()) ? 0.0D : Double.parseDouble(elem.getText());
                        int scale = scaleValue != null ? Integer.parseInt(scaleValue) : 2;
                        dataCD.addField(elem.getName(), DataUtils.getDoubleField(value, scale));
                    } else if ("int".equals(dataType)) {
                        int value = "".equals(elem.getText()) ? 0 : Integer.parseInt(elem.getText().trim());
                        dataCD.addField(elem.getName(), DataUtils.getIntField(value));
                    } else if ("image".equals(dataType)) {
                        dataCD.addField(elem.getName(), DataUtils.getImageField(Base64.decode(elem.getText())));
                    } else if (log.isWarnEnabled()) {
                        log.warn("字段[ " + elem.getName() + " ]的数据类型定义错误，type=[" + dataType + "]");
                    }
                }

                return dataCD;
            }
        }
    }

    /**
     *
     * @param data
     * @param encoding
     * @return
     */
    public static byte[] CDtoXml(CompositeData data, String encoding) {
        StringBuffer sbd = new StringBuffer();
        sbd.append("<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>\n");
        sbd.append("<SERVICE>\n");
        CompositeData data_cd = null;

        for(int i = 0; i < HEAD.length; ++i) {
            String node = HEAD[i];
            if ("PRINTER".equals(node)) {
                data.setCascadeCreate(false);
            } else {
                data.setCascadeCreate(true);
            }

            data_cd = data.getStruct(node);
            if (data_cd != null) {
                sbd.append("<" + node + ">\n");
                sbd.append(parseCD(data_cd));
                sbd.append("</" + node + ">\n");
            } else if (log.isWarnEnabled()) {
                log.warn("节点[ " + node + " ]不包含元素");
            }
        }

        sbd.append("</SERVICE>");
        byte[] retBytes = (byte[])null;

        try {
            retBytes = sbd.toString().getBytes(encoding);
        } catch (UnsupportedEncodingException var6) {
            ;
        }

        return retBytes;
    }

    /**
     *
     * @param data
     * @return
     */
    public static String parseCD(CompositeData data) {
        StringBuffer sbd = new StringBuffer();
        Field field = null;
        Iterator it = data.iterator();

        while(true) {
            while(it.hasNext()) {
                String subName = (String)it.next();
                AtomData subData = data.getObject(subName);
                if (subData instanceof Field) {
                    getMark(sbd, subName, data, (Field)field);
                } else if (subData instanceof Array) {
                    int size = ((Array)subData).size();

                    for(int i = 0; i < size; ++i) {
                        sbd.append("<").append(subName).append(">\n");
                        sbd.append(parseCD(data.getArray(subName).getStruct(i)));
                        sbd.append("</").append(subName).append(">\n");
                    }
                } else {
                    if (log.isDebugEnabled()) {
                        log.debug(subName + "结构体");
                    }

                    sbd.append("<").append(subName).append(">").append(parseCD(data.getStruct(subName))).append("</").append(subName).append(">");
                }
            }

            return sbd.toString();
        }
    }

    /**
     *
     * @param sbd
     * @param subName
     * @param data
     * @param field
     */
    private static void getMark(StringBuffer sbd, String subName, CompositeData data, Field field) {
        if (data.getField(subName) != null) {
            field = data.getField(subName);
            FieldType ftype = field.getFieldType();
            if (ftype == FieldType.FIELD_STRING) {
                sbd.append("<").append(subName).append(">");
                sbd.append(replaceXML(field.strValue()));
                sbd.append("</").append(subName).append(">\n");
            } else if (ftype == FieldType.FIELD_DOUBLE) {
                int length = field.getAttr().getLength();
                int scale = field.getAttr().getScale();
                DecimalFormat df = new DecimalFormat(getDecimalFormat(length, scale));
                sbd.append("<").append(subName).append(" type=\"double\" scale=\"").append(field.getAttr().getScale()).append("\">");
                sbd.append(df.format(field.doubleValue()));
                sbd.append("</").append(subName).append(">\n");
            } else if (ftype == FieldType.FIELD_INT) {
                sbd.append("<").append(subName).append(" type=\"int\">");
                sbd.append(field.intValue());
                sbd.append("</").append(subName).append(">\n");
            } else if (ftype == FieldType.FIELD_IMAGE) {
                sbd.append("<").append(subName).append(" type=\"image\">");
                sbd.append(Base64.encode(field.imageValue()));
                sbd.append("</").append(subName).append(">\n");
            }
        }

    }

    /**
     *
     * @param value
     * @return
     */
    private static String replaceXML(String value) {
        if (value.contains("&")) {
            value = value.replace("&", "&amp;");
        }

        if (value.contains("<")) {
            value = value.replace("<", "&lt;");
        }

        if (value.contains(">")) {
            value = value.replace(">", "&gt;");
        }

        if (value.contains("'")) {
            value = value.replace("'", "&apos;");
        }

        if (value.contains("\"")) {
            value = value.replace("\"", "&quot;");
        }

        return value;
    }

    /**
     *
     * @param len
     * @param scale
     * @return
     */
    public static String getDecimalFormat(int len, int scale) {
        StringBuffer sb = new StringBuffer();
        if (len < scale) {
            len = scale + 1;
        }

        int i;
        for(i = 0; i < len - scale - 1; ++i) {
            sb.append("#");
        }

        if (scale == 0) {
            sb.append("0");
        } else {
            sb.append("0.");

            for(i = 0; i < scale; ++i) {
                sb.append("0");
            }
        }

        return sb.toString();
    }
}

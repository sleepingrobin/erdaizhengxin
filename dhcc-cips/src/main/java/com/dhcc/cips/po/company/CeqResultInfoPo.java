package com.dhcc.cips.po.company;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 企业查询日志表
 * </p>
 *
 * @author LIMIN
 * @since 2019-05-30
 */
@Data
@TableName("CEQ_RESULTINFO")
public class CeqResultInfoPo extends Model<CeqResultInfoPo> {

    private static final long serialVersionUID = 1L;

    @TableId("ID")
    private String id;

    @TableField("CLIENT_NAME")
    private String clientName;

    @TableField("LOANCARD_CODE")
    private String loancardCode;

    @TableField("US_CREDIT_CODE")
    private String usCreditCode;

    @TableField("COPR_CODE")
    private String coprCode;

    @TableField("ORG_CREDIT_CODE")
    private String orgCreditCode;

    @TableField("GS_REGI_CODE")
    private String gsRegiCode;

    @TableField("DS_REGI_CODE")
    private String dsRegiCode;

    @TableField("REGI_TYPE_CODE")
    private String regiTypeCode;

    @TableField("FRGCORPNO")
    private String frgcorpno;

    @TableField("AUTHARCHIVE_ID")
    private String autharchiveId;

    @TableField("QRY_REASON")
    private String qryReason;

    @TableField("QUERY_FORMAT")
    private String queryFormat;

    @TableField("OPER_ORG")
    private String operOrg;

    @TableField("OPERATOR")
    private String operator;

    @TableField("CREDIT_USER")
    private String creditUser;

    @TableField("QUERY_ORG")
    private String queryOrg;

    @TableField("QUERY_TYPE")
    private String queryType;

    @TableField("QTIME_LIMIT")
    private String qtimeLimit;

    @TableField("RESULT_TYPE")
    private String resultType;

    @TableField("QUERY_MODE")
    private String queryMode;

    @TableField("STATUS")
    private String status;

    @TableField("SOURCE")
    private String source;

    @TableField("CERTIFICATION_MARK")
    private String certificationMark;

    @TableField("BATCH_FLAG")
    private String batchFlag;

    @TableField("MSG_NO")
    private String msgNo;

    @TableField("FLOW_ID")
    private String flowId;

    @TableField("CHANNEL_ID")
    private String channelId;

    @TableField("CSTMSYS_ID")
    private String cstmsysId;

    @TableField("CLIENT_IP")
    private String clientIp;

    @TableField("ERROR_INFO")
    private String errorInfo;

    @TableField("QUERY_TIME")
    private LocalDateTime queryTime;

    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;

    @TableField("CHECK_WAY")
    private String checkWay;

    @TableField("ASSOCBSNSS_DATA")
    private String assocbsnssData;

    @TableField("REK_USER")
    private String rekUser;

    @TableField("REK_ORG")
    private String rekOrg;

    @TableField("REK_TIME")
    private Date rekTime;

    @TableField("CHECK_ID")
    private String checkId;

    @TableField("CREDIT_ID")
    private String creditId;

    @TableField("REPORT_ID")
    private String reportId;

    @TableField("ARCHIVE_REVISE")
    private String archiveRevise;

    @TableField("PASSWORD")
    private String password;

    @TableField("REPORT_VERSION")
    private String reportVersion;

    @TableField("REPORT_FORMAT")
    private String reportFormat;

    @TableField("QUERYREASON_ID")
    private String queryreasonId;

    @TableField("HTML_PATH")
    private String htmlPath;

    @TableField("XML_PATH")
    private String xmlPath;

    @TableField("JSON_PATH")
    private String jsonPath;

    @TableField("PDF_PATH")
    private String pdfPath;

    @TableField("CONTEXT_FILE_PATH")
    private String contextFilePath;

    @TableField("USE_TIME")
    private Integer useTime;

    @TableField("CALL_SYS_USER")
    private String callSysUser;

    @TableField("RECHECK_USER_NAME")
    private String recheckUserName;

    @TableField("EXT1")
    private String ext1;

    @TableField("EXT2")
    private String ext2;

    @TableField("EXT3")
    private String ext3;

    @TableField("EXT4")
    private String ext4;

    @TableField("EXT5")
    private String ext5;

    @TableField("EXT6")
    private String ext6;

    @TableField("CLIENT_ID")
    private String clientId;

    @TableField("OPER_ORG_NAME")
    private String operOrgName;

    @TableField("OPERATOR_NAME")
    private String operatorName;

    @TableField("AUTHORIZA_VESION")
    private String authorizaVesion;

    @TableField("REQ_ACCEPT_TIME")
    private LocalDateTime reqAcceptTime;

    @TableField("LOGIN_START_TIME")
    private LocalDateTime loginStartTime;

    @TableField("LOGIN_END_TIME")
    private LocalDateTime loginEndTime;

    @TableField("QUERY_START_TIME")
    private LocalDateTime queryStartTime;

    @TableField("QUERY_END_TIME")
    private LocalDateTime queryEndTime;

    @TableField("ANALY_START_TIME")
    private LocalDateTime analyStartTime;

    @TableField("ANALY_END_TIME")
    private LocalDateTime analyEndTime;

    @TableField("CALC_START_TIME")
    private LocalDateTime calcStartTime;

    @TableField("CALC_END_TIME")
    private LocalDateTime calcEndTime;

    @TableField("QUERY_RETURN_TIME")
    private LocalDateTime queryReturnTime;

    @TableField("SOURCE_TYPE")
    private String sourceType;

    @TableField("REPORT_CODE")
    private String indexCalErrorInfo;

}

package com.dhcc.cips.po.person;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 基本概况信息段
 * </p>
 *
 * @author LIMIN
 * @since 2019-05-29
 */
@Data
@TableName("PB01A_BASICPROINFO")
public class PersonBaseInfoPo {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId("ID")
    private String id;

    /**
     * 信用报告id
     */
    @TableField("REPORT_ID")
    private String reportId;

    /**
     * 性别
     */
    @TableField("SEX")
    private String sex;

    /**
     * 出生日期
     */
    @TableField("BIRTH_DATE")
    private LocalDateTime birthDate;

    /**
     * 学历
     */
    @TableField("EDUCATION")
    private String education;

    /**
     * 学位
     */
    @TableField("DEGREE")
    private String degree;

    /**
     * 就业状况
     */
    @TableField("EMPLOYMENT")
    private String employment;

    /**
     * 电子邮箱
     */
    @TableField("EMAIL")
    private String email;

    /**
     * 通讯地址
     */
    @TableField("POSTAL_ADDRESS")
    private String postalAddress;

    /**
     * 国籍
     */
    @TableField("NATIONALITY")
    private String nationality;

    /**
     * 户籍地址
     */
    @TableField("PERMA_ADDRESS")
    private String permaAddress;

}
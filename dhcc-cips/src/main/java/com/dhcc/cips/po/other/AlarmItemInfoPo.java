/*
 * =========================================================================
 *  Program Name：CIPS
 *  
 *  Description：This project includes two functions. The major is enquiring
 *  	custom's credit info, which includes alarm info, calculated info, 
 *  	compared info and credit report;  the minor is providing auto-login, 
 *  	fetching, parsing credit info from the credit center of China, and 
 *  	saving relative info into local Database.
 * 
 *	Copyright ©2012 GBICC Pte. Ltd. All Rights Reserved
 *
 *  This software is confidential and proprietary to GBICC Pte. Ltd. You shall
 *  use this software only in accordance with the terms of the license
 *  agreement you entered into with GBICC.  No aspect or part or all of this
 *  software may be reproduced, modified or disclosed without full and
 *  direct written authorisation from GBICC.
 *
 *  GBICC SUPPLIES THIS SOFTWARE ON AN "AS IS" BASIS. GBICC MAKES NO
 *  REPRESENTATIONS OR WARRANTIES, EITHER EXPRESSLY OR IMPLIEDLY, ABOUT THE
 *  SUITABILITY OR NON-INFRINGEMENT OF THE SOFTWARE. GBICC SHALL NOT BE LIABLE
 *  FOR ANY LOSSES OR DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 *  MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 * =========================================================================
 */
package com.dhcc.cips.po.other;

import java.io.Serializable;

import lombok.Data;


/**
 * 预警规则PO
 */
@Data
public class AlarmItemInfoPo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String cId;
	private String zbType;
	private String alarmItem;
	private String alarmCond;
	private String alarmValue;
	
}

package com.dhcc.cips.po.other;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author YangJiaheng
 * @date 2019年5月15日17:56:37
 * @description 组织机构对象
 * 使用该对象作为调用前置系统接口的参数
 */
@Data
public class SystemOrgPo implements Serializable {

    private static final long serialVersionUID = 572469958481487707L;

    /**
     * 内部机构号
     */
    private String orgId;

    /**
     * 机构名称
     */
    private String orgName;

    /**
     * 上级机构
     */
    private String uporg;

    /**
     * 行政区划
     */
    private String areaCode;

    /**
     * 详细地址
     */
    private String address;

    /**
     * 邮政编码
     */
    private String postCode;

    /**
     * 启用标志
     */
    private String recordStopFlag;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否是法人机构
     */
    private String corporateFlag;

    /**
     * 法人机构ID
     */
    private String corporateId;

}

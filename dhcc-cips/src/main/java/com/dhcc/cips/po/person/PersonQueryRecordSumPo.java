package com.dhcc.cips.po.person;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 查询记录概要信息
 * </p>
 *
 * @author LIMIN
 * @since 2019-05-29
 */
@Data
@TableName("PC05B_QUERYRECORDSUM")
public class PersonQueryRecordSumPo {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId("ID")
    private String id;

    /**
     * 信用id
     */
    @TableField("REPORT_ID")
    private String reportId;

    /**
     * 最近 1 个月内的查询机构数（贷款审批）
     */
    @TableField("LASTONE_QUORGLOAD")
    private String lastoneQuorgload;

    /**
     * 最近 1 个月内的查询机构数（信用卡审批）
     */
    @TableField("LASTONE_QUORGCRE")
    private String lastoneQuorgcre;

    /**
     * 最近 1 个月内的查询次数（贷款审批）
     */
    @TableField("LASTONE_QUNUMLOAD")
    private String lastoneQunumload;

    /**
     * 最近 1 个月内的查询次数（信用卡审批）
     */
    @TableField("LASTONE_QUNUMCRE")
    private String lastoneQunumcre;

    /**
     * 最近1个月内的查询次数（本人查询）
     */
    @TableField("LASTMON_QURNUM")
    private String lastmonQurnum;

    /**
     * 最近 2 年内的查询次数（贷后管理）
     */
    @TableField("LASTYLOAN_QRNUM")
    private String lastyloanQrnum;

    /**
     * 最近 2 年内的查询次数（担保资格审查）
     */
    @TableField("LASTYEXA_QRNUM")
    private String lastyexaQrnum;

    /**
     * 最近 2 年内的查询次数（特约商户实名审查）
     */
    @TableField("LASTYNAME_QRNUM")
    private String lastynameQrnum;


}

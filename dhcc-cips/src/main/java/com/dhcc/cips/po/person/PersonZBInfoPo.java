package com.dhcc.cips.po.person;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.dhcc.cips.bo.person.PersonExtGuarantyInfoBo;
import com.dhcc.cips.bo.person.PersonLoanInfoBo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 *
 * 个人指标信息PO
 * @author limin
 * @since 2019-06-17
 */
@Data
@TableName("ZX_P_ZB_INFO")
public class PersonZBInfoPo extends Model<PersonZBInfoPo> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键（报告编号）
     */
    @TableId("REPORT_NO")
    @XStreamAlias("REPORT_NO")
    private String reportNo;

    /**
     * 首张贷款发放日期
     */
    @TableField("FIRST_DK_RELEASE_DATE")
    @XStreamAlias("GRANT_YEAR_MON")
    private String firstDkReleaseDate;

    /**
     * 首张贷记卡发卡日期
     */
    @TableField("FIRST_DJK_RELEASE_DATE")
    @XStreamAlias("CARD_GRANT_YEAR_MON")
    private String firstDjkReleaseDate;

    /**
     * 首张准贷记卡发卡日期
     */
    @TableField("FIRST_ZDJK_RELEASE_DATE")
    @XStreamAlias("YEAR_MON")
    private String firstZdjkReleaseDate;

    /**
     * 与银行建立信贷关系月数
     */
    @TableField("RELA_BANK_MON_NUM")
    @XStreamAlias("CONTACT_MONTHS")
    private String relaBankMonNum;

    /**
     * 授信总额（信用总额）
     */
    @TableField("CRED_TOTAL_AMT")
    @XStreamAlias("CREDIT_TOTAL_AMT")
    private String credTotalAmt;

    /**
     * 授信余额（信用总余额）
     */
    @TableField("CRED_TOTAL_BALAN")
    @XStreamAlias("CREDIT_BALANCE")
    private String credTotalBalan;

    /**
     * 有信用业务的银行数
     */
    @TableField("CRED_BIZ_BANK_NUM")
    @XStreamAlias("BUSS_BANK_NUM")
    private String credBizBankNum;

    /**
     * 现有贷款授信银行数
     */
    @TableField("CURR_LOAN_BANK_NUM")
    @XStreamAlias("LOAN_BANK_NUM")
    private String currLoanBankNum;

    /**
     * 现有信用卡授信银行数
     */
    @TableField("CURR_CRED_BANK_NUM")
    @XStreamAlias("CREDIT_BANK_NUM")
    private String currCredBankNum;

    /**
     * 现有信用产品总数
     */
    @TableField("CURR_CL_PROD_NUM")
    @XStreamAlias("CL_PROD_NUM")
    private String currClProdNum;

    /**
     * 对外担保金额
     */
    @TableField("GUAR_CONTR_AMT")
    @XStreamAlias("ENSURE_AMT")
    private String guarContrAmt;

    /**
     * 对外担保本金余额
     */
    @XStreamAlias("ENSURE_BAL")
    @TableField("GUAR_CONTR_BALAN")
    private String guarContrBalan;

    /**
     * 历史信贷产品数（5年内历史）
     */
    @TableField("CL_PROD_L_5_Y_NUM")
    @XStreamAlias("HIST_CL_PROD_NUM")
    private String clProdL5YNum;

    /**
     * 最近一个月内的查询次数
     */
    @TableField("LATE_MON_ENQU_COUNT")
    @XStreamAlias("LM_QRY_TMS")
    private String lateMonEnquCount;

    /**
     * 最近一年内的查询次数
     */
    @TableField("LATE_YEAR_ENQU_COUNT")
    @XStreamAlias("LY_QRY_TMS")
    private String lateYearEnquCount;

    /**
     * 最近1个月内的查询次数/最近1年内的查询次数（比率）
     */
    @TableField("LATE_MON_YEAR_ENQU_RATE")
    @XStreamAlias("MON_YEAR_QRY_RATE")
    private String lateMonYearEnquRate;

    /**
     * 信用产品数和查询记录数比率
     */
    @TableField("CL_PROD_ENQU_RECO_RATE")
    @XStreamAlias("CL_PD_QRY_RATE")
    private String clProdEnquRecoRate;

    /**
     * 信用卡张数
     */
    @TableField("CRED_CARD_NUM")
    @XStreamAlias("CREDIT_CARD_NUM")
    private String credCardNum;

    /**
     * 当前信用卡逾期总额
     */
    @TableField("CRED_CARD_TOTAL_AMT")
    @XStreamAlias("CARD_CREDIT_TOTAL_AMT")
    private String credCardTotalAmt;

    /**
     * 信用卡总余额
     */
    @TableField("CRED_CARD_TOTAL_BALAN")
    @XStreamAlias("CREDIT_CARD_BAL")
    private String credCardTotalBalan;

    /**
     * 当前信用卡额度使用率
     */
    @TableField("C_CRED_CARD_USE_FREQU")
    @XStreamAlias("CREDIT_AMT_USE_RATE")
    private String cCredCardUseFrequ;

    /**
     * 他行信用卡最高额度
     */
    @TableField("OTHE_BANK_CRED_MAX_AMT")
    @XStreamAlias("MAX_AMT")
    private String otheBankCredMaxAmt;

    /**
     * 他行信用卡最低额度
     */
    @TableField("OTHE_BANK_CRED_MIN_AMT")
    @XStreamAlias("MIN_AMT")
    private String otheBankCredMinAmt;

    /**
     * 信用卡平均额度
     */
    @TableField("CRED_CARD_AVER_AMT")
    @XStreamAlias("CREDIT_CARD_AVG_LMT")
    private String credCardAverAmt;

    /**
     * 最近3个月信用卡发数/总发卡数
     */
    @TableField("LATE_MON_CRED_SUM_RATE")
    @XStreamAlias("L3M_CARD_ISSUE_RATE")
    private String lateMonCredSumRate;

    /**
     * 信用卡使用率
     */
    @TableField("CRED_CARD_USE_FREQU")
    @XStreamAlias("NUM_USE_RATE")
    private String credCardUseFrequ;

    /**
     * 历史信用卡最高额度使用率
     */
    @TableField("HIST_CRED_CARD_USE_FREQU")
    @XStreamAlias("MAX_AMT_USE_RATE")
    private String histCredCardUseFrequ;

    /**
     * 当前信用卡逾期总额
     */
    @TableField("C_OD_AMT")
    @XStreamAlias("OVERDUE_AMT")
    private String cOdAmt;

    /**
     * 过去2年内信用卡最高逾期期数
     */
    @TableField("L_2_Y_OD_MAX_TIME")
    @XStreamAlias("L2Y_MAX_ODUE_PRDS")
    private String l2YOdMaxTime;

    /**
     * 历史最大逾期（透支）期数
     */
    @TableField("C_MAX_OD_NUM")
    @XStreamAlias("CREDIT_HIST_MAX_ODUE_PRDS")
    private String cMaxOdNum;

    /**
     * 过去两年内逾期三期以上（包括三期）信用卡张数
     */
    @TableField("L_2_Y_OD_2_TIME_CRED_NUM")
    @XStreamAlias("L2Y_ODUE_CARD_NUM")
    private String l2YOd2TimeCredNum;

    /**
     * 贷记卡12月内未还最低还款额次数
     */
    @TableField("LATE_12_MON_MIN_RP_NUM")
    @XStreamAlias("OVER_TIMES")
    private String late12MonMinRpNum;

    /**
     * 单张信用卡额度使用率
     */
    @TableField("PER_CRED_CARD_USED_RATE")
    @XStreamAlias("SIGN_CRDT_CARD_LMT_PCT")
    private String perCredCardUsedRate;

    /**
     * 未结清贷款笔数
     */
    @TableField("UN_SETTL_LOAN_NUM")
    @XStreamAlias("UNSETT_LOAN_NUM")
    private String unSettlLoanNum;

    /**
     * 未结清经营性贷款笔数
     */
    @TableField("UN_SETTL_JYX_LOAN_NUM")
    @XStreamAlias("UNSETT_MNGMT_LOAN_NUM")
    private String unSettlJyxLoanNum;

    /**
     * 将来2个月未结清经营性贷款到期笔数
     */
    @TableField("F_2_M_UN_SETT_JYX_LOAN_NUM")
    @XStreamAlias("F2M_UNSETT_MATR_NUM")
    private String f2MUnSettJyxLoanNum;

    /**
     * 将来2个月贷款到期金额数（贷款有无集中到期）
     */
    @TableField("F_2_M_UN_SETT_LOAN_AMT")
    private String f2MUnSettLoanAmt;

    /**
     * 未结清经营性贷款笔数/未结清贷款笔数
     */
    @TableField("UN_SETT_JYX_LOAN_NUM_RATE")
    @XStreamAlias("MNGMT_UNSETT_NUM_RATE")
    private String unSettJyxLoanNumRate;

    /**
     * 未结清经营性贷金额/未结清贷款金额
     */
    @TableField("UN_SETT_JYX_LOAN_AMT_RATE")
    @XStreamAlias("MNGMT_UNSETT_BAL_RATE")
    private String unSettJyxLoanAmtRate;

    /**
     * 他行经营性贷款最高额度
     */
    @TableField("O_BANK_JYX_LOAN_MAX_AMT")
    @XStreamAlias("OTH_MAX_MNGMT_LOAN_AMT")
    private String oBankJyxLoanMaxAmt;

    /**
     * 他行未结清非抵质押贷款笔数
     */
    @TableField("O_B_UN_SETT_LOAN_FDZY_NUM")
    @XStreamAlias("UNSETT_UNPLG_LOAN_NUM")
    private String oBUnSettLoanFdzyNum;

    /**
     * 他行未结清非抵质押贷款余额
     */
    @TableField("O_B_UN_SETT_LOAN_FDZY_AMT")
    @XStreamAlias("UNSETT_UNPLG_LOAN_BAL")
    private String oBUnSettLoanFdzyAmt;

    /**
     * 贷款总授信额度（未结清）
     */
    @TableField("LOAN_TOTAL_AMT")
    @XStreamAlias("LOAN_AMT")
    private String loanTotalAmt;

    /**
     * 贷款总余额
     */
    @TableField("LOAN_TOTAL_BALAN")
    @XStreamAlias("LOAN_BALANCE")
    private String loanTotalBalan;

    /**
     * 平均贷款余额（未结清）
     */
    @TableField("LOAN_AVER_BALAN")
    @XStreamAlias("AVG_LOAN_BAL")
    private String loanAverBalan;

    /**
     * 过去2个月贷款发放数/总未结清贷款数
     */
    @TableField("L_2_M_L_UN_SETT_L_NUM_RATE")
    @XStreamAlias("L2M_UNSETT_NUM_RATE")
    private String l2MLUnSettLNumRate;

    /**
     * 最近1年贷款最大合同金额
     */
    @TableField("L_1_Y_MAX_LOAN_AMT")
    @XStreamAlias("MAX_LOAN_AMT")
    private String l1YMaxLoanAmt;

    /**
     * 当前贷款逾期总额
     */
    @TableField("C_OD_TOTAL_AMT")
    @XStreamAlias("LOAN_OVERDUE_AMT")
    private String cOdTotalAmt;

    /**
     * 当前贷款最大逾期期数
     */
    @TableField("C_OD_MAX_NUM")
    @XStreamAlias("LOAN_MAX_ODUE_PRDS")
    private String cOdMaxNum;

    /**
     * 贷款逾期总额占合同金额比例
     */
    @TableField("OD_SUM_CONTR_SUM_AMT_RATE")
    @XStreamAlias("ODUE_LOAN_AMT_RATE")
    private String odSumContrSumAmtRate;

    /**
     * 贷款逾期账户数占总信贷账户比例
     */
    @TableField("OD_ACCT_CL_ACCT_RATE")
    @XStreamAlias("LOAN_ODUE_ACCT_RATE")
    private String odAcctClAcctRate;

    /**
     * 过去两年内经营性贷款逾期两期以上（包括两期）贷款笔数
     */
    @TableField("L_2_Y_JYX_LOAN_OD_2_TIME_NUM")
    @XStreamAlias("MATURE_ODUE_NUM")
    private String l2YJyxLoanOd2TimeNum;

    /**
     * 质押担保笔数占比（贷款、非结清）
     */
    @TableField("LOAN_ZY_GUAN_NUM_RATE")
    @XStreamAlias("PLG_GNT_NUM_RATE")
    private String loanZyGuanNumRate;

    /**
     * 抵押担保笔数占比（贷款、未结清）
     */
    @TableField("LOAN_DY_GUAN_NUM_RATE")
    @XStreamAlias("CLT_GNT_NUM_RATE")
    private String loanDyGuanNumRate;

    /**
     * 自然人保证笔数占比（贷款、未结清）
     */
    @TableField("LOAN_PN_GUAN_NUM_RATE")
    @XStreamAlias("GNR_GNT_NUM_RATE")
    private String loanPnGuanNumRate;

    /**
     * 其他担保笔数占比（贷款、未结清）
     */
    @TableField("LOAN_OT_GUAN_NUM_RATE")
    @XStreamAlias("OTH_GNT_NUM_RATE")
    private String loanOtGuanNumRate;

    /**
     * 质押担保金额占比（贷款、未结清）
     */
    @TableField("LOAN_ZY_GUAN_AMT_RATE")
    @XStreamAlias("PLG_GNT_BAL_RATE")
    private String loanZyGuanAmtRate;

    /**
     * 抵押担保金额占比（贷款、未结清）
     */
    @TableField("LOAN_DY_GUAN_AMT_RATE")
    @XStreamAlias("CLT_GNT_BAL_RATE")
    private String loanDyGuanAmtRate;

    /**
     * 自然人保证金额占比（贷款、未结清）
     */
    @TableField("LOAN_PN_GUAN_AMT_RATE")
    @XStreamAlias("GNR_GNT_BAL_RATE")
    private String loanPnGuanAmtRate;

    /**
     * 其他担保金额占比（贷款、未结清）
     */
    @TableField("LOAN_OT_GUAN_AMT_RATE")
    @XStreamAlias("OTH_GNT_BAL_RATE")
    private String loanOtGuanAmtRate;

    /**
     * 是否存在逾期记录（历史逾期标识） 0：否 1：是
     */
    @TableField("OD_RECO_TOTAL_NUM")
    @XStreamAlias("ODUE_FLAG")
    private String odRecoTotalNum;

    /**
     * 过去24个月有多少个月存在拖欠账户（贷款）
     */
    @TableField("LAST_24_MON_OWE_ACCT_NUM")
    @XStreamAlias("MONTH_COUNT")
    private String last24MonOweAcctNum;

    /**
     * 以非结清形式销户数
     */
    @TableField("UN_SETT_COLSE_ACCT_NUM")
    @XStreamAlias("CLOSE_ACCT_NUM")
    private String unSettColseAcctNum;

    /**
     * 当前逾期账户数
     */
    @TableField("C_OD_ACCT_NUM")
    @XStreamAlias("ODUE_ACCT_NUM")
    private String cOdAcctNum;

    /**
     * 平均月份间隔（未销户、已激活信用卡及未结清贷款）
     */
    @TableField("AMFAOT")
    @XStreamAlias("MON_ITRV")
    private String amfaot;

    /**
     * 申请月信用卡最大使用的额度比
     */
    @TableField("MU_CC")
    @XStreamAlias("APLY_MON_CRDT_CARD_MAX_PCT")
    private String muCc;

    /**
     * 不良余额比例
     */
    @TableField("BITDILMT")
    @XStreamAlias("BAD_BAL_RTO")
    private String bitdilmt;

    /**
     * 近6个月的最大逾期期数
     */
    @TableField("WDIL6M")
    @XStreamAlias("LAST_SIX_MON_MIX_ODUE_PRD_CNT")
    private String wdil6m;

    /**
     * 近12个月的最大逾期期数
     */
    @TableField("WDIL12M")
    @XStreamAlias("LAST_TWO_MON_MIX_ODUE_PRD_CNT")
    private String wdil12m;

    /**
     * 近24个月的最大逾期期数
     */
    @TableField("WDI24M")
    @XStreamAlias("LTM_MAX_ODUE_PRD_CNT")
    private String wdi24m;

    /**
     * 非正常账户最差状态
     */
    @TableField("WAS")
    @XStreamAlias("BAD_ACCT_ST")
    private String was;

    /**
     * 过多征信查询
     */
    @TableField("LMBNE")
    @XStreamAlias("LSM_OWNED_VLD_QUERY_TMS")
    private String lmbne;

    /**
     * 当前最大逾期期数
     */
    @TableField("CDS")
    @XStreamAlias("CRN_ODUE_PRD_CNT_NO")
    private String cds;

    /**
     * 信用透支额与贷款每月还款额之和
     */
    @TableField("OUMD")
    @XStreamAlias("CONL_PER_MON_RPY_NO_SUM")
    private String oumd;

    /**
     * 信用额与担保贷款额之和
     */
    @TableField("OUMUE")
    @XStreamAlias("CRDT_LMT_WRNT_LOAN_LMT_SUM")
    private String oumue;

    /**
     * 未激活信用卡张数
     */
    @TableField("WJH_CC_NUM")
    @XStreamAlias("UNACT_CRDT_CARD_NO")
    private String wjhCcNum;

    /**
     * 未激活信用卡金额
     */
    @TableField("WJH_CC_AMT")
    private String wjhCcAmt;

    /**
     * 销户信用卡张数
     */
    @TableField("XH_CC_NUM")
    @XStreamAlias("CANCEL_CARD_NUM")
    private String xhCcNum;

    /**
     * 贷款最近6个月平均应还款金额
     */
    @TableField("LOAN_6_MON_AVER_RP_AMT")
    @XStreamAlias("L6M_AVG_DUE_LOAN_AMT")
    private String loan6MonAverRpAmt;

    /**
     * 信用卡最近6个月使用金额
     */
    @TableField("CC_L_6_MON_AVER_USE_AMT")
    @XStreamAlias("L6M_AVG_DUE_CARD_AMT")
    private String ccL6MonAverUseAmt;

    /**
     * 历史贷款逾期笔数
     */
    @TableField("LOAN_HIS_OD_NUM")
    @XStreamAlias("ODUE_TOTAL_NUM")
    private String loanHisOdNum;

    /**
     * 贷款单月最高逾期总额
     */
    @TableField("LOAN_PER_MON_OD_AMT")
    @XStreamAlias("LOAN_MONTH_MAX_ODUE_AMT")
    private String loanPerMonOdAmt;

    /**
     * 信用卡单月最高逾期金额
     */
    @TableField("CC_PER_MON_OD_AMT")
    @XStreamAlias("MONTH_MAX_ODUE_AMT")
    private String ccPerMonOdAmt;

    /**
     * 信用卡最长逾期月数
     */
    @TableField("CC_MAX_OD_MON_NUM")
    @XStreamAlias("OVERDUE_MONTHS")
    private String ccMaxOdMonNum;

    /**
     * 对外担保笔数
     */
    @TableField("EXT_GUAR_NUM")
    @XStreamAlias("ENSURE_NUM")
    private String extGuarNum;

    /**
     * 贷款组合担保笔数占比
     */
    @TableField("LOAN_ZH_GUAN_NUM_RATE")
    @XStreamAlias("CMB_GNT_NUM_RATE")
    private String loanZhGuanNumRate;

    /**
     * 贷款农户联保笔数占比
     */
    @TableField("LOAN_LB_GUAN_NUM_RATE")
    @XStreamAlias("JNT_GNT_NUM_RATE")
    private String loanLbGuanNumRate;

    /**
     * 贷款信用/免担保笔数占比
     */
    @TableField("LOAN_XY_GUAN_NUM_RATE")
    @XStreamAlias("CREDIT_GNT_NUM_RATE")
    private String loanXyGuanNumRate;

    /**
     * 贷款组合担保金额占比
     */
    @TableField("LOAN_ZH_GUAN_AMT_RATE")
    @XStreamAlias("CMB_GNT_BAL_RATE")
    private String loanZhGuanAmtRate;

    /**
     * 贷款农户联保金额占比
     */
    @TableField("LOAN_LB_GUAN_AMT_RATE")
    @XStreamAlias("JNT_GNT_BAL_RATE")
    private String loanLbGuanAmtRate;

    /**
     * 贷款信用/免担保金额占比
     */
    @TableField("LOAN_XY_GUAN_AMT_RATE")
    @XStreamAlias("CREDIT_GNT_BAL_RATE")
    private String loanXyGuanAmtRate;

    /**
     * 最近1个月贷款审核查询记录
     */
    @TableField("LATE_MON_LOAN_COUNT")
    @XStreamAlias("LM_LOAN_INSPCT_QUERY_RCRD")
    private String lateMonLoanCount;

    /**
     * 最近1个月担保资格查询记录
     */
    @TableField("LATE_MON_GUAN_COUNT")
    @XStreamAlias("LM_WRNT_QUALF_QUERY_RCRD")
    private String lateMonGuanCount;

    /**
     * 最近1个月信用卡审批查询记录
     */
    @TableField("LATE_MON_CARD_COUNT")
    @XStreamAlias("LM_CRDT_CARD_APRV_QUERY_RCRD")
    private String lateMonCardCount;

    /**
     * 最近3个月贷款审核查询记录
     */
    @TableField("LATE_SEA_LOAN_COUNT")
    @XStreamAlias("LTM_LOAN_INSPCT_QUERY_RCRD")
    private String lateSeaLoanCount;

    /**
     * 贷款信用/最近3个月担保资格查询记录
     */
    @TableField("LATE_SEA_GUAN_COUNT")
    @XStreamAlias("LTM_WRNT_QUALF_QUERY_RCRD")
    private String lateSeaGuanCount;

    /**
     * 最近3个月信用卡审批查询记录
     */
    @TableField("LATE_SEA_CARD_COUNT")
    @XStreamAlias("LTM_CRDT_CARD_APRV_QUERY_RCRD")
    private String lateSeaCardCount;

    /**
     * 最近6个月贷款审核查询记录
     */
    @TableField("LATE_HLY_LOAN_COUNT")
    @XStreamAlias("LSM_LOAN_INSPCT_QUERY_RCRD")
    private String lateHlyLoanCount;

    /**
     * 最近6个月担保资格查询记录
     */
    @TableField("LATE_HLY_GUAN_COUNT")
    @XStreamAlias("LSM_WRNT_QUALF_QUERY_RCRD")
    private String lateHlyGuanCount;

    /**
     * 最近6个月信用卡审批查询记录
     */
    @TableField("LATE_HLY_CARD_COUNT")
    @XStreamAlias("LSM_CRDT_CARD_APRV_QUERY_RCRD")
    private String lateHlyCardCount;

    /**
     * 最近12个月贷款审核查询记录
     */
    @TableField("LATE_YEAR_LOAN_COUNT")
    @XStreamAlias("LOY_LOAN_INSPCT_QUERY_RCRD")
    private String lateYearLoanCount;

    /**
     * 最近12个月担保资格查询记录
     */
    @TableField("LATE_YEAR_GUAN_COUNT")
    @XStreamAlias("LOY_WRNT_QUALF_QUERY_RCRD")
    private String lateYearGuanCount;

    /**
     * 最近12个月信用卡审批查询记录
     */
    @TableField("LATE_YEAR_CARD_COUNT")
    @XStreamAlias("LOY_CRDT_CARD_APRV_QUERY_RCRD")
    private String lateYearCardCount;

    /**
     * 未来1个月内到期贷款笔数
     */
    @TableField("F_MON_LOAN_COUNT")
    private String fMonLoanCount;

    /**
     * 未来1个月内到期贷款金额
     */
    @TableField("F_MON_LOAN_MONEY")
    private String fMonLoanMoney;

    /**
     * 未来1个月内到期贷款余额
     */
    @TableField("F_MON_LOAN_BALANCE")
    private String fMonLoanBalance;

    /**
     * 未来3个月内到期贷款笔数
     */
    @TableField("F_SEA_LOAN_COUNT")
    private String fSeaLoanCount;

    /**
     * 未来3个月内到期贷款金额
     */
    @TableField("F_SEA_LOAN_MONEY")
    private String fSeaLoanMoney;

    /**
     * 未来3个月内到期贷款余额
     */
    @TableField("F_SEA_LOAN_BALANCE")
    private String fSeaLoanBalance;

    /**
     * 未来6个月内到期贷款笔数
     */
    @TableField("F_HLY_LOAN_COUNT")
    private String fHlyLoanCount;

    /**
     * 未来6个月内到期贷款金额
     */
    @TableField("F_HLY_LOAN_MONEY")
    private String fHlyLoanMoney;

    /**
     * 未来6个月内到期贷款余额
     */
    @TableField("F_HLY_LOAN_BALANCE")
    private String fHlyLoanBalance;

    /**
     * 最近1个月新发放信用卡
     */
    @TableField("LATE_MON_CARD_NUM")
    @XStreamAlias("LM_NEW_GRNT_CRDT_CARD")
    private String lateMonCardNum;

    /**
     * 最近1个月贷款笔数
     */
    @TableField("LATE_MON_LOAN_NUM")
    @XStreamAlias("LAST_MON_LOAN_CNT")
    private String lateMonLoanNum;

    /**
     * 最近1个月贷款授信总额
     */
    @TableField("LATE_MON_CREDIT")
    @XStreamAlias("LAST_MON_CRDT_AMT")
    private String lateMonCredit;

    /**
     * 最近3个月新发放信用卡
     */
    @TableField("LATE_SEA_CARD_NUM")
    @XStreamAlias("LTM_NEW_GRNT_CRDT_CARD")
    private String lateSeaCardNum;

    /**
     * 最近3个月贷款笔数
     */
    @TableField("LATE_SEA_LOAN_NUM")
    @XStreamAlias("LAST_THR_MON_LOAN_CNT")
    private String lateSeaLoanNum;

    /**
     * 最近3个月贷款授信总额
     */
    @TableField("LATE_SEA_CREDIT")
    @XStreamAlias("LAST_THR_MON_CRDT_AMT")
    private String lateSeaCredit;

    /**
     * 最近6个月新发放信用卡
     */
    @TableField("LATE_HLY_CARD_NUM")
    @XStreamAlias("LSM_NEW_GRNT_CRDT_CARD")
    private String lateHlyCardNum;

    /**
     * 最近6个月贷款笔数
     */
    @TableField("LATE_HLY_LOAN_NUM")
    @XStreamAlias("LAST_SIX_MON_LOAN_CNT")
    private String lateHlyLoanNum;

    /**
     * 最近6个月贷款授信总额
     */
    @TableField("LATE_HLY_CREDIT")
    @XStreamAlias("LAST_SIX_MON_CRDT_AMT")
    private String lateHlyCredit;

    /**
     * 他行授信总额
     */
    @TableField("OTH_CRED_TOTAL_AMT")
    @XStreamAlias("OTH_CREDIT_TOT_AMT")
    private String othCredTotalAmt;

    /**
     * 过去2年内贷款最高逾期期数
     */
    @TableField("L_2_Y_LD_MAX_NUM")
    @XStreamAlias("LOAN_HIST_MAX_ODUE_PRDS")
    private String l2YLdMaxNum;

    /**
     * 个人住房贷款余额
     */
    @TableField("HOUSING_LOAN_AMT")
    @XStreamAlias("IDV_HS_LOAN_BAL")
    private String housingLoanAmt;

    /**
     * 个人汽车贷款余额
     */
    @TableField("CAR_LOAN_AMT")
    @XStreamAlias("IDV_CAR_LOAN_BAL")
    private String carLoanAmt;

    /**
     * 个人经营性贷款余额
     */
    @TableField("PER_JYX_LOAN_BALAN")
    @XStreamAlias("IDV_MATURE_LOAN_BAL")
    private String perJyxLoanBalan;

    /**
     * 信用卡最近6个月平均透支率
     */
    @TableField("C_C_6M_AVG_USE_RATE")
    @XStreamAlias("AVG_OVERDRAFT_RATE")
    private String cC6mAvgUseRate;

    /**
     * 最近6个月负债额度
     */
    @TableField("F_6MON_RP_AMT")
    @XStreamAlias("TXN_AMT")
    private String f6monRpAmt;

    /**
     * 最近3个月审核查询机构家数
     */
    @TableField("LATE_3MON_ENQU_ORG_NUM")
    @XStreamAlias("L3M_ENQU_ORG_NUM")
    private String late3monEnquOrgNum;

    /**
     * 最近3个月审核通过机构家数
     */
    @TableField("LATE_3MON_ORG_PASS_NUM")
    @XStreamAlias("L3M_PASS_ORG_NUM")
    private String late3monOrgPassNum;

    /**
     * 最近5年内经营性贷款逾期机构数
     */
    @TableField("LATE_5Y_JYX_LOAN_OD_NUM")
    @XStreamAlias("L5Y_MATURE_ODUE_NUM")
    private String late5yJyxLoanOdNum;

    /**
     * 信用卡总透支率
     */
    @TableField("C_C_ALL_USE_RATE")
    @XStreamAlias("OVERDRAFT_RATE")
    private String cCAllUseRate;

    /**
     * 信用卡累计逾期月份数
     */
    @TableField("CC_SUM_OD_MON_NUM")
    @XStreamAlias("TOTAL_OVERDUE_MONTHS")
    private String ccSumOdMonNum;

    /**
     * 最近2年内非正常担保金
     */
    @TableField("LATE_2YEAR_UNNM_GUAR_AMT")
    @XStreamAlias("L2Y_UNNM_GUAR_AMT")
    private String late2yearUnnmGuarAmt;

    @TableField("LATE_5YEAR_UNNM_GUAR_AMT")
    private String late5yearUnnmGuarAmt;

    /**
     * 剔除贷款单月最高逾期金额
     */
    @TableField("REM_LOAN_CAT_PER_MON_OD_AMT")
    @XStreamAlias("OTHER_LOAN_OVERDUE_AMT")
    private String remLoanCatPerMonOdAmt;

    /**
     * 当前银行服务家数
     */
    @TableField("C_BANK_SERVICE_NUM")
    private String cBankServiceNum;

    /**
     * 剔除贷款类别总授信额度
     */
    @TableField("CREDIT_AMT")
    @XStreamAlias("CREDIT_AMT")
    private String creditAmt;

    /**
     * 剔除贷款类别银行服务家数
     */
    @TableField("REM_LOAN_CAT_BANK_NUM")
    @XStreamAlias("OTHER_LOAN_BANK_NUM")
    private String remLoanCatBankNum;

    /**
     * 最近5年内贷款逾期最长逾期月数
     */
    @TableField("LOAN_HIS_MAX_OD_MON_NUM")
    @XStreamAlias("L5Y_MAX_ODUE_MONTHS")
    private String loanHisMaxOdMonNum;

    /**
     * 过去5年内贷款最高逾期期数
     */
    @TableField("LOAN_HIS_SUM_OD_MON_NUM")
    @XStreamAlias("MAX_OVERDUE_MONTHS")
    private String loanHisSumOdMonNum;

    /**
     * 最近24个月经营性贷款逾期次数
     */
    @TableField("LATE_2Y_JYX_LOAN_OD_NUM")
    @XStreamAlias("LTM_MNGMT_LOAN_ODUE_TMS")
    private String late2yJyxLoanOdNum;

    /**
     * 最近24个月贷款最长逾期月数
     */
    @TableField("LATE_2Y_LOAN_MAX_OD_NUM")
    @XStreamAlias("LTM_LOAN_MAX_ODUE_MNTHS")
    private String late2yLoanMaxOdNum;

    /**
     * 最近6个月信用卡额度使用率(不含未激活)
     */
    @TableField("CRED_CARD_6M_USE_RATE")
    @XStreamAlias("LSM_CREDIT_AMT_USE_RATE")
    private String credCard6mUseRate;

    /**
     * 他行经营性贷款授信机构数
     */
    @TableField("OTHR_BNK_MNGMT_LOAN_ORG_NUM")
    @XStreamAlias("OTHR_BNK_MNGMT_LOAN_FINC_NO")
    private String othrBnkMngmtLoanOrgNum;

    /**
     * 信用卡授信总金额（不含未激活）
     */
    @TableField("CRED_CARD_TOTAL_AMT_OUT_WJQ")
    @XStreamAlias("SMY_CREDIT_AMT")
    private String credCardTotalAmtOutWjq;

    /**
     * 呆账笔数
     */
    @TableField("DZ_ACCT_NUM")
    @XStreamAlias("BAD_DBT_NUM")
    private String dzAcctNum;

    /**
     * 呆账余额
     */
    @TableField("DZ_ACCT_BALAN")
    @XStreamAlias("BAD_DBT_BAL")
    private String dzAcctBalan;

    /**
     * 资产处置笔数
     */
    @TableField("ZC_ASSET_NUM")
    @XStreamAlias("AST_DSPS_NUM")
    private String zcAssetNum;

    /**
     * 资产处置余额
     */
    @TableField("ZC_ASSET_BALAN")
    @XStreamAlias("AST_DSPS_AMT")
    private String zcAssetBalan;

    /**
     * 保证人代偿笔数
     */
    @TableField("GU_COMP_NUM")
    @XStreamAlias("GNR_CMPNSTRY_NUM")
    private String guCompNum;

    /**
     * 保证人代偿余额
     */
    @TableField("GU_COMP_BALAN")
    @XStreamAlias("GNR_CMPNSTRY_BAL")
    private String guCompBalan;

    /**
     * 当前逾期金额(信用卡+贷款）
     */
    @TableField("CURR_UNSETT_OD_BALAN")
    @XStreamAlias("CRN_ODUE_AMT")
    private String currUnsettOdBalan;

    /**
     * 最近24个月逾期次数
     */
    @TableField("LATE_2Y_ALL_OD_NUM")
    @XStreamAlias("LTM_ODUE_TMS")
    private String late2yAllOdNum;

    /**
     * 最近24个月信用卡逾期次数
     */
    @TableField("LATE_2Y_CARD_OD_NUM")
    @XStreamAlias("LTM_CRDT_CARD_ODUE_TMS")
    private String late2yCardOdNum;

    /**
     * 最近24个月消费性贷款逾期次数
     */
    @TableField("LATE_2Y_XFX_LOAN_OD_NUM")
    @XStreamAlias("LTM_CNSMPTN_LOAN_ODUE_TMS")
    private String late2yXfxLoanOdNum;

    /**
     * 近2年最长逾期月数
     */
    @TableField("LATE_2Y_MAX_OD_NUM")
    @XStreamAlias("LTY_MAX_ODUE_MNTHS")
    private String late2yMaxOdNum;

    /**
     * 最近24个月经营性贷款最长逾期月数
     */
    @TableField("LATE_2Y_JYX_LOAN_MAX_OD_NUM")
    @XStreamAlias("LTM_MNGMT_LOAN_MAX_ODUE_MNTHS")
    private String late2yJyxLoanMaxOdNum;

    /**
     * 最近24个月消费性贷款最长逾期月数
     */
    @TableField("LATE_2Y_XFX_LOAN_MAX_OD_NUM")
    @XStreamAlias("LTM_CNSMPTN_LAN_MX_ODUE_MNTHS")
    private String late2yXfxLoanMaxOdNum;

    /**
     * 最近24个月信用卡最长逾期月数
     */
    @TableField("LATE_2Y_CARD_MAX_OD_NUM")
    @XStreamAlias("LTM_CRDT_CARD_MAX_ODUE_MNTHS")
    private String late2yCardMaxOdNum;

    /**
     * 最近24个月贷记卡最长逾期月数
     */
    @TableField("LATE_2Y_DJK_MAX_OD_NUM")
    @XStreamAlias("LTM_CR_CARD_MAX_ODUE_MNTHS")
    private String late2yDjkMaxOdNum;

    /**
     * 最近24个月准贷记卡最长逾期月数
     */
    @TableField("LATE_2Y_ZDJK_MAX_OD_NUM")
    @XStreamAlias("LTM_SM_CRDT_CARD_MX_OD_MNTHS")
    private String late2yZdjkMaxOdNum;

    /**
     * 贷款五级分类为非正常的余额
     */
    @TableField("UN_NORMAL_LOAN_BALAN")
    @XStreamAlias("UNNRL_BAL")
    private String unNormalLoanBalan;

    /**
     * 贷款五级分类为非正常的笔数
     */
    @TableField("UN_NORMAL_LOAN_NUM")
    @XStreamAlias("UNNRL_NUM")
    private String unNormalLoanNum;

    /**
     * 他行授信机构数(不含未激活)
     */
    @TableField("OTH_BANK_BUSI_ORG_NUM")
    @XStreamAlias("OTHR_BNK_CRDT_ORG_ACTVT_CNT")
    private String othBankBusiOrgNum;

    /**
     * 他行授信总余额(不含未激活)
     */
    @TableField("OTH_BANK_CREDIT_TOTAL_AMT")
    @XStreamAlias("OTHR_BNK_CRDT_ACTVT_TOT_BAL")
    private String othBankCreditTotalAmt;

    /**
     * 信用卡使用额度
     */
    @TableField("CRED_CARD_USE_AMT")
    private String credCardUseAmt;

    /**
     * 最近60个月贷记卡最长逾期月数
     */
    @TableField("LATE_5Y_DJK_MAX_OD_NUM")
    @XStreamAlias("LSTM_CR_CARD_MAX_ODUE_MNTHS")
    private String late5yDjkMaxOdNum;

    /**
     * 最近60个月准贷记卡最长逾期月数
     */
    @TableField("LATE_5Y_ZDJK_MAX_OD_NUM")
    @XStreamAlias("LSTM_SM_CRDT_CARD_MX_OD_MNTHS")
    private String late5yZdjkMaxOdNum;

    /**
     * 最近6个月金融机构审批通过家数
     */
    @TableField("LATE_6MON_ORG_PASS_NUM")
    @XStreamAlias("LSM_FNNC_ORG_APPR_PS_CNT")
    private String late6monOrgPassNum;

    /**
     * 最近6个月审批查询金融机构家数
     */
    @TableField("LATE_6MON_ENQU_ORG_NUM")
    @XStreamAlias("LSM_APPR_FNNC_ORG_CNT")
    private String late6monEnquOrgNum;

    /**
     * 强制执行记录条数
     */
    @TableField("FORCE_DO_RECORD_NUM")
    @XStreamAlias("FRC_EXCT_RCRD_CNT")
    private String forceDoRecordNum;

    /**
     * 欠税记录数
     */
    @TableField("OWING_TAXES_RECORD_NUM")
    private String owingTaxesRecordNum;

    /**
     * 民事判决记录数
     */
    @TableField("CIVIL_JUDGEMENT_RECORD_NUM")
    private String civilJudgementRecordNum;

    /**
     * 行政处罚记录数
     */
    @TableField("ADMIN_PUNISH_RECORD_NUM")
    private String adminPunishRecordNum;

    /**
     * 电信欠费记录数
     */
    @TableField("TELE_ARREARAGE_RECORD_NUM")
    private String teleArrearageRecordNum;

    /**
     * 当前经营性贷款逾期金额
     */
    @TableField("SETT_JYX_LOAN_OD_AMT")
    @XStreamAlias("CRN_MNGMT_LOAN_ODUE_AMT")
    private String settJyxLoanOdAmt;

    /**
     * 当前非正常对外贷款担保金额
     */
    @TableField("CURR_BAD_EXTGUAN_LOAN_AMT")
    @XStreamAlias("CRN_UNNRL_OT_LOAN_WRNT_AMT")
    private String currBadExtguanLoanAmt;

    /**
     * 授信机构数（不含未激活）
     */
    @TableField("CRED_BUSI_ORG_OUT_WJQ")
    @XStreamAlias("CRDT_ORG_ACTVT_CNT")
    private String credBusiOrgOutWjq;

    /**
     * 最近60个月经营性贷款授信机构数
     */
    @TableField("LATE_60M_JYX_LOAN_ORGNUM")
    @XStreamAlias("LSTM_MNGMT_LOAN_CRDT_ORG_CNT")
    private String late60mJyxLoanOrgnum;

    /**
     * 信用卡授信机构数（不含未激活）
     */
    @TableField("CRED_CARD_ORGNUM_OUT_WJQ")
    @XStreamAlias("CRDT_CARD_CRDT_ORG_ACTVT_CNT")
    private String credCardOrgnumOutWjq;

    /**
     * 授信总余额（不含未激活）
     */
    @TableField("CRED_TOTAL_BALAN_OUT_WJQ")
    @XStreamAlias("CRDT_ACTVT_TOT_BAL")
    private String credTotalBalanOutWjq;

    /**
     * 信用卡张数（不含未激活）
     */
    @TableField("CRED_CARD_NUM_OUT_WJQ")
    @XStreamAlias("CRDT_CARD_ACTVT_CNT")
    private String credCardNumOutWjq;

    /**
     * 信用卡使用张数
     */
    @TableField("CRED_CARD_USED_NUM")
    @XStreamAlias("CRDT_CARD_USE_CNT")
    private String credCardUsedNum;

    /**
     * 最近3个月审批通过机构比例
     */
    @TableField("LATE_3M_APPROVE_PASS_RATE")
    @XStreamAlias("L3M_APROVE_PASS_RATE")
    private String late3mApprovePassRate;

    /**
     * 是否有预警信息
     */
    @TableField("EARLY_WARNING_INFO_NUM")
    @XStreamAlias("WRNG_MSG_FLG")
    private String earlyWarningInfoNum;

    /**
     * 预警明细信息
     */
    @TableField("EARLY_WARNING_INFO_CONTEXT")
    @XStreamAlias("WRNG_DTL_MSG")
    private String earlyWarningInfoContext;

    /**
     * 个人经营性贷款余额
     */
    @TableField("CURR_JYX_LOAN_BALAN")
    @XStreamAlias("IDV_MATD_LOAN_BAL")
    private String currJyxLoanBalan;

    /**
     * 最近5年内经营性贷款逾期银行家数
     */
    @TableField("LATE_5Y_JYX_LOAN_OD_BK_NUM")
    @XStreamAlias("LFY_MNGE_LOAN_ODUE_BNK_NO")
    private String late5yJyxLoanOdBkNum;

    /**
     * 最近2年内经营性贷款单月最高逾期金额
     */
    @TableField("L_2Y_JYX_LOAN_PMON_MAX_OD_AMT")
    @XStreamAlias("LTY_MNGE_LOAN_MON_ODUE_AMT")
    private String l2yJyxLoanPmonMaxOdAmt;

    /**
     * 授信余额(经营性贷款+已激活信用卡)
     */
    @TableField("CREDIT_EXTENSION_BALAN_OUT_WJH")
    @XStreamAlias("CRDT_BAL")
    private String creditExtensionBalanOutWjh;

    /**
     * 他行贷款授信机构数
     */
    @TableField("OTH_BANK_LOAN_ORG_NUM")
    @XStreamAlias("OTHR_BNK_LOAN_CRDT_ORG_CNT")
    private String othBankLoanOrgNum;

    /**
     * 他行贷款授信余额
     */
    @TableField("OTH_BANK_LOAN_CREDIT_BAL")
    @XStreamAlias("OTHR_BNK_LOAN_CRDT_BAL")
    private String othBankLoanCreditBal;

    /**
     * 他行经营性贷款授信余额
     */
    @TableField("OTH_BANK_MNGMT_LOAN_BAL")
    @XStreamAlias("OTHR_BNK_MNGE_LOAN_CRDT_BAL")
    private String othBankMngmtLoanBal;

    /**
     * 他行消费性贷款授信机构数
     */
    @TableField("OTH_BANK_XFX_LOAN_ORG_NUM")
    @XStreamAlias("OTHR_BNK_CNSMPTN_CRDT_ORG_CNT")
    private String othBankXfxLoanOrgNum;

    /**
     * 他行消费性贷款授信余额
     */
    @TableField("OTH_BANK_XFX_LOAN_BAL")
    @XStreamAlias("OTHR_BNK_CNSMPTN_CRDT_BAL")
    private String othBankXfxLoanBal;

    /**
     * 近3个月新增授信
     */
    @TableField("LATE_SEA_ADD_AMT")
    private String lateSeaAddAmt;

    /**
     * 近3个月到期结清授信
     */
    @TableField("LATE_SEA_END_SETT_AMT")
    private String lateSeaEndSettAmt;

    /**
     * 近3个月新增授信比例
     */
    @TableField("LATE_SEA_ADD_AMT_RATE")
    private String lateSeaAddAmtRate;

    /**
     * 近3个月新增银行合作家数
     */
    @TableField("LATE_SEA_ADD_BANK_NUM")
    private String lateSeaAddBankNum;

    /**
     * 近3个月终止合作银行家数
     */
    @TableField("LATE_SEA_END_BANK_NUM")
    private String lateSeaEndBankNum;

    /**
     * 近3个月新增银行合作家数比例
     */
    @TableField("LATE_SEA_ADD_BANK_RATE")
    private String lateSeaAddBankRate;

    /**
     * 最近3个月到期的贷款笔数
     */
    @TableField("LATE_SEA_END_LOAN_NUM")
    private String lateSeaEndLoanNum;

    /**
     * 非正常账户数
     */
    @TableField("UN_NORMAL_ACCOUNT_NUM")
    @XStreamAlias("ABNRL_ACCT_CNT")
    private String unNormalAccountNum;

    /**
     * 未来3个月信用卡到期笔数
     */
    @TableField("F_SEA_CARD_COUNT")
    private String fSeaCardCount;

    /**
     * 最近2个月信用卡到期笔数
     */
    @TableField("LATE_2_M_CARD_NUM")
    private String late2MCardNum;

    /**
     * 最近2个月贷款到期笔数
     */
    @TableField("LATE_2_M_LOAN_NUM")
    private String late2MLoanNum;

    /**
     * 近两年办理过住房贷款的笔数
     */
    @TableField("LATE_2_Y_HOUSING_LOAN_NUM")
    private String late2YHousingLoanNum;

    /**
     * 非正常贷款对外担保笔数
     */
    @TableField("CURR_BAD_EXTGUAN_LOAN_NUM")
    private String currBadExtguanLoanNum;

    /**
     * 当前贷款逾期期数
     */
    @TableField("CURR_UNSETT_LOAN_OD_NUM")
    @XStreamAlias("LOAN_ODUE_PRD_CNT")
    private String currUnsettLoanOdNum;

    /**
     * 当前信用卡逾期期数
     */
    @TableField("CURR_UNSETT_CARD_OD_NUM")
    @XStreamAlias("CRDT_CARD_ODUE_PRD_CNT")
    private String currUnsettCardOdNum;

    /**
     * 当前贷款最高逾期金额
     */
    @TableField("CURR_UNSETT_LOAN_MAX_OD_AMT")
    @XStreamAlias("LOAN_HIGH_ODUE_AMT")
    private String currUnsettLoanMaxOdAmt;

    /**
     * 当前信用卡最高逾期金额
     */
    @TableField("CURR_UNSETT_CARD_MAX_OD_AMT")
    @XStreamAlias("CRDT_CARD_HIGH_ODUE_AMT")
    private String currUnsettCardMaxOdAmt;

    /**
     * 特殊交易（“提前结清”的除外）的贷款笔数
     */
    @TableField("SPEC_WJQ_LOAN_NUM")
    @XStreamAlias("SPCL_TXN_LOAN_NUM")
    private String specWjqLoanNum;

    /**
     * 特殊交易类型为以资抵债总次数
     */
    @TableField("SPEC_YZDZ_LOAN_NUM")
    @XStreamAlias("SPCL_TXN_TP_TMS")
    private String specYzdzLoanNum;

    /**
     * 贷记卡状态为“呆账”或“止付”或“冻结”账户数
     */
    @TableField("DJK_DZ_OR_ZF_OR_DJ_ACC_NUM")
    @XStreamAlias("CRDT_CARD_ACCT_CNT")
    private String djkDzOrZfOrDjAccNum;

    /**
     * 近2年信用卡最大连续逾期月份数
     */
    @TableField("LATE_2Y_CARD_MAX_SERIAL_OD_NUM")
    @XStreamAlias("LTY_CRDT_CARD_ODUE_MNTHS")
    private String late2yCardMaxSerialOdNum;

    /**
     * 近2个月消费金融公司等非银行金融机构发放贷款的笔数
     */
    @TableField("LATE_2M_LOAN_FYH_NUM")
    @XStreamAlias("LTWM_GRNT_LOAN_NUM")
    private String late2mLoanFyhNum;

    /**
     * 近1年消费金融公司等非银行金融机构发放贷款的笔数
     */
    @TableField("LATE_1Y_LOAN_FYH_NUM")
    @XStreamAlias("LOY_GRNT_LOAN_NUM")
    private String late1yLoanFyhNum;

    /**
     * 近2年信用卡最大同月逾期卡张数
     */
    @TableField("LATE_2Y_CARD_MAX_MON_OD_NUM")
    @XStreamAlias("LTY_CRDT_CARD_ODUE_NO")
    private String late2yCardMaxMonOdNum;

    /**
     * 近两年信用卡逾期账户数
     */
    @TableField("LATE_2Y_CARD_OD_ACC_NUM")
    @XStreamAlias("NTY_CRDT_CARD_ODUE_ACCT_NUM")
    private String late2yCardOdAccNum;

    /**
     * 近两年信用卡最高逾期金额
     */
    @TableField("LATE_2Y_CARD_MAX_OD_AMT")
    @XStreamAlias("NTY_CRDT_CARD_HIGH_ODUE_AMT")
    private String late2yCardMaxOdAmt;

    /**
     * 首次除贷款和信用卡发生业务日期
     */
    @TableField("FIRST_XZDJK_RELEASE_DATE")
    private String firstXzdjkReleaseDate;

    /**
     * 未结清非循环贷账户授信总额
     */
    @TableField("UN_SETT_FXH_CREDIT_AMT")
    private String unSettFxhCreditAmt;

    /**
     * 未结清循环额度下分账户授信总额
     */
    @TableField("UN_SETT_XHF_CREDIT_AMT")
    private String unSettXhfCreditAmt;

    /**
     * 未销户循环贷账户授信总额
     */
    @TableField("UN_CLO_XH_CREDIT_AMT")
    private String unCloXhCreditAmt;

    /**
     * 未销户贷记卡账户授信总额
     */
    @TableField("UN_CLO_DJ_CREDIT_AMT")
    private String unCloDjCreditAmt;

    /**
     * 未销户准贷记卡账户总授信额度
     */
    @TableField("UN_CLO_ZDJ_CREDIT_AMT")
    private String unCloZdjCreditAmt;

    /**
     * 未结清非循环贷账户总余额
     */
    @TableField("UN_SETT_FXHD_ALL_BALAN")
    private String unSettFxhdAllBalan;

    /**
     * 未结清循环额度下分账户总余额
     */
    @TableField("UN_SETT_XHF_ALL_BALAN")
    private String unSettXhfAllBalan;

    /**
     * 未销户循环贷账户总本金余额
     */
    @TableField("UN_SETT_XHD_PRIN_BALAN")
    private String unSettXhdPrinBalan;

    /**
     * 未销户贷记卡账户总已用额度
     */
    @TableField("UN_CLO_DJ_USED_AMT")
    private String unCloDjUsedAmt;

    /**
     * 未销户准贷记卡账户总已用额度
     */
    @TableField("UN_CLO_ZDJ_USED_AMT")
    private String unCloZdjUsedAmt;

    /**
     * 未销户贷记卡账户数
     */
    @TableField("UN_CLO_DJ_ACCOUNT_NUM")
    private String unCloDjAccountNum;

    /**
     * 未销户准贷记卡账户数
     */
    @TableField("UN_CLO_ZDJ_ACCOUNT_NUM")
    private String unCloZdjAccountNum;

    /**
     * 3个月内人民币信用卡发卡数
     */
    @TableField("LATE_3MON_ISSUE_RMB_CARD_NUM")
    private String late3monIssueRmbCardNum;

    /**
     * 正在使用信用卡账户数
     */
    @TableField("USE_CREDIT_CARD_NUM")
    private String useCreditCardNum;

    /**
     * 贷记卡账户最大使用率
     */
    @TableField("C_DJK_ACCOUNT_MAX_USE_RATE")
    private String cDjkAccountMaxUseRate;

    /**
     * 准贷记卡账户最大使用率
     */
    @TableField("C_ZDJK_ACCOUNT_MAX_USE_RATE")
    private String cZdjkAccountMaxUseRate;

    /**
     * 当前贷记卡逾期总额求和
     */
    @TableField("CURR_CREDIT_CARD_DUE_SUM")
    private String currCreditCardDueSum;

    /**
     * 准贷记卡透支180天以上未付余额总额
     */
    @TableField("UN_PAY_180_MORE_BALAN_SUM")
    private String unPay180MoreBalanSum;

    /**
     * 贷记卡账户最高逾期期数
     */
    @TableField("C_DUE_DJK_MAX_NUM")
    private String cDueDjkMaxNum;

    /**
     * 准贷记卡账户最高逾期期数
     */
    @TableField("C_DUE_ZDJK_MAX_NUM")
    private String cDueZdjkMaxNum;

    /**
     * 贷记卡账户两年内逾期期数大于等于3的账户数
     */
    @TableField("LATE_2Y_DJK_DUE_3_ACCOUNT_NUM")
    private String late2yDjkDue3AccountNum;

    /**
     * 准贷记卡账户两年内逾期期数大于等于3的账户数
     */
    @TableField("LATE_2Y_ZDJK_DUE_3_ACCOUNT_NUM")
    private String late2yZdjkDue3AccountNum;

    /**
     * 未结清非循环贷账户数
     */
    @TableField("UN_SETT_FXHD_ACCOUNT_NUM")
    private String unSettFxhdAccountNum;

    /**
     * 未结清循环额度下分账户账户数
     */
    @TableField("UN_SETT_XHF_ACCOUNT_NUM")
    private String unSettXhfAccountNum;

    /**
     * 未结清循环贷账户数
     */
    @TableField("UN_SETT_XHD_ACCOUNT_NUM")
    private String unSettXhdAccountNum;

    /**
     * 将来2个月非循环贷账户到期余额总和
     */
    @TableField("F_2MON_FXHD_EXPIRE_BALAN_SUM")
    private String f2monFxhdExpireBalanSum;

    /**
     * 将来2个月循环贷账户到期余额总和
     */
    @TableField("F_2MON_XHD_EXPIRE_BALAN_SUM")
    private String f2monXhdExpireBalanSum;

    /**
     * 将来2个月循环额度下分账户到期余额总和
     */
    @TableField("F_2MON_XHF_EXPIRE_BALAN_SUM")
    private String f2monXhfExpireBalanSum;

    /**
     * 未结清经营性贷款借款金额合计
     */
    @TableField("UN_SETT_JYX_LOAN_MONEY")
    private String unSettJyxLoanMoney;

    /**
     * 非循环贷账户开立日期距离报告期2个月的内的账户数
     */
    @TableField("LATE_2MON_FXHD_RE_OPEN_NUM")
    private String late2monFxhdReOpenNum;

    /**
     * 循环额度下分账户开立日期距离报告期2个月的内的账户数
     */
    @TableField("LATE_2MON_XHF_RE_OPEN_NUM")
    private String late2monXhfReOpenNum;

    /**
     * 循环贷账户开立日期距离报告期2个月的内的账户数
     */
    @TableField("LATE_2MON_XHD_RE_OPEN_NUM")
    private String late2monXhdReOpenNum;

    /**
     * 当前非循环贷账户逾期总额
     */
    @TableField("C_FXHD_LOAN_DUE_AMT")
    private String cFxhdLoanDueAmt;

    /**
     * 当前循环贷账户逾期总额
     */
    @TableField("C_XHF_LOAN_DUE_AMT")
    private String cXhfLoanDueAmt;

    /**
     * 当前循环贷账户逾期总额
     */
    @TableField("C_XHD_LOAN_DUE_AMT")
    private String cXhdLoanDueAmt;

    /**
     * 当前非循环贷账户最大逾期期数
     */
    @TableField("C_FXHD_LOAN_MAX_DUE_NUM")
    private String cFxhdLoanMaxDueNum;

    /**
     * 当前循环额度下分账户最大逾期期数
     */
    @TableField("C_XHF_LOAN_MAX_DUE_NUM")
    private String cXhfLoanMaxDueNum;

    /**
     * 当前循环贷账户最大逾期期数
     */
    @TableField("C_XHD_LOAN_MAX_DUE_NUM")
    private String cXhdLoanMaxDueNum;

    /**
     * 当前非循环贷账户逾期账户总数
     */
    @TableField("C_FXHD_LOAN_DUE_SUM")
    private String cFxhdLoanDueSum;

    /**
     * 当前循环额度下分账户逾期账户总数
     */
    @TableField("C_XHF_LOAN_DUE_SUM")
    private String cXhfLoanDueSum;

    /**
     * 当前循环贷账户逾期账户总数
     */
    @TableField("C_XHD_LOAN_DUE_SUM")
    private String cXhdLoanDueSum;

    /**
     * 非循环贷账户担保方式为“抵押”的账户数
     */
    @TableField("ASS_FXHD_DY_COUNT_NUM")
    private String assFxhdDyCountNum;

    /**
     * 循环额度下分账户担保方式为“抵押”的账户数
     */
    @TableField("ASS_XHF_DY_COUNT_NUM")
    private String assXhfDyCountNum;

    /**
     * 循环贷账户担保方式为“抵押”的账户数
     */
    @TableField("ASS_XHD_DY_COUNT_NUM")
    private String assXhdDyCountNum;

    /**
     * 非循环贷账户担保方式为“质押”的账户数
     */
    @TableField("ASS_FXHD_ZY_COUNT_NUM")
    private String assFxhdZyCountNum;

    /**
     * 循环额度下分账户担保方式为“质押”的账户数
     */
    @TableField("ASS_XHF_ZY_COUNT_NUM")
    private String assXhfZyCountNum;

    /**
     * 循环贷账户担保方式为“质押”的账户数
     */
    @TableField("ASS_XHD_ZY_COUNT_NUM")
    private String assXhdZyCountNum;

    /**
     * 非循环贷账户担保方式为“保证”的账户数
     */
    @TableField("ASS_FXHD_BZ_COUNT_NUM")
    private String assFxhdBzCountNum;

    /**
     * 循环额度下分账户担保方式为“保证”的账户数
     */
    @TableField("ASS_XHF_BZ_COUNT_NUM")
    private String assXhfBzCountNum;

    /**
     * 循环贷账户担保方式为“保证”的账户数
     */
    @TableField("ASS_XHD_BZ_COUNT_NUM")
    private String assXhdBzCountNum;

    /**
     * 非循环贷账户担保方式为“信用/免担保，组合（含保证），组合（不含保证），农户联保，其他”的账户数
     */
    @TableField("ASS_FXHD_CREDIT_COUNT_NUM")
    private String assFxhdCreditCountNum;

    /**
     * 循环额度下分账户担保方式为“信用/免担保，组合（含保证），组合（不含保证），农户联保，其他”的账户数
     */
    @TableField("ASS_XHF_CREDIT_COUNT_NUM")
    private String assXhfCreditCountNum;

    /**
     * 循环贷账户担保方式为“信用/免担保，组合（含保证），组合（不含保证），农户联保，其他”的账户数
     */
    @TableField("ASS_XHD_CREDIT_COUNT_NUM")
    private String assXhdCreditCountNum;

    /**
     * 非循环贷账户担保方式为“质押”的借款金额总和
     */
    @TableField("ASS_FXHD_ZY_LOAN_MONEY")
    private String assFxhdZyLoanMoney;

    /**
     * 循环额度下分账户担保方式为“质押”的借款金额总和
     */
    @TableField("ASS_XHF_ZY_LOAN_MONEY")
    private String assXhfZyLoanMoney;

    /**
     * 循环贷账户担保方式为“质押”的借款金额总和
     */
    @TableField("ASS_XHD_ZY_LOAN_MONEY")
    private String assXhdZyLoanMoney;

    /**
     * 非循环贷账户担保方式为“抵押”的借款金额总和
     */
    @TableField("ASS_FXHD_DY_LOAN_MONEY")
    private String assFxhdDyLoanMoney;

    /**
     * 循环额度下分账户担保方式为“抵押”的借款金额总和
     */
    @TableField("ASS_XHF_DY_LOAN_MONEY")
    private String assXhfDyLoanMoney;

    /**
     * 循环贷账户担保方式为“抵押”的借款金额总和
     */
    @TableField("ASS_XHD_DY_LOAN_MONEY")
    private String assXhdDyLoanMoney;

    /**
     * 非循环贷账户担保方式为“保证”的借款金额总和
     */
    @TableField("ASS_FXHD_BZ_LOAN_MONEY")
    private String assFxhdBzLoanMoney;

    /**
     * 循环额度下分账户担保方式为“保证”的借款金额总和
     */
    @TableField("ASS_XHF_BZ_LOAN_MONEY")
    private String assXhfBzLoanMoney;

    /**
     * 循环贷账户担保方式为“保证”的借款金额总和
     */
    @TableField("ASS_XHD_BZ_LOAN_MONEY")
    private String assXhdBzLoanMoney;

    /**
     * 非循环贷账户担保方式为“信用/免担保，组合（含保证），组合（不含保证），农户联保，其他”的借款金额总和
     */
    @TableField("ASS_FXHD_CREDIT_LOAN_MONEY")
    private String assFxhdCreditLoanMoney;

    /**
     * 循环额度下分账户担保方式为“信用/免担保，组合（含保证），组合（不含保证），农户联保，其他”的借款金额总和
     */
    @TableField("ASS_XHF_CREDIT_LOAN_MONEY")
    private String assXhfCreditLoanMoney;

    /**
     * 循环贷账户担保方式为“信用/免担保，组合（含保证），组合（不含保证），农户联保，其他”的借款金额总和
     */
    @TableField("ASS_XHD_CREDIT_LOAN_MONEY")
    private String assXhdCreditLoanMoney;

    /**
     * 非循环贷账户24个月还款记录有逾期情况的账户总数
     */
    @TableField("LATE_2Y_FXHD_DUE_COUNT_SUM")
    private String late2yFxhdDueCountSum;

    /**
     * 循环额度下分账户24个月还款记录有逾期情况的账户总数
     */
    @TableField("LATE_2Y_XHF_DUE_COUNT_SUM")
    private String late2yXhfDueCountSum;

    /**
     * 循环贷账户24个月还款记录有逾期情况的账户总数
     */
    @TableField("LATE_2Y_XHD_DUE_COUNT_SUM")
    private String late2yXhdDueCountSum;

    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    @TableField("PER_LOAN_INFO")
    private String perLoanInfo;

    /***********************************************非表中字段****************************************/
    /**
     * 最近3个月审批通过机构比例
     */
    @XStreamAlias("LTM_APPR_PS_ORG_RTO")
    private String ltmApprPsOrgOrto;

    /**
     * 逾期信用卡张数
     */
    @XStreamAlias("ODUE_CARD_NUM")
    private String odueCardNum;

    /**
     * 最近5年内最长逾期月数
     */
    @XStreamAlias("L5Y_MAX_ODUE_MONTHS")
    private String l5yMaxOdueMonths;

    /**
     * 近2年经营性贷款逾期次数
     */
    @XStreamAlias("NTY_MATURE_ODUE_NUM")
    private String ntyMatureOdueNum;

    /**
     * 近2年经营性贷款逾期次数
     */
    @XStreamAlias("NTY_LOAN_ODUE_MON_NO")
    private String ntyLoanOdueMonNo;
}
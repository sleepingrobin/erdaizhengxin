package com.dhcc.cips.po.other;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.io.Serializable;


/**
 * 预警项PO
 */
@Data
public class AlarmConfigInfoPo extends Model<AlarmConfigInfoPo> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String cId;
    private String userType;
    private String userName;
    private String alarmName;
    private String alarmRule;
}

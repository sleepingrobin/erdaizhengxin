package com.dhcc.cips.po.company;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.dhcc.cips.bo.company.CompanyExtGuarantyInfoBo;
import com.dhcc.cips.bo.company.CompanyLoanInfoBo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Data;

/**
 * <p>
 * 企业指标PO
 * </p>
 *
 * @since 2019-06-11
 */
@Data
@TableName("ZX_C_ZB_INFO")
public class CompanyZBInfoPo extends Model<CompanyZBInfoPo> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 报告编号（主键）
     */
    @TableId("REPORT_NO")
    @XStreamAlias("RPRT_NO")
    private String reportNo;

    /**
     * 注册资金
     */
    @TableField("REGIST_AMT")
    @XStreamAlias("RGST_FND")
    private String registAmt;

    /**
     * 资本集中度
     */
    @TableField("REGIST_MAX_AMT_RATE")
    @XStreamAlias("CPTL_CNCTRT_RTO")
    private String registMaxAmtRate;

    /**
     * 出资方数量
     */
    @TableField("REGIST_NUM")
    @XStreamAlias("SPNSR_CNT")
    private String registNum;

    /**
     * 平均出资金额
     */
    @TableField("REGIST_AVER_AMT")
    @XStreamAlias("AV_CTB_AMT")
    private String registAverAmt;

    /**
     * 信贷业务种类数
     */
    @TableField("CRED_LOAN_TYPE_NUM")
    @XStreamAlias("LOAN_BUSS_VRTY_CNT")
    private String credLoanTypeNum;

    /**
     * 信用总发生额（未结清信贷业务）
     */
    @TableField("CRED_TOTAL_AMT")
    @XStreamAlias("CRDT_TOT_OCCR_AMT")
    private String credTotalAmt;

    /**
     * 信用总余额（未结清信贷业务）
     */
    @TableField("CRED_TOTAL_BALAN")
    @XStreamAlias("CRDT_TOT_BAL")
    private String credTotalBalan;

    /**
     * 信用业务笔数（未结清信贷业务）
     */
    @TableField("CRED_BIZ_NUM")
    @XStreamAlias("CRDT_BUSS_NUM")
    private String credBizNum;

    /**
     * 贷款业务总发生额/未结清信用总发生额
     */
    @TableField("L_UN_SETT_CL_TOTAL_AMT_RATE")
    @XStreamAlias("LOAN_ON_UNCL_TOT_AMT_PCT")
    private String lUnSettClTotalAmtRate;

    /**
     * 贷款业务总余额/未结清信用总余额
     */
    @TableField("L_UN_SETT_CL_TOTAL_BALA_RATE")
    @XStreamAlias("LOAN_ON_UNCL_TOT_BAL_PCT")
    private String lUnSettClTotalBalaRate;

    /**
     * 贷款业务最高额度（未结清信贷业务）
     */
    @TableField("LOAN_MAX_AMT")
    @XStreamAlias("HIGH_LOAN_LMT")
    private String loanMaxAmt;

    /**
     * 平均贷款额度（未结清贷款业务）
     */
    @TableField("LOAN_AVER_AMT")
    @XStreamAlias("AV_LOAN_LMT")
    private String loanAverAmt;

    /**
     * 未结清不良信贷信用总发生额
     */
    @TableField("UN_SETT_BAD_CL_TOTAL_AMT")
    @XStreamAlias("UNCL_BAD_CRDT_TOT_AMT")
    private String unSettBadClTotalAmt;

    /**
     * 未结清不良信贷信用总余额
     */
    @TableField("UN_SETT_BAD_CL_TOTAL_BALAN")
    @XStreamAlias("UNCL_BAD_CRDT_TOT_BAL")
    private String unSettBadClTotalBalan;

    /**
     * 未结清不良信用业务笔数
     */
    @TableField("UN_SETT_BAD_CL_BIZ_NUM")
    @XStreamAlias("UNCL_BAD_CRDT_BUSS_NUM")
    private String unSettBadClBizNum;

    /**
     * 未结清不良总余额占未结清信用总余额比例
     */
    @TableField("UN_SETT_BAD_BALAN_RATE")
    @XStreamAlias("UNCL_BAD_ON_CRDT_TOT_BAL_PCT")
    private String unSettBadBalanRate;

    /**
     * 未结清不良总笔数占未结清信贷总笔数比例
     */
    @TableField("UN_SETT_BAD_NUM_RATE")
    @XStreamAlias("UNCL_BAD_ON_CRDT_TOT_NUM_PCT")
    private String unSettBadNumRate;

    /**
     * 已结清信用总发生额
     */
    @TableField("SETT_CL_TOTAL_AMT")
    @XStreamAlias("ALCL_CRDT_TOT_OCCR_AMT")
    private String settClTotalAmt;

    /**
     * 已结清信用业务笔数
     */
    @TableField("SETT_CL_TOTAL_NUM")
    @XStreamAlias("ALCL_CRDT_BUSS_NUM")
    private String settClTotalNum;

    /**
     * 已结清贷款业务总发生额/已结清信用总发生额
     */
    @TableField("SETT_L_SETT_CL_AMT_RATE")
    @XStreamAlias("ALCL_LOAN_ON_CRDT_TOT_AMT_PCT")
    private String settLSettClAmtRate;

    /**
     * 已结清贷款业务最高额度
     */
    @TableField("SETT_LOAN_MAX_AMT")
    @XStreamAlias("ALCL_HIGH_LOAN_HIGH_LMT")
    private String settLoanMaxAmt;

    /**
     * 已结清平均贷款额度
     */
    @TableField("SETT_LOAN_AVER_AMT")
    @XStreamAlias("ALCL_AV_LOAN_LMT")
    private String settLoanAverAmt;

    /**
     * 已结清不良信贷信用总发生额
     */
    @TableField("SETT_BAD_BIZ_TOTAL_AMT")
    @XStreamAlias("ALCL_BAD_CRDT_TOT_OCCR_AMT")
    private String settBadBizTotalAmt;

    /**
     * 已结清不良信用业务笔数
     */
    @TableField("SETT_BAD_BIZ_NUM")
    @XStreamAlias("ALCL_BAD_CRDT_BUSS_NUM")
    private String settBadBizNum;

    /**
     * 历史累计不良业务笔数
     */
    @TableField("BAD_BIZ_TOTAL_NUM")
    @XStreamAlias("HIST_TOT_BAD_BUSS_NUM")
    private String badBizTotalNum;

    /**
     * 欠息总额
     */
    @TableField("DI_TOTAL_AMT")
    @XStreamAlias("OW_INT_TOT_AMT")
    private String diTotalAmt;

    /**
     * 拖欠总金额
     */
    @TableField("OD_TOTAL_AMT")
    @XStreamAlias("DLQ_TOT_AMT")
    private String odTotalAmt;

    /**
     * 对外担保保证合同笔数合计
     */
    @TableField("C_O_BZ_GURA_CONTR_NUM")
    @XStreamAlias("EXT_WRT_WRNT_CTR_TOT_NUM")
    private String cOBzGuraContrNum;

    /**
     * 对外担保保证担保合同金额本外币合计
     */
    @TableField("C_O_BZ_GURA_CONTR_AMT")
    @XStreamAlias("EXT_WRT_WRNT_CTR_TOT_AMT")
    private String cOBzGuraContrAmt;

    /**
     * 本担保人保证担保金额本外币合计
     */
    @TableField("C_O_BZ_GURA_AMT")
    @XStreamAlias("WRT_WRNT_TOT_AMT")
    private String cOBzGuraAmt;

    /**
     * 本担保人保证担保金额合计/保证担保合同金额合计
     */
    @TableField("C_O_BZ_GURA_AMT_RATE")
    @XStreamAlias("WRT_WRNT_ON_CTR_TOT_AMT_PCT")
    private String cOBzGuraAmtRate;

    /**
     * 对外担保保证合同单笔平均合同金额
     */
    @TableField("C_O_BZ_GURA_AVER_CONTR_AMT")
    @XStreamAlias("EXT_WRT_WRNT_AV_CTR_AMT")
    private String cOBzGuraAverContrAmt;

    /**
     * 本担保人单笔保证平均金额
     */
    @TableField("C_O_BZ_GURA_AVER_AMT")
    @XStreamAlias("WRT_WRNT_AV_AMT")
    private String cOBzGuraAverAmt;

    /**
     * 对外担保抵押合同笔数合计
     */
    @TableField("C_O_DY_GURA_CONTR_NUM")
    @XStreamAlias("EXT_PLG_WRNT_CTR_TOT_NUM")
    private String cODyGuraContrNum;

    /**
     * 对外担保抵押担保合同金额本外币合计
     */
    @TableField("C_O_DY_GURA_CONTR_AMT")
    @XStreamAlias("EXT_PLG_WRNT_CTR_TOT_AMT")
    private String cODyGuraContrAmt;

    /**
     * 本担保人抵押担保金额本外币合计
     */
    @TableField("C_O_DY_GURA_AMT")
    @XStreamAlias("PLG_WRNT_TOT_AMT")
    private String cODyGuraAmt;

    /**
     * 本担保人抵押担保金额合计/抵押担保合同金额合计
     */
    @TableField("C_O_DY_GURA_AMT_RATE")
    @XStreamAlias("PLG_WRNT_ON_TOT_CTR_AMT_PCT")
    private String cODyGuraAmtRate;

    /**
     * 对外担保抵押合同单笔平均合同金额
     */
    @TableField("C_O_DY_GURA_AVER_CONTR_AMT")
    @XStreamAlias("EXT_PLG_WRNT_AV_CTR_AMT")
    private String cODyGuraAverContrAmt;

    /**
     * 本担保人单笔抵押平均金额
     */
    @TableField("C_O_DY_GURA_AVER_AMT")
    @XStreamAlias("PLG_WRNT_AV_AMT")
    private String cODyGuraAverAmt;

    /**
     * 对外担保质押合同笔数合计
     */
    @TableField("C_O_ZY_GURA_CONTR_NUM")
    @XStreamAlias("EXT_PLDG_WRNT_CTR_TOT_NUM")
    private String cOZyGuraContrNum;

    /**
     * 质对外担保押担保合同金额本外币合计
     */
    @TableField("C_O_ZY_GURA_CONTR_AMT")
    @XStreamAlias("EXT_PLDG_WRNT_CTR_TOT_AMT")
    private String cOZyGuraContrAmt;

    /**
     * 本担保人质押担保金额本外币合计
     */
    @TableField("C_O_ZY_GURA_AMT")
    @XStreamAlias("PLDG_WRNT_TOT_AMT")
    private String cOZyGuraAmt;

    /**
     * 本担保人质押担保金额合计/质押担保合同金额合计
     */
    @TableField("C_O_ZY_GURA_AMT_RATE")
    @XStreamAlias("PLDG_WRNT_ON_TOT_CTR_AMT_PCT")
    private String cOZyGuraAmtRate;

    /**
     * 对外担保质押合同单笔平均合同金额
     */
    @TableField("C_O_ZY_GURA_AVER_CONTR_AMT")
    @XStreamAlias("EXT_PLDG_WRNT_AV_CTR_AMT")
    private String cOZyGuraAverContrAmt;

    /**
     * 本担保人单笔质押平均金额
     */
    @TableField("C_O_ZY_GURA_AVER_AMT")
    @XStreamAlias("PLDG_WRNT_AV_AMT")
    private String cOZyGuraAverAmt;

    /**
     * 担保合同笔数合计
     */
    @TableField("O_GURA_CONTR_TOTAL_NUM")
    @XStreamAlias("WRNT_CTR_TOT_NUM")
    private String oGuraContrTotalNum;

    /**
     * 担保合同金额本外币合计
     */
    @TableField("O_GURA_CONTR_TOTAL_AMT")
    @XStreamAlias("WRNT_CTR_TOT_AMT")
    private String oGuraContrTotalAmt;

    /**
     * 本担保人担保金额本外币合计
     */
    @TableField("O_GURA_TOTAL_AMT")
    @XStreamAlias("WRNT_TOT_AMT")
    private String oGuraTotalAmt;

    /**
     * 本担保人担保金额合计/担保合同金额合计
     */
    @TableField("O_GURA_AMT_RATE")
    @XStreamAlias("WRNT_ON_CTR_TOT_AMT_PCT")
    private String oGuraAmtRate;

    /**
     * 对外担保合同单笔平均合同金额
     */
    @TableField("O_GURA_CONTR_AVER_AMT")
    @XStreamAlias("EXT_WRNT_AV_CTR_AMT")
    private String oGuraContrAverAmt;

    /**
     * 本担保人单笔担保平均金额
     */
    @TableField("O_GURA_AVER_AMT")
    @XStreamAlias("WRNT_AV_CTR_AMT")
    private String oGuraAverAmt;

    /**
     * 保证合同笔数合计
     */
    @TableField("C_BZ_GURA_CONTR_NUM")
    @XStreamAlias("WRT_WRNT_CTR_TOT_NUM")
    private String cBzGuraContrNum;

    /**
     * 保证担保合同金额本外币合计
     */
    @TableField("C_BZ_GURA_CONTR_AMT")
    @XStreamAlias("WRT_WRNT_CTR_TOT_AMT")
    private String cBzGuraContrAmt;

    /**
     * 保证担保合同单笔平均金额
     */
    @TableField("C_BZ_GURA_AVER_AMT")
    @XStreamAlias("WRT_WRNT_AV_CTR_AMT")
    private String cBzGuraAverAmt;

    /**
     * 抵押合同笔数合计
     */
    @TableField("C_DY_GURA_CONTR_NUM")
    @XStreamAlias("PLG_WRNT_CTR_TOT_NUM")
    private String cDyGuraContrNum;

    /**
     * 抵押担保合同金额本外币合计
     */
    @TableField("C_DY_GURA_CONTR_AMT")
    @XStreamAlias("PLG_WRNT_CTR_TOT_AMT")
    private String cDyGuraContrAmt;

    /**
     * 抵押担保合同单笔平均金额
     */
    @TableField("C_DY_GURA_AVER_AMT")
    @XStreamAlias("PLG_WRNT_AV_CTR_AMT")
    private String cDyGuraAverAmt;

    /**
     * 质押合同笔数合计
     */
    @TableField("C_ZY_GURA_CONTR_NUM")
    @XStreamAlias("PLDG_WRNT_CTR_TOT_NUM")
    private String cZyGuraContrNum;

    /**
     * 质押担保合同金额本外币合计
     */
    @TableField("C_ZY_GURA_CONTR_AMT")
    @XStreamAlias("PLDG_WRNT_CTR_TOT_AMT")
    private String cZyGuraContrAmt;

    /**
     * 质押担保合同单笔平均金额
     */
    @TableField("C_ZY_GURA_AVER_AMT")
    @XStreamAlias("PLDG_WRNT_AV_CTR_AMT")
    private String cZyGuraAverAmt;

    /**
     * 贷款卡状态
     */
    @TableField("LOAN_CARD_STATUS")
    @XStreamAlias("CRDT_CARD_ST")
    private String loanCardStatus;

    /**
     * 垫款总额
     */
    @TableField("ADVAN_TOTAL_AMT")
    @XStreamAlias("ADV_PYMT_TOT_AMT")
    private String advanTotalAmt;

    /**
     * 正常类保证担保合同余额
     */
    @TableField("C_BZ_GURA_TOTAL_ZC_BALAN")
    @XStreamAlias("NRL_TP_WRT_WRNT_CTR_BAL")
    private String cBzGuraTotalZcBalan;

    /**
     * 正常类质押担保合同余额
     */
    @TableField("C_ZY_GURA_TOTAL_ZC_BALAN")
    @XStreamAlias("NRL_TP_PLDG_WRNT_CTR_BAL")
    private String cZyGuraTotalZcBalan;

    /**
     * 正常类抵押担保合同余额
     */
    @TableField("C_DY_GURA_TOTAL_ZC_BALAN")
    @XStreamAlias("NRL_TP_PLG_WRNT_CTR_BAL")
    private String cDyGuraTotalZcBalan;

    /**
     * 关注类保证担保合同余额
     */
    @TableField("C_BZ_GURA_TOTAL_GZ_BALAN")
    @XStreamAlias("ATTN_TP_WRT_WRNT_CTR_BAL")
    private String cBzGuraTotalGzBalan;

    /**
     * 关注类质押担保合同余额
     */
    @TableField("C_ZY_GURA_TOTAL_GZ_BALAN")
    @XStreamAlias("ATTN_TP_PLDG_WRNT_CTR_BAL")
    private String cZyGuraTotalGzBalan;

    /**
     * 关注类抵押担保合同余额
     */
    @TableField("C_DY_GURA_TOTAL_GZ_BALAN")
    @XStreamAlias("ATTN_TP_PLG_WRNT_CTR_BAL")
    private String cDyGuraTotalGzBalan;

    /**
     * 不良类保证担保合同余额
     */
    @TableField("C_BZ_GURA_TOTAL_BL_BALAN")
    @XStreamAlias("BAD_TP_WRT_WRNT_CTR_BAL")
    private String cBzGuraTotalBlBalan;

    /**
     * 不良类质押担保合同余额
     */
    @TableField("C_ZY_GURA_TOTAL_BL_BALAN")
    @XStreamAlias("BAD_TP_PLDG_WRNT_CTR_BAL")
    private String cZyGuraTotalBlBalan;

    /**
     * 不良类抵押担保合同余额
     */
    @TableField("C_DY_GURA_TOTAL_BL_BALAN")
    @XStreamAlias("BAD_TP_PLG_WRNT_CTR_BAL")
    private String cDyGuraTotalBlBalan;

    /**
     * 贷款笔数
     */
    @TableField("CREDT_LOAN_COUNT")
    @XStreamAlias("UNSETT_LOAN_NUM")
    private String credtLoanCount;

    /**
     * 关注类业务笔数
     */
    @TableField("ATTENTION_COUNT")
    @XStreamAlias("UNCL_ATTN_TP_BUSS_NUM")
    private String attentionCount;

    /**
     * 未结清信贷业务敞口
     */
    @TableField("UN_SETT_CREDIT_EXPOSURE")
    @XStreamAlias("UNCL_LOAN_CRDT_EAD")
    private String unSettCreditExposure;

    /**
     * 未结清贷款授信机构数
     */
    @TableField("UN_SETT_LOAN_RZBANK_NUM")
    @XStreamAlias("UNCL_LOAN_FINC_BNK_CNT")
    private String unSettLoanRzbankNum;

    /**
     * 未结清贷款总额
     */
    @TableField("UN_SETT_LOAN_TOTAL_AMT")
    @XStreamAlias("UNCL_LOAN_TOT_AMT")
    private String unSettLoanTotalAmt;

    /**
     * 未结清承兑敞口
     */
    @TableField("UN_SETT_CDHP_EXPOSURE")
    @XStreamAlias("UNCL_ACPT_EAD")
    private String unSettCdhpExposure;

    /**
     * 未结清承兑笔数
     */
    @TableField("UN_SETT_CDHP_COUNT")
    @XStreamAlias("UNCL_ACPT_NUM")
    private String unSettCdhpCount;

    /**
     * 未结清信用证总额
     */
    @TableField("UN_SETT_XYZ_EXPOSURE")
    @XStreamAlias("UNCL_LC_TOT_AMT")
    private String unSettXyzExposure;

    /**
     * 未结清信用证笔数
     */
    @TableField("UN_SETT_XYZ_COUNT")
    @XStreamAlias("UNCL_LC_NUM")
    private String unSettXyzCount;

    /**
     * 未结清贸易融资总额
     */
    @TableField("UN_SETT_MYRZ_EXPOSURE")
    @XStreamAlias("UNCL_TRD_FINC_TOT_AMT")
    private String unSettMyrzExposure;

    /**
     * 未结清贸易融资笔数
     */
    @TableField("UN_SETT_MYRZ_COUNT")
    @XStreamAlias("UNCL_TRD_FINC_NUM")
    private String unSettMyrzCount;

    /**
     * 未来6个月到期业务总额
     */
    @TableField("F_6MON_EXPIRED_BIZ_AMT")
    @XStreamAlias("FSM_MATD_BUSS_TOT_AMT")
    private String f6monExpiredBizAmt;

    /**
     * 未来6个月到期业务笔数
     */
    @TableField("F_6MON_EXPIRED_BIZ_COUNT")
    @XStreamAlias("FSM_MATD_BUSS_NUM")
    private String f6monExpiredBizCount;

    /**
     * 对外担保金额
     */
    @TableField("GUAR_CONTR_AMT")
    @XStreamAlias("EXT_WRNT_AMT")
    private String guarContrAmt;

    /**
     * 对外担保金额笔数
     */
    @TableField("GUAR_CONTR_COUNT")
    @XStreamAlias("EXT_WRNT_NUM")
    private String guarContrCount;

    /**
     * 最近12个月金融机构审批通过家数
     */
    @TableField("LATE_12MON_ORG_PASS_NUM")
    @XStreamAlias("LOY_FNNC_ORG_APPR_PS_CNT")
    private String late12monOrgPassNum;

    /**
     * 未结清不良/违约贷款金额
     */
    @TableField("UN_SETT_BAD_LOAN_AMT")
    @XStreamAlias("BAD_ON_BRK_CNTR_LOAN_AMT_PCT")
    private String unSettBadLoanAmt;

    /**
     * 未结清不良/违约贷款笔数
     */
    @TableField("UN_SETT_BAD_LOAN_COUNT")
    @XStreamAlias("BAD_ON_BRK_CNTR_LOAN_NUM_PCT")
    private String unSettBadLoanCount;

    /**
     * 未结清关注类贷款金额
     */
    @TableField("UN_SETT_CONCERN_LOAN_AMT")
    @XStreamAlias("CRN_ATTN_TP_LOAN_AMT")
    private String unSettConcernLoanAmt;

    /**
     * 未结清关注类贷款笔数
     */
    @TableField("UN_SETT_CONCERN_LOAN_COUNT")
    @XStreamAlias("CRN_ATTN_TP_LOAN_NUM")
    private String unSettConcernLoanCount;

    /**
     * 未结清欠息金额
     */
    @TableField("CURR_DEBIT_INTEREST_AMT")
    @XStreamAlias("CRN_OW_INT_AMT")
    private String currDebitInterestAmt;

    /**
     * 未结清欠息笔数
     */
    @TableField("CURR_DEBIT_INTEREST_COUNT")
    @XStreamAlias("CRN_OW_INT_NUM")
    private String currDebitInterestCount;

    /**
     * 未结清垫款金额
     */
    @TableField("CURR_ADVAN_AMT")
    @XStreamAlias("CRN_ADV_PYMT_AMT")
    private String currAdvanAmt;

    /**
     * 未结清垫款笔数
     */
    @TableField("CURR_ADVAN_COUNT")
    @XStreamAlias("CRN_ADV_PYMT_NUM")
    private String currAdvanCount;

    /**
     * 非正常贷款对外担保金额
     */
    @TableField("UNNM_LOAN_GUAR_AMT")
    @XStreamAlias("NON_NRL_LOAN_EXT_WRNT_AMT")
    private String unnmLoanGuarAmt;

    /**
     * 非正常贷款对外担保笔数
     */
    @TableField("UNNM_LOAN_GUAR_COUNT")
    @XStreamAlias("NON_NRL_LOAN_EXT_WRNT_NUM")
    private String unnmLoanGuarCount;

    /**
     * 近4年已结清欠息金额
     */
    @TableField("SETT_DEBIT_INTEREST_AMT")
    @XStreamAlias("LFRY_ALCL_OW_INT_AMT")
    private String settDebitInterestAmt;

    /**
     * 近4年已结清欠息笔数
     */
    @TableField("SETT_DEBIT_INTEREST_COUNT")
    @XStreamAlias("LFRY_ALCL_OW_INT_NUM")
    private String settDebitInterestCount;

    /**
     * 近4年已结清垫款金额
     */
    @TableField("SETT_ADVAN_AMT")
    @XStreamAlias("LFRY_ALCL_ADV_PYMT_AMT")
    private String settAdvanAmt;

    /**
     * 近4年已结清垫款笔数
     */
    @TableField("SETT_ADVAN_COUNT")
    @XStreamAlias("LFRY_ALCL_ADV_PYMT_NUM")
    private String settAdvanCount;

    /**
     * 近4年已结清不良/违约贷款金额
     */
    @TableField("SETT_BAD_LOAN_AMT")
    @XStreamAlias("LFRY_ALCL_BAD_BRK_CNTR_AMT_PCT")
    private String settBadLoanAmt;

    /**
     * 近4年已结清不良/违约贷款笔数
     */
    @TableField("SETT_BAD_LOAN_COUNT")
    @XStreamAlias("LFRY_ALCL_BAD_BRK_CNTR_NUM_PCT")
    private String settBadLoanCount;

    /**
     * 近1年已结清关注类贷款金额
     */
    @TableField("SETT_CONCERN_LOAN_AMT")
    @XStreamAlias("LOY_ALCL_ATTN_TP_LOAN_AMT")
    private String settConcernLoanAmt;

    /**
     * 近1年已结清关注类贷款笔数
     */
    @TableField("SETT_CONCERN_LOAN_COUNT")
    @XStreamAlias("LOY_ALCL_ATTN_TP_LOAN_NUM")
    private String settConcernLoanCount;

    /**
     * 是否有预警信息
     */
    @TableField("EARLY_WARNING_INFO_NUM")
    @XStreamAlias("WRNG_MSG_FLG")
    private String earlyWarningInfoNum;

    /**
     * 预警明细信息
     */
    @TableField("EARLY_WARNING_INFO_CONTEXT")
    @XStreamAlias("WRNG_DTL_MSG")
    private String earlyWarningInfoContext;

    /**
     * 未结清保函敞口
     */
    @TableField("UNSETT_BAOH_EXPOSURE")
    @XStreamAlias("UNCL_GNT_EAD")
    private String unsettBaohExposure;

    /**
     * 未结清保理余额
     */
    @TableField("UNSETT_BAO1_BALAN")
    @XStreamAlias("UNCL_FTR_BAL")
    private String unsettBao1Balan;

    /**
     * 他行授信机构数
     */
    @TableField("OTH_BANK_BUSI_ORG_NUM")
    @XStreamAlias("OTHR_BNK_FINC_CNT")
    private String othBankBusiOrgNum;

    /**
     * 未结清关注及不良业务余额
     */
    @TableField("UNSETT_BAD_BUSI_BALAN")
    @XStreamAlias("UNCL_ATTN_BAD_BUSS_BAL")
    private String unsettBadBusiBalan;

    /**
     * 已结清欠息金额
     */
    @TableField("SETT_DEBIT_INTEREST_ALL_AMT")
    @XStreamAlias("ALCL_OW_INT_AMT")
    private String settDebitInterestAllAmt;

    /**
     * 已结清欠息笔数
     */
    @TableField("SETT_DEBIT_INTEREST_ALL_NUM")
    @XStreamAlias("ALCL_OW_INT_NUM")
    private String settDebitInterestAllNum;

    /**
     * 已结清垫款金额
     */
    @TableField("SETT_ADVAN_ALL_AMT")
    @XStreamAlias("ALCL_ADV_PYMT_AMT")
    private String settAdvanAllAmt;

    /**
     * 已结清垫款笔数
     */
    @TableField("SETT_ADVAN_ALL_NUM")
    @XStreamAlias("ALCL_ADV_PYMT_NUM")
    private String settAdvanAllNum;

    /**
     * 已结清不良/违约贷款金额
     */
    @TableField("SETT_BAD_LOAN_ALL_AMT")
    @XStreamAlias("ALCL_BAD_BRK_CNTR_LOAN_AMT_PCT")
    private String settBadLoanAllAmt;

    /**
     * 已结清不良/违约贷款笔数
     */
    @TableField("SETT_BAD_LOAN_ALL_NUM")
    @XStreamAlias("ALCL_BAD_BRK_CNTR_LOAN_NUM_PCT")
    private String settBadLoanAllNum;

    /**
     * 已结清关注类贷款金额
     */
    @TableField("SETT_CONCERN_LOAN_ALL_AMT")
    @XStreamAlias("ALCL_ATTN_TP_LOAN_AMT")
    private String settConcernLoanAllAmt;

    /**
     * 已结清关注类贷款笔数
     */
    @TableField("SETT_CONCERN_LOAN_ALL_NUM")
    @XStreamAlias("ALCL_ATTN_TP_LOAN_NUM")
    private String settConcernLoanAllNum;

    /**
     * 强制执行记录条数
     */
    @TableField("FORCE_DO_RECORD_NUM")
    @XStreamAlias("FRC_EXCT_RCRD_CNT")
    private String forceDoRecordNum;

    /**
     * 近6个月金融机构审批通过家数
     */
    @TableField("LATE_6MON_ORG_PASS_NUM")
    @XStreamAlias("LSM_FNNC_ORG_APPR_PS_CNT")
    private String late6monOrgPassNum;

    /**
     * 未结清信贷业务融资银行家数
     */
    @TableField("UNSETT_CRED_BUSI_ORG_NUM")
    @XStreamAlias("UNCL_LOAN_BUSS_FINC_BNK_CNT")
    private String unsettCredBusiOrgNum;

    /**
     * 企业注册地址
     */
    @TableField("COMPANY_REGIST_ADDR")
    @XStreamAlias("REG_ADDRESS")
    private String companyRegistAddr;

    /**
     * 未结清授信机构数
     */
    @TableField("UN_SETT_CREDIT_ORG_NUM")
    @XStreamAlias("UNCL_CRDT_ORG_CNT")
    private String unSettCreditOrgNum;

    /**
     * 五级分类为非正常的贷款笔数
     */
    @TableField("UN_NORMAL_LOAN_NUM")
    @XStreamAlias("UNNRL_TP_LOAN_NUM")
    private String unNormalLoanNum;

    /**
     * 五级分类为非正常的贷款余额
     */
    @TableField("UN_NORMAL_LOAN_BALAN")
    @XStreamAlias("UNNRL_TP_LOAN_BAL")
    private String unNormalLoanBalan;

    /**
     * 担保及第三方代偿笔数
     */
    @TableField("GUAN_COMP_NUM")
    @XStreamAlias("WRNT_TR_PR_CMPNSTRY_NUM")
    private String guanCompNum;

    /**
     * 担保及第三方代偿余额
     */
    @TableField("GUAN_COMP_BALAN")
    @XStreamAlias("WRNT_TR_PR_CMPNSTRY_BAL")
    private String guanCompBalan;

    /**
     * 资产处置笔数
     */
    @TableField("ZC_ASSET_NUM")
    @XStreamAlias("AST_DSPS_NUM")
    private String zcAssetNum;

    /**
     * 资产处置余额
     */
    @TableField("ZC_ASSET_BALAN")
    @XStreamAlias("AST_DSPS_AMT")
    private String zcAssetBalan;

    /**
     * 近2年关注及不良信贷业务笔数
     */
    @TableField("LATE_2Y_BAD_BUSI_NUM")
    @XStreamAlias("LTY_ATTN_BAD_LOAN_BUSS_NUM")
    private String late2yBadBusiNum;

    /**
     * 他行授信额度
     */
    @TableField("OTH_BANK_CRED_AMT")
    private String othBankCredAmt;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    /**
     * 贷款信息数组
     */
    @TableField("LOAN_INFO_ARRAY")
    private List<CompanyLoanInfoBo> loanInfoArray;

    /**
     * 担保合同信息数组
     */
    @TableField("GRNY_CTR_ARRAY")
    private List<CompanyExtGuarantyInfoBo> grnyCtrArray;

    /**
     * 贷款信息json
     */
    @XStreamOmitField
    @TableField("COM_LOAN_INFO")
    private String comLoanInfo;

}
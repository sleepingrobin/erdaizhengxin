package com.dhcc.cips.controller;

import com.dhcc.cips.service.other.EsbService;
import com.dhcc.cips.service.other.EsbWebService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Yang Jiaheng
 * @date 2019年4月1日
 * @description 征信查询系统对外提供接口类，根据不同的业务场景代码选择不同的服务
 */
@Slf4j
@RestController
@RequestMapping("cips/esbservice")
public class ESBServiceController implements EsbWebService {

    @Autowired
    private EsbService esbService;

    /**s
     * 征信查询系统对外提供服务接口
     *
     * @return
     */
    @ApiOperation(value = "征信查询系统对外提供服务接口", notes = "征信查询系统对外提供服务接口")
    @GetMapping(value = "", produces = "application/json; charset=utf-8")
    @Override
    public String exec(@RequestParam(value = "esbXml") @ApiParam(value = "服务请求参数") String esbXml) {
        return esbService.exec(esbXml);
    }



}

package com.dhcc.cips.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * 对外合作控制层
 */
@Slf4j
@RestController
@RequestMapping("cips/cips_cooperation")
public class CooperationController {
    @Autowired
    private ESBServiceController esbServiceController;

    @RequestMapping(value = "/parseReport.do", method = {RequestMethod.POST})
    public @ResponseBody
    void parse(HttpServletRequest request, HttpServletResponse response) {
        try {
            String esbXml = "";
            BufferedReader r = request.getReader();
            String s = "";
            StringBuffer buffer = new StringBuffer();
            while ((s = r.readLine()) != null) {
                buffer.append(s);
                buffer.append("\r\n");
            }
            esbXml = buffer.toString();
            log.info("esbXml:" + esbXml);
            String res = esbServiceController.exec(esbXml);
            response.setContentType("application/xml;charset=UTF-8");
            response.getWriter().write(res);
        } catch (Exception e) {
            log.error("程序异常:" + e.getMessage());
            try {
                response.setContentType("application/xml;charset=UTF-8");
                response.getWriter().write("程序异常:" + e.getMessage());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

}

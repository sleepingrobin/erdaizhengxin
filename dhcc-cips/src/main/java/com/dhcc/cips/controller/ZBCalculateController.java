package com.dhcc.cips.controller;


import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.response.EntityResponse;
import com.dhcc.cips.service.company.CompanyReportParseService;
import com.dhcc.cips.service.person.PersonReportParseService;
import com.dhcc.cips.util.IPConfig;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.UUID;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author haokai
 * @since 2019-06-13
 */
@RestController
@Slf4j
@RefreshScope
@RequestMapping("cips/calculation")
public class ZBCalculateController {
    @Value("${server.port}")
    private String port;
    @Autowired
    private PersonReportParseService personReportParseService;
    @Autowired
    private CompanyReportParseService companyReportParseService;

    /**
     * bo解析
     *
     * @param params
     * @return
     */
    @ApiOperation(value = "bo解析", notes = "bo解析")
    @PostMapping(value = "/bo", produces = "application/json; charset=utf-8")
    public EntityResponse<?> getIndexCalculationXml(@RequestBody Map<String, Object> params) {
        String ipPort = "ip:" + IPConfig.getInet4Address() + ";port:" + port;
        String uuid = String.valueOf(params.get("uuid"));
        try {
            String reportContext = String.valueOf(params.get("reportPath"));
            String creditType = String.valueOf(params.get("creditType"));
            if (null != reportContext && null != creditType) {
                reportContext = reportContext.replace("[", "").replace("]", "");
                creditType = creditType.replace("[", "").replace("]", "");
                log.debug("reportContext: " + reportContext.length() + ";creditType: " + creditType + ";uuid: " + uuid);
                if (creditType.equals("1")) {
                    personReportParseService.parseWithXml(reportContext);
                }
                if (creditType.equals("2")) {
                    companyReportParseService.parseWithXml(reportContext);
                }
                return EntityResponse.ok("指标计算解析完成" + ipPort);
            }
            return EntityResponse.error(Constants.FAIL_CODE, uuid + ";指标计算请求数据解析失败" + ipPort);
        } catch (Exception e) {
            log.error("getIndexCalculationXml", e);
            e.printStackTrace(); //todo
            return EntityResponse.error(Constants.FAIL_CODE, uuid + ";指标计算解析处理异常" + ipPort);
        }
    }

    /**
     * XML报告解析
     *
     * @return
     */
    @ApiOperation(value = "XML报告解析", notes = "XML报告解析")
    @PostMapping(value = "/xml", produces = "application/json; charset=utf-8")
    public EntityResponse<?> getIndexCalculationXml(@RequestParam(value = "xmlFile") @ApiParam(value = "xml征信文件", required = true) MultipartFile xmlFile,
                                                    @RequestParam(value = "creditType") @ApiParam(value = "征信报告类型(1.个人征信报告 2.企业征信报告)", required = true) String creditType) {
        String path = System.getProperty("user.dir") + "\\" + UUID.randomUUID() + ".txt";
        File file = new File(path);
        InputStream is = null;
        try {
            if (xmlFile.isEmpty()) {
                return EntityResponse.error(Constants.FAIL_CODE, "xml文件路径为空");
            }
            String fileType = xmlFile.getContentType();
            if (fileType.equals("text/xml")) {
                FileUtils.copyInputStreamToFile(xmlFile.getInputStream(), file);
                is = new FileInputStream(file);
                StringBuffer sb = new StringBuffer();
                byte[] bytes = new byte[1024];
                for (int n; (n = is.read(bytes)) != -1; ) {
                    sb.append(new String(bytes, 0, n, "utf-8"));
                }
                if (creditType.equals("1")) {
                    personReportParseService.parseWithXml(sb.toString());
                }
                if (creditType.equals("2")) {
                    companyReportParseService.parseWithXml(sb.toString());
                }
            }

        } catch (Exception e) {
            log.error("getIndexCalculationXml:", e);
            e.printStackTrace();//todo
            return EntityResponse.error(Constants.FAIL_CODE, "xml报告-指标解析入库失败");
        } finally {
            try {
                if (null != is) {
                    is.close();
                }
            } catch (IOException e) {
                log.error("IOException:", e);
            }
            if (file.exists()) {
                file.delete();
            }
        }
        return EntityResponse.ok("xml报告-指标解析入库完成");
    }

    /**
     * HTML报告解析
     *
     * @return
     */
    @ApiOperation(value = "HTML报告解析", notes = "HTML报告解析")
    @PostMapping(value = "/html", produces = "application/json; charset=utf-8")
    public EntityResponse<?> getIndexCalculationHtml(@RequestParam(value = "htmlFile") @ApiParam(value = "html征信报告", required = true) MultipartFile htmlFile,
                                                     @RequestParam(value = "creditType") @ApiParam(value = "征信报告类型(1.个人征信报告 2.企业征信报告)", required = true) String creditType) {
        String path = System.getProperty("user.dir") + "\\" + UUID.randomUUID() + ".txt";
        File file = new File(path);
        InputStream is = null;
        try {
            if (htmlFile.isEmpty()) {
                return EntityResponse.error(Constants.FAIL_CODE, "xml文件路径为空");
            }
            String fileType = htmlFile.getContentType();
            if (fileType.equals("text/html")) {
                FileUtils.copyInputStreamToFile(htmlFile.getInputStream(), file);
                is = new FileInputStream(file);
                StringBuffer sb = new StringBuffer();
                byte[] bytes = new byte[1024];
                for (int n; (n = is.read(bytes)) != -1; ) {
                    sb.append(new String(bytes, 0, n, "utf-8"));
                }
                if (creditType.equals("1")) {
                    personReportParseService.parseWithHtml(sb.toString());
                }
                if (creditType.equals("2")) {
                    companyReportParseService.parseWithHtml(sb.toString());
                }
            }
        } catch (Exception e) {
            log.error("getIndexCalculationHtml:", e);
            e.printStackTrace();
            return EntityResponse.error(Constants.FAIL_CODE, "html报告-指标解析入库失败");
        } finally {
            try {
                if (null != is) {
                    is.close();
                }
            } catch (IOException e) {
                log.error("IOException:", e);
            }
            if (file.exists()) {
                file.delete();
            }
        }
        return EntityResponse.ok("html报告-指标解析完成");
    }
}

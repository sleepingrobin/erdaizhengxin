package com.dhcc.cips.controller;

import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.response.EntityResponse;
import com.dhcc.cips.service.other.MsgService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author Yang Jiaheng
 * @date 2019-4-29 15:45:05
 * @description 短信接口
 */
@Slf4j
@RestController
@RequestMapping("cips/message")
@RefreshScope
public class MessageController {

    /**
     * 短信内容
     */
    @Value("${msgContent}")
    private String msgContent;
    @Autowired
    private MsgService msgService;
    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;

    @ApiOperation(value = "发送短信", notes = "发送短信")
    @PostMapping("sendCode")
    public EntityResponse<?> sendCode(String username, String phoneNum) throws Exception {
        try {
            // 生成六位随机数
            String verification = String.valueOf((int) ((Math.random() * 9 + 1) * 100000));
            log.info("存入的用户名为：" + username + ", 验证码：" + verification);

            // 调用短信服务
            // 短信内容
            EntityResponse response = msgService.sendMessage(username, msgContent + verification);
            //如果短信发送成功，才设置redis
            if (Constants.SUCCESS_CODE.equals(response.getCode())) {
                // 将验证码存入redis
                redisTemplate.opsForValue().set(username + "_VERIFICATION", verification, 10L, TimeUnit.MINUTES);
            }
            return response;

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }
    }

    @ApiOperation(value = "校对验证码", notes = "校对验证码")
    @GetMapping("checkCode")
    public EntityResponse<?> code(String code, String username) {
        //判断验证码是否失效
        //从redis中取得验证码
        String rCode = (String) redisTemplate.opsForValue().get(username + "_VERIFICATION");

        //对比验证码是否正确
        if (rCode != null && !rCode.isEmpty()) {
            if (rCode.equals(code)) {
                //正确：
                //没有设置超时时间，也让他登录吧
                redisTemplate.delete(username + "_VERIFICATION");
                return EntityResponse.ok("验证通过");
            } else {
                //验证码错误
                return EntityResponse.error(Constants.FAIL_CODE, "验证码输入错误或已过期，请重新输入");
            }
        } else {
            //从redis未取到验证码
            return EntityResponse.error(Constants.FAIL_CODE, "验证码输入错误或已过期，请重新输入");
        }
    }

    @ApiOperation(value = "获取用户列表", notes = "获取用户角色有审核角色的用户名map")
    @GetMapping("getUserList")
    public List<Map> selectPhoneNumber(String orgId) {
        List<Map> map = msgService.selectUserListByOrg(orgId);
        return map;
    }

    @ApiOperation(value = "判断用户是否存在", notes = "列表时需要判断查询用户是否存在，如果存在，展示查询按钮；否则不展示查询按钮")
    @GetMapping("isExist")
    public EntityResponse<?> isExist(String userId, HttpServletRequest request, HttpServletResponse response) {
        int isExist = msgService.queryUserByUserId(userId);

        return EntityResponse.ok(isExist);
    }

}

package com.dhcc.cips.mapper.other;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author zcg
 * @Description TODO
 * @Date 2019-3-28 17:12
 */
@Mapper
public interface CqCreditUserMapper {

    /**
     * 机构号查询 适用于支行分行查询
     *
     * @param branchSplitId
     * @return
     */
    List<String> selectCreditUser(@Param("branchSplitId") String branchSplitId, @Param("queryType") String queryType);

    /**
     * 根据征信用户查询前置系统查询用户
     *
     * @param creditUser
     * @return
     */
    String selectPersonQueryUser(@Param("creditUser") String creditUser);

    /**
     * 根据征信用户查询前置系统用户
     *
     * @param creditUser
     * @return
     */
    String selectCompanyQueryUser(@Param("creditUser") String creditUser);
}

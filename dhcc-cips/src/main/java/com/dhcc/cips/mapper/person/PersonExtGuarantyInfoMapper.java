package com.dhcc.cips.mapper.person;

import com.dhcc.cips.bo.person.PersonExtGuarantyInfoBo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 对外担保信息操作类
 */
@Mapper
public interface PersonExtGuarantyInfoMapper {

    /**
     * 查询对外担保信息数组
     * @param map 报告编号
     * @return 对外担保信息数组
     */
    List<PersonExtGuarantyInfoBo> selectExtGuarantyInfoByRepoNo(Map map);
}

package com.dhcc.cips.mapper.person;

import com.dhcc.cips.bo.person.PersonLoanInfoBo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 贷款信息操作类
 */
@Mapper
public interface PersonLoanInfoMapper {

    /**
     * 根据报告编号查询贷款信息
     *
     * @param map 报告编号
     * @return 贷款信息数组列表
     */
    List<PersonLoanInfoBo> selectLoanInfoByRepoNo(Map map);
}

package com.dhcc.cips.mapper.company;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhcc.cips.po.company.CeqResultInfoPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author LIMIN
 * @since 2019-05-30
 */
@Mapper
public interface CeqResultInfoMapper extends BaseMapper<CeqResultInfoPo> {
    /**
     * 根据证件类型+征信号码查询报告查询信息
     *
     * @param map 参数表
     * @return 最新查询记录信息
     */
    CeqResultInfoPo selectByLoancardNo(Map map);
}

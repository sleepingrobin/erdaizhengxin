package com.dhcc.cips.mapper.person;

import com.dhcc.cips.po.person.PersonBaseInfoPo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * 个人基本信息操作类
 */
@Mapper
public interface PersonBaseInfoMapper {

    /**
     * 查询基础信息
     *
     * @param map 报告编号
     * @return 个人基础信息
     */
    PersonBaseInfoPo selectBaseInfo(Map map);

}

package com.dhcc.cips.mapper.other;

import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 手机短信mapper
 *
 * @author YangJiaheng
 * @date 2019-5-6 09:55:58
 *
 */
@Mapper
public interface MsgMapper {

    List<Map> selectUserList(HashMap<String, String> map);

    String selectPhoneByUsername(String username);
}

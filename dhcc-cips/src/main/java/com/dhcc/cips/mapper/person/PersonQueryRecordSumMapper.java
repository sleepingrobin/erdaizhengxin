package com.dhcc.cips.mapper.person;

import com.dhcc.cips.po.person.PersonQueryRecordSumPo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * 征信查询记录操作类
 */
@Mapper
public interface PersonQueryRecordSumMapper {

    /**
     * 查询征信查询记录信息
     * @param map 报告编号
     * @return 查询记录
     */
    PersonQueryRecordSumPo selectQueryRecordInfo(Map map);

}

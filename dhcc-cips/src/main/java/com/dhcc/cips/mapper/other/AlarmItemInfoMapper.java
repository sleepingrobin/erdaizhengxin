package com.dhcc.cips.mapper.other;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhcc.cips.po.other.AlarmItemInfoPo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 预警规则Mapper
 *
 * @author limin
 * @date 2019年6月4日
 */
@Mapper
public interface AlarmItemInfoMapper extends BaseMapper<AlarmItemInfoPo> {

    /**
     * 通过ID查询预警规则
     *
     * @param cId
     * @return
     */
    List<AlarmItemInfoPo> selectByCId(String cId);

}

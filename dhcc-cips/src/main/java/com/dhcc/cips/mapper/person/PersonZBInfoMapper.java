package com.dhcc.cips.mapper.person;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhcc.cips.po.person.PersonZBInfoPo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * <p>
 * 个人指标报告 Mapper 接口
 * </p>
 *
 * @author haokai
 * @since 2019-3-5
 */
@Mapper
public interface PersonZBInfoMapper extends BaseMapper<PersonZBInfoPo> {


    /**
     * 查询个人指标
     *
     * @param map 参数列表
     * @return 个人指标do
     */
    PersonZBInfoPo selectPersonZBInfoByReportNo(Map map);

}

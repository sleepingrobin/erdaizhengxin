package com.dhcc.cips.mapper.other;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhcc.cips.po.other.AlarmConfigInfoPo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 预警信息Mapper
 *
 * @author limin
 * @date 2019年6月4日
 */
@Mapper
public interface AlarmConfigInfoMapper extends BaseMapper<AlarmConfigInfoPo> {

    /**
     * 通过类型查询预警信息（P:个人 C:企业）
     *
     * @param type
     * @return
     */
    List<AlarmConfigInfoPo> selectByUserType(String type);

}

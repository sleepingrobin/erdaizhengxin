package com.dhcc.cips.mapper.company;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dhcc.cips.po.company.CompanyZBInfoPo;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * <p>
 * 企业指标报告 Mapper 接口
 * </p>
 *
 * @author haokai
 * @since 2019-3-5
 */
@Mapper
public interface CompanyZBInfoMapper extends BaseMapper<CompanyZBInfoPo> {

    /**
     * 查询企业指标信息
     *
     * @param map 报告编号
     * @return 企业指标po
     */
    CompanyZBInfoPo selectCompanyZBInfoByReportNo(Map map);

}

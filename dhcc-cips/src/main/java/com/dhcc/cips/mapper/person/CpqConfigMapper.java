package com.dhcc.cips.mapper.person;

import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author limin
 * @Description
 * @Date 2019-6-10
 */
@Mapper
public interface CpqConfigMapper {


    /**
     * 根据机构id查询用户所在的法人机构对应的查询方式（接口查询、网页查询）
     *
     * @param branchId
     * @return
     */
    Map<String, String> selectQueryType(String branchId);

    /**
     * 根据机构号查询报告过期时间
     * @param branchId
     * @return
     */
    String selectReportQueryType(String branchId);
}

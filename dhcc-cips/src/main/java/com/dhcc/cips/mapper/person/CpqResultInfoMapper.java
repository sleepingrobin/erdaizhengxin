package com.dhcc.cips.mapper.person;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.dhcc.cips.po.person.CpqResultInfoPo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 个人查询日志表操作类
 */
@Mapper
public interface CpqResultInfoMapper extends BaseMapper<CpqResultInfoPo> {

    /**
     * 根据证件类型+征信号码查询报告信息
     *
     * @param map 参数表
     * @return 最新查询记录信息
     */
    CpqResultInfoPo selectByCertId(Map map);
}

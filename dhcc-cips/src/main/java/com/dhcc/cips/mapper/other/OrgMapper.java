package com.dhcc.cips.mapper.other;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 组织机构Mapper
 *
 * @author YangJiaheng
 * @date 2019年5月20日10:33:01
 *
 */
@Mapper
public interface OrgMapper {

    String selectOrgIdByOrgId(String orgId);

    void deleteOrgsByIds(List orgList);

    void deleteOrgByOrgCode(String orgCode);

    void updateOrgsByOrgCode(List orgList);
}

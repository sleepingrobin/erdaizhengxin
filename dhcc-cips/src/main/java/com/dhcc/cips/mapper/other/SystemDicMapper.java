package com.dhcc.cips.mapper.other;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author limin
 * @Description 系统参数mapper
 * @Date 2019-6-10
 */
@Mapper
public interface SystemDicMapper {



    /**
     * 查询业务类型
     *
     * @return
     */
    String selectBusinessLine();


}

package com.dhcc.cips.constants;

import java.util.List;
import java.util.Map;

public class Constants {


    /**
     * 通用成功返回信息
     */
    public static final String SUCCESS_CODE = "000000";
    public static final String SUCCESS_MASSAGE = "交易成功！";
    public static final String SUCCESS_S = "S";

    /**
     * 通用失败返回信息
     */
    public static final String FAIL_CODE = "000001";
    public static final String FAIL_MASSAGE = "交易失败！";
    public static final String FAIL_F = "F";

    //信用报告版式   00授信机构版
    public static final String queryFormat = "00";


    public static final String GLOBAL_TYPE_NULL_ANO = "证件类型不能为空";
    public static final String GLOBAL_ID_NULL_ANO = "证件号码不能为空";
    public static final String CLIENT_NAME_NULL_ANO = "客户名称不能为空";
    public static final String REASON_CODE_NULL_ANO = "查询原因不能为空";
    public static final String LOANCARD_NO_NULL_ANO = "贷款卡号不能为空";
    public static final String LOANCARD_PASSWORD_NULL_ANO = "贷款卡密码不能为空";
    public static final String MGR_ORG_NO_NULL_ANO = "客户经理机构号不能为空";
    public static final String MGR_ORG_NAME_NULL_ANO = "客户经理机构名称不能为空";
    public static final String CLNT_MGR_NO_NULL_ANO = "客户经理工号不能为空";
    public static final String CLNT_MGR_NM_NULL_ANO = "客户经理名称不能为空";
    public static final String SIGN_MD_NULL_ANO = "签署方式不能为空";
    public static final String CHCK_USERID_NULL_ANO = "审核人工号不能为空";
    public static final String CHCK_USERNAME_NULLE_ANO = "审核人名称不能为空";
    public static final String QUERY_MGR_NO_NULL_ANO = "查看人工号不能为空";
    public static final String CORP_NAME_NULL_ANO = "法人机构名称不能为空";
    public static final String REPORT_STRING_NULL_ANO = "征信报告字符串不能为空";
    public static final String QUERY_DATE_NULL_ANO = "查询日期不能为空";
    public static final String PRE_EVAL_SEQ_NO_NULL_ANO = "预评估流水号不能为空";

    public static final String CHCK_TM_ANO = "审核时间不能为空时间，且审核时间格式为:yyyyMMdd hh:mm:ss";
    public static final String REPORT_NULL = "此客户无本地征信报告";
    public static final String ZB_NULL = "此客户无本地征信指标信息";
    public static final String QUERY_RECORD_NULL = "此客户无征信查询记录";

    public static final String SUCCESS_NO_REPORT = "未找到相应报告编号";
    public static final String BTCH_RTNG_FLAG_ANO = "批量评级标志数据异常";


    /**
     * 个人xml请求开始
     */
    public static final String PERSTART = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.queryapi.person.creditquery.dhcc.com.cn/\">\n" +
            "<soapenv:Header/>\n" + "<soapenv:Body>\n" + "<ser:getCreditReport>\n" + "<parXml>\n" + "<![CDATA[\n" + "<singleQuery>\n";
    /**
     * 个人xml请求结束
     */
    public static final String PEREND = "</singleQuery>\n" + "]]>\n" + "</parXml>\n" + "</ser:getCreditReport>\n" + "</soapenv:Body>\n" + "</soapenv:Envelope>";
    /**
     * 企业xml请求开始
     */
    public static final String ENTSTART = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://service.queryapi.ent.creditquery.dhcc.com.cn/\">\n" +
            "<soapenv:Header/>\n" + "<soapenv:Body>\n" + "<ser:getCreditReport>\n" + "<parXml>\n" + "<![CDATA[\n" + "<singleQuery>";
    /**
     * 企业xml请求结束
     */
    public static final String ENTEND = "</singleQuery>]]>\n" + "</parXml>\n" + "</ser:getCreditReport>\n" + "</soapenv:Body>\n";

    /*个人借贷账户类型代码*/
    public static final String ACC_TYPE_D1 = "D1";//非循环贷账户
    public static final String ACC_TYPE_D2 = "D2";//
    public static final String ACC_TYPE_R1 = "R1";//循环贷账户
    public static final String ACC_TYPE_R2 = "R2";//贷记卡账户
    public static final String ACC_TYPE_R3 = "R3";//准贷记卡账户
    public static final String ACC_TYPE_R4 = "R4";//循环额度下分账户
    public static final String ACC_TYPE_C1 = "R4";//催收账户

    /*还款状态*/
    public static final String OVERDUE_STATUS = "1234567";
    public static final String OVERDUE1 = "234567";
    public static final String OVERDUE = "1234567B";
    public static final String OVERDUE2 = "1234567ZDB";
    public static final String OVERDUE3 = "1234567BG";
    public static final String REPAY_STAT_1 = "1";
    public static final String REPAY_STAT_2 = "2";
    public static final String REPAY_STAT_3 = "3";
    public static final String REPAY_STAT_4 = "4";
    public static final String REPAY_STAT_5 = "5";
    public static final String REPAY_STAT_6 = "6";
    public static final String REPAY_STAT_7 = "7";
    public static final String REPAY_STAT_B = "B";
    public static final String REPAY_STAT_C = "C";
    public static final String REPAY_STAT_G = "G";
    public static final String REPAY_STAT_N = "N";
    public static final String REPAY_STAT_UNKNOW = "#";

    /**
     * 账户状态
     */
    public static final String Accmount5 = "呆账";
    public static final String Accmount8 = "司法追偿";
    public static final String Accmount6 = "担保物不足";
    public static final String Accmount7 = "强制平仓";
    public static final String Accmount2 = "冻结";
    public static final String Accmount31 = "银行止付";
    public static final String Accmount3 = "止付";
    public static final String Accmount9 = "逾期";

    /*贷款业务种类*/
    public static final String LOAN_BUS_TYPE_11 = "11";//个人住房商业贷款
    public static final String LOAN_BUS_TYPE_12 = "12";//个人商用房（含商住两用）贷款
    public static final String LOAN_BUS_TYPE_13 = "13";//个人住房公积金贷款
    public static final String LOAN_BUS_TYPE_19 = "19";//
    public static final String LOAN_BUS_TYPE_21 = "21";//个人汽车消费贷款
    public static final String LOAN_BUS_TYPE_31 = "31";//个人助学贷款
    public static final String LOAN_BUS_TYPE_32 = "32";//国家助学贷款
    public static final String LOAN_BUS_TYPE_33 = "33";//商业助学贷款
    public static final String LOAN_BUS_TYPE_41 = "41";//个人经营性贷款
    public static final String LOAN_BUS_TYPE_51 = "51";//农户贷款
    public static final String LOAN_BUS_TYPE_52 = "52";//经营性农户贷款
    public static final String LOAN_BUS_TYPE_53 = "53";//消费性农户贷款
    public static final String LOAN_BUS_TYPE_91 = "91";//其他个人消费贷款
    public static final String LOAN_BUS_TYPE_99 = "99";//其他贷款

    // 本机构 00
    public static final String ORG_TYPE_00 = "00";
    /*业务种类*/
    public static final String BUS_TYPE_11 = "11";
    public static final String BUS_TYPE_12 = "12";
    public static final String BUS_TYPE_13 = "13";
    public static final String BUS_TYPE_19 = "19";
    public static final String BUS_TYPE_21 = "21";
    public static final String BUS_TYPE_31 = "31";
    public static final String BUS_TYPE_32 = "32";
    public static final String BUS_TYPE_33 = "33";
    public static final String BUS_TYPE_41 = "41";
    public static final String BUS_TYPE_51 = "51";
    public static final String BUS_TYPE_52 = "52";
    public static final String BUS_TYPE_53 = "53";
    public static final String BUS_TYPE_91 = "91";
    public static final String BUS_TYPE_99 = "99";


    /*D1\R1\R2\R3\R4 账户状态*/
    public static final String ACC_STAT_1 = "1";//正常
    public static final String ACC_STAT_2 = "2";//逾期
    public static final String ACC_STAT_3 = "3";//结清
    public static final String ACC_STAT_4 = "4";//呆账
    public static final String ACC_STAT_5 = "5";//银行止付
    public static final String ACC_STAT_6 = "6";//担保物不足
    public static final String ACC_STAT_7 = "7";//
    public static final String ACC_STAT_8 = "8";//司法追偿
    public static final String ACC_STAT_9 = "9";
    public static final String ACC_STAT_31 = "31";//

    /*相关还款责任类型*/
    public static final String RelepayType_1 = "1";
    public static final String RelepayType_9 = "9";


    /*五级分类*/
    public static final String FIF_TYPE_1 = "1";
    public static final String FIF_TYPE_2 = "2";
    public static final String FIF_TYPE_3 = "3";
    public static final String FIF_TYPE_4 = "4";
    public static final String FIF_TYPE_5 = "5";
    public static final String FIF_TYPE_9 = "9";

    /*业务类型*/
    public static final String BUSINESS_11 = "11";//个人住房贷款
    public static final String BUSINESS_12 = "12";//个人商用（包括商住两用）贷款
    public static final String BUSINESS_19 = "19";//其他类贷款
    public static final String BUSINESS_21 = "21";//贷记卡
    public static final String BUSINESS_22 = "22";//准贷记卡
    public static final String BUSINESS_99 = "99";//其他

    //特殊交易类型
    public static final String SPECIAL_TYPE_1 = "1"; //展期
    public static final String SPECIAL_TYPE_2 = "2"; //担保人（第三方）代偿
    public static final String SPECIAL_TYPE_3 = "3"; //以资抵债
    public static final String SPECIAL_TYPE_4 = "4"; //提前还款
    public static final String SPECIAL_TYPE_5 = "5"; //提前结清
    public static final String SPECIAL_TYPE_6 = "6"; //强制平仓，未结清
    public static final String SPECIAL_TYPE_7 = "7"; //强制平仓，已结清

    /*担保方式*/
    public static final String GUARANTY_STYLE = "45679";
    public static final String GUARANTY_1 = "1";
    public static final String GUARANTY_2 = "2";
    public static final String GUARANTY_3 = "3";
    public static final String GUARANTY_4 = "4";
    public static final String GUARANTY_5 = "5";
    public static final String GUARANTY_6 = "6";
    public static final String GUARANTY_7 = "7";
    public static final String GUARANTY_9 = "9";


    /*逾期信息汇总帐户类型 */
    public static final String Overdue_1 = "1";
    public static final String Overdue_2 = "2";
    public static final String Overdue_3 = "3";
    public static final String Overdue_4 = "4";
    public static final String Overdue_5 = "5";

    /*还款责任人类型 */
    public static final String RepayPerson_1 = "1";
    public static final String RepayPerson_2 = "2";
    public static final String RepayPerson_3 = "3";
    public static final String RepayPerson_4 = "4";
    public static final String RepayPerson_5 = "5";
    public static final String FIF_TYPE_6 = "6";//违约
    public static final String RepayPerson_9 = "9";//未分类

    /*被追偿业务类型 */
    public static final String PersonRecovery_1 = "1";
    public static final String PersonRecovery_2 = "2";

    /*被追偿业务类型 */
    public static final String RepayResponse_1 = "1";
    public static final String RepayResponse_9 = "9";

    /*查询原因代码 */
    public static final String QueryCause_01 = "01";
    public static final String QueryCause_02 = "02";
    public static final String QueryCause_03 = "03";
    public static final String QueryCause_08 = "08";

    /*公共信息类型 */
    public static final String PUBLICINFO_1 = "1";
    public static final String PUBLICINFO_2 = "2";
    public static final String PUBLICINFO_3 = "3";
    public static final String PUBLICINFO_4 = "4";
    /* 代偿（垫款）标志 */
    public static final String COMPENS_FLAG_0 = "0";//否
    public static final String COMPENS_FLAG_1 = "1";//是

    /* 账户活动状态 */
    public static final String ACTIV_STATUS_1 = "1";//未结清
    public static final String ACTIV_STATUS_2 = "2";//已结清

    /* 资产质量分类 */
    public static final String QUAL_SORT_1 = "1";//正常类
    public static final String QUAL_SORT_2 = "2";//关注类
    public static final String QUAL_SORT_3 = "3";//不良类
    public static final String QUAL_SORT_0 = "0";//合计

    /*企业借贷业务类型*/
    public static final String BUS_TYPE_1 = "1";//中长期借款
    public static final String BUS_TYPE_2 = "2";//短期借款
    public static final String BUS_TYPE_3 = "3";//循环透支
    public static final String BUS_TYPE_4 = "4";//贴现
    public static final String BUS_TYPE_0 = "0";//合计

    /*企业担保业务类型*/
    public static final String BS_BUS_TYPE_1 = "1";//银行承兑汇票
    public static final String BS_BUS_TYPE_2 = "2";//信用证
    public static final String BS_BUS_TYPE_3 = "3";//银行保函
    public static final String BS_BUS_TYPE_9 = "9";//其他

    /* 担保交易业务种类细分*/
    public static final String BS_BUS_TYPE_51 = "51";//信用证
    public static final String BS_BUS_TYPE_61 = "61";//银行承兑汇票
    public static final String BS_BUS_TYPE_62 = "62";//商业承兑汇票
    public static final String BS_BUS_TYPE_71 = "71";//融资类银行保函
    public static final String BS_BUS_TYPE_72 = "72";//非融资类银行保函

    public static final String BS_BUS_TYPE_01 = "01";//贷款担保
    public static final String BS_BUS_TYPE_02 = "02";//票据承兑担保
    public static final String BS_BUS_TYPE_03 = "03";//贸易融资担保
    public static final String BS_BUS_TYPE_04 = "04";//项目融资担保
    public static final String BS_BUS_TYPE_05 = "05";//信用证资担保
    public static final String BS_BUS_TYPE_06 = "06";//其他融资担保

    /* 责任类型*/
    public static final String LIABILY_TYPE_2 = "2";//担保人、反担保人
    public static final String LIABILY_TYPE_0 = "0";//合计

    //最后一次还款形式
    public static final String LAST_REPAY_TYPE_21 = "21";//21-担保代偿

    //信贷业务大类
    public static final String CREDIT_BUSS_CATE_1 = "1";//贷款
    public static final String CREDIT_BUSS_CATE_2 = "2";//信用卡
    public static final String CREDIT_BUSS_CATE_9 = "9";//其他

    //信贷业务大类
    public static final String CREDIT_BUSS_TYPE_11 = "11";//个人住房贷款
    public static final String CREDIT_BUSS_TYPE_12 = "12";//个人商用房（包括商住两用房）贷款
    public static final String CREDIT_BUSS_TYPE_19 = "19";//其他贷款
    public static final String CREDIT_BUSS_TYPE_21 = "21";//贷记卡
    public static final String CREDIT_BUSS_TYPE_22 = "22";//准贷记卡
    public static final String CREDIT_BUSS_TYPE_99 = "99";//其他

    //对私证件类型
    public static final String PERSON_CRETIFICATE_TYPE_B0100 = "B0100";//身份证
    public static final String PERSON_CRETIFICATE_TYPE_B0101 = "B0101";//居民身份证
    public static final String PERSON_CRETIFICATE_TYPE_B0102 = "B0102";//临时身份证

    //对公证件类型
    public static final String COMPANY_CRETIFICATE_TYPE_A0200 = "A0200";//组织机构代码
    public static final String COMPANY_CRETIFICATE_TYPE_A1300 = "A1300";//贷款卡

    //客户分类
    public static String USER_TYPE_PERSON = "P";//个人
    public static String USER_NAME_PERSON = "个人";
    public static String USER_TYPE_COMPANY = "C";//企业
    public static String USER_NAME_COMPANY = "企业";

    //预警规则
    public static String ALARM_RULE_0 = "0";
    public static String ALARM_RULE_1 = "1";
    public static String ALARM_RULE_2 = "2";
    public static String ALARM_RULE_3 = "3";

    //预警条件
    public static String ALARM_COND_1 = "1";
    public static String ALARM_COND_2 = "2";
    public static String ALARM_COND_0 = "0";
    public static String ALARM_COND_21 = "21";
    public static String ALARM_COND_10 = "10";

    public static String NO_ALARM_VALUE = "-1";

    public static Map<String, String> PSN_ZB_ALL_MAP;// 被担保信息
    public static Map<String, String> COM_ZB_ALL_MAP;// 被担保信息

    public static List<String> P_RATE_FIELD_LIST;
    public static List<String> C_RATE_FIELD_LIST;


    //个人查询原因
    public static String P_REASON_DHGL = "01";//贷后管理
    //企业查询原因
    public static String C_REASON_DHGL = "03";//贷后管理

    /* 系统编号 */
    public static String SYSTEM_ID_LOAN = "202020";//信贷系统
    public static String SYSTEM_ID_CIPS = "211010";//征信查询系统
    public static String SYSTEM_ID_SMS = "239010";//短信平台
}

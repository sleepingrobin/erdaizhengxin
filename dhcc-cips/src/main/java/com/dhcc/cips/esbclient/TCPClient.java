package com.dhcc.cips.esbclient;

import com.dc.eai.data.CompositeData;
import com.dcfs.esb.client.ESBClient_TCP;
import com.dcfs.esb.client.exception.TimeoutException;
import com.dcfs.esb.config.ClientConfig_TCP;
import com.dhcc.cips.constants.Constants;
import com.dhcc.cips.service.impl.other.EsbParentMessageServiceImpl;
import com.dhcc.cips.util.DateUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * @author limin
 * @date 2019-6-23
 * @description TCPClient客户端
 */
@Slf4j
@Data
public class TCPClient extends EsbParentMessageServiceImpl {

    private CompositeData req_cd = new CompositeData();		// 请求报文
    private CompositeData req_body = new CompositeData();	// 请求报文体
    private CompositeData req_syshead = new CompositeData();	// 请求系统报文头
    private CompositeData req_apphead = new CompositeData();	// 请求应用报文头

    private CompositeData resp_cd = new CompositeData();	// 应答报文
    private CompositeData resp_syshead = new CompositeData();	// 应答系统报文头
    private CompositeData resp_apphead = new CompositeData();	// 应答应用系统报文头
    private CompositeData resp_body = new CompositeData();	// 应答报文体

    private String serviceCode;	//服务代码
    private String serviceScene;	//服务场景

    private String branchId;	//发送方机构ID
    private String userId;	//服务请求者身份

    private String providerId;	//服务提供者ID
    private String bussSeqNo;	//业务流水号
    private String consumerSeqNo;//交易流水号

    private String retCode;		//交易返回代码
    private String retMessage;	//交易返回信息
    private String BusinessSerialno;//业务流水号
    private String corpCode;//法人标识
    private String sourceType;//渠道

    public TCPClient() throws Exception{
        initConfig();
    }
    /**
     * 组装报文体
     * @parmar serviceCode 服务代码
     * @parmar serviceScene 服务场景
     * @parmar providerId 系统ID
     * @throws Exception
     *  @return 交易成功状态
     * **/
    public TCPClient(String serviceCode, String serviceScene, String providerId) throws Exception{
        this();
        this.serviceCode = serviceCode;
        this.serviceScene = serviceScene;
        this.providerId = providerId;
    }

    /**
     * 组装报文体
     * @parmar sServerID 服务代码
     * @parmar sScene 服务场景
     * @parmar sResSystemID 系统ID
     * @parmar sbranch_id 交易机构ID
     * @parmar suser_id 交易机构柜员ID
     * @throws Exception
     *  @return 交易成功状态
     * **/
    public TCPClient(String serviceCode, String serviceScene, String providerId, String branchId, String userId) throws Exception{
        this();
        this.serviceCode = serviceCode;
        this.serviceScene = serviceScene;
        this.providerId = providerId;
        this.branchId = branchId;
        this.userId = userId;
    }

    /***
     * 初始化ESB配置
     * ****/
    private void initConfig() throws Exception {
        try {
            //初始化esb配置信息
            ClientConfig_TCP.init();
        } catch (Exception e) {
            log.error("交易流水号[" + consumerSeqNo + "]" + "与ESB交易配置初始化失败！失败原因：[" + e.getMessage() + "]");
            throw new Exception(e);
        }
    }

    /**
     * 组装报文头（组ESB CD报文）
     */
    private void initHeader() throws Exception{
        //服务码
        addField(req_syshead, "SERVICE_CODE", "string", 11,this.getServiceCode());
        //服务场景
        addField(req_syshead, "SERVICE_SCENE", "string", 2,this.getServiceScene());
        //交易流水号
        addField(req_syshead, "consumerSeqNo", "string", 52, consumerSeqNo);
        String tran_date = DateUtil.getNowTime("yyyyMMddHHmmssSSS");
        //交易日期
        addField(req_syshead, "TRAN_DATE", "string", 8, tran_date.substring(0,8));
        //交易时间
        addField(req_syshead, "TRAN_TIMESTAMP", "string", 9, tran_date.substring(8,17));
        //原始系统编号
		/*if(this.getOrg_sys_id()!=null && !"".equals(this.getOrg_sys_id()) && !"undefind".equals(this.getOrg_sys_id())){
			addField(req_syshead,"ORG_SYS_ID","string",6,this.getOrg_sys_id());
		}else{
			addField(req_syshead,"ORG_SYS_ID","string",6,SYSTEM_ID_LOAN);
		}*/
        //请求系统编号
        addField(req_syshead, "CONSUMER_ID", "string", 6, Constants.SYSTEM_ID_CIPS);
        //渠道类型
        String Source_type = sourceType;
        addField(req_syshead, "SOURCE_TYPE", "string", 2, Source_type);
        //响应系统编号
        addField(req_syshead, "PROVIDER_ID", "string", 6, this.getProviderId());
        //发送方机构ID
        addField(req_syshead, "BRANCH_ID", "string", 20, this.getBranchId());
        //服务请求者身份
        addField(req_syshead, "USER_ID", "string", 20, this.getUserId());
        //法人标志
        String corpCode = this.getCorpCode();
        if (StringUtils.isEmpty(corpCode)) corpCode = "9999";
        addField(req_syshead, "CORP_CODE", "string", 20, corpCode);


        /********应用报文体设置*********************/
        //业务流水号
        String bussSeqNo = this.getBussSeqNo();
        if (StringUtils.isEmpty(bussSeqNo)) {
            bussSeqNo = consumerSeqNo ;
        }
        addField(this.req_apphead, "BUSS_SEQ_NO", "string", 52, bussSeqNo);
    }

    /**
     * 发送报文
     * @parmar req_data 要发送报文信息
     * @throws Exception
     *  @return 交易成功状态
     * **/
    public boolean doTransaction(CompositeData req_data) {
        boolean flag = false;
        try {
            //初始化报文
            this.setReq_cd(req_data);
            //打印交易请求信息
//            printReq();
            //发送报文
            send();
            //检查交易状态
            flag = checkTranStatus();
        } catch (TimeoutException e){
            setRetCode("000001");
            setRetMessage("通讯异常，无法与ESB通讯主机建立连接！[" + e.getMessage() + "]");
            log.error("交易流水号[" + consumerSeqNo + "]" + "通讯异常，无法与ESB通讯主机建立连接！[" + e.getMessage() + "]");
        } catch (Exception e) {
            setRetMessage("交易异常！[" + e.getMessage() + "]");
            setRetCode("000001");
            log.error("交易流水号[" + consumerSeqNo + "]" + "交易异常！[" + e.getMessage() + "]");
        } finally {
            log.info("交易流水号[" + consumerSeqNo + "]" + "本次交易执行结束！" + this.retCode);
        }
        return flag;
    }

    /**
     * 发送报文
     *	 * @throws Exception
     *  @return 交易成功状态
     * **/
    public boolean doTransaction(){
        boolean flag = false;
        try {
            //打印交易请求信息
            printReq();
            //初始化报文
            init_req_cd();
            //发送报文
            send();
            //检查交易状态
            flag = checkTranStatus();

        } catch (TimeoutException e){
            setRetCode("000001");
            setRetMessage("通讯异常，无法与ESB通讯主机建立连接！[" + e.getMessage() + "]");
            log.error("交易流水号[" + consumerSeqNo + "]" + "通讯异常，无法与ESB通讯主机建立连接！[" + e.getMessage() + "]");
        } catch (Exception e) {
            setRetCode("000001");
            setRetMessage("交易异常！[" + e.getMessage() + "]");
            log.error("交易流水号[" + consumerSeqNo + "]" + "交易异常！[" + e.getMessage() + "]");
        } finally {
            log.info("交易流水号[" + consumerSeqNo + "]" + "本次交易执行结束！" + this.retCode);
        }
        return flag;
    }

    /**
     * 检查交易返回状态
     */
    private boolean checkTranStatus(){
        boolean status = false;

        log.info("交易流水号[" + consumerSeqNo + "]" + "开始检查交易返回状态...");
        String ret_msg = resp_syshead.getArray("RET").getStruct(0).getField("RET_MSG").strValue();
        String ret_code = resp_syshead.getArray("RET").getStruct(0).getField("RET_CODE").strValue();
        if("000000".equals(ret_code)){
            status = true;
            ret_msg ="交易成功";

        }
        setRetMessage(ret_msg);
        setRetCode(ret_code);

        log.info("交易流水号[" + consumerSeqNo + "]" + "交易响应码: " + this.getRetCode() + ",交易响应信息: " + this.getRetMessage());

        return status;
    }

    /**
     * 发送报文并取得返回报文
     * @throws Exception
     *
     * **/
    private void send() throws Exception{
        try {
            log.info("交易流水号[" + consumerSeqNo + "]" + "[" + this.getServiceCode()+this.getServiceScene() + "]" + "开始发送交易请求报文...");

            log.info("请求报文："+this.req_cd);
            this.resp_cd = ESBClient_TCP.request(this.req_cd);
            log.info("反馈报文："+this.resp_cd);

            log.info("交易流水号[" + consumerSeqNo + "]"+"交易完成");

            this.resp_syshead = resp_cd.getStruct("SYS_HEAD");
            this.resp_apphead = resp_cd.getStruct("APP_HEAD");
            this.resp_body = resp_cd.getStruct("BODY");
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }

    }


    /**
     * 初始化报文
     * @throws Exception
     *
     */
    private void init_req_cd() throws Exception {
        log.info("交易流水号[" + consumerSeqNo + "]" + "开始初始化交易报文...");
        log.info("交易流水号[" + consumerSeqNo + "]" + "开始组装请求系统报文头...");
        if (this.getReq_syshead() == null || this.getReq_syshead().size()==0){
            log.info("交易流水号[" + consumerSeqNo + "]" + "初始化请求系统报文头...");
            initHeader();
        }
        this.req_cd.addStruct("SYS_HEAD", this.getReq_syshead());
        log.info("交易流水号[" + consumerSeqNo + "]" + "初始化请求应用系统报文头...");
        if (req_apphead != null) {
            this.req_cd.addStruct("APP_HEAD", req_apphead);
        }
        if (this.getReq_body() != null) {
            this.req_cd.addStruct("BODY", this.getReq_body());
        }
    }

    /**
     * 打印交易请求
     *
     */
    private void printReq(){
        log.info("交易流水号[" + consumerSeqNo + "]");
        log.info("交易流水号[" + consumerSeqNo + "]" + "服务请求机构[" + this.branchId + "]");
        log.info("交易流水号[" + consumerSeqNo + "]" + "服务请求人[" + this.userId + "]");
        log.info("交易流水号[" + consumerSeqNo + "]" + "服务代码[" + this.serviceCode + "]");
        log.info("交易流水号[" + consumerSeqNo + "]" + "服务场景[" + this.serviceScene + "]");
        log.info("交易流水号[" + consumerSeqNo + "]" + "服务提供者ID[" + this.providerId + "]");
        log.info("交易流水号[" + consumerSeqNo + "]" + "业务流水号[" + this.bussSeqNo + "]");
    }
}